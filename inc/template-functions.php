<?php

/**
 * Remove hook added in a class.
 *
 * @param     $hook
 * @param     $class
 * @param     $function
 * @param int $priority
 */
function remove_class_filter( $hook, $class, $function, $priority = 10 ) {

    global $wp_filter;

    if ( empty( $wp_filter[ $hook ]->callbacks ) || ! isset( $wp_filter[ $hook ]->callbacks[ $priority ] ) ) {
        return;
    }

    foreach ( $wp_filter[ $hook ]->callbacks[ $priority ] as $callback_key => $callback_value ) {

        // Check if we have class action
        if ( is_array( $callback_value['function'] ) && count( $callback_value['function'] ) == 2 ) {
            // Actually remove if we have needed class and function
            if ( is_a( $callback_value['function'][0], $class ) && $callback_value['function'][1] == $function ) {
                $wp_filter[ $hook ]->remove_filter( $hook, $callback_key, $priority );
            }
        }
    }
}

// Mimic WP core return functions
if ( ! function_exists( '__return_one' ) ) {
    function __return_one() { //phpcs:ignore WordPress.NamingConventions.ValidFunctionName.FunctionDoubleUnderscore
        return 1;
    }
}

/**
 * Get ISO 639-1 code of current language
 *
 * @return bool|string
 */
function kapsula_get_current_language() {
    return function_exists( 'pll_current_language' ) ? pll_current_language() : substr( get_locale(), 0, 2 );
}

/**
 * Brand list for payment
 *
 * @param $order
 *
 * @return bool
 */
function products_contain_brands( $products ) {

    if ( empty( $products ) ) {
        return false;
    }

    $brands_list = explode( ',', get_option( 'brand_names_for_jewellery', [] ) );

    if ( empty( $brands_list ) ) {
        // @deprecated
        // Need to remove after list will be added in admin side!
        $brands_list = [
            'anna-andres-jewelry',
            'cotejeunot',
            'dukachi',
            'koval',
            'logvin-jewelry',
            'natabelz-jewelry',
            'natkina',
            'rimmart',
            'samokish',
            'silverwood-jewelry',
            'yastrebjewelry',
            'framiore',
            'dressed-undressed',
            'secret_garden',
            'wow-studio-decor'
        ];
    }

    foreach ( $products as $item ) {

        $product = $item->get_product();
        $id = $product->is_type( 'variation' ) ? $product->get_parent_id() : $product->get_id();
        $terms = wp_get_post_terms( $id, 'product_brand' );

        if ( ! empty( $terms ) ) {
            foreach ( $terms as $term ) {
                if ( in_array( $term->slug, $brands_list ) ) {
                    return true;
                }
            }
        }
    }

    return false;
}

add_action( 'woocommerce_login_form_end', 'action_woocommerce_login_form', 10, 0 );
add_action( 'woocommerce_register_form', 'action_woocommerce_login_form', 100, 0 );
/**
 * Output for social login buttons
 * Output for social register buttons on register form
 */
function action_woocommerce_login_form() {
    if ( class_exists( 'NextendSocialLogin', false ) ) {
        echo NextendSocialLogin::renderButtonsWithContainer();
    }
}

add_filter( 'login_redirect', 'login_redirect', 10, 3 );
/**
 * Redirect to my-account URL to prevent loading page from the cache.
 * For administrator and designer redirect to admin URL
 *
 * @return string
 */
function login_redirect( $redirect_to, $requested_redirect_to, $user ) {

    if ( ! empty( $user->roles ) ) {
        if ( array_intersect( [ 'administrator', 'designer' ], (array) $user->roles ) ) {
            return admin_url();
        }
    }

    return wc_get_page_permalink( 'myaccount' );
}

add_filter( 'logout_redirect', 'logout_redirect', 10, 3 );
/**
 * Redirect to login URL to prevent loading page from the cache.
 * For administrator and designer redirect to admin URL
 *
 * @return string
 */
function logout_redirect( $redirect_to, $requested_redirect_to, $user ) {

    if ( ! empty( $user->roles ) ) {
        if ( array_intersect( [ 'administrator', 'designer' ], (array) $user->roles ) ) {
            return admin_url();
        }
    }

    return wp_login_url( $redirect_to );
}