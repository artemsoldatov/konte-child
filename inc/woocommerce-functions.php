<?php

/**
 * Custom sorting sizes.
 *
 * @param $a
 * @param $b
 *
 * @return int
 */
function sort_size_attributes( $a, $b ) {

    $sorted_sizes = get_custom_sorted_sizes();

    return array_search( $a, $sorted_sizes ) <= array_search( $b, $sorted_sizes ) ? -1 : 1;
}

/**
 * Order of size attributes.
 *
 * @return string[]
 */
function get_custom_sorted_sizes() {
    return [ 'xxs', 'xs', 'xs-s', 's', 's-m', 'm', 'm-l', 'l', 'l-xl', 'xl', 'xxl', 'xxxl' ];
}

/**
 * User sorting function for terms.
 *
 * @param $a
 * @param $b
 *
 * @return int
 */
function sort_terms_attributes( $a, $b ) {

    if ( ! is_a( $a, 'WP_Term' ) || ! is_a( $b, 'WP_Term' ) ) {
        return 0;
    }

    $sorted_sizes = get_custom_sorted_sizes();

    if ( ! in_array( $a->slug, $sorted_sizes ) || ! in_array( $b->slug, $sorted_sizes ) ) {
        return $a->name <= $b->name ? -1 : 1;
    }

    return sort_size_attributes( $a->slug, $b->slug );
}

/**
 * For All Export plugin to get brand ID. Remove this if plugin uninstalled.
 *
 * @param int $id
 *
 * @return int|string $brand_id
 */
function get_brand_id_from_product_id( $id ) {

    $brand = wp_get_post_terms( $id, 'product_brand' );
    $brand_id = isset( $brand[0] ) ? $brand[0]->term_id : '';

    return $brand_id;
}

/**
 * Get all variations for product without checking admin option
 * 'hide out of stock products from catalog'. We don't need
 * out of stock items in catalog but need to get all variations.
 *
 * @param $product
 *
 * @return array
 */
function get_all_product_variations( $product ) {

    $available_variations = [];

    if ( ! is_a( $product, 'WC_Product' ) || ! $product->is_type( 'variable' ) ) {
        return $available_variations;
    }

    foreach ( $product->get_children() as $child_id ) {
        $variation = wc_get_product( $child_id );

        // Hide out of stock variations if 'Hide out of stock items from the catalog' is checked
        if ( ! $variation || ! $variation->exists() || ( apply_filters( 'woocommerce_hide_invisible_variations', false, $product->get_id(), $variation ) && ! $variation->variation_is_visible() ) ) {
            continue;
        }

        $available_variations[] = $product->get_available_variation( $variation );
    }

    return $available_variations;
}

/**
 *
 * @param $variation_id
 *
 * @return bool
 */
function is_variation_in_cart( $variation_id ) {

    foreach ( WC()->cart->get_cart() as $key => $cart_data ) {

        $_product = $cart_data['data'];

        if ( ! $_product->is_type( 'variation' ) ) {
            continue;
        }

        if ( $variation_id == $_product->get_id() ) {
            return true;
        }
    }

    return false;
}

if ( ! function_exists( 'wc_dropdown_variation_attribute_options' ) ) {

    /**
     * Output a list of variation attributes for use in the cart forms.
     *
     * @param array $args Arguments.
     *
     * @since 2.4.0
     */
    function wc_dropdown_variation_attribute_options( $args = [] ) {
        $args = wp_parse_args(
            apply_filters( 'woocommerce_dropdown_variation_attribute_options_args', $args ),
            [
                'options' => false,
                'attribute' => false,
                'product' => false,
                'selected' => false,
                'name' => '',
                'id' => '',
                'class' => '',
                'show_option_none' => __( 'Choose an option', 'woocommerce' ),
            ]
        );

        // Get selected value.
        if ( false === $args['selected'] && $args['attribute'] && $args['product'] instanceof WC_Product ) {
            $selected_key = 'attribute_' . sanitize_title( $args['attribute'] );
            $args['selected'] = isset( $_REQUEST[ $selected_key ] ) ? wc_clean( wp_unslash( $_REQUEST[ $selected_key ] ) ) : $args['product']->get_variation_default_attribute( $args['attribute'] ); // WPCS: input var ok, CSRF ok, sanitization ok.
        }

        $options = $args['options'];
        $product = $args['product'];
        $attribute = $args['attribute'];
        $name = $args['name'] ? $args['name'] : 'attribute_' . sanitize_title( $attribute );
        $id = $args['id'] ? $args['id'] : sanitize_title( $attribute );
        $class = $args['class'];
        $show_option_none = (bool) $args['show_option_none'];
        $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' ); // We'll do our best to hide the placeholder, but we'll need to show something when resetting options.

        if ( empty( $options ) && ! empty( $product ) && ! empty( $attribute ) ) {
            $attributes = $product->get_variation_attributes();
            $options = $attributes[ $attribute ];
        }

        $html = '<select id="' . esc_attr( $id ) . '" class="' . esc_attr( $class ) . '" name="' . esc_attr( $name ) . '" data-attribute_name="attribute_' . esc_attr( sanitize_title( $attribute ) ) . '" data-show_option_none="' . ( $show_option_none ? 'yes' : 'no' ) . '">';
        $html .= '<option value="" disabled="disabled">' . esc_html( $show_option_none_text ) . '</option>';

        if ( ! empty( $options ) ) {
            if ( $product && taxonomy_exists( $attribute ) ) {
                // Get terms if this is a taxonomy - ordered. We need the names too.
                $terms = wc_get_product_terms(
                    $product->get_id(),
                    $attribute,
                    [
                        'fields' => 'all',
                    ]
                );

                $available_variations = get_all_product_variations( $product );
                $available_attributes = [];
                foreach ( $available_variations as $variation ) {

                    $attribute_name = $variation['attributes']['attribute_pa_size'];

                    $available_attributes[ $attribute_name ]['in_showroom'] = intval( apply_filters( 'is_in_showroom', false, $variation['variation_id'] ) );
                    $available_attributes[ $attribute_name ]['in_shop'] = intval( apply_filters( 'is_in_shop', false, $variation['variation_id'] ) );

                    $is_in_cart = array_search(
                        $variation['variation_id'],
                        array_map(
                            function ( $data ) {
                                return $data['variation_id'];
                            },
                            WC()->cart->get_cart()
                        )
                    );

                    if ( $variation['max_qty'] != 'NULL' && $variation['is_in_stock'] ) {

                        if ( $is_in_cart ) {
                            $available_attributes[ $attribute_name ]['class'] = 'in-cart';
                            $available_attributes[ $attribute_name ]['disabled'] = true;
                            $available_attributes[ $attribute_name ]['postfix'] = ' - ' . strtolower( __( 'В корзине', 'kapsula' ) );
                        } elseif ( $available_attributes[ $attribute_name ]['in_showroom'] ) {
                            $available_attributes[ $attribute_name ]['postfix'] = ' - ' . strtolower( __( 'В шоуруме', 'kapsula' ) );
                            $available_attributes[ $attribute_name ]['class'] = 'in-showroom';
                        } elseif ( $available_attributes[ $attribute_name ]['in_shop'] ) {
                            $available_attributes[ $attribute_name ]['postfix'] = ' - ' . strtolower( __( 'Готов к отправке', 'kapsula' ) );
                            $available_attributes[ $attribute_name ]['class'] = 'in-showroom';
                        } elseif ( $variation['quantity'] == 1 ) {
                            $available_attributes[ $attribute_name ]['postfix'] = ' - ' . __( 'последний', 'kapsula' );
                            $available_attributes[ $attribute_name ]['class'] = 'last';
                        } else {
                            $available_attributes[ $attribute_name ]['postfix'] = '';
                            $available_attributes[ $attribute_name ]['class'] = 'available';
                        }
                    } else {
                        $available_attributes[ $attribute_name ]['postfix'] = ' - продано';
                        $available_attributes[ $attribute_name ]['disabled'] = 'disabled';
                        $available_attributes[ $attribute_name ]['class'] = 'sold attached';
                    }
                }

                foreach ( $terms as $term ) {
                    if ( in_array( $term->slug, $options, true ) ) {
                        $class = isset( $available_attributes[ $term->slug ] ) ? $available_attributes[ $term->slug ]['class'] : '';
                        $postfix = isset( $available_attributes[ $term->slug ] ) ? $available_attributes[ $term->slug ]['postfix'] : '';
                        $disabled = isset( $available_attributes[ $term->slug ] ) && isset( $available_attributes[ $term->slug ]['disabled'] );
                        $html .= '<option value="' . esc_attr( $term->slug ) . '" class="' . $class . '" ' . disabled( $disabled, true, false ) . ' ' . selected( sanitize_title( $args['selected'] ), $term->slug, false ) . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $term->name, $term, $attribute, $product ) ) . ' ' . $postfix . '</option>';
                    }
                }
            } else {
                foreach ( $options as $option ) {
                    // This handles < 2.4.0 bw compatibility where text attributes were not sanitized.
                    $selected = sanitize_title( $args['selected'] ) === $args['selected'] ? selected( $args['selected'], sanitize_title( $option ), false ) : selected( $args['selected'], $option, false );
                    $html .= '<option value="' . esc_attr( $option ) . '" ' . $selected . '>' . esc_html( apply_filters( 'woocommerce_variation_option_name', $option, null, $attribute, $product ) ) . '</option>';
                }
            }
        }

        $html .= '</select>';

        echo apply_filters( 'woocommerce_dropdown_variation_attribute_options_html', $html, $args ); // WPCS: XSS ok.
    }
}

/**
 * Get feed id from order.
 *
 * @param $order
 * @param $name
 *
 * @return false|int|string
 */
function get_feed_id( $order, $name ) {

    $fees = $order->get_fees();

    foreach ( $fees as $key => $fee ) {
        if ( $fee['name'] == $name ) {
            return $key;
        }
    }

    return false;
}

/**
 * Order of Ukraine Cities.
 *
 * @return string[]
 */
function get_sort_cities_order() {
    return [ 'Киев', 'Одесса', 'Харьков', 'Львов', 'Днепр' ];
}

function sort_cities( $cities ) {
    if ( empty( $cities ) ) {
        return $cities;
    }

    $priorities = get_sort_cities_order();

    foreach ( array_reverse( $priorities ) as $priority ) {
        $key = array_search( $priority, $cities );

        if ( $key !== false ) {
            $city = $cities[ $key ];
            unset( $cities[ $key ] );
            array_unshift($cities, $city );
        }
    }

    return $cities;
}
