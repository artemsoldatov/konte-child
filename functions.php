<?php

define( 'THEME_ASSETS', get_stylesheet_directory_uri() . '/assets' );

// Additional theme functions
include 'inc/template-functions.php';
include 'inc/woocommerce-functions.php';

// Child theme autoloader
require_once 'classes/class-autoloader.php';

// Child theme functionality
\dev\Kapsula\Theme\Theme::init();
