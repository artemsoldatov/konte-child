jQuery( document ).ready( function( $ ) {
    if($('#password').length && !$('#password').val().length) {
        $('#password').closest('p.woocommerce-form-row').removeClass('focused');
    }

    $('#password').focus(function(){
        $(this).closest('p.woocommerce-form-row').addClass('focused');
    }).focusout(function(){
        if(!$(this).val().length) {
            $(this).closest('p.woocommerce-form-row').removeClass('focused');
        }
    });
} );