var $ = jQuery;

function t_throttle(e, t, o) {
    var i, n;
    return t || (t = 250), function () {
        var r = o || this, a = +new Date, s = arguments;
        i && a < i + t ? (clearTimeout(n), n = setTimeout(function () {
            i = a, e.apply(r, s)
        }, t)) : (i = a, e.apply(r, s))
    }
}

window.isSearchBot = !1, function (e) {
    if ($isMobile = !1, /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && ($isMobile = !0), /Bot/i.test(navigator.userAgent) && (window.isSearchBot = !0), window.isMobile = $isMobile, 1 == $isMobile) {
        var t = function () {
            for (var t = document.body.querySelectorAll(".t-cover__carrier"), o = e(window).height(), i = 0, n = 0, r = t.length; n < r; n++) (s = (a = t[n]).style).height.indexOf("vh") > -1 && (i = parseInt(s.height) / 100, c = Math.round(o * i) + "px", (d = e(a).parent(".t-cover")) && (d = d[0]) && (l = d.querySelector(".t-cover__filter"), h = d.querySelector(".t-cover__wrapper"), l && (l.style.height = c), h && (h.style.height = c), s.height = d.style.height = c));
            var a, s, c, d, l, h, u = document.body.querySelectorAll("[data-height-correct-vh]");
            for (o = e(window).height(), i = 0, n = 0, r = u.length; n < r; n++) (s = (a = u[n]).style).height.indexOf("vh") > -1 && (i = parseInt(s.height) / 100, c = o + "px", d = e(a).parent(".t-cover"), s.height = c)
        };
        e(document).ready(function () {
            setTimeout(function () {
                t()
            }, 400)
        }), e(window).load(function () {
            setTimeout(function () {
                t()
            }, 400)
        })
    }
    1 == $isMobile && (e(window).width() < 480 ? (e(document).ready(function () {
        var t;
        e("div[data-customstyle=yes]").each(function (t) {
            e(this).css("font-size").replace("px", "") > 26 && (e(this).css("font-size", ""), e(this).css("line-height", ""))
        }), e("[field]").find("span").each(function (t) {
            e(this).css("font-size").replace("px", "") > 26 && e(this).css("font-size", "")
        }), e(".t-title, .t-name, .t-heading, .t-descr, .t-text, .t-subtitle").not(".tn-elem, .tn-atom").each(function (o) {
            if (void 0 !== (t = e(this).attr("style")) && "" != t && t.indexOf("font-size") > -1 && e(this).css("font-size").replace("px", "") > 26) {
                var i = t.replace("font-size", "fontsize").replace("line-height", "lineheight");
                e(this).attr("style", i)
            }
        })
    }), e(window).load(function () {
        var t = e(window).width();
        e(".r").each(function () {
            var o = e(this);
            e(this).find("div").not("[data-auto-correct-mobile-width=false], .tn-elem, .tn-atom").each(function () {
                var i = parseInt(e(this).outerWidth(!0));
                i > t && (console.log("Block not optimized for mobile width. Block width:" + i + " Block id:" + o.attr("id")), console.log(e(this)), o.css("overflow", "auto"), i - 3 > t && o.css("word-break", "break-all"))
            })
        })
    })) : e(window).width() < 900 && e(document).ready(function () {
        var t;
        e("div[data-customstyle=yes]").each(function (t) {
            e(this).css("font-size").replace("px", "") > 30 && (e(this).css("font-size", ""), e(this).css("line-height", ""))
        }), e("[field]").find("span").each(function (t) {
            e(this).css("font-size").replace("px", "") > 30 && e(this).css("font-size", "")
        }), e(".t-title, .t-name, .t-heading, .t-descr, .t-text, .t-subtitle").not(".tn-elem, .tn-atom").each(function (o) {
            if (void 0 !== (t = e(this).attr("style")) && "" != t && t.indexOf("font-size") > -1 && e(this).css("font-size").replace("px", "") > 30) {
                var i = t.replace("font-size", "fontsize").replace("line-height", "lineheight");
                e(this).attr("style", i)
            }
        })
    }))
}(jQuery), function (e) {
    function t() {
        this.setScrollListener()
    }

    t.prototype.videoTags = [], t.prototype.defaultConfig = {isNeedStop: !1}, t.prototype.videoConfigs = [], t.prototype.registerNewVideo = function (e, t) {
        if (!(e instanceof HTMLVideoElement)) throw new Error("Wrong tag passed into registerNewVideo");
        return -1 == this.videoTags.indexOf(e) && (this.videoTags.push(e), this.videoConfigs.push(void 0 === t ? this.defaultConfig : t), this.scrollCb(), !0)
    }, t.prototype.unergisterVideo = function (e) {
        if (!(e instanceof HTMLVideoElement)) throw new Error("Wrong tag passed into unregisterNewVideo");
        var t;
        return (t = this.videoTags.indexOf(e)) > -1 && ("function" == typeof e.remove ? e.remove() : e.parentNode && e.parentNode.removeChild(e), this.pauseVideo(e, this.videoConfigs[t]), this.videoTags.splice(t, 1), this.videoConfigs.splice(t, 1), !0)
    }, t.prototype.pauseVideo = function (e, t) {
        if (!t) throw new Error("Wrong config type!");
        e.pause(), t.isNeedStop && e.load()
    }, t.prototype.setScrollListener = function () {
        e(window).bind("scroll", t_throttle(jQuery.proxy(this.scrollCb, this), 200))
    }, t.prototype.scrollCb = function () {
        for (var t = e(window).height(), o = null, i = 0, n = this.videoTags.length; i < n; i++) {
            if (o = this.videoTags[i], _vrect = this.getVideoBoundingRect(o, !1), Math.abs(_vrect.top) < t && Math.abs(_vrect.top) > t / 2) {
                var r = 1 - (Math.abs(_vrect.top) - t / 2) / (t / 2) - .2;
                r > 0 && r <= 1 && 0 != o.volume && (o.volume = r)
            }
            Math.abs(_vrect.top) > t || 0 == _vrect.height ? this.pauseVideo(o, this.videoConfigs[i]) : o.paused && o.play()
        }
    }, t.prototype.getVideoObject = function (e) {
        for (var t = 0, o = this.videoTags.length; t > o; t++) {
            var i = this.videoTags[t];
            if (i.v === e) return i
        }
        return null
    }, t.prototype.getVideoBoundingRect = function (t, o) {
        void 0 === o && (o = !0);
        var i = null;
        return o && (i = e(t).parents(".r")[0]) || (i = t), i.getBoundingClientRect()
    }, window.videoLoadProcessor = new t
}(jQuery), function (e) {
    function t() {
        this.setScrollCb(), this.itemHeight = screen.availHeight;
        this.itemTransitionTop = .25 * this.itemHeight, this.activeItemIndex = null, this.windowHeight = document.documentElement.clientHeight || window.innerHeight || screen.availHeight, this.topOffsetShift = -150, e(window).resize(jQuery.proxy(this.recalculateAllSequencesOffsets, this)), this._resizeInterval = setInterval(jQuery.proxy(this.scrollCb, this), 500)
    }

    function o(t) {
        var o, i = e("#rec" + t), n = i.find(".t-cover").height();
        (o = i.find("div[data-hook-content]").outerHeight()) > 300 && n < o + 40 && ((o = o + 120) > 1e3 && (o += 100), console.log("auto correct cover height: " + o), i.find(".t-cover").height(o), i.find(".t-cover__filter").height(o), i.find(".t-cover__carrier").height(o), i.find(".t-cover__wrapper").height(o), 0 == $isMobile && setTimeout(function () {
            var e = i.find(".t-cover__carrier");
            e.find("iframe").length > 0 && (console.log("correct video from cover_fixcontentheight"), r(e, o + "px")), e.find("video").length > 0 && console.log("correct html5video from cover_fixcontentheight")
        }, 2e3))
    }

    function i(e, t, o, i) {
        var n, r, a, s, c;
        n = e.offset().top, r = e.height(), a = t.scrollTop(), s = t.height(), c = o.getPlayerState(), a + s > n && a <= n + r ? (1 !== c && o.playVideo(), "yes" == i && (a > n + r - 100 ? o.setVolume(30) : a > n + r - 200 ? o.setVolume(70) : a + s < n + 200 ? o.setVolume(30) : o.setVolume(100))) : a + s < n && a + s > n - 500 ? 2 !== c && (o.playVideo(), o.pauseVideo()) : 2 !== c && o.pauseVideo()
    }

    t.prototype.defaultConfig = {
        orientation: "vertical",
        speedFactor: 1,
        automated: !1
    }, t.prototype.sequenceObjects = [], t.prototype.recalculateAllSequencesOffsets = function () {
        this._resizeTimeout && clearTimeout(this._resizeTimeout), this._resizeInterval && clearInterval(this._resizeInterval), this._resizeTimeout = setTimeout(jQuery.proxy(function () {
            this.scrollCb(), this._resizeInterval = setInterval(jQuery.proxy(this.scrollCb, this), 500)
        }, this), 10)
    }, t.prototype.registerNewBlock = function (e) {
        if (!(e instanceof HTMLElement)) throw new Error("Wrong node type in registerNewBlock");
        for (var t = 0, o = this.sequenceObjects.length; t < o; t++) if (this.sequenceObjects[t].sequenceBlock === e) return !1;
        var i = e.querySelector('[data-hook="sequence-holder"]'), n = 0, r = this.getAbsoluteTopOffset(i),
            a = function () {
                var t = Array.prototype.slice.call(e.querySelectorAll('[data-hook="sequence-item"]'), 0), o = [];
                return t.forEach(jQuery.proxy(function (e, t, i) {
                    var r = this.getItemHeight(e), a = e.querySelector('[data-hook="item-background"]');
                    e.style.height = r + "px", a.style.height = this.itemHeight + "px", t < i.length - 1 && (n += r), o.push({
                        node: e,
                        height: r,
                        topOffset: this.getAbsoluteTopOffset(e.querySelector(".txt-holder")) - (t == i.length - 1 ? 0 : this.topOffsetShift),
                        backgroundHolder: a
                    })
                }, this)), o
            }.call(this), s = (this.itemHeight, {
                sequenceBlock: e,
                sequenceHolder: i,
                sequenceHolderTopOffset: r,
                sequenceHeight: n,
                items: a,
                started: !1,
                prevBackgroundColor: ""
            });
        return this.sequenceObjects.push(s), this.scrollCb(), !0
    }, t.prototype.getItemHeight = function (e) {
        var t = e.querySelector("[data-hook='item-text']");
        e.querySelector("[data-hook='item-background']");
        st = e.style;
        var o = parseFloat(getComputedStyle(t).top);
        return t.style.top = o + "px", Math.max(t.clientHeight + o, this.itemHeight)
    }, t.prototype.fixTextBlocksPosition = function (e) {
        txtBlocks = Array.prototype.slice.call(e.querySelectorAll('[data-hook="item-text"]'), 0), txtBlocks.forEach(function (e, t, o) {
            e.parentNode.querySelector("[data-hook='item-background']").style.top = "-" + e.clientHeight + "px"
        })
    }, t.prototype.unergisterBlock = function (e) {
        for (var t = 0, o = this.sequenceObjects.length, i = null; t < o; t++) if (this.sequenceObjects[t].sequenceBlock === e) {
            i = t;
            break
        }
        return null !== i && (this.sequenceObjects.splice(i, 1), !0)
    }, t.prototype.getAbsoluteTopOffset = function (e) {
        var t = e.offsetTop;
        for (e = e.offsetParent; null != e;) t += e.offsetTop, e = e.offsetParent;
        return t
    }, t.prototype.processSequence = function (e) {
        0 == e.started && (e.prevBackgroundColor = document.body.style.backgroundColor, document.body.style.backgroundColor = "rgb(0, 0, 0)", e.started = !0);
        e.sequenceBlock, e.sequenceHolder;
        for (var t, o, i, n, r = e.items, a = null, s = 0, c = r.length; s < c; s++) if ((t = r[s].node).querySelector(".txt-holder"), (n = t.getBoundingClientRect()).top < this.itemTransitionTop && n.bottom < n.height + this.itemTransitionTop && n.bottom > this.itemTransitionTop) {
            a = s;
            break
        }
        if (null != a) {
            (i = n.top / this.itemTransitionTop) > 1 ? i = 1 : i < 0 && (i = 0);
            for (s = 0, c = r.length; s < c; s++) t = r[s].node, "fixed" != (o = r[s].backgroundHolder.style).position && (o.position = "fixed"), s == a ? (o.opacity = 1 - i, t.querySelector(".txt-holder").style.opacity = 1 - i) : s == a - 1 ? (o.opacity = i, t.querySelector(".txt-holder").style.opacity = i) : (o.opacity = 0, t.querySelector(".txt-holder").style.opacity = 0)
        }
    }, t.prototype.stopSequence = function (e) {
        0 != e.started && (e.items.forEach(function (e, t, o) {
            e.backgroundHolder.style.position = "relative", e.backgroundHolder.style.display = "block", e.backgroundHolder.style.opacity = 1
        }), document.body.style.backgroundColor = e.prevBackgroundColor, e.started = !1)
    }, t.prototype.scrollCb = function () {
        e(window).scrollTop();
        for (var t, o = 0, i = this.sequenceObjects.length; o < i; o++) {
            var n = (t = this.sequenceObjects[o]).sequenceHolder.getBoundingClientRect();
            n.top < 0 && n.bottom > 0 && n.bottom > n.height - t.sequenceHeight - 100 ? this.processSequence(t) : this.stopSequence(t)
        }
    }, t.prototype.setScrollCb = function () {
        this._scrollCb = jQuery.proxy(this.scrollCb, this), e(window).bind("scroll", t_throttle(this._scrollCb, 200))
    }, window.sequenceController = new t, window.processVideo = function (t) {
        mp4Src = e(t).attr("data-content-video-url-mp4"), webmSrc = e(t).attr("data-content-video-url-webm"), e(t).css("background-color", "transparent"), e(t).css("background-image", "");
        var o = {
            mp4: mp4Src,
            webm: webmSrc,
            preload: "none",
            autoplay: !1,
            loop: !0,
            scale: !0,
            zIndex: 0,
            width: "100%"
        };
        vid = e(t).videoBG(o), videoLoadProcessor.registerNewVideo(vid, {isNeedStop: !1})
    }, window.cover_init = function (t) {
        e(document).ready(function () {
            var i = document.body.querySelector("#coverCarry" + t), n = e(i), r = n.attr("data-content-cover-bg"),
                a = n.attr("data-content-cover-height"), s = n.attr("data-content-cover-parallax"),
                c = n.attr("data-content-video-url-mp4"), d = n.attr("data-content-video-url-webm"),
                l = n.attr("data-content-video-url-youtube"), h = n.attr("data-content-video-noloop"),
                u = n.attr("data-content-video-nomute"), p = n.attr("data-content-bg-base64"),
                f = n.attr("data-content-video-nocover");
            r || (r = ""), a || (a = ""), s || (s = ""), c || (c = ""), d || (d = ""), l || (l = ""), h || (h = ""), u || (u = ""), l || (l = ""), p || (p = ""), f && "yes" == f && (c = "", d = "", l = ""), !$isMobile || "" == d && "" == c && "" == l || n.css("background-image", "url('" + r + "')"), setTimeout(function () {
                o(t)
            }, 500);
            var v = e("#rec" + t).find("img[data-hook-clogo]");
            if (v.length && v.load(function () {
                setTimeout(function () {
                    o(t)
                }, 500)
            }), $isMobile && e(window).on("orientationchange", function () {
                o(t)
            }), ("" !== c || "" !== d || "" !== l) && 0 == $isMobile) if ("" != l || "" == c && "" == d) {
                if ("" != l) {
                    n.css("background-color", "#000000"), n.css("background-image", ""), n.attr("data-content-cover-bg", "");
                    b = 0;
                    (x = e(window)).scroll(function () {
                        w && window.clearTimeout(w), w = window.setTimeout(function () {
                            var e, t, o;
                            (b = n.find("iframe").length) > 0 || (e = n.offset().top, t = n.height(), (o = x.scrollTop()) + x.height() > e - 500 && o <= e + t + 500 && processYoutubeVideo(i, a))
                        }, 100)
                    }), x.scroll()
                }
            } else {
                if (n.css("background-color", "#000000"), n.css("background-image", "url('https://tilda.ws/img/spinner-white.gif')"), n.css("background-size", "auto"), n.attr("data-content-cover-bg", ""), "" != h) var m = !1; else m = !0;
                if ("" != u) var g = 1; else g = "";
                var w, y = "";
                "fixed" == s && (a.indexOf("vh") > -1 && parseInt(a) > 100 && (n.css("height", "100vh"), y = "yes"), a.indexOf("px") > -1 && parseInt(a) > e(window).height() && (n.css("height", "100vh"), y = "yes"));
                var b = "", x = e(window), T = n.parent();
                x.scroll(function () {
                    var e, t, o;
                    (w && window.clearTimeout(w), w = window.setTimeout(function () {
                        var e, t, o;
                        if (!(b > 0) && (e = n.offset().top, t = n.height(), (o = x.scrollTop()) + x.height() > e - 500 && o <= e + t + 500)) {
                            var i = n.videoBG({
                                mp4: c,
                                webm: d,
                                poster: "",
                                preload: "none",
                                autoplay: !1,
                                loop: m,
                                volume: g,
                                scale: !0,
                                zIndex: 0,
                                width: "100%"
                            });
                            videoLoadProcessor.registerNewVideo(i), b = 1
                        }
                    }, 100), "fixed" == s && "yes" == y) && (e = T.offset().top, t = T.height(), (o = x.scrollTop()) >= e + t - x.height() ? (n.css("position", "absolute"), n.css("bottom", "0px"), n.css("top", "auto")) : o >= e ? (n.css("position", "fixed"), n.css("top", "0px")) : o < e && (n.css("position", "relative"), n.css("top", "auto")))
                }), x.scroll()
            }
            if ("dynamic" == s && 0 == $isMobile && n.parallax("50%", .2, !0), "yes" == p && "" != r && "" == c && "" == d && "" == l) {
                e("<img/>").attr("src", r).load(function () {
                    e(this).remove(), n.css("background-image", "url('" + r + "')"), n.css("opacity", "1")
                }), n.css("background-image", ""), n.css("opacity", "0"), n.css("transition", "opacity 25ms")
            }
            var k = e("#rec" + t).find(".t-cover__arrow-wrapper");
            k.length > 0 && k.click(function () {
                var o = e("#rec" + t).height();
                // o > 0 && e("html, body").animate({scrollTop: e("#rec" + t).offset().top + o}, 500);
				o > 0 && e("html, body").animate({scrollTop: e('#rec101334156').offset().top}, 700);
            })
        })
    }, e(document).ready(function () {
        e(".t-cover__carrier").each(function () {
            var t = e(this).attr("data-content-cover-id");
            t > 0 && cover_init(t)
        })
    });
    var n = e.Deferred();

    function r(t, o) {
        console.log("setWidthHeightYoutubeVideo:" + o);
        var i = t.find("iframe"), n = t.attr("data-content-video-nocover"),
            r = t.attr("data-content-video-noadcut-youtube"), a = t.attr("data-content-video-ratio"), s = .5625;
        if (a > 0 && (s = 1 * parseFloat(a)), "yes" != n) {
            if (o || (o = "100vh"), o.indexOf("vh") > -1) {
                var c = window.innerHeight;
                c || (c = e(window).height());
                var d = Math.floor(c * (parseInt(o) / 100))
            } else d = parseInt(o);
            var l = Math.floor(parseInt(window.innerWidth));
            l || (l = e(window).width());
            var h = l, u = y = l * s, p = y, f = 1;
            if ("yes" == r || (u = u + 110 + 110, p = y - 220), p < d) if (y < d) f = d / y + .02; else f = y / d + .02;
            var v = Math.floor(h * f), m = Math.floor(u * f), g = m - d, w = v - l;
            i.height(m + "px"), i.width(v + "px"), g > 0 && i.css("margin-top", -Math.floor(g / 2) + "px"), w > 0 && i.css("margin-left", -Math.floor(w / 2) + "px")
        } else {
            var y;
            o || (y = Math.floor(t.width() * s)), o && o.indexOf("vh") > -1 ? y = Math.floor(window.innerHeight * (parseInt(o) / 100)) : o && (y = parseInt(o)), i.css("width", "100%"), i.height(y + "px")
        }
    }

    window.processYoutubeVideo = function (t, o) {
        !function () {
            if ("yes" !== window.loadytapi_flag) {
                window.loadytapi_flag = "yes";
                var e = document.createElement("script");
                e.src = "https://www.youtube.com/iframe_api";
                var t = document.getElementsByTagName("script")[0];
                t.parentNode.insertBefore(e, t)
            }
        }();
        n.then(function () {
            var n, a = e(t), s = a.attr("data-content-video-url-youtube"), c = a.attr("data-content-video-nomute"),
                d = a.attr("data-content-video-noloop"), l = a.attr("data-content-video-nocover"),
                h = document.createElement("iframe");
            h.src = function (e, t, o) {
                -1 == e.indexOf("https://www.youtube.com/embed") && (e = "https://www.youtube.com/embed" + ("/" == e[0] ? e : "/" + e));
                var i = location.protocol + "//" + location.host;
                return "yes" != t ? (e[e.length - 1], e = e + "?autoplay=1&loop=1&enablejsapi=1&&playerapiid=featuredytplayer&controls=0&modestbranding=1&rel=0&showinfo=0&color=white&iv_load_policy=3&theme=light&wmode=transparent&origin=" + i + "&playlist=" + function (e) {
                    for (var t = e.split("/"), o = null, i = 0, n = t.length; i < n; i++) "embed" == t[i] && (o = t[i + 1]);
                    return o
                }(e)) : (e[e.length - 1], e = e + "?autoplay=0&loop=0&enablejsapi=1&&playerapiid=featuredytplayer&controls=1&modestbranding=1&rel=0&showinfo=0&color=black&iv_load_policy=3&theme=dark&wmode=transparent&origin=" + i), "yes" !== o && (e += "&mute=1"), e
            }(s, l, c), h.frameBorder = 0, h.allow = "autoplay", t.appendChild(h), 0 == $isMobile && new YT.Player(h, {
                events: {
                    onReady: function (o) {
                        var n, r, a, s, d, l, h;
                        n = t, r = o.target, a = c, d = e(window), l = e(n), h = 0, d.scroll(function () {
                            s && (window.clearTimeout(s), h >= 15 && (i(l, d, r, a), h = 0), h++), s = window.setTimeout(function () {
                                i(l, d, r, a), h = 0
                            }, 100)
                        }), d.scroll(), o.target.setVolume && "yes" != c && o.target.setVolume(0), o.target.setLoop(!0)
                    }, onStateChange: function (t) {
                        if (t.target.setVolume && "yes" != c && t.target.setVolume(0), -1 === t.data) {
                            var o = window.fix_scrolltop_beforestop_youtube;
                            o >= 0 && (e("html, body").scrollTop(o), delete window.fix_scrolltop_beforestop_youtube)
                        }
                        t.data === YT.PlayerState.PLAYING ? n = window.setInterval(function () {
                            var e = t.target.getCurrentTime(), o = t.target.getDuration();
                            e + 1 > o && 0 !== o && (t.target.seekTo(0), "yes" === d && (t.target.stopVideo(), t.target.clearVideo()))
                        }, 1e3) : window.clearTimeout(n)
                    }
                }
            });
            r(a, o)
        })
    }, window.onYouTubeIframeAPIReady = function () {
        n.resolve()
    }
}(jQuery), function (e) {
    function t() {
        this.callbacks = {}
    }

    t.prototype.defaultConfig = {single: !1, context: null}, t.prototype.addEventListener = function (e, t, o) {
        evtCallbacks = this._getEventCallbacks(e), evtCallbacks || (evtCallbacks = this.callbacks[e] = []), evtCallbacks.push({
            callback: t,
            config: "object" == typeof o ? o : this.defaultConfig
        })
    }, t.prototype._getEventCallbacks = function (e) {
        return this.callbacks[e]
    }, t.prototype.removeEventListener = function (e, t) {
        var o = this._getEventCallbacks(e);
        if (!o) return !1;
        for (var i = 0, n = o.length; i < n; i++) if (t === o[i].callback) return o.splice(i, 1), !0;
        return !1
    }, t.prototype.emitEvent = function (e, t) {
        var o = [];
        extend(o, this._getEventCallbacks(e));
        for (var i, n, r, a = 0, s = o.length; a < s; a++) n = (i = o[a]).callback, (r = i.config).context ? n.call(r.context, t) : n(t), r.single && this.removeEventListener(e, n)
    }, window.observer = new t
}(jQuery), function (e) {
    e(document).ready(function () {
        if (0 == $isMobile && "yes" !== e("#allrecords").attr("data-blocks-animationoff") && 0 == window.isSearchBot) {
            e(".r").each(function (t) {
                e(this).attr("style") && -1 !== e(this).attr("style").indexOf("background-color") && e(this).attr("data-animationappear", "off")
            });
            var t = e(".r").not("[data-animationappear=off], [data-screen-min], [data-screen-max]"), o = e(window);

            function i() {
                if (t.length) for (var i, n = t.length - 1; n >= 0; n--) (i = e(t[n])).offset().top < o.scrollTop() + o.height() - 100 && (i.removeClass("r_hidden"), i.addClass("r_showed"), t.splice(n, 1))
            }

            t.each(function (t) {
                a = e(this).offset().top, b = o.scrollTop() + o.height() + 300, a > 1e3 && a > b ? e(this).addClass("r_hidden") : e(this).addClass("r_showed"), e(this).addClass("r_anim")
            }), o.bind("scroll", t_throttle(i, 200)), i()
        }
        e("body").height() + 70 < e(window).height() && e(".t-tildalabel").css("display", "none")
    })
}(jQuery), function (e) {
    function t() {
        var t = e(window);
        window.winWidth = t.width(), window.winHeight = t.height()
    }

    function o() {
        var t, o, i, n = e(window).width();
        e("div.r[data-screen-max], div.r[data-screen-min]").each(function (r) {
            i = e(this).css("display"), void 0 === (t = e(this).attr("data-screen-max")) && (t = 1e4), t = parseInt(t), void 0 === (o = e(this).attr("data-screen-min")) && (o = 0), (o = parseInt(o)) <= t && (n <= t && n > o ? "block" != i && e(this).css("display", "block") : "none" != i && e(this).css("display", "none"))
        })
    }

    e(document).ready(function () {
        t(), o(), e(window).bind("resize", t_throttle(t, 200)), e(window).bind("resize", t_throttle(o, 200))
    })
}(jQuery), function (e) {
    e.fn.videoBG = function (t, o) {
        o = {};
        if ("object" == typeof t) o = e.extend({}, e.fn.videoBG.defaults, t); else {
            if (t) return e(t).videoBG(o);
            o = e.fn.videoBG.defaults
        }
        var i = e(this);
        if (i.length) {
            "static" != i.css("position") && i.css("position") || i.css("position", "relative"), 0 == o.width && (o.width = i.width()), 0 == o.height && (o.height = i.height()), o.textReplacement && (o.scale = !0, i.width(o.width).height(o.height).css("text-indent", "-9999px"));
            var n = e.fn.videoBG.video(o);
            return o.scale && n.height(o.height).width(o.width), i.append(n), void 0 === i.attr("data-content-video-nomute") && i.find("video").prop("muted", !0), function (t, o) {
                var i = t.closest(".t-cover__carrier"), n = o + "";
                console.log("setWidthHeightHTMLVideo:" + n);
                var r = i.find("video"), a = i.attr("data-content-video-nocover"),
                    s = i.attr("data-content-video-ratio"), c = .5625;
                if (s > 0 && (c = 1 * parseFloat(s)), "yes" != a) {
                    if (n || (n = "100vh"), n.indexOf("vh") > -1) {
                        var d = window.innerHeight;
                        d || (d = e(window).height());
                        var l = Math.floor(d * (parseInt(n) / 100))
                    } else l = parseInt(n);
                    var h = Math.floor(parseInt(window.innerWidth));
                    h || (h = e(window).width());
                    var u = h, p = y = h * c, f = 1;
                    y < l && (f = y < l ? l / y + .02 : y / l + .02);
                    var v = Math.floor(u * f), m = Math.floor(p * f), g = m - l, w = v - h;
                    r.height(m + "px"), r.width(v + "px"), g > 0 && r.css("margin-top", -Math.floor(g / 2) + "px"), w > 0 && r.css("margin-left", -Math.floor(w / 2) + "px")
                } else {
                    var y;
                    n || (y = Math.floor(i.width() * c)), n && n.indexOf("vh") > -1 ? y = Math.floor(window.innerHeight * (parseInt(n) / 100)) : n && (y = parseInt(n)), r.css("width", "100%"), r.height(y + "px")
                }
            }(n, o.height), n.find("video")[0]
        }
    }, e.fn.videoBG.setFullscreen = function (t) {
        var o, i = e(window).width(), n = e(window).height();
        if (t.css("min-height", 0).css("min-width", 0), t.parent().width(i).height(n), i / n > t.aspectRatio) t.width(i).height("auto"), (o = (t.height() - n) / 2) < 0 && (o = 0), t.css("top", -o); else if (t.width("auto").height(n), (o = (t.width() - i) / 2) < 0 && (o = 0), t.css("left", -o), 0 === o) setTimeout(function () {
            e.fn.videoBG.setFullscreen(t)
        }, 500);
        e("body > .videoBG_wrapper").width(i).height(n)
    }, e.fn.videoBG.video = function (t) {
        var o = e("<div/>");
        o.addClass("videoBG").css("position", t.position).css("z-index", t.zIndex).css("top", 0).css("left", 0).css("height", t.height).css("width", t.width).css("opacity", t.opacity).css("overflow", "hidden");
        var i, n = e("<video/>");
        (n.css("position", "relative").css("z-index", t.zIndex).attr("poster", t.poster).css("top", 0).css("left", 0).css("min-width", "100%").css("min-height", "100%"), t.autoplay && n.attr("autoplay", t.autoplay), t.volume > 0 ? n.prop("volume", t.volume) : n.prop("volume", 0), t.fullscreen) && (n.bind("canplay", function () {
            n.aspectRatio = n.width() / n.height(), e.fn.videoBG.setFullscreen(n)
        }), e(window).resize(function () {
            clearTimeout(i), i = setTimeout(function () {
                e.fn.videoBG.setFullscreen(n)
            }, 100)
        }), e.fn.videoBG.setFullscreen(n));
        var r = n[0];
        t.loop && (loops_left = t.loop, n.bind("ended", function () {
            loops_left && r.play(), !0 !== loops_left && loops_left--
        })), n.bind("canplay", function () {
            t.autoplay && r.play()
        }), e.fn.videoBG.supportsVideo() && (e.fn.videoBG.supportType("webm") && "" != t.webm ? n.attr("src", t.webm) : e.fn.videoBG.supportType("mp4") && "" != t.mp4 ? n.attr("src", t.mp4) : n.attr("src", t.ogv));
        var a = e("<img/>");
        return a.attr("src", t.poster).css("position", "absolute").css("z-index", t.zIndex).css("top", 0).css("left", 0).css("min-width", "100%").css("min-height", "100%"), e.fn.videoBG.supportsVideo() ? o.html(n) : o.html(a), t.textReplacement && (o.css("min-height", 1).css("min-width", 1), n.css("min-height", 1).css("min-width", 1), a.css("min-height", 1).css("min-width", 1), o.height(t.height).width(t.width), n.height(t.height).width(t.width), a.height(t.height).width(t.width)), e.fn.videoBG.supportsVideo(), o
    }, e.fn.videoBG.supportsVideo = function () {
        return document.createElement("video").canPlayType
    }, e.fn.videoBG.supportType = function (t) {
        if (!e.fn.videoBG.supportsVideo()) return !1;
        var o = document.createElement("video");
        switch (t) {
            case"webm":
                return o.canPlayType('video/webm; codecs="vp8, vorbis"');
            case"mp4":
                return o.canPlayType('video/mp4; codecs="avc1.42E01E, mp4a.40.2"');
            case"ogv":
                return o.canPlayType('video/ogg; codecs="theora, vorbis"')
        }
        return !1
    }, e.fn.videoBG.wrapper = function () {
        var t = e("<div/>");
        return t.addClass("videoBG_wrapper").css("position", "absolute").css("top", 0).css("left", 0), t
    }, e.fn.videoBG.defaults = {
        mp4: "",
        ogv: "",
        webm: "",
        poster: "",
        autoplay: !0,
        loop: !0,
        scale: !1,
        position: "absolute",
        opacity: 1,
        textReplacement: !1,
        zIndex: 0,
        width: 0,
        height: 0,
        fullscreen: !1,
        imgFallback: !0
    }
}(jQuery), function (e) {
    var t = e(window), o = t.height();
    t.resize(function () {
        o = t.height()
    }), e.fn.parallax = function (i, n, r) {
        var a, s = e(this), c = void 0 !== document.body.style["-webkit-transform"];

        function d() {
            var i = t.scrollTop();
            s.each(function () {
                var t = e(this), r = t.offset().top, s = a(t), d = this.getBoundingClientRect();
                if (!(r + s < i || r > i + o)) {
                    var l = -1 * Math.round(d.top * n);
                    c ? this.style["-webkit-transform"] = "translateY(" + l + "px)" : this.style.top = l + "px"
                }
            })
        }

        c && s.css("position", "relative"), window.correctFirstTop4Parallax = function () {
            s.each(function () {
                s.offset().top
            })
        }, window.correctFirstTop4Parallax(), a = r ? function (e) {
            return e.outerHeight(!0)
        } : function (e) {
            return e.height()
        }, (arguments.length < 1 || null === i) && (i = "50%"), (arguments.length < 2 || null === n) && (n = .1), (arguments.length < 3 || null === r) && (r = !0), e(window).resize(window.correctFirstTop4Parallax), t.bind("scroll", d).resize(d), "complete" !== document.readyState ? window.addEventListener("load", function () {
            d()
        }) : d()
    }
}(jQuery), window.Tilda = window.Tilda || {}, function (e) {
    Tilda.sendEventToStatistics = function (t, o, i, n) {
        var r = "/" == t.substring(0, 1), a = [], s = 0, c = e("#allrecords").data("fb-event");
        if (c = !(!c || "nosend" != c), i || (i = window.location.href), (n = n ? parseFloat(n) : 0) > 0) if (window.dataLayer || (window.dataLayer = []), -1 != t.indexOf("/tilda/") && -1 != t.indexOf("/payment/") && window.tildaForm && window.tildaForm.orderIdForStat > "") (i = {
            ecommerce: {
                purchase: {
                    actionField: {id: window.tildaForm.orderIdForStat},
                    products: window.tildaForm.arProductsForStat
                }
            }
        }).event = "purchase"; else if (i && i.ecommerce && (i.ecommerce.add && i.ecommerce.add.products ? a = i.ecommerce.add.products : i.ecommerce.remove && i.ecommerce.remove.products && (a = i.ecommerce.remove.products), a && a.length > 0)) {
            for (s = 0; s < a.length; s++) a[s].id || (a[s].sku ? a[s].id = a[s].sku : a[s].recid && a[s].lid && (a[s].id = a[s].recid + "_" + a[s].lid));
            i.ecommerce.add && i.ecommerce.add.products ? (i.ecommerce.add.products = a, i.event = "addToCart") : i.ecommerce.remove && i.ecommerce.remove.products ? (i.ecommerce.remove.products = a, i.event = "removeFromCart") : (r ? (i.event = "pageView", i.eventAction = t) : i.event = t, i.title = o, i.value = n)
        }
        null != window.dataLayer && (r ? n > 0 ? i && i.ecommerce ? window.dataLayer.push(i) : window.dataLayer.push({
            event: "pageView",
            eventAction: t,
            title: o,
            value: n,
            product: i
        }) : window.dataLayer.push({
            event: "pageView",
            eventAction: t,
            title: o,
            referer: i
        }) : i && i.ecommerce && i.ecommerce ? window.dataLayer.push(i) : window.dataLayer.push({
            event: t,
            eventAction: o,
            value: n,
            referer: i
        }));
        try {
            if (window.gtagTrackerID && "gtag" == window.mainTracker) if (r) if (i && i.event) {
                var d = e("#allrecords").data("tilda-currency") || "RUB";
                "purchase" == i.event ? gtag("event", "purchase", {
                    transaction_id: i.ecommerce.purchase.actionField.id,
                    value: parseFloat(n).toFixed(2),
                    currency: d,
                    items: i.ecommerce.purchase.products
                }) : "addToCart" == i.event && i.ecommerce.add ? gtag("event", "add_to_cart", {items: i.ecommerce.add.products}) : "removeFromCart" == i.event && i.ecommerce.remove && gtag("event", "remove_from_cart", {items: i.ecommerce.remove.products})
            } else gtag("config", window.gtagTrackerID, {
                page_title: o,
                page_path: t
            }); else gtag("event", t, {event_category: "tilda", event_label: o, value: n})
        } catch (e) {
        }
        if (window.ga && "tilda" != window.mainTracker && "gtag" != window.mainTracker) if (r) if (i && i.event) {
            try {
                if (window.Tilda.isLoadGAEcommerce || (window.Tilda.isLoadGAEcommerce = !0, ga("require", "ec")), ga("set", "currencyCode", e("#allrecords").data("tilda-currency")), "purchase" == i.event) {
                    var l = i.ecommerce.purchase.products.length;
                    for (u = 0; u < l; u++) h = i.ecommerce.purchase.products[u], ga("ec:addProduct", {
                        id: h.id || u,
                        name: h.name,
                        price: parseFloat(h.price).toFixed(2),
                        quantity: h.quantity
                    });
                    ga("ec:setAction", "purchase", {
                        id: i.ecommerce.purchase.actionField.id,
                        revenue: parseFloat(n).toFixed(2)
                    })
                } else if ("addToCart" == i.event && i.ecommerce.add) {
                    l = i.ecommerce.add.products.length;
                    for (u = 0; u < l; u++) h = i.ecommerce.add.products[u], ga("ec:addProduct", {
                        id: h.id || u,
                        name: h.name,
                        price: parseFloat(h.price).toFixed(2),
                        quantity: h.quantity
                    });
                    ga("ec:setAction", "add")
                } else if ("removeFromCart" == i.event && i.ecommerce.remove) {
                    var h, u;
                    l = i.ecommerce.remove.products.length;
                    for (u = 0; u < l; u++) h = i.ecommerce.remove.products[u], ga("ec:addProduct", {
                        id: h.id || u,
                        name: h.name,
                        price: parseFloat(h.price).toFixed(2),
                        quantity: h.quantity
                    });
                    ga("ec:setAction", "remove")
                }
            } catch (e) {
            }
            ga("send", {hitType: "pageview", page: t, title: o, params: i})
        } else ga("send", {hitType: "pageview", page: t, title: o}); else ga("send", {
            hitType: "event",
            eventCategory: "tilda",
            eventAction: t,
            eventLabel: o,
            eventValue: n
        });
        if (window.mainMetrika > "" && window[window.mainMetrika] && (r ? n > 0 ? window[window.mainMetrika].hit(t, {
            title: o,
            order_price: n,
            params: i
        }) : window[window.mainMetrika].hit(t, {title: o}) : n > 0 ? window[window.mainMetrika].reachGoal(t, {
            title: o,
            params: i,
            order_price: n
        }) : window[window.mainMetrika].reachGoal(t, {title: o, referer: i})), null != window.fbq && 0 == c) if (r) {
            var p = e("#allrecords").data("tilda-currency");
            -1 == t.indexOf("tilda/") || -1 == t.indexOf("/payment/") && -1 == t.indexOf("/submitted/") ? -1 != t.indexOf("tilda/popup") ? window.fbq("track", "ViewContent", {
                content_name: o,
                content_category: "popup",
                content_ids: [t],
                content_type: "popup"
            }) : window.fbq("track", "ViewContent", {
                content_name: o,
                content_category: "event",
                content_ids: [t],
                content_type: "event"
            }) : n > 0 && p ? window.fbq("track", "InitiateCheckout", {
                content_name: o,
                content_category: t,
                value: n,
                currency: p
            }) : window.fbq("track", "Lead", {content_name: o, content_category: t})
        } else window.fbq("track", t, {content_name: o, value: n});
        "function" == typeof window.tildastat && (r ? (t.indexOf("payment") > 0 && t.indexOf("tilda/form") > -1 && (t = t.replace("tilda/form", "tilda/rec")), window.tildastat("pageview", {page: t})) : window.tildastat("pageview", {page: "/tilda/event/" + t}))
    }, Tilda.saveUTM = function () {
        try {
            var e = window.location.href, t = "", o = "";
            if (-1 !== e.toLowerCase().indexOf("utm_") && "string" == typeof (t = (t = (e = e.toLowerCase()).split("?"))[1])) {
                var i, n = t.split("&");
                for (i in n) "utm_" == n[i].split("=")[0].substring(0, 4) && (o = o + n[i] + "|||");
                if (o.length > 0) {
                    var r = new Date;
                    r.setDate(r.getDate() + 30), document.cookie = "TILDAUTM=" + encodeURIComponent(o) + "; path=/; expires=" + r.toUTCString()
                }
            }
        } catch (e) {
        }
    }, e(document).ready(function () {
        var t = navigator.userAgent.toLowerCase(), o = -1 != t.indexOf("msie") && parseInt(t.split("msie")[1]);
        8 != o && 9 != o || e(".t-btn").each(function () {
            var t = e(this).attr("href");
            e(this).find("table").length > 0 && t > "" && -1 == t.indexOf("#popup:") && -1 == t.indexOf("#price:") && e(this).click(function (t) {
                t.preventDefault();
                var o = e(this).attr("href");
                window.location.href = o
            })
        });
        try {
            1 == e("#allrecords").length && "no" == e("#allrecords").data("tilda-cookie") || Tilda.saveUTM()
        } catch (e) {
        }
        e(".r").off("click", "a.js-click-stat"), e(".r").on("click", "a.js-click-stat", function (t) {
            var o = e(this).data("tilda-event-name"), i = e(this).text(), n = e(this).attr("href") || "",
                r = e(this).attr("target");
            if (o || (o = "/tilda/click/".$(this).closest(".r").attr("id") + "/?url=" + n), Tilda.sendEventToStatistics(o, i), "http" == n.substring(0, 4)) return window.setTimeout(function () {
                var t, o, i = "", a = "";
                if ("_blank" == r) {
                    if (-1 != n.indexOf("?") && (i = n.split("?"), n = i[0], -1 != (i = i[1]).indexOf("#") && (i = i.split("#"), n = n + "#" + i[1], i = i[0]), i = i.split("&")), 0 == e("#tildaredirectform").length ? e("body").append('<form id="tildaredirectform" target="_blank" method="GET" action="' + n + '" style="display:none;"></form>') : e("#tildaredirectform").attr("method", "GET").attr("action", n), a = "", i.length > 0) for (t in i) (o = i[t].split("=")) && o.length > 0 && (a += '<input type="hidden" name="' + o[0] + '" value="' + (o[1] ? o[1] : "") + '">');
                    e("#tildaredirectform").html(a), e("#tildaredirectform").submit()
                } else window.location.href = n
            }, 300), t.preventDefault(), !1
        }), e("input.js-amount").each(function () {
            var t = e(this).val();
            t = t.replace(/,/g, "."), t = parseFloat(t.replace(/[^0-9\.]/g, "")), e(this).val(t)
        }), Tilda.showFormError = function (e, t) {
            var o = e.find(".js-errorbox-all");
            o && 0 != o.length || (e.prepend('<div class="js-errorbox-all"></div>'), o = e.find(".js-errorbox-all"));
            var i = o.find(".js-rule-error-all");
            i && 0 != i.length || (o.append('<p class="js-rule-error-all"></p>'), i = o.find(".js-rule-error-all")), "string" == typeof t ? i.html(t) : t && t.responseText ? i.html(t.responseText + ". Later, plaese try again.") : t && t.statusText ? i.html("Error - " + t.statusText + ". Later, plaese try again.") : i.html("Unknown error. Later, plaese try again."), i.show(), o.show()
        }, Tilda.robokassaPayment = function (t, o, i) {
            return e.ajax({
                type: "POST",
                url: "https://forms.tildacdn.com/payment/robokassa/",
                data: t.serialize(),
                dataType: "text",
                success: function (n) {
                    o.removeClass("t-btn_sending"), o.data("form-sending-status", "0"), o.data("submitform", "");
                    var r = o.closest(".r").attr("id");
                    if ("{" == n.substring(0, 1)) {
                        if (window.JSON && window.JSON.parse ? json = window.JSON.parse(n) : json = e.parseJSON(n), !json) return void Tilda.showFormError(t, !1);
                        if (json.error > "") return void Tilda.showFormError(t, json.error)
                    } else if ("http" == n.substring(0, 4)) {
                        Tilda.sendEventToStatistics("/tilda/payment/" + r + "/click/", "Payment button: " + o.val(), "", i);
                        var a = n;
                        window.setTimeout(function () {
                            window.location.href = a
                        }, 500)
                    } else Tilda.showFormError(t, n)
                },
                fail: function (e) {
                    o.removeClass("t-btn_sending"), o.data("form-sending-status", "0"), o.data("submitform", "");
                    Tilda.showFormError(t, e)
                },
                timeout: 15e3
            })
        }
    })
}(jQuery);

function t_sldsInit(t) {
    var s = $("#rec" + t);
    if (0 === s.length) return !1;
    var e = s.find(".t-slds__item"), d = e.length, a = e.filter(":first"), i = e.filter(":last"), r = $(window).width(),
        l = s.find(".t-slds__items-wrapper");
    if ("true" == l.attr("data-slider-stop")) return !1;
    t_slds_changeImageUrl(t);
    var n = window.navigator.userAgent, _ = n.indexOf("MSIE"), o = "", c = !1;
    0 < _ && (8 != (o = parseInt(n.substring(_ + 5, n.indexOf(".", _)))) && 9 != o || (c = !0)), 1 == c && l.removeClass("t-slds_animated-fast").removeClass("t-slds_animated-slow").addClass("t-slds_animated-none t-slds_ie").attr("data-slider-correct-height", "true"), window.$isMobile && 1 == l.hasClass("t-slds_animated-none") && l.removeClass("t-slds_animated-none").addClass("t-slds_animated-fast"), "true" == l.attr("data-slider-initialized") && (d -= 2), l.attr("data-slider-initialized", "true"), l.attr("data-slider-totalslides", d), l.attr("data-slider-pos", 1), l.attr("data-slider-cycle", ""), l.attr("data-slider-animated", "");
    var f = l.attr("data-slider-pos");
    0 == s.find(".t-slds__item[data-slide-index=0]").length && (a.before(i.clone(!0).attr("data-slide-index", "0")), s.find(".t-slds__item[data-slide-index=0]").find(".t-zoomable").removeClass("t-zoomable")), 0 == s.find(".t-slds__item[data-slide-index=" + (d + 1) + "]").length && (i.after(a.clone(!0).attr("data-slide-index", d + 1).removeClass("t-slds__item_active")).addClass("t-slds__item-loaded"), s.find(".t-slds__item[data-slide-index=" + (d + 1) + "]").find(".t-zoomable").removeClass("t-zoomable")), t_slds_SliderWidth(t), "true" == l.attr("data-slider-correct-height") && t_slds_SliderHeight(t), t_slds_SliderArrowsHeight(t), t_slds_ActiveSlide(t, f, d), t_slds_initSliderControls(t), t_slds_ActiveCaption(t, f, d), 0 < l.attr("data-slider-timeout") && t_slds_initAutoPlay(t, f, d), s.find(".t-slds__item-loaded").length < d + 2 && t_slds_UpdateImages(t, f), "yes" == l.attr("data-slider-arrows-nearpic") && t_slds_positionArrows(t), !0 !== c && t_slds_initSliderSwipe(t, r), s.find(".t-slds").css("visibility", ""), $(window).bind("resize", t_throttle(function () {
        t_slds_SliderWidth(t), t_slideMove(t), t_slds_positionArrows(t)
    }, 200)), $(window).load(function () {
        "true" == l.attr("data-slider-correct-height") && t_slds_UpdateSliderHeight(t), t_slds_UpdateSliderArrowsHeight(t)
    })
}

function t_slds_initSliderControls(a) {
    var i = $("#rec" + a), r = i.find(".t-slds__items-wrapper"), t = i.find(".t-slds__container").width();
    r.attr("data-slider-transition");
    if ("true" == r.attr("data-slider-stop")) return !1;
    r.css({transform: "translate3d(-" + t + "px, 0, 0)"}), i.find(".t-slds__arrow_wrapper").click(function () {
        var t = r.attr("data-slider-animated"), s = parseFloat(r.attr("data-slider-pos")),
            e = parseFloat(r.attr("data-slider-totalslides")), d = "";
        "" == t && (r.attr("data-slider-animated", "yes"), "left" === $(this).attr("data-slide-direction") ? "false" == r.attr("data-slider-with-cycle") && 1 == s ? s = 1 : s-- : "false" == r.attr("data-slider-with-cycle") && s == e ? s = e : s++, r.attr("data-slider-pos", s), s != e + 1 && 0 != s || (d = "yes"), r.attr("data-slider-cycle", d), t_slideMove(a));
        i.trigger("updateSlider")
    }), i.find(".t-slds__bullet").click(function () {
        var t = parseFloat($(this).attr("data-slide-bullet-for"));
        r.attr("data-slider-pos", t), t_slideMove(a), i.trigger("updateSlider")
    })
}

function t_slideMove(t) {
    var s = $("#rec" + t), e = s.find(".t-slds__items-wrapper"), d = s.find(".t-slds__container").width(),
        a = parseFloat(e.attr("data-slider-transition")),
        i = (e.attr("data-slider-animated"), parseFloat(e.attr("data-slider-pos"))),
        r = parseFloat(e.attr("data-slider-totalslides")), l = e.attr("data-slider-cycle"),
        n = s.find(".t-slds__items-wrapper").hasClass("t-slds_animated-none");
    if ("true" == e.attr("data-slider-stop")) return !1;
    "false" == e.attr("data-slider-with-cycle") && i == r ? s.find(".t-slds__arrow_wrapper-right").fadeOut(300) : s.find(".t-slds__arrow_wrapper-right").fadeIn(300), "false" == e.attr("data-slider-with-cycle") && 1 == i ? s.find(".t-slds__arrow_wrapper-left").fadeOut(300) : s.find(".t-slds__arrow_wrapper-left").fadeIn(300), e.addClass("t-slds_animated"), e.css({transform: "translate3d(-" + d * i + "px, 0, 0)"}), setTimeout(function () {
        e.removeClass("t-slds_animated"), e.attr("data-slider-animated", ""), "yes" == l && (i == r + 1 && (i = 1), 0 == i && (i = r), e.css({transform: "translate3d(-" + d * i + "px, 0, 0)"}), !0 !== n && t_slds_ActiveSlide(t, i, r), e.attr("data-slider-pos", i)), "y" == window.lazy && t_lazyload_update()
    }, a), t_slds_ActiveBullet(t, i, r), t_slds_ActiveSlide(t, i, r), "true" == e.attr("data-slider-correct-height") && t_slds_SliderHeight(t), t_slds_SliderArrowsHeight(t), t_slds_ActiveCaption(t, i, r), s.find(".t-slds__item-loaded").length < r + 2 && t_slds_UpdateImages(t, i)
}

function t_slds_updateSlider(t) {
    var s = $("#rec" + t);
    t_slds_SliderWidth(t);
    var e = s.find(".t-slds__items-wrapper"), d = s.find(".t-slds__container").width(),
        a = parseFloat(e.attr("data-slider-pos"));
    e.css({transform: "translate3d(-" + d * a + "px, 0, 0)"}), "true" == e.attr("data-slider-correct-height") && t_slds_UpdateSliderHeight(t), t_slds_UpdateSliderArrowsHeight(t)
}

function t_slds_UpdateImages(t, s) {
    var e = $("#rec" + t).find('.t-slds__item[data-slide-index="' + s + '"]');
    e.addClass("t-slds__item-loaded"), e.next().addClass("t-slds__item-loaded"), e.prev().addClass("t-slds__item-loaded")
}

function t_slds_ActiveCaption(t, s, e) {
    var d = $("#rec" + t), a = d.find(".t-slds__caption"),
        i = d.find('.t-slds__caption[data-slide-caption="' + s + '"]');
    a.removeClass("t-slds__caption-active"), 0 == s ? i = d.find('.t-slds__caption[data-slide-caption="' + e + '"]') : s == e + 1 && (i = d.find('.t-slds__caption[data-slide-caption="1"]')), i.addClass("t-slds__caption-active")
}

function t_slds_scrollImages(t, s) {
    var e = $("#rec" + t), d = (s < 0 ? "" : "-") + Math.abs(s).toString();
    e.find(".t-slds__items-wrapper").css("transform", "translate3d(" + d + "px, 0, 0)")
}

function t_slds_ActiveBullet(t, s, e) {
    var d = $("#rec" + t), a = d.find(".t-slds__bullet"),
        i = d.find('.t-slds__bullet[data-slide-bullet-for="' + s + '"]');
    a.removeClass("t-slds__bullet_active"), 0 == s ? i = d.find('.t-slds__bullet[data-slide-bullet-for="' + e + '"]') : s == e + 1 && (i = d.find('.t-slds__bullet[data-slide-bullet-for="1"]')), i.addClass("t-slds__bullet_active")
}

function t_slds_ActiveSlide(t, s, e) {
    var d = $("#rec" + t), a = d.find(".t-slds__item"), i = d.find('.t-slds__item[data-slide-index="' + s + '"]'),
        r = d.find(".t-slds__items-wrapper").hasClass("t-slds_animated-none");
    a.removeClass("t-slds__item_active"), 0 == s && 0 == r ? d.find('.t-slds__item[data-slide-index="' + e + '"]').addClass("t-slds__item_active") : 0 == s && 1 == r ? i = d.find('.t-slds__item[data-slide-index="' + e + '"]') : s == e + 1 && 0 == r ? d.find('.t-slds__item[data-slide-index="1"]').addClass("t-slds__item_active") : s == e + 1 && 1 == r && (i = d.find('.t-slds__item[data-slide-index="1"]')), i.addClass("t-slds__item_active")
}

function t_slds_SliderWidth(t) {
    var s = $("#rec" + t), e = s.find(".t-slds__container").width(), d = s.find(".t-slds__item").length;
    if ("true" == s.find(".t-slds__items-wrapper").attr("data-slider-stop")) return !1;
    s.find(".t-slds__items-wrapper").width(e * d), s.find(".t-slds__item").width(e)
}

function t_slds_SliderHeight(t) {
    var s = $("#rec" + t);
    s.find(".t-slds__items-wrapper").height(s.find(".t-slds__item_active").height())
}

function t_slds_UpdateSliderHeight(t) {
    var s = $("#rec" + t);
    s.find(".t-slds__items-wrapper").css("height", ""), s.find(".t-slds__items-wrapper").height(s.find(".t-slds__item_active").height())
}

function t_slds_SliderArrowsHeight(t) {
    var s = $("#rec" + t);
    s.find(".t-slds__arrow_wrapper").height(s.find(".t-slds__item_active").height())
}

function t_slds_UpdateSliderArrowsHeight(t) {
    var s = $("#rec" + t);
    s.find(".t-slds__arrow_wrapper").css("height", ""), s.find(".t-slds__arrow_wrapper").height(s.find(".t-slds__item_active").height())
}

function t_slds_initAutoPlay(r, l, n) {
    var _ = $("#rec" + r), t = _.find(".t-slds"), o = _.find(".t-slds__items-wrapper"),
        s = parseFloat(o.attr("data-slider-timeout")), c = "";
    if (stopSlider = o.attr("data-slider-stop"), "true" == stopSlider) return !1;
    t.hover(function (t) {
        o.attr("data-slider-hovered", "yes")
    }, function (t) {
        o.attr("data-slider-hovered", "")
    }), setInterval(function () {
        var t = $(window).scrollTop(), s = $(window).height(), e = _.offset().top, d = _.innerHeight(),
            a = o.attr("data-slider-hovered"), i = o.attr("data-slider-autoplay-ignore-hover");
        e < t + s / 2 && t < e + d && "yes" != a && "yes" != i && ("false" == o.attr("data-slider-with-cycle") && l == n ? l = n : l++, o.attr("data-slider-pos", l), l != n + 1 && 0 != l || (c = "yes"), t_slideMove(r), "yes" == c && (l == n + 1 && (l = 1), 0 == l && (l = n), o.attr("data-slider-pos", l)), o.attr("data-slider-cycle", c))
    }, s)
}

function t_slds_positionArrows(t) {
    var s = $("#rec" + t);
    container = s.find(".t-slds__arrow_container-outside"), inner = s.find(".t-slds__item").width(), arrowleft = s.find(".t-slds__arrow-left").width(), arrowright = s.find(".t-slds__arrow-right").width(), container.css({"max-width": arrowleft + arrowright + inner + 120 + "px"})
}

function t_slds_initSliderSwipe(n, t) {
    var s, _ = $("#rec" + n), o = !1;
    if ("true" == _.find(".t-slds__items-wrapper").attr("data-slider-stop")) return !1;
    delete Hammer.defaults.cssProps.userSelect, hammer = new Hammer(_.find(".t-slds__items-wrapper")[0], {
        domEvents: !0,
        threshold: 0,
        inputClass: Hammer.TouchInput,
        recognizers: [[Hammer.Pan, {direction: Hammer.DIRECTION_HORIZONTAL}]]
    }), $(window).bind("scroll", function () {
        o = !0, clearTimeout(s), s = setTimeout(function () {
            o = !1
        }, 250)
    }), hammer.on("pan", function (t) {
        if (o) return !1;
        var s = _.find(".t-slds__container").width(), e = _.find(".t-slds__items-wrapper"),
            d = parseFloat(e.attr("data-slider-pos")), a = parseFloat(e.attr("data-slider-totalslides")), i = "",
            r = t.deltaX, l = 100 / a * t.deltaX / $(window).innerWidth();
        if ("true" == e.attr("data-slider-stop")) return !1;
        t_slds_scrollImages(n, s * d - r), t.isFinal && (1 < t.velocityX ? ("false" == e.attr("data-slider-with-cycle") && 1 == d ? d = 1 : d--, e.attr("data-slider-pos", d), 0 == d && (i = "yes"), e.attr("data-slider-cycle", i)) : t.velocityX < -1 ? ("false" == e.attr("data-slider-with-cycle") && d == a ? d = a : d++, e.attr("data-slider-pos", d), d == a + 1 && (i = "yes"), e.attr("data-slider-cycle", i)) : l <= -20 / a ? ("false" == e.attr("data-slider-with-cycle") && d == a ? d = a : d++, e.attr("data-slider-pos", d), d == a + 1 && (i = "yes"), e.attr("data-slider-cycle", i)) : 20 / a <= l && ("false" == e.attr("data-slider-with-cycle") && 1 == d ? d = 1 : d--, e.attr("data-slider-pos", d), 0 == d && (i = "yes"), e.attr("data-slider-cycle", i)), t_slideMove(n))
    })
}

function t_slds_changeImageUrl(t) {
    $("#rec" + t).find(".t-slds__img").each(function () {
        var t;
        void 0 !== (t = $(this)).attr("data-src") && ((t = $(this)).attr("src", t.attr("data-src")), t.removeAttr("data-src"))
    })
}

/*! Hammer.JS - v2.0.8 - 2016-04-23
 * http://hammerjs.github.io/
 *
 * Copyright (c) 2016 Jorik Tangelder;
 * Licensed under the MIT license */
!function (a, b, c, d) {
    "use strict";

    function e(a, b, c) {
        return setTimeout(j(a, c), b)
    }

    function f(a, b, c) {
        return Array.isArray(a) ? (g(a, c[b], c), !0) : !1
    }

    function g(a, b, c) {
        var e;
        if (a) if (a.forEach) a.forEach(b, c); else if (a.length !== d) for (e = 0; e < a.length;) b.call(c, a[e], e, a), e++; else for (e in a) a.hasOwnProperty(e) && b.call(c, a[e], e, a)
    }

    function h(b, c, d) {
        var e = "DEPRECATED METHOD: " + c + "\n" + d + " AT \n";
        return function () {
            var c = new Error("get-stack-trace"),
                d = c && c.stack ? c.stack.replace(/^[^\(]+?[\n$]/gm, "").replace(/^\s+at\s+/gm, "").replace(/^Object.<anonymous>\s*\(/gm, "{anonymous}()@") : "Unknown Stack Trace",
                f = a.console && (a.console.warn || a.console.log);
            return f && f.call(a.console, e, d), b.apply(this, arguments)
        }
    }

    function i(a, b, c) {
        var d, e = b.prototype;
        d = a.prototype = Object.create(e), d.constructor = a, d._super = e, c && la(d, c)
    }

    function j(a, b) {
        return function () {
            return a.apply(b, arguments)
        }
    }

    function k(a, b) {
        return typeof a == oa ? a.apply(b ? b[0] || d : d, b) : a
    }

    function l(a, b) {
        return a === d ? b : a
    }

    function m(a, b, c) {
        g(q(b), function (b) {
            a.addEventListener(b, c, !1)
        })
    }

    function n(a, b, c) {
        g(q(b), function (b) {
            a.removeEventListener(b, c, !1)
        })
    }

    function o(a, b) {
        for (; a;) {
            if (a == b) return !0;
            a = a.parentNode
        }
        return !1
    }

    function p(a, b) {
        return a.indexOf(b) > -1
    }

    function q(a) {
        return a.trim().split(/\s+/g)
    }

    function r(a, b, c) {
        if (a.indexOf && !c) return a.indexOf(b);
        for (var d = 0; d < a.length;) {
            if (c && a[d][c] == b || !c && a[d] === b) return d;
            d++
        }
        return -1
    }

    function s(a) {
        return Array.prototype.slice.call(a, 0)
    }

    function t(a, b, c) {
        for (var d = [], e = [], f = 0; f < a.length;) {
            var g = b ? a[f][b] : a[f];
            r(e, g) < 0 && d.push(a[f]), e[f] = g, f++
        }
        return c && (d = b ? d.sort(function (a, c) {
            return a[b] > c[b]
        }) : d.sort()), d
    }

    function u(a, b) {
        for (var c, e, f = b[0].toUpperCase() + b.slice(1), g = 0; g < ma.length;) {
            if (c = ma[g], e = c ? c + f : b, e in a) return e;
            g++
        }
        return d
    }

    function v() {
        return ua++
    }

    function w(b) {
        var c = b.ownerDocument || b;
        return c.defaultView || c.parentWindow || a
    }

    function x(a, b) {
        var c = this;
        this.manager = a, this.callback = b, this.element = a.element, this.target = a.options.inputTarget, this.domHandler = function (b) {
            k(a.options.enable, [a]) && c.handler(b)
        }, this.init()
    }

    function y(a) {
        var b, c = a.options.inputClass;
        return new (b = c ? c : xa ? M : ya ? P : wa ? R : L)(a, z)
    }

    function z(a, b, c) {
        var d = c.pointers.length, e = c.changedPointers.length, f = b & Ea && d - e === 0,
            g = b & (Ga | Ha) && d - e === 0;
        c.isFirst = !!f, c.isFinal = !!g, f && (a.session = {}), c.eventType = b, A(a, c), a.emit("hammer.input", c), a.recognize(c), a.session.prevInput = c
    }

    function A(a, b) {
        var c = a.session, d = b.pointers, e = d.length;
        c.firstInput || (c.firstInput = D(b)), e > 1 && !c.firstMultiple ? c.firstMultiple = D(b) : 1 === e && (c.firstMultiple = !1);
        var f = c.firstInput, g = c.firstMultiple, h = g ? g.center : f.center, i = b.center = E(d);
        b.timeStamp = ra(), b.deltaTime = b.timeStamp - f.timeStamp, b.angle = I(h, i), b.distance = H(h, i), B(c, b), b.offsetDirection = G(b.deltaX, b.deltaY);
        var j = F(b.deltaTime, b.deltaX, b.deltaY);
        b.overallVelocityX = j.x, b.overallVelocityY = j.y, b.overallVelocity = qa(j.x) > qa(j.y) ? j.x : j.y, b.scale = g ? K(g.pointers, d) : 1, b.rotation = g ? J(g.pointers, d) : 0, b.maxPointers = c.prevInput ? b.pointers.length > c.prevInput.maxPointers ? b.pointers.length : c.prevInput.maxPointers : b.pointers.length, C(c, b);
        var k = a.element;
        o(b.srcEvent.target, k) && (k = b.srcEvent.target), b.target = k
    }

    function B(a, b) {
        var c = b.center, d = a.offsetDelta || {}, e = a.prevDelta || {}, f = a.prevInput || {};
        b.eventType !== Ea && f.eventType !== Ga || (e = a.prevDelta = {
            x: f.deltaX || 0,
            y: f.deltaY || 0
        }, d = a.offsetDelta = {x: c.x, y: c.y}), b.deltaX = e.x + (c.x - d.x), b.deltaY = e.y + (c.y - d.y)
    }

    function C(a, b) {
        var c, e, f, g, h = a.lastInterval || b, i = b.timeStamp - h.timeStamp;
        if (b.eventType != Ha && (i > Da || h.velocity === d)) {
            var j = b.deltaX - h.deltaX, k = b.deltaY - h.deltaY, l = F(i, j, k);
            e = l.x, f = l.y, c = qa(l.x) > qa(l.y) ? l.x : l.y, g = G(j, k), a.lastInterval = b
        } else c = h.velocity, e = h.velocityX, f = h.velocityY, g = h.direction;
        b.velocity = c, b.velocityX = e, b.velocityY = f, b.direction = g
    }

    function D(a) {
        for (var b = [], c = 0; c < a.pointers.length;) b[c] = {
            clientX: pa(a.pointers[c].clientX),
            clientY: pa(a.pointers[c].clientY)
        }, c++;
        return {timeStamp: ra(), pointers: b, center: E(b), deltaX: a.deltaX, deltaY: a.deltaY}
    }

    function E(a) {
        var b = a.length;
        if (1 === b) return {x: pa(a[0].clientX), y: pa(a[0].clientY)};
        for (var c = 0, d = 0, e = 0; b > e;) c += a[e].clientX, d += a[e].clientY, e++;
        return {x: pa(c / b), y: pa(d / b)}
    }

    function F(a, b, c) {
        return {x: b / a || 0, y: c / a || 0}
    }

    function G(a, b) {
        return a === b ? Ia : qa(a) >= qa(b) ? 0 > a ? Ja : Ka : 0 > b ? La : Ma
    }

    function H(a, b, c) {
        c || (c = Qa);
        var d = b[c[0]] - a[c[0]], e = b[c[1]] - a[c[1]];
        return Math.sqrt(d * d + e * e)
    }

    function I(a, b, c) {
        c || (c = Qa);
        var d = b[c[0]] - a[c[0]], e = b[c[1]] - a[c[1]];
        return 180 * Math.atan2(e, d) / Math.PI
    }

    function J(a, b) {
        return I(b[1], b[0], Ra) + I(a[1], a[0], Ra)
    }

    function K(a, b) {
        return H(b[0], b[1], Ra) / H(a[0], a[1], Ra)
    }

    function L() {
        this.evEl = Ta, this.evWin = Ua, this.pressed = !1, x.apply(this, arguments)
    }

    function M() {
        this.evEl = Xa, this.evWin = Ya, x.apply(this, arguments), this.store = this.manager.session.pointerEvents = []
    }

    function N() {
        this.evTarget = $a, this.evWin = _a, this.started = !1, x.apply(this, arguments)
    }

    function O(a, b) {
        var c = s(a.touches), d = s(a.changedTouches);
        return b & (Ga | Ha) && (c = t(c.concat(d), "identifier", !0)), [c, d]
    }

    function P() {
        this.evTarget = bb, this.targetIds = {}, x.apply(this, arguments)
    }

    function Q(a, b) {
        var c = s(a.touches), d = this.targetIds;
        if (b & (Ea | Fa) && 1 === c.length) return d[c[0].identifier] = !0, [c, c];
        var e, f, g = s(a.changedTouches), h = [], i = this.target;
        if (f = c.filter(function (a) {
            return o(a.target, i)
        }), b === Ea) for (e = 0; e < f.length;) d[f[e].identifier] = !0, e++;
        for (e = 0; e < g.length;) d[g[e].identifier] && h.push(g[e]), b & (Ga | Ha) && delete d[g[e].identifier], e++;
        return h.length ? [t(f.concat(h), "identifier", !0), h] : void 0
    }

    function R() {
        x.apply(this, arguments);
        var a = j(this.handler, this);
        this.touch = new P(this.manager, a), this.mouse = new L(this.manager, a), this.primaryTouch = null, this.lastTouches = []
    }

    function S(a, b) {
        a & Ea ? (this.primaryTouch = b.changedPointers[0].identifier, T.call(this, b)) : a & (Ga | Ha) && T.call(this, b)
    }

    function T(a) {
        var b = a.changedPointers[0];
        if (b.identifier === this.primaryTouch) {
            var c = {x: b.clientX, y: b.clientY};
            this.lastTouches.push(c);
            var d = this.lastTouches, e = function () {
                var a = d.indexOf(c);
                a > -1 && d.splice(a, 1)
            };
            setTimeout(e, cb)
        }
    }

    function U(a) {
        for (var b = a.srcEvent.clientX, c = a.srcEvent.clientY, d = 0; d < this.lastTouches.length; d++) {
            var e = this.lastTouches[d], f = Math.abs(b - e.x), g = Math.abs(c - e.y);
            if (db >= f && db >= g) return !0
        }
        return !1
    }

    function V(a, b) {
        this.manager = a, this.set(b)
    }

    function W(a) {
        if (p(a, jb)) return jb;
        var b = p(a, kb), c = p(a, lb);
        return b && c ? jb : b || c ? b ? kb : lb : p(a, ib) ? ib : hb
    }

    function X() {
        if (!fb) return !1;
        var b = {}, c = a.CSS && a.CSS.supports;
        return ["auto", "manipulation", "pan-y", "pan-x", "pan-x pan-y", "none"].forEach(function (d) {
            b[d] = c ? a.CSS.supports("touch-action", d) : !0
        }), b
    }

    function Y(a) {
        this.options = la({}, this.defaults, a || {}), this.id = v(), this.manager = null, this.options.enable = l(this.options.enable, !0), this.state = nb, this.simultaneous = {}, this.requireFail = []
    }

    function Z(a) {
        return a & sb ? "cancel" : a & qb ? "end" : a & pb ? "move" : a & ob ? "start" : ""
    }

    function $(a) {
        return a == Ma ? "down" : a == La ? "up" : a == Ja ? "left" : a == Ka ? "right" : ""
    }

    function _(a, b) {
        var c = b.manager;
        return c ? c.get(a) : a
    }

    function aa() {
        Y.apply(this, arguments)
    }

    function ba() {
        aa.apply(this, arguments), this.pX = null, this.pY = null
    }

    function ca() {
        aa.apply(this, arguments)
    }

    function da() {
        Y.apply(this, arguments), this._timer = null, this._input = null
    }

    function ea() {
        aa.apply(this, arguments)
    }

    function fa() {
        aa.apply(this, arguments)
    }

    function ga() {
        Y.apply(this, arguments), this.pTime = !1, this.pCenter = !1, this._timer = null, this._input = null, this.count = 0
    }

    function ha(a, b) {
        return b = b || {}, b.recognizers = l(b.recognizers, ha.defaults.preset), new ia(a, b)
    }

    function ia(a, b) {
        this.options = la({}, ha.defaults, b || {}), this.options.inputTarget = this.options.inputTarget || a, this.handlers = {}, this.session = {}, this.recognizers = [], this.oldCssProps = {}, this.element = a, this.input = y(this), this.touchAction = new V(this, this.options.touchAction), ja(this, !0), g(this.options.recognizers, function (a) {
            var b = this.add(new a[0](a[1]));
            a[2] && b.recognizeWith(a[2]), a[3] && b.requireFailure(a[3])
        }, this)
    }

    function ja(a, b) {
        var c = a.element;
        if (c.style) {
            var d;
            g(a.options.cssProps, function (e, f) {
                d = u(c.style, f), b ? (a.oldCssProps[d] = c.style[d], c.style[d] = e) : c.style[d] = a.oldCssProps[d] || ""
            }), b || (a.oldCssProps = {})
        }
    }

    function ka(a, c) {
        var d = b.createEvent("Event");
        d.initEvent(a, !0, !0), d.gesture = c, c.target.dispatchEvent(d)
    }

    var la, ma = ["", "webkit", "Moz", "MS", "ms", "o"], na = b.createElement("div"), oa = "function", pa = Math.round,
        qa = Math.abs, ra = Date.now;
    la = "function" != typeof Object.assign ? function (a) {
        if (a === d || null === a) throw new TypeError("Cannot convert undefined or null to object");
        for (var b = Object(a), c = 1; c < arguments.length; c++) {
            var e = arguments[c];
            if (e !== d && null !== e) for (var f in e) e.hasOwnProperty(f) && (b[f] = e[f])
        }
        return b
    } : Object.assign;
    var sa = h(function (a, b, c) {
            for (var e = Object.keys(b), f = 0; f < e.length;) (!c || c && a[e[f]] === d) && (a[e[f]] = b[e[f]]), f++;
            return a
        }, "extend", "Use `assign`."), ta = h(function (a, b) {
            return sa(a, b, !0)
        }, "merge", "Use `assign`."), ua = 1, va = /mobile|tablet|ip(ad|hone|od)|android/i, wa = "ontouchstart" in a,
        xa = u(a, "PointerEvent") !== d, ya = wa && va.test(navigator.userAgent), za = "touch", Aa = "pen",
        Ba = "mouse", Ca = "kinect", Da = 25, Ea = 1, Fa = 2, Ga = 4, Ha = 8, Ia = 1, Ja = 2, Ka = 4, La = 8, Ma = 16,
        Na = Ja | Ka, Oa = La | Ma, Pa = Na | Oa, Qa = ["x", "y"], Ra = ["clientX", "clientY"];
    x.prototype = {
        handler: function () {
        }, init: function () {
            this.evEl && m(this.element, this.evEl, this.domHandler), this.evTarget && m(this.target, this.evTarget, this.domHandler), this.evWin && m(w(this.element), this.evWin, this.domHandler)
        }, destroy: function () {
            this.evEl && n(this.element, this.evEl, this.domHandler), this.evTarget && n(this.target, this.evTarget, this.domHandler), this.evWin && n(w(this.element), this.evWin, this.domHandler)
        }
    };
    var Sa = {mousedown: Ea, mousemove: Fa, mouseup: Ga}, Ta = "mousedown", Ua = "mousemove mouseup";
    i(L, x, {
        handler: function (a) {
            var b = Sa[a.type];
            b & Ea && 0 === a.button && (this.pressed = !0), b & Fa && 1 !== a.which && (b = Ga), this.pressed && (b & Ga && (this.pressed = !1), this.callback(this.manager, b, {
                pointers: [a],
                changedPointers: [a],
                pointerType: Ba,
                srcEvent: a
            }))
        }
    });
    var Va = {pointerdown: Ea, pointermove: Fa, pointerup: Ga, pointercancel: Ha, pointerout: Ha},
        Wa = {2: za, 3: Aa, 4: Ba, 5: Ca}, Xa = "pointerdown", Ya = "pointermove pointerup pointercancel";
    a.MSPointerEvent && !a.PointerEvent && (Xa = "MSPointerDown", Ya = "MSPointerMove MSPointerUp MSPointerCancel"), i(M, x, {
        handler: function (a) {
            var b = this.store, c = !1, d = a.type.toLowerCase().replace("ms", ""), e = Va[d],
                f = Wa[a.pointerType] || a.pointerType, g = f == za, h = r(b, a.pointerId, "pointerId");
            e & Ea && (0 === a.button || g) ? 0 > h && (b.push(a), h = b.length - 1) : e & (Ga | Ha) && (c = !0), 0 > h || (b[h] = a, this.callback(this.manager, e, {
                pointers: b,
                changedPointers: [a],
                pointerType: f,
                srcEvent: a
            }), c && b.splice(h, 1))
        }
    });
    var Za = {touchstart: Ea, touchmove: Fa, touchend: Ga, touchcancel: Ha}, $a = "touchstart",
        _a = "touchstart touchmove touchend touchcancel";
    i(N, x, {
        handler: function (a) {
            var b = Za[a.type];
            if (b === Ea && (this.started = !0), this.started) {
                var c = O.call(this, a, b);
                b & (Ga | Ha) && c[0].length - c[1].length === 0 && (this.started = !1), this.callback(this.manager, b, {
                    pointers: c[0],
                    changedPointers: c[1],
                    pointerType: za,
                    srcEvent: a
                })
            }
        }
    });
    var ab = {touchstart: Ea, touchmove: Fa, touchend: Ga, touchcancel: Ha},
        bb = "touchstart touchmove touchend touchcancel";
    i(P, x, {
        handler: function (a) {
            var b = ab[a.type], c = Q.call(this, a, b);
            c && this.callback(this.manager, b, {pointers: c[0], changedPointers: c[1], pointerType: za, srcEvent: a})
        }
    });
    var cb = 2500, db = 25;
    i(R, x, {
        handler: function (a, b, c) {
            var d = c.pointerType == za, e = c.pointerType == Ba;
            if (!(e && c.sourceCapabilities && c.sourceCapabilities.firesTouchEvents)) {
                if (d) S.call(this, b, c); else if (e && U.call(this, c)) return;
                this.callback(a, b, c)
            }
        }, destroy: function () {
            this.touch.destroy(), this.mouse.destroy()
        }
    });
    var eb = u(na.style, "touchAction"), fb = eb !== d, gb = "compute", hb = "auto", ib = "manipulation", jb = "none",
        kb = "pan-x", lb = "pan-y", mb = X();
    V.prototype = {
        set: function (a) {
            a == gb && (a = this.compute()), fb && this.manager.element.style && mb[a] && (this.manager.element.style[eb] = a), this.actions = a.toLowerCase().trim()
        }, update: function () {
            this.set(this.manager.options.touchAction)
        }, compute: function () {
            var a = [];
            return g(this.manager.recognizers, function (b) {
                k(b.options.enable, [b]) && (a = a.concat(b.getTouchAction()))
            }), W(a.join(" "))
        }, preventDefaults: function (a) {
            var b = a.srcEvent, c = a.offsetDirection;
            if (this.manager.session.prevented) return void b.preventDefault();
            var d = this.actions, e = p(d, jb) && !mb[jb], f = p(d, lb) && !mb[lb], g = p(d, kb) && !mb[kb];
            if (e) {
                var h = 1 === a.pointers.length, i = a.distance < 2, j = a.deltaTime < 250;
                if (h && i && j) return
            }
            return g && f ? void 0 : e || f && c & Na || g && c & Oa ? this.preventSrc(b) : void 0
        }, preventSrc: function (a) {
            this.manager.session.prevented = !0, a.preventDefault()
        }
    };
    var nb = 1, ob = 2, pb = 4, qb = 8, rb = qb, sb = 16, tb = 32;
    Y.prototype = {
        defaults: {}, set: function (a) {
            return la(this.options, a), this.manager && this.manager.touchAction.update(), this
        }, recognizeWith: function (a) {
            if (f(a, "recognizeWith", this)) return this;
            var b = this.simultaneous;
            return a = _(a, this), b[a.id] || (b[a.id] = a, a.recognizeWith(this)), this
        }, dropRecognizeWith: function (a) {
            return f(a, "dropRecognizeWith", this) ? this : (a = _(a, this), delete this.simultaneous[a.id], this)
        }, requireFailure: function (a) {
            if (f(a, "requireFailure", this)) return this;
            var b = this.requireFail;
            return a = _(a, this), -1 === r(b, a) && (b.push(a), a.requireFailure(this)), this
        }, dropRequireFailure: function (a) {
            if (f(a, "dropRequireFailure", this)) return this;
            a = _(a, this);
            var b = r(this.requireFail, a);
            return b > -1 && this.requireFail.splice(b, 1), this
        }, hasRequireFailures: function () {
            return this.requireFail.length > 0
        }, canRecognizeWith: function (a) {
            return !!this.simultaneous[a.id]
        }, emit: function (a) {
            function b(b) {
                c.manager.emit(b, a)
            }

            var c = this, d = this.state;
            qb > d && b(c.options.event + Z(d)), b(c.options.event), a.additionalEvent && b(a.additionalEvent), d >= qb && b(c.options.event + Z(d))
        }, tryEmit: function (a) {
            return this.canEmit() ? this.emit(a) : void (this.state = tb)
        }, canEmit: function () {
            for (var a = 0; a < this.requireFail.length;) {
                if (!(this.requireFail[a].state & (tb | nb))) return !1;
                a++
            }
            return !0
        }, recognize: function (a) {
            var b = la({}, a);
            return k(this.options.enable, [this, b]) ? (this.state & (rb | sb | tb) && (this.state = nb), this.state = this.process(b), void (this.state & (ob | pb | qb | sb) && this.tryEmit(b))) : (this.reset(), void (this.state = tb))
        }, process: function (a) {
        }, getTouchAction: function () {
        }, reset: function () {
        }
    }, i(aa, Y, {
        defaults: {pointers: 1}, attrTest: function (a) {
            var b = this.options.pointers;
            return 0 === b || a.pointers.length === b
        }, process: function (a) {
            var b = this.state, c = a.eventType, d = b & (ob | pb), e = this.attrTest(a);
            return d && (c & Ha || !e) ? b | sb : d || e ? c & Ga ? b | qb : b & ob ? b | pb : ob : tb
        }
    }), i(ba, aa, {
        defaults: {event: "pan", threshold: 10, pointers: 1, direction: Pa}, getTouchAction: function () {
            var a = this.options.direction, b = [];
            return a & Na && b.push(lb), a & Oa && b.push(kb), b
        }, directionTest: function (a) {
            var b = this.options, c = !0, d = a.distance, e = a.direction, f = a.deltaX, g = a.deltaY;
            return e & b.direction || (b.direction & Na ? (e = 0 === f ? Ia : 0 > f ? Ja : Ka, c = f != this.pX, d = Math.abs(a.deltaX)) : (e = 0 === g ? Ia : 0 > g ? La : Ma, c = g != this.pY, d = Math.abs(a.deltaY))), a.direction = e, c && d > b.threshold && e & b.direction
        }, attrTest: function (a) {
            return aa.prototype.attrTest.call(this, a) && (this.state & ob || !(this.state & ob) && this.directionTest(a))
        }, emit: function (a) {
            this.pX = a.deltaX, this.pY = a.deltaY;
            var b = $(a.direction);
            b && (a.additionalEvent = this.options.event + b), this._super.emit.call(this, a)
        }
    }), i(ca, aa, {
        defaults: {event: "pinch", threshold: 0, pointers: 2}, getTouchAction: function () {
            return [jb]
        }, attrTest: function (a) {
            return this._super.attrTest.call(this, a) && (Math.abs(a.scale - 1) > this.options.threshold || this.state & ob)
        }, emit: function (a) {
            if (1 !== a.scale) {
                var b = a.scale < 1 ? "in" : "out";
                a.additionalEvent = this.options.event + b
            }
            this._super.emit.call(this, a)
        }
    }), i(da, Y, {
        defaults: {event: "press", pointers: 1, time: 251, threshold: 9}, getTouchAction: function () {
            return [hb]
        }, process: function (a) {
            var b = this.options, c = a.pointers.length === b.pointers, d = a.distance < b.threshold,
                f = a.deltaTime > b.time;
            if (this._input = a, !d || !c || a.eventType & (Ga | Ha) && !f) this.reset(); else if (a.eventType & Ea) this.reset(), this._timer = e(function () {
                this.state = rb, this.tryEmit()
            }, b.time, this); else if (a.eventType & Ga) return rb;
            return tb
        }, reset: function () {
            clearTimeout(this._timer)
        }, emit: function (a) {
            this.state === rb && (a && a.eventType & Ga ? this.manager.emit(this.options.event + "up", a) : (this._input.timeStamp = ra(), this.manager.emit(this.options.event, this._input)))
        }
    }), i(ea, aa, {
        defaults: {event: "rotate", threshold: 0, pointers: 2}, getTouchAction: function () {
            return [jb]
        }, attrTest: function (a) {
            return this._super.attrTest.call(this, a) && (Math.abs(a.rotation) > this.options.threshold || this.state & ob)
        }
    }), i(fa, aa, {
        defaults: {event: "swipe", threshold: 10, velocity: .3, direction: Na | Oa, pointers: 1},
        getTouchAction: function () {
            return ba.prototype.getTouchAction.call(this)
        },
        attrTest: function (a) {
            var b, c = this.options.direction;
            return c & (Na | Oa) ? b = a.overallVelocity : c & Na ? b = a.overallVelocityX : c & Oa && (b = a.overallVelocityY), this._super.attrTest.call(this, a) && c & a.offsetDirection && a.distance > this.options.threshold && a.maxPointers == this.options.pointers && qa(b) > this.options.velocity && a.eventType & Ga
        },
        emit: function (a) {
            var b = $(a.offsetDirection);
            b && this.manager.emit(this.options.event + b, a), this.manager.emit(this.options.event, a)
        }
    }), i(ga, Y, {
        defaults: {
            event: "tap",
            pointers: 1,
            taps: 1,
            interval: 300,
            time: 250,
            threshold: 9,
            posThreshold: 10
        }, getTouchAction: function () {
            return [ib]
        }, process: function (a) {
            var b = this.options, c = a.pointers.length === b.pointers, d = a.distance < b.threshold,
                f = a.deltaTime < b.time;
            if (this.reset(), a.eventType & Ea && 0 === this.count) return this.failTimeout();
            if (d && f && c) {
                if (a.eventType != Ga) return this.failTimeout();
                var g = this.pTime ? a.timeStamp - this.pTime < b.interval : !0,
                    h = !this.pCenter || H(this.pCenter, a.center) < b.posThreshold;
                this.pTime = a.timeStamp, this.pCenter = a.center, h && g ? this.count += 1 : this.count = 1, this._input = a;
                var i = this.count % b.taps;
                if (0 === i) return this.hasRequireFailures() ? (this._timer = e(function () {
                    this.state = rb, this.tryEmit()
                }, b.interval, this), ob) : rb
            }
            return tb
        }, failTimeout: function () {
            return this._timer = e(function () {
                this.state = tb
            }, this.options.interval, this), tb
        }, reset: function () {
            clearTimeout(this._timer)
        }, emit: function () {
            this.state == rb && (this._input.tapCount = this.count, this.manager.emit(this.options.event, this._input))
        }
    }), ha.VERSION = "2.0.8", ha.defaults = {
        domEvents: !1,
        touchAction: gb,
        enable: !0,
        inputTarget: null,
        inputClass: null,
        preset: [[ea, {enable: !1}], [ca, {enable: !1}, ["rotate"]], [fa, {direction: Na}], [ba, {direction: Na}, ["swipe"]], [ga], [ga, {
            event: "doubletap",
            taps: 2
        }, ["tap"]], [da]],
        cssProps: {
            userSelect: "none",
            touchSelect: "none",
            touchCallout: "none",
            contentZooming: "none",
            userDrag: "none",
            tapHighlightColor: "rgba(0,0,0,0)"
        }
    };
    var ub = 1, vb = 2;
    ia.prototype = {
        set: function (a) {
            return la(this.options, a), a.touchAction && this.touchAction.update(), a.inputTarget && (this.input.destroy(), this.input.target = a.inputTarget, this.input.init()), this
        }, stop: function (a) {
            this.session.stopped = a ? vb : ub
        }, recognize: function (a) {
            var b = this.session;
            if (!b.stopped) {
                this.touchAction.preventDefaults(a);
                var c, d = this.recognizers, e = b.curRecognizer;
                (!e || e && e.state & rb) && (e = b.curRecognizer = null);
                for (var f = 0; f < d.length;) c = d[f], b.stopped === vb || e && c != e && !c.canRecognizeWith(e) ? c.reset() : c.recognize(a), !e && c.state & (ob | pb | qb) && (e = b.curRecognizer = c), f++
            }
        }, get: function (a) {
            if (a instanceof Y) return a;
            for (var b = this.recognizers, c = 0; c < b.length; c++) if (b[c].options.event == a) return b[c];
            return null
        }, add: function (a) {
            if (f(a, "add", this)) return this;
            var b = this.get(a.options.event);
            return b && this.remove(b), this.recognizers.push(a), a.manager = this, this.touchAction.update(), a
        }, remove: function (a) {
            if (f(a, "remove", this)) return this;
            if (a = this.get(a)) {
                var b = this.recognizers, c = r(b, a);
                -1 !== c && (b.splice(c, 1), this.touchAction.update())
            }
            return this
        }, on: function (a, b) {
            if (a !== d && b !== d) {
                var c = this.handlers;
                return g(q(a), function (a) {
                    c[a] = c[a] || [], c[a].push(b)
                }), this
            }
        }, off: function (a, b) {
            if (a !== d) {
                var c = this.handlers;
                return g(q(a), function (a) {
                    b ? c[a] && c[a].splice(r(c[a], b), 1) : delete c[a]
                }), this
            }
        }, emit: function (a, b) {
            this.options.domEvents && ka(a, b);
            var c = this.handlers[a] && this.handlers[a].slice();
            if (c && c.length) {
                b.type = a, b.preventDefault = function () {
                    b.srcEvent.preventDefault()
                };
                for (var d = 0; d < c.length;) c[d](b), d++
            }
        }, destroy: function () {
            this.element && ja(this, !1), this.handlers = {}, this.session = {}, this.input.destroy(), this.element = null
        }
    }, la(ha, {
        INPUT_START: Ea,
        INPUT_MOVE: Fa,
        INPUT_END: Ga,
        INPUT_CANCEL: Ha,
        STATE_POSSIBLE: nb,
        STATE_BEGAN: ob,
        STATE_CHANGED: pb,
        STATE_ENDED: qb,
        STATE_RECOGNIZED: rb,
        STATE_CANCELLED: sb,
        STATE_FAILED: tb,
        DIRECTION_NONE: Ia,
        DIRECTION_LEFT: Ja,
        DIRECTION_RIGHT: Ka,
        DIRECTION_UP: La,
        DIRECTION_DOWN: Ma,
        DIRECTION_HORIZONTAL: Na,
        DIRECTION_VERTICAL: Oa,
        DIRECTION_ALL: Pa,
        Manager: ia,
        Input: x,
        TouchAction: V,
        TouchInput: P,
        MouseInput: L,
        PointerEventInput: M,
        TouchMouseInput: R,
        SingleTouchInput: N,
        Recognizer: Y,
        AttrRecognizer: aa,
        Tap: ga,
        Pan: ba,
        Swipe: fa,
        Pinch: ca,
        Rotate: ea,
        Press: da,
        on: m,
        off: n,
        each: g,
        merge: ta,
        extend: sa,
        assign: la,
        inherit: i,
        bindFn: j,
        prefixed: u
    });
    var wb = "undefined" != typeof a ? a : "undefined" != typeof self ? self : {};
    wb.Hammer = ha, "function" == typeof define && define.amd ? define(function () {
        return ha
    }) : "undefined" != typeof module && module.exports ? module.exports = ha : a[c] = ha
}(window, document, "Hammer");

//# sourceMappingURL=hammer.min.js.map
function t142_checkSize(recid) {
    var el = $("#rec" + recid).find(".t142__submit");
    if (el.length) {
        var btnheight = el.height() + 5;
        var textheight = el[0].scrollHeight;
        if (btnheight < textheight) {
            var btntext = el.text();
            el.addClass("t142__submit-overflowed");
            el.html("<span class=\"t142__text\">" + btntext + "</span>")
        }
    }
}

function t228_highlight() {
    var url = window.location.href;
    var pathname = window.location.pathname;
    if (url.substr(url.length - 1) == "/") {
        url = url.slice(0, -1)
    }
    if (pathname.substr(pathname.length - 1) == "/") {
        pathname = pathname.slice(0, -1)
    }
    if (pathname.charAt(0) == "/") {
        pathname = pathname.slice(1)
    }
    if (pathname == "") {
        pathname = "/"
    }
    $(".t228__list_item a[href='" + url + "']").addClass("t-active");
    $(".t228__list_item a[href='" + url + "/']").addClass("t-active");
    $(".t228__list_item a[href='" + pathname + "']").addClass("t-active");
    $(".t228__list_item a[href='/" + pathname + "']").addClass("t-active");
    $(".t228__list_item a[href='" + pathname + "/']").addClass("t-active");
    $(".t228__list_item a[href='/" + pathname + "/']").addClass("t-active")
}

function t228_checkAnchorLinks(recid) {
    if ($(window).width() >= 960) {
        var t228_navLinks = $("#rec" + recid + " .t228__list_item a:not(.tooltipstered)[href*='#']");
        if (t228_navLinks.length > 0) {
            setTimeout(function () {
                t228_catchScroll(t228_navLinks)
            }, 500)
        }
    }
}

function t228_catchScroll(t228_navLinks) {
    var t228_clickedSectionId = null, t228_sections = new Array(), t228_sectionIdTonavigationLink = [],
        t228_interval = 100, t228_lastCall, t228_timeoutId;
    t228_navLinks = $(t228_navLinks.get().reverse());
    t228_navLinks.each(function () {
        var t228_cursection = t228_getSectionByHref($(this));
        if (typeof t228_cursection.attr("id") != "undefined") {
            t228_sections.push(t228_cursection)
        }
        t228_sectionIdTonavigationLink[t228_cursection.attr("id")] = $(this)
    });
    t228_updateSectionsOffsets(t228_sections);
    t228_sections.sort(function (a, b) {
        return b.attr("data-offset-top") - a.attr("data-offset-top")
    });
    $(window).bind('resize', t_throttle(function () {
        t228_updateSectionsOffsets(t228_sections)
    }, 200));
    $('.t228').bind('displayChanged', function () {
        t228_updateSectionsOffsets(t228_sections)
    });
    setInterval(function () {
        t228_updateSectionsOffsets(t228_sections)
    }, 5000);
    t228_highlightNavLinks(t228_navLinks, t228_sections, t228_sectionIdTonavigationLink, t228_clickedSectionId);
    t228_navLinks.click(function () {
        var t228_clickedSection = t228_getSectionByHref($(this));
        if (!$(this).hasClass("tooltipstered") && typeof t228_clickedSection.attr("id") != "undefined") {
            t228_navLinks.removeClass('t-active');
            $(this).addClass('t-active');
            t228_clickedSectionId = t228_getSectionByHref($(this)).attr("id")
        }
    });
    $(window).scroll(function () {
        var t228_now = new Date().getTime();
        if (t228_lastCall && t228_now < (t228_lastCall + t228_interval)) {
            clearTimeout(t228_timeoutId);
            t228_timeoutId = setTimeout(function () {
                t228_lastCall = t228_now;
                t228_clickedSectionId = t228_highlightNavLinks(t228_navLinks, t228_sections, t228_sectionIdTonavigationLink, t228_clickedSectionId)
            }, t228_interval - (t228_now - t228_lastCall))
        } else {
            t228_lastCall = t228_now;
            t228_clickedSectionId = t228_highlightNavLinks(t228_navLinks, t228_sections, t228_sectionIdTonavigationLink, t228_clickedSectionId)
        }
    })
}

function t228_updateSectionsOffsets(sections) {
    $(sections).each(function () {
        var t228_curSection = $(this);
        t228_curSection.attr("data-offset-top", t228_curSection.offset().top)
    })
}

function t228_getSectionByHref(curlink) {
    var t228_curLinkValue = curlink.attr("href").replace(/\s+/g, '');
    if (t228_curLinkValue[0] == '/') {
        t228_curLinkValue = t228_curLinkValue.substring(1)
    }
    if (curlink.is('[href*="#rec"]')) {
        return $(".r[id='" + t228_curLinkValue.substring(1) + "']")
    } else {
        return $(".r[data-record-type='215']").has("a[name='" + t228_curLinkValue.substring(1) + "']")
    }
}

function t228_highlightNavLinks(t228_navLinks, t228_sections, t228_sectionIdTonavigationLink, t228_clickedSectionId) {
    var t228_scrollPosition = $(window).scrollTop(), t228_valueToReturn = t228_clickedSectionId;
    if (t228_sections.length != 0 && t228_clickedSectionId == null && t228_sections[t228_sections.length - 1].attr("data-offset-top") > (t228_scrollPosition + 300)) {
        t228_navLinks.removeClass('t-active');
        return null
    }
    $(t228_sections).each(function (e) {
        var t228_curSection = $(this), t228_sectionTop = t228_curSection.attr("data-offset-top"),
            t228_id = t228_curSection.attr('id'), t228_navLink = t228_sectionIdTonavigationLink[t228_id];
        if (((t228_scrollPosition + 300) >= t228_sectionTop) || (t228_sections[0].attr("id") == t228_id && t228_scrollPosition >= $(document).height() - $(window).height())) {
            if (t228_clickedSectionId == null && !t228_navLink.hasClass('t-active')) {
                t228_navLinks.removeClass('t-active');
                t228_navLink.addClass('t-active');
                t228_valueToReturn = null
            } else {
                if (t228_clickedSectionId != null && t228_id == t228_clickedSectionId) {
                    t228_valueToReturn = null
                }
            }
            return !1
        }
    });
    return t228_valueToReturn
}

function t228_setPath() {
}

function t228_setWidth(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t228").each(function () {
            var el = $(this);
            var left_exist = el.find('.t228__leftcontainer').length;
            var left_w = el.find('.t228__leftcontainer').outerWidth(!0);
            var max_w = left_w;
            var right_exist = el.find('.t228__rightcontainer').length;
            var right_w = el.find('.t228__rightcontainer').outerWidth(!0);
            var items_align = el.attr('data-menu-items-align');
            if (left_w < right_w) max_w = right_w;
            max_w = Math.ceil(max_w);
            var center_w = 0;
            el.find('.t228__centercontainer').find('li').each(function () {
                center_w += $(this).outerWidth(!0)
            });
            var padd_w = 40;
            var maincontainer_width = el.find(".t228__maincontainer").outerWidth(!0);
            if (maincontainer_width - max_w * 2 - padd_w * 2 > center_w + 20) {
                if (items_align == "center" || typeof items_align === "undefined") {
                    el.find(".t228__leftside").css("min-width", max_w + "px");
                    el.find(".t228__rightside").css("min-width", max_w + "px");
                    el.find(".t228__list").removeClass("t228__list_hidden")
                }
            } else {
                el.find(".t228__leftside").css("min-width", "");
                el.find(".t228__rightside").css("min-width", "")
            }
        })
    }
}

function t228_setBg(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t228").each(function () {
            var el = $(this);
            if (el.attr('data-bgcolor-setbyscript') == "yes") {
                var bgcolor = el.attr("data-bgcolor-rgba");
                el.css("background-color", bgcolor)
            }
        })
    } else {
        $(".t228").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-hex");
            el.css("background-color", bgcolor);
            el.attr("data-bgcolor-setbyscript", "yes")
        })
    }
}

function t228_appearMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t228").each(function () {
            var el = $(this);
            var appearoffset = el.attr("data-appearoffset");
            if (appearoffset != "") {
                if (appearoffset.indexOf('vh') > -1) {
                    appearoffset = Math.floor((window.innerHeight * (parseInt(appearoffset) / 100)))
                }
                appearoffset = parseInt(appearoffset, 10);
                if ($(window).scrollTop() >= appearoffset) {
                    if (el.css('visibility') == 'hidden') {
                        el.finish();
                        el.css("top", "-50px");
                        el.css("visibility", "visible");
                        var topoffset = el.data('top-offset');
                        if (topoffset && parseInt(topoffset) > 0) {
                            el.animate({"opacity": "1", "top": topoffset + "px"}, 200, function () {
                            })
                        } else {
                            el.animate({"opacity": "1", "top": "0px"}, 200, function () {
                            })
                        }
                    }
                } else {
                    el.stop();
                    el.css("visibility", "hidden");
                    el.css("opacity", "0")
                }
            }
        })
    }
}

function t228_changebgopacitymenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t228").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-rgba");
            var bgcolor_afterscroll = el.attr("data-bgcolor-rgba-afterscroll");
            var bgopacityone = el.attr("data-bgopacity");
            var bgopacitytwo = el.attr("data-bgopacity-two");
            var menushadow = el.attr("data-menushadow");
            if (menushadow == '100') {
                var menushadowvalue = menushadow
            } else {
                var menushadowvalue = '0.' + menushadow
            }
            if ($(window).scrollTop() > 20) {
                el.css("background-color", bgcolor_afterscroll);
                if (bgopacitytwo == '0' || (typeof menushadow == "undefined" && menushadow == !1)) {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            } else {
                el.css("background-color", bgcolor);
                if (bgopacityone == '0.0' || (typeof menushadow == "undefined" && menushadow == !1)) {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            }
        })
    }
}

function t228_createMobileMenu(recid) {
    var window_width = $(window).width(), el = $("#rec" + recid), menu = el.find(".t228"),
        burger = el.find(".t228__mobile");
    burger.click(function (e) {
        menu.fadeToggle(300);
        $(this).toggleClass("t228_opened")
    })
    $(window).bind('resize', t_throttle(function () {
        window_width = $(window).width();
        if (window_width > 980) {
            menu.fadeIn(0)
        }
    }, 200))
}

function t396_init(recid) {
    var data = '';
    var res = t396_detectResolution();
    t396_initTNobj();
    t396_switchResolution(res);
    t396_updateTNobj();
    t396_artboard_build(data, recid);
    window.tn_window_width = $(window).width();
    $(window).resize(function () {
        tn_console('>>>> t396: Window on Resize event >>>>');
        t396_waitForFinalEvent(function () {
            if ($isMobile) {
                var ww = $(window).width();
                if (ww != window.tn_window_width) {
                    t396_doResize(recid)
                }
            } else {
                t396_doResize(recid)
            }
        }, 500, 'resizeruniqueid' + recid)
    });
    $(window).on("orientationchange", function () {
        tn_console('>>>> t396: Orient change event >>>>');
        t396_waitForFinalEvent(function () {
            t396_doResize(recid)
        }, 600, 'orientationuniqueid' + recid)
    });
    $(window).load(function () {
        var ab = $('#rec' + recid).find('.t396__artboard');
        t396_allelems__renderView(ab)
    });
    var rec = $('#rec' + recid);
    if (rec.attr('data-connect-with-tab') == 'yes') {
        rec.find('.t396').bind('displayChanged', function () {
            var ab = rec.find('.t396__artboard');
            t396_allelems__renderView(ab)
        })
    }
}

function t396_doResize(recid) {
    var ww = $(window).width();
    window.tn_window_width = ww;
    var res = t396_detectResolution();
    var ab = $('#rec' + recid).find('.t396__artboard');
    t396_switchResolution(res);
    t396_updateTNobj();
    t396_ab__renderView(ab);
    t396_allelems__renderView(ab)
}

function t396_detectResolution() {
    var ww = $(window).width();
    var res;
    res = 1200;
    if (ww < 1200) {
        res = 960
    }
    if (ww < 960) {
        res = 640
    }
    if (ww < 640) {
        res = 480
    }
    if (ww < 480) {
        res = 320
    }
    return (res)
}

function t396_initTNobj() {
    tn_console('func: initTNobj');
    window.tn = {};
    window.tn.canvas_min_sizes = ["320", "480", "640", "960", "1200"];
    window.tn.canvas_max_sizes = ["480", "640", "960", "1200", ""];
    window.tn.ab_fields = ["height", "width", "bgcolor", "bgimg", "bgattachment", "bgposition", "filteropacity", "filtercolor", "filteropacity2", "filtercolor2", "height_vh", "valign"]
}

function t396_updateTNobj() {
    tn_console('func: updateTNobj');
    if (typeof window.zero_window_width_hook != 'undefined' && window.zero_window_width_hook == 'allrecords' && $('#allrecords').length) {
        window.tn.window_width = parseInt($('#allrecords').width())
    } else {
        window.tn.window_width = parseInt($(window).width())
    }
    window.tn.window_height = parseInt($(window).height());
    if (window.tn.curResolution == 1200) {
        window.tn.canvas_min_width = 1200;
        window.tn.canvas_max_width = window.tn.window_width
    }
    if (window.tn.curResolution == 960) {
        window.tn.canvas_min_width = 960;
        window.tn.canvas_max_width = 1200
    }
    if (window.tn.curResolution == 640) {
        window.tn.canvas_min_width = 640;
        window.tn.canvas_max_width = 960
    }
    if (window.tn.curResolution == 480) {
        window.tn.canvas_min_width = 480;
        window.tn.canvas_max_width = 640
    }
    if (window.tn.curResolution == 320) {
        window.tn.canvas_min_width = 320;
        window.tn.canvas_max_width = 480
    }
    window.tn.grid_width = window.tn.canvas_min_width;
    window.tn.grid_offset_left = parseFloat((window.tn.window_width - window.tn.grid_width) / 2)
}

var t396_waitForFinalEvent = (function () {
    var timers = {};
    return function (callback, ms, uniqueId) {
        if (!uniqueId) {
            uniqueId = "Don't call this twice without a uniqueId"
        }
        if (timers[uniqueId]) {
            clearTimeout(timers[uniqueId])
        }
        timers[uniqueId] = setTimeout(callback, ms)
    }
})();

function t396_switchResolution(res, resmax) {
    tn_console('func: switchResolution');
    if (typeof resmax == 'undefined') {
        if (res == 1200) resmax = '';
        if (res == 960) resmax = 1200;
        if (res == 640) resmax = 960;
        if (res == 480) resmax = 640;
        if (res == 320) resmax = 480
    }
    window.tn.curResolution = res;
    window.tn.curResolution_max = resmax
}

function t396_artboard_build(data, recid) {
    tn_console('func: t396_artboard_build. Recid:' + recid);
    tn_console(data);
    var ab = $('#rec' + recid).find('.t396__artboard');
    t396_ab__renderView(ab);
    ab.find('.tn-elem').each(function () {
        var item = $(this);
        if (item.attr('data-elem-type') == 'text') {
            t396_addText(ab, item)
        }
        if (item.attr('data-elem-type') == 'image') {
            t396_addImage(ab, item)
        }
        if (item.attr('data-elem-type') == 'shape') {
            t396_addShape(ab, item)
        }
        if (item.attr('data-elem-type') == 'button') {
            t396_addButton(ab, item)
        }
        if (item.attr('data-elem-type') == 'video') {
            t396_addVideo(ab, item)
        }
        if (item.attr('data-elem-type') == 'html') {
            t396_addHtml(ab, item)
        }
        if (item.attr('data-elem-type') == 'tooltip') {
            t396_addTooltip(ab, item)
        }
        if (item.attr('data-elem-type') == 'form') {
            t396_addForm(ab, item)
        }
    });
    $('#rec' + recid).find('.t396__artboard').removeClass('rendering').addClass('rendered');
    if (ab.attr('data-artboard-ovrflw') == 'visible') {
        $('#allrecords').css('overflow', 'hidden')
    }
}

function t396_ab__renderView(ab) {
    var fields = window.tn.ab_fields;
    for (var i = 0; i < fields.length; i++) {
        t396_ab__renderViewOneField(ab, fields[i])
    }
    var ab_min_height = t396_ab__getFieldValue(ab, 'height');
    var ab_max_height = t396_ab__getHeight(ab);
    var offset_top = 0;
    if (ab_min_height == ab_max_height) {
        offset_top = 0
    } else {
        var ab_valign = t396_ab__getFieldValue(ab, 'valign');
        if (ab_valign == 'top') {
            offset_top = 0
        } else if (ab_valign == 'center') {
            offset_top = parseFloat((ab_max_height - ab_min_height) / 2).toFixed(1)
        } else if (ab_valign == 'bottom') {
            offset_top = parseFloat((ab_max_height - ab_min_height)).toFixed(1)
        } else if (ab_valign == 'stretch') {
            offset_top = 0;
            ab_min_height = ab_max_height
        } else {
            offset_top = 0
        }
    }
    ab.attr('data-artboard-proxy-min-offset-top', offset_top);
    ab.attr('data-artboard-proxy-min-height', ab_min_height);
    ab.attr('data-artboard-proxy-max-height', ab_max_height)
}

function t396_addText(ab, el) {
    tn_console('func: addText');
    var fields_str = 'top,left,width,container,axisx,axisy,widthunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_addImage(ab, el) {
    tn_console('func: addImage');
    var fields_str = 'img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    el.find('img').on("load", function () {
        t396_elem__renderViewOneField(el, 'top');
        if (typeof $(this).attr('src') != 'undefined' && $(this).attr('src') != '') {
            setTimeout(function () {
                t396_elem__renderViewOneField(el, 'top')
            }, 2000)
        }
    }).each(function () {
        if (this.complete) $(this).load()
    });
    el.find('img').on('tuwidget_done', function (e, file) {
        t396_elem__renderViewOneField(el, 'top')
    })
}

function t396_addShape(ab, el) {
    tn_console('func: addShape');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_addButton(ab, el) {
    tn_console('func: addButton');
    var fields_str = 'top,left,width,height,container,axisx,axisy,caption,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    return (el)
}

function t396_addVideo(ab, el) {
    tn_console('func: addVideo');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    var viel = el.find('.tn-atom__videoiframe');
    var viatel = el.find('.tn-atom');
    viatel.css('background-color', '#000');
    var vihascover = viatel.attr('data-atom-video-has-cover');
    if (typeof vihascover == 'undefined') {
        vihascover = ''
    }
    if (vihascover == 'y') {
        viatel.click(function () {
            var viifel = viel.find('iframe');
            if (viifel.length) {
                var foo = viifel.attr('data-original');
                viifel.attr('src', foo)
            }
            viatel.css('background-image', 'none');
            viatel.find('.tn-atom__video-play-link').css('display', 'none')
        })
    }
    var autoplay = t396_elem__getFieldValue(el, 'autoplay');
    var showinfo = t396_elem__getFieldValue(el, 'showinfo');
    var loop = t396_elem__getFieldValue(el, 'loop');
    var mute = t396_elem__getFieldValue(el, 'mute');
    var startsec = t396_elem__getFieldValue(el, 'startsec');
    var endsec = t396_elem__getFieldValue(el, 'endsec');
    var tmode = $('#allrecords').attr('data-tilda-mode');
    var url = '';
    var viyid = viel.attr('data-youtubeid');
    if (typeof viyid != 'undefined' && viyid != '') {
        url = '//www.youtube.com/embed/';
        url += viyid + '?rel=0&fmt=18&html5=1';
        url += '&showinfo=' + (showinfo == 'y' ? '1' : '0');
        if (loop == 'y') {
            url += '&loop=1&playlist=' + viyid
        }
        if (startsec > 0) {
            url += '&start=' + startsec
        }
        if (endsec > 0) {
            url += '&end=' + endsec
        }
        if (mute == 'y') {
            url += '&mute=1'
        }
        if (vihascover == 'y') {
            url += '&autoplay=1';
            viel.html('<iframe id="youtubeiframe" width="100%" height="100%" data-original="' + url + '" frameborder="0" allowfullscreen data-flag-inst="y"></iframe>')
        } else {
            if (typeof tmode != 'undefined' && tmode == 'edit') {
            } else {
                if (autoplay == 'y') {
                    url += '&autoplay=1'
                }
            }
            if (window.lazy == 'y') {
                viel.html('<iframe id="youtubeiframe" class="t-iframe" width="100%" height="100%" data-original="' + url + '" frameborder="0" allowfullscreen data-flag-inst="lazy"></iframe>');
                el.append('<script>lazyload_iframe = new LazyLoad({elements_selector: ".t-iframe"});</script>')
            } else {
                viel.html('<iframe id="youtubeiframe" width="100%" height="100%" src="' + url + '" frameborder="0" allowfullscreen data-flag-inst="y"></iframe>')
            }
        }
    }
    var vivid = viel.attr('data-vimeoid');
    if (typeof vivid != 'undefined' && vivid > 0) {
        url = '//player.vimeo.com/video/';
        url += vivid + '?color=ffffff&badge=0';
        if (showinfo == 'y') {
            url += '&title=1&byline=1&portrait=1'
        } else {
            url += '&title=0&byline=0&portrait=0'
        }
        if (loop == 'y') {
            url += '&loop=1'
        }
        if (mute == 'y') {
            url += '&muted=1'
        }
        if (vihascover == 'y') {
            url += '&autoplay=1';
            viel.html('<iframe data-original="' + url + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
        } else {
            if (typeof tmode != 'undefined' && tmode == 'edit') {
            } else {
                if (autoplay == 'y') {
                    url += '&autoplay=1'
                }
            }
            if (window.lazy == 'y') {
                viel.html('<iframe class="t-iframe" data-original="' + url + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                el.append('<script>lazyload_iframe = new LazyLoad({elements_selector: ".t-iframe"});</script>')
            } else {
                viel.html('<iframe src="' + url + '" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>')
            }
        }
    }
}

function t396_addHtml(ab, el) {
    tn_console('func: addHtml');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_addTooltip(ab, el) {
    tn_console('func: addTooltip');
    var fields_str = 'width,height,top,left,';
    fields_str += 'container,axisx,axisy,widthunits,heightunits,leftunits,topunits,tipposition';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el);
    var pinEl = el.find('.tn-atom__pin');
    var tipEl = el.find('.tn-atom__tip');
    var tipopen = el.attr('data-field-tipopen-value');
    if (isMobile || (typeof tipopen != 'undefined' && tipopen == 'click')) {
        t396_setUpTooltip_mobile(el, pinEl, tipEl)
    } else {
        t396_setUpTooltip_desktop(el, pinEl, tipEl)
    }
    setTimeout(function () {
        $('.tn-atom__tip-img').each(function () {
            var foo = $(this).attr('data-tipimg-original');
            if (typeof foo != 'undefined' && foo != '') {
                $(this).attr('src', foo)
            }
        })
    }, 3000)
}

function t396_addForm(ab, el) {
    tn_console('func: addForm');
    var fields_str = 'width,top,left,';
    fields_str += 'inputs,container,axisx,axisy,widthunits,leftunits,topunits';
    var fields = fields_str.split(',');
    el.attr('data-fields', fields_str);
    t396_elem__renderView(el)
}

function t396_elem__setFieldValue(el, prop, val, flag_render, flag_updateui, res) {
    if (res == '') res = window.tn.curResolution;
    if (res < 1200 && prop != 'zindex') {
        el.attr('data-field-' + prop + '-res-' + res + '-value', val)
    } else {
        el.attr('data-field-' + prop + '-value', val)
    }
    if (flag_render == 'render') elem__renderViewOneField(el, prop);
    if (flag_updateui == 'updateui') panelSettings__updateUi(el, prop, val)
}

function t396_elem__getFieldValue(el, prop) {
    var res = window.tn.curResolution;
    var r;
    if (res < 1200) {
        if (res == 960) {
            r = el.attr('data-field-' + prop + '-res-960-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-value')
            }
        }
        if (res == 640) {
            r = el.attr('data-field-' + prop + '-res-640-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-res-960-value');
                if (typeof r == 'undefined') {
                    r = el.attr('data-field-' + prop + '-value')
                }
            }
        }
        if (res == 480) {
            r = el.attr('data-field-' + prop + '-res-480-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-res-640-value');
                if (typeof r == 'undefined') {
                    r = el.attr('data-field-' + prop + '-res-960-value');
                    if (typeof r == 'undefined') {
                        r = el.attr('data-field-' + prop + '-value')
                    }
                }
            }
        }
        if (res == 320) {
            r = el.attr('data-field-' + prop + '-res-320-value');
            if (typeof r == 'undefined') {
                r = el.attr('data-field-' + prop + '-res-480-value');
                if (typeof r == 'undefined') {
                    r = el.attr('data-field-' + prop + '-res-640-value');
                    if (typeof r == 'undefined') {
                        r = el.attr('data-field-' + prop + '-res-960-value');
                        if (typeof r == 'undefined') {
                            r = el.attr('data-field-' + prop + '-value')
                        }
                    }
                }
            }
        }
    } else {
        r = el.attr('data-field-' + prop + '-value')
    }
    return (r)
}

function t396_elem__renderView(el) {
    tn_console('func: elem__renderView');
    var fields = el.attr('data-fields');
    if (!fields) {
        return !1
    }
    fields = fields.split(',');
    for (var i = 0; i < fields.length; i++) {
        t396_elem__renderViewOneField(el, fields[i])
    }
}

function t396_elem__renderViewOneField(el, field) {
    var value = t396_elem__getFieldValue(el, field);
    if (field == 'left') {
        value = t396_elem__convertPosition__Local__toAbsolute(el, field, value);
        el.css('left', parseFloat(value).toFixed(1) + 'px')
    }
    if (field == 'top') {
        value = t396_elem__convertPosition__Local__toAbsolute(el, field, value);
        el.css('top', parseFloat(value).toFixed(1) + 'px')
    }
    if (field == 'width') {
        value = t396_elem__getWidth(el, value);
        el.css('width', parseFloat(value).toFixed(1) + 'px');
        var eltype = el.attr('data-elem-type');
        if (eltype == 'tooltip') {
            var pinSvgIcon = el.find('.tn-atom__pin-icon');
            if (pinSvgIcon.length > 0) {
                var pinSize = parseFloat(value).toFixed(1) + 'px';
                pinSvgIcon.css({'width': pinSize, 'height': pinSize})
            }
            el.css('height', parseInt(value).toFixed(1) + 'px')
        }
    }
    if (field == 'height') {
        var eltype = el.attr('data-elem-type');
        if (eltype == 'tooltip') {
            return
        }
        value = t396_elem__getHeight(el, value);
        el.css('height', parseFloat(value).toFixed(1) + 'px')
    }
    if (field == 'container') {
        t396_elem__renderViewOneField(el, 'left');
        t396_elem__renderViewOneField(el, 'top')
    }
    if (field == 'width' || field == 'height' || field == 'fontsize' || field == 'fontfamily' || field == 'letterspacing' || field == 'fontweight' || field == 'img') {
        t396_elem__renderViewOneField(el, 'left');
        t396_elem__renderViewOneField(el, 'top')
    }
    if (field == 'inputs') {
        value = el.find('.tn-atom__inputs-textarea').val();
        try {
            t_zeroForms__renderForm(el, value)
        } catch (err) {
        }
    }
}

function t396_elem__convertPosition__Local__toAbsolute(el, field, value) {
    value = parseInt(value);
    if (field == 'left') {
        var el_container, offset_left, el_container_width, el_width;
        var container = t396_elem__getFieldValue(el, 'container');
        if (container == 'grid') {
            el_container = 'grid';
            offset_left = window.tn.grid_offset_left;
            el_container_width = window.tn.grid_width
        } else {
            el_container = 'window';
            offset_left = 0;
            el_container_width = window.tn.window_width
        }
        var el_leftunits = t396_elem__getFieldValue(el, 'leftunits');
        if (el_leftunits == '%') {
            value = t396_roundFloat(el_container_width * value / 100)
        }
        value = offset_left + value;
        var el_axisx = t396_elem__getFieldValue(el, 'axisx');
        if (el_axisx == 'center') {
            el_width = t396_elem__getWidth(el);
            value = el_container_width / 2 - el_width / 2 + value
        }
        if (el_axisx == 'right') {
            el_width = t396_elem__getWidth(el);
            value = el_container_width - el_width + value
        }
    }
    if (field == 'top') {
        var ab = el.parent();
        var el_container, offset_top, el_container_height, el_height;
        var container = t396_elem__getFieldValue(el, 'container');
        if (container == 'grid') {
            el_container = 'grid';
            offset_top = parseFloat(ab.attr('data-artboard-proxy-min-offset-top'));
            el_container_height = parseFloat(ab.attr('data-artboard-proxy-min-height'))
        } else {
            el_container = 'window';
            offset_top = 0;
            el_container_height = parseFloat(ab.attr('data-artboard-proxy-max-height'))
        }
        var el_topunits = t396_elem__getFieldValue(el, 'topunits');
        if (el_topunits == '%') {
            value = (el_container_height * (value / 100))
        }
        value = offset_top + value;
        var el_axisy = t396_elem__getFieldValue(el, 'axisy');
        if (el_axisy == 'center') {
            el_height = t396_elem__getHeight(el);
            value = el_container_height / 2 - el_height / 2 + value
        }
        if (el_axisy == 'bottom') {
            el_height = t396_elem__getHeight(el);
            value = el_container_height - el_height + value
        }
    }
    return (value)
}

function t396_ab__setFieldValue(ab, prop, val, res) {
    if (res == '') res = window.tn.curResolution;
    if (res < 1200) {
        ab.attr('data-artboard-' + prop + '-res-' + res, val)
    } else {
        ab.attr('data-artboard-' + prop, val)
    }
}

function t396_ab__getFieldValue(ab, prop) {
    var res = window.tn.curResolution;
    var r;
    if (res < 1200) {
        if (res == 960) {
            r = ab.attr('data-artboard-' + prop + '-res-960');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '')
            }
        }
        if (res == 640) {
            r = ab.attr('data-artboard-' + prop + '-res-640');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '-res-960');
                if (typeof r == 'undefined') {
                    r = ab.attr('data-artboard-' + prop + '')
                }
            }
        }
        if (res == 480) {
            r = ab.attr('data-artboard-' + prop + '-res-480');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '-res-640');
                if (typeof r == 'undefined') {
                    r = ab.attr('data-artboard-' + prop + '-res-960');
                    if (typeof r == 'undefined') {
                        r = ab.attr('data-artboard-' + prop + '')
                    }
                }
            }
        }
        if (res == 320) {
            r = ab.attr('data-artboard-' + prop + '-res-320');
            if (typeof r == 'undefined') {
                r = ab.attr('data-artboard-' + prop + '-res-480');
                if (typeof r == 'undefined') {
                    r = ab.attr('data-artboard-' + prop + '-res-640');
                    if (typeof r == 'undefined') {
                        r = ab.attr('data-artboard-' + prop + '-res-960');
                        if (typeof r == 'undefined') {
                            r = ab.attr('data-artboard-' + prop + '')
                        }
                    }
                }
            }
        }
    } else {
        r = ab.attr('data-artboard-' + prop)
    }
    return (r)
}

function t396_ab__renderViewOneField(ab, field) {
    var value = t396_ab__getFieldValue(ab, field)
}

function t396_allelems__renderView(ab) {
    tn_console('func: allelems__renderView: abid:' + ab.attr('data-artboard-recid'));
    ab.find(".tn-elem").each(function () {
        t396_elem__renderView($(this))
    })
}

function t396_ab__filterUpdate(ab) {
    var filter = ab.find('.t396__filter');
    var c1 = filter.attr('data-filtercolor-rgb');
    var c2 = filter.attr('data-filtercolor2-rgb');
    var o1 = filter.attr('data-filteropacity');
    var o2 = filter.attr('data-filteropacity2');
    if ((typeof c2 == 'undefined' || c2 == '') && (typeof c1 != 'undefined' && c1 != '')) {
        filter.css("background-color", "rgba(" + c1 + "," + o1 + ")")
    } else if ((typeof c1 == 'undefined' || c1 == '') && (typeof c2 != 'undefined' && c2 != '')) {
        filter.css("background-color", "rgba(" + c2 + "," + o2 + ")")
    } else if (typeof c1 != 'undefined' && typeof c2 != 'undefined' && c1 != '' && c2 != '') {
        filter.css({background: "-webkit-gradient(linear, left top, left bottom, from(rgba(" + c1 + "," + o1 + ")), to(rgba(" + c2 + "," + o2 + ")) )"})
    } else {
        filter.css("background-color", 'transparent')
    }
}

function t396_ab__getHeight(ab, ab_height) {
    if (typeof ab_height == 'undefined') ab_height = t396_ab__getFieldValue(ab, 'height');
    ab_height = parseFloat(ab_height);
    var ab_height_vh = t396_ab__getFieldValue(ab, 'height_vh');
    if (ab_height_vh != '') {
        ab_height_vh = parseFloat(ab_height_vh);
        if (isNaN(ab_height_vh) === !1) {
            var ab_height_vh_px = parseFloat(window.tn.window_height * parseFloat(ab_height_vh / 100));
            if (ab_height < ab_height_vh_px) {
                ab_height = ab_height_vh_px
            }
        }
    }
    return (ab_height)
}

function t396_hex2rgb(hexStr) {
    var hex = parseInt(hexStr.substring(1), 16);
    var r = (hex & 0xff0000) >> 16;
    var g = (hex & 0x00ff00) >> 8;
    var b = hex & 0x0000ff;
    return [r, g, b]
}

String.prototype.t396_replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement)
};

function t396_elem__getWidth(el, value) {
    if (typeof value == 'undefined') value = parseFloat(t396_elem__getFieldValue(el, 'width'));
    var el_widthunits = t396_elem__getFieldValue(el, 'widthunits');
    if (el_widthunits == '%') {
        var el_container = t396_elem__getFieldValue(el, 'container');
        if (el_container == 'window') {
            value = parseFloat(window.tn.window_width * parseFloat(parseInt(value) / 100))
        } else {
            value = parseFloat(window.tn.grid_width * parseFloat(parseInt(value) / 100))
        }
    }
    return (value)
}

function t396_elem__getHeight(el, value) {
    if (typeof value == 'undefined') value = t396_elem__getFieldValue(el, 'height');
    value = parseFloat(value);
    if (el.attr('data-elem-type') == 'shape' || el.attr('data-elem-type') == 'video' || el.attr('data-elem-type') == 'html') {
        var el_heightunits = t396_elem__getFieldValue(el, 'heightunits');
        if (el_heightunits == '%') {
            var ab = el.parent();
            var ab_min_height = parseFloat(ab.attr('data-artboard-proxy-min-height'));
            var ab_max_height = parseFloat(ab.attr('data-artboard-proxy-max-height'));
            var el_container = t396_elem__getFieldValue(el, 'container');
            if (el_container == 'window') {
                value = parseFloat(ab_max_height * parseFloat(value / 100))
            } else {
                value = parseFloat(ab_min_height * parseFloat(value / 100))
            }
        }
    } else if (el.attr('data-elem-type') == 'button') {
        value = value
    } else {
        value = parseFloat(el.innerHeight())
    }
    return (value)
}

function t396_roundFloat(n) {
    n = Math.round(n * 100) / 100;
    return (n)
}

function tn_console(str) {
    if (window.tn_comments == 1) console.log(str)
}

function t396_setUpTooltip_desktop(el, pinEl, tipEl) {
    var timer;
    pinEl.mouseover(function () {
        $('.tn-atom__tip_visible').each(function () {
            var thisTipEl = $(this).parents('.t396__elem');
            if (thisTipEl.attr('data-elem-id') != el.attr('data-elem-id')) {
                t396_hideTooltip(thisTipEl, $(this))
            }
        });
        clearTimeout(timer);
        if (tipEl.css('display') == 'block') {
            return
        }
        t396_showTooltip(el, tipEl)
    });
    pinEl.mouseout(function () {
        timer = setTimeout(function () {
            t396_hideTooltip(el, tipEl)
        }, 300)
    })
}

function t396_setUpTooltip_mobile(el, pinEl, tipEl) {
    pinEl.on('click', function (e) {
        if (tipEl.css('display') == 'block' && $(e.target).hasClass("tn-atom__pin")) {
            t396_hideTooltip(el, tipEl)
        } else {
            t396_showTooltip(el, tipEl)
        }
    });
    var id = el.attr("data-elem-id");
    $(document).click(function (e) {
        var isInsideTooltip = ($(e.target).hasClass("tn-atom__pin") || $(e.target).parents(".tn-atom__pin").length > 0);
        if (isInsideTooltip) {
            var clickedPinId = $(e.target).parents(".t396__elem").attr("data-elem-id");
            if (clickedPinId == id) {
                return
            }
        }
        t396_hideTooltip(el, tipEl)
    })
}

function t396_hideTooltip(el, tipEl) {
    tipEl.css('display', '');
    tipEl.css({"left": "", "transform": "", "right": ""});
    tipEl.removeClass('tn-atom__tip_visible');
    el.css('z-index', '')
}

function t396_showTooltip(el, tipEl) {
    var pos = el.attr("data-field-tipposition-value");
    if (typeof pos == 'undefined' || pos == '') {
        pos = 'top'
    }
    ;var elSize = el.height();
    var elTop = el.offset().top;
    var elBottom = elTop + elSize;
    var elLeft = el.offset().left;
    var elRight = el.offset().left + elSize;
    var winTop = $(window).scrollTop();
    var winWidth = $(window).width();
    var winBottom = winTop + $(window).height();
    var tipElHeight = tipEl.outerHeight();
    var tipElWidth = tipEl.outerWidth();
    var padd = 15;
    if (pos == 'right' || pos == 'left') {
        var tipElRight = elRight + padd + tipElWidth;
        var tipElLeft = elLeft - padd - tipElWidth;
        if ((pos == 'right' && tipElRight > winWidth) || (pos == 'left' && tipElLeft < 0)) {
            pos = 'top'
        }
    }
    if (pos == 'top' || pos == 'bottom') {
        var tipElRight = elRight + (tipElWidth / 2 - elSize / 2);
        var tipElLeft = elLeft - (tipElWidth / 2 - elSize / 2);
        if (tipElRight > winWidth) {
            var rightOffset = -(winWidth - elRight - padd);
            tipEl.css({"left": "auto", "transform": "none", "right": rightOffset + "px"})
        }
        if (tipElLeft < 0) {
            var leftOffset = -(elLeft - padd);
            tipEl.css({"left": leftOffset + "px", "transform": "none"})
        }
    }
    if (pos == 'top') {
        var tipElTop = elTop - padd - tipElHeight;
        if (winTop > tipElTop) {
            pos = 'bottom'
        }
    }
    if (pos == 'bottom') {
        var tipElBottom = elBottom + padd + tipElHeight;
        if (winBottom < tipElBottom) {
            pos = 'top'
        }
    }
    tipEl.attr('data-tip-pos', pos);
    tipEl.css('display', 'block');
    tipEl.addClass('tn-atom__tip_visible');
    el.css('z-index', '1000')
}

function t412_unifyHeights(recid) {
    var el = $("#rec" + recid);
    el.find('.t412__descr').css('height', "auto");
    $('#rec' + recid + ' .t412 .t-container').each(function () {
        var highestBox2 = 0;
        $('.t412__descr', this).each(function () {
            if ($(this).height() > highestBox2) highestBox2 = $(this).height()
        });
        if ($(window).width() >= 960 && $(this).is(':visible')) {
            $('.t412__descr', this).css('height', highestBox2)
        } else {
            $('.t412__descr', this).css('height', "auto")
        }
    });
    el.find('.t412__title').css('height', "auto");
    $('#rec' + recid + ' .t412 .t-container').each(function () {
        var highestBox3 = 0;
        $('.t412__title', this).each(function () {
            if ($(this).height() > highestBox3) highestBox3 = $(this).height()
        });
        if ($(window).width() >= 960 && $(this).is(':visible')) {
            $('.t412__title', this).css('height', highestBox3)
        } else {
            $('.t412__title', this).css('height', "auto")
        }
    });
    el.find('.t412__wrapper').css('height', "auto");
    $('#rec' + recid + ' .t412 .t-container').each(function () {
        var highestBox = 0;
        $('.t412__wrapper', this).each(function () {
            if ($(this).height() > highestBox) highestBox = $(this).height()
        });
        if ($(window).width() >= 960 && $(this).is(':visible')) {
            $('.t412__wrapper', this).css('height', highestBox)
        } else {
            $('.t412__wrapper', this).css('height', "auto")
        }
    })
}

function t452_scrollToTop() {
    $('html, body').animate({scrollTop: 0}, 700)
}

function t454_setLogoPadding(recid) {
    if ($(window).width() > 980) {
        var t454__menu = $('#rec' + recid + ' .t454');
        var t454__logo = t454__menu.find('.t454__logowrapper');
        var t454__leftpart = t454__menu.find('.t454__leftwrapper');
        var t454__rightpart = t454__menu.find('.t454__rightwrapper');
        t454__leftpart.css("padding-right", t454__logo.width() / 2 + 50);
        t454__rightpart.css("padding-left", t454__logo.width() / 2 + 50)
    }
}

function t454_highlight() {
    var url = window.location.href;
    var pathname = window.location.pathname;
    if (url.substr(url.length - 1) == "/") {
        url = url.slice(0, -1)
    }
    if (pathname.substr(pathname.length - 1) == "/") {
        pathname = pathname.slice(0, -1)
    }
    if (pathname.charAt(0) == "/") {
        pathname = pathname.slice(1)
    }
    if (pathname == "") {
        pathname = "/"
    }
    $(".t454__list_item a[href='" + url + "']").addClass("t-active");
    $(".t454__list_item a[href='" + url + "/']").addClass("t-active");
    $(".t454__list_item a[href='" + pathname + "']").addClass("t-active");
    $(".t454__list_item a[href='/" + pathname + "']").addClass("t-active");
    $(".t454__list_item a[href='" + pathname + "/']").addClass("t-active");
    $(".t454__list_item a[href='/" + pathname + "/']").addClass("t-active")
}

function t454_checkAnchorLinks(recid) {
    if ($(window).width() >= 960) {
        var t454_navLinks = $("#rec" + recid + " .t454__list_item a:not(.tooltipstered)[href*='#']");
        if (t454_navLinks.length > 0) {
            t454_catchScroll(t454_navLinks)
        }
    }
}

function t454_catchScroll(t454_navLinks) {
    var t454_clickedSectionId = null, t454_sections = new Array(), t454_sectionIdTonavigationLink = [],
        t454_interval = 100, t454_lastCall, t454_timeoutId;
    t454_navLinks = $(t454_navLinks.get().reverse());
    t454_navLinks.each(function () {
        var t454_cursection = t454_getSectionByHref($(this));
        if (typeof t454_cursection.attr("id") != "undefined") {
            t454_sections.push(t454_cursection)
        }
        t454_sectionIdTonavigationLink[t454_cursection.attr("id")] = $(this)
    });
    t454_updateSectionsOffsets(t454_sections);
    $(window).bind('resize', t_throttle(function () {
        t454_updateSectionsOffsets(t454_sections)
    }, 200));
    $('.t454').bind('displayChanged', function () {
        t454_updateSectionsOffsets(t454_sections)
    });
    setInterval(function () {
        t454_updateSectionsOffsets(t454_sections)
    }, 5000);
    t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId);
    t454_navLinks.click(function () {
        if (!$(this).hasClass("tooltipstered")) {
            t454_navLinks.removeClass('t-active');
            t454_sectionIdTonavigationLink[t454_getSectionByHref($(this)).attr("id")].addClass('t-active');
            t454_clickedSectionId = t454_getSectionByHref($(this)).attr("id")
        }
    });
    $(window).scroll(function () {
        var t454_now = new Date().getTime();
        if (t454_lastCall && t454_now < (t454_lastCall + t454_interval)) {
            clearTimeout(t454_timeoutId);
            t454_timeoutId = setTimeout(function () {
                t454_lastCall = t454_now;
                t454_clickedSectionId = t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId)
            }, t454_interval - (t454_now - t454_lastCall))
        } else {
            t454_lastCall = t454_now;
            t454_clickedSectionId = t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId)
        }
    })
}

function t454_updateSectionsOffsets(sections) {
    $(sections).each(function () {
        var t454_curSection = $(this);
        t454_curSection.attr("data-offset-top", t454_curSection.offset().top)
    })
}

function t454_getSectionByHref(curlink) {
    var t454_curLinkValue = curlink.attr("href").replace(/\s+/g, '');
    if (curlink.is('[href*="#rec"]')) {
        return $(".r[id='" + t454_curLinkValue.substring(1) + "']")
    } else {
        return $(".r[data-record-type='215']").has("a[name='" + t454_curLinkValue.substring(1) + "']")
    }
}

function t454_highlightNavLinks(t454_navLinks, t454_sections, t454_sectionIdTonavigationLink, t454_clickedSectionId) {
    var t454_scrollPosition = $(window).scrollTop(), t454_valueToReturn = t454_clickedSectionId;
    if (t454_sections.length != 0 && t454_clickedSectionId == null && t454_sections[t454_sections.length - 1].attr("data-offset-top") > (t454_scrollPosition + 300)) {
        t454_navLinks.removeClass('t-active');
        return null
    }
    $(t454_sections).each(function (e) {
        var t454_curSection = $(this), t454_sectionTop = t454_curSection.attr("data-offset-top"),
            t454_id = t454_curSection.attr('id'), t454_navLink = t454_sectionIdTonavigationLink[t454_id];
        if (((t454_scrollPosition + 300) >= t454_sectionTop) || (t454_sections[0].attr("id") == t454_id && t454_scrollPosition >= $(document).height() - $(window).height())) {
            if (t454_clickedSectionId == null && !t454_navLink.hasClass('t-active')) {
                t454_navLinks.removeClass('t-active');
                t454_navLink.addClass('t-active');
                t454_valueToReturn = null
            } else {
                if (t454_clickedSectionId != null && t454_id == t454_clickedSectionId) {
                    t454_valueToReturn = null
                }
            }
            return !1
        }
    });
    return t454_valueToReturn
}

function t454_setPath() {
}

function t454_setBg(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t454").each(function () {
            var el = $(this);
            if (el.attr('data-bgcolor-setbyscript') == "yes") {
                var bgcolor = el.attr("data-bgcolor-rgba");
                el.css("background-color", bgcolor)
            }
        })
    } else {
        $(".t454").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-hex");
            el.css("background-color", bgcolor);
            el.attr("data-bgcolor-setbyscript", "yes")
        })
    }
}

function t454_appearMenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t454").each(function () {
            var el = $(this);
            var appearoffset = el.attr("data-appearoffset");
            if (appearoffset != "") {
                if (appearoffset.indexOf('vh') > -1) {
                    appearoffset = Math.floor((window.innerHeight * (parseInt(appearoffset) / 100)))
                }
                appearoffset = parseInt(appearoffset, 10);
                if ($(window).scrollTop() >= appearoffset) {
                    if (el.css('visibility') == 'hidden') {
                        el.finish();
                        el.css("top", "-80px");
                        el.css("visibility", "visible");
                        el.animate({"opacity": "1", "top": "0px"}, 200, function () {
                        })
                    }
                } else {
                    el.stop();
                    el.css("visibility", "hidden")
                }
            }
        })
    }
}

function t454_changebgopacitymenu(recid) {
    var window_width = $(window).width();
    if (window_width > 980) {
        $(".t454").each(function () {
            var el = $(this);
            var bgcolor = el.attr("data-bgcolor-rgba");
            var bgcolor_afterscroll = el.attr("data-bgcolor-rgba-afterscroll");
            var bgopacityone = el.attr("data-bgopacity");
            var bgopacitytwo = el.attr("data-bgopacity-two");
            var menushadow = el.attr("data-menushadow");
            if (menushadow == '100') {
                var menushadowvalue = menushadow
            } else {
                var menushadowvalue = '0.' + menushadow
            }
            if ($(window).scrollTop() > 20) {
                el.css("background-color", bgcolor_afterscroll);
                if (bgopacitytwo == '0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            } else {
                el.css("background-color", bgcolor);
                if (bgopacityone == '0.0' || menushadow == ' ') {
                    el.css("box-shadow", "none")
                } else {
                    el.css("box-shadow", "0px 1px 3px rgba(0,0,0," + menushadowvalue + ")")
                }
            }
        })
    }
}

function t454_createMobileMenu(recid) {
    var window_width = $(window).width(), el = $("#rec" + recid), menu = el.find(".t454"),
        burger = el.find(".t454__mobile");
    burger.click(function (e) {
        menu.fadeToggle(300);
        $(this).toggleClass("t454_opened")
    })
    $(window).bind('resize', t_throttle(function () {
        window_width = $(window).width();
        if (window_width > 980) {
            menu.fadeIn(0)
        }
    }, 200))
}

function t477_setHeight(recid) {
    var el = $('#rec' + recid);
    el.find('.t-container').each(function () {
        var highestBox = 0;
        $('.t477__col', this).each(function () {
            if ($(this).height() > highestBox) highestBox = $(this).height()
        });
        $('.t477__textwrapper', this).css('height', highestBox);
        $('.t477__blockimg', this).css('height', highestBox)
    })
}

function t480_setHeight(recid) {
    var el = $('#rec' + recid);
    var sizer = el.find('.t480__sizer');
    var height = sizer.height();
    var width = sizer.width();
    var ratio = width / height;
    var imgwrapper = el.find(".t480__blockimg, .t480__textwrapper");
    var imgwidth = imgwrapper.width();
    if (height != $(window).height()) {
        imgwrapper.css({'height': ((imgwidth / ratio) + 'px')})
    }
}


jQuery(document).ready(function ($) {
    t228_highlight();
});
jQuery(window).resize(function () {
    t228_setBg('101333342');
});
jQuery(document).ready(function ($) {
    t228_setBg('101333342');
});
jQuery(document).ready(function ($) {
    t228_createMobileMenu('101333342');
});
jQuery(document).ready(function ($) {
});
jQuery(document).ready(function ($) {
});
jQuery(document).ready(function ($) {
    setTimeout(function () {
        var $root = $('html, body');
        $('a[href*="#"]:not([href="#"],.carousel-control,.t-carousel__control,[href^="#price"],[href^="#popup"],[href^="#prodpopup"],[href^="#order"])').click(function () {
            var target = $(this.hash);
            if (target.length == 0) {
                target = $('a[name="' + this.hash.substr(1) + '"]');
            }
            if (target.length == 0) {
                return true;
            }
            $root.animate({scrollTop: target.offset().top + 3}, 500);
            return false;
        });
    }, 500);
});
jQuery(document).ready(function ($) {
    t412_unifyHeights('101334179');
    setTimeout(function () {
        t412_unifyHeights('101334179');
    }, 500);
});
jQuery('.t412').bind('displayChanged', function () {
    t412_unifyHeights('101334179');
});
jQuery(window).resize(function () {
    t412_unifyHeights('101334179');
});
jQuery(window).load(function () {
    t412_unifyHeights('101334179');
});
jQuery(document).ready(function ($) {
    t396_init('101334323');
});
jQuery(document).ready(function ($) {
    t396_init('101334444');
});
jQuery(document).ready(function ($) {
    t396_init('101334528');
});
jQuery(document).ready(function ($) {
    t396_init('101334601');
});
jQuery(document).ready(function ($) {
    t396_init('101334698');
});
jQuery(document).ready(function ($) {
    t396_init('101336270');
});
jQuery(document).ready(function ($) {
    //t_sldsInit( '101336447' );
});
jQuery(document).ready(function ($) {
    /*$( '.t605' ).bind( 'displayChanged', function () {
		t_slds_updateSlider( '101336447' );
	} );*/
});

function t_reviews_social_init(recid) {
    var rec = $('#rec' + recid);
    var social = rec.find('.t-review-social-links');
    if (social !== 'undefined' && social.length > 0) {
        social.each(function () {
            var $this = $(this);
            var socialAttr = $this.attr('data-social-links');
            var socialWrapper = $this.find('.t-review-social-links__wrapper');
            var links;
            $this.removeAttr('data-social-links');
            if (socialAttr !== 'undefined') {
                links = socialAttr.split(',');
                links.forEach(function (item) {
                    t_reviews_social_add_item(item, socialWrapper);
                });
            }
        });
    }
}

function t_reviews_social_add_item(item, socialWrapper) {
    var fill = socialWrapper.attr('data-social-color') || '#000000';
    var fb = '';
    fb = '<div class="t-review-social-links__item">';
    fb += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        fb += '<svg class="t-review-social-links__svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="30px" height="30px" viewBox="0 0 48 48" enable-background="new 0 0 48 48" xml:space="preserve" fill="' + fill + '"><desc>Facebook</desc><path d="M47.761,24c0,13.121-10.638,23.76-23.758,23.76C10.877,47.76,0.239,37.121,0.239,24c0-13.124,10.638-23.76,23.764-23.76C37.123,0.24,47.761,10.876,47.761,24 M20.033,38.85H26.2V24.01h4.163l0.539-5.242H26.2v-3.083c0-1.156,0.769-1.427,1.308-1.427h3.318V9.168L26.258,9.15c-5.072,0-6.225,3.796-6.225,6.224v3.394H17.1v5.242h2.933V38.85z"></path></svg>';
    } else {
        fb += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M21.1 7.8C22.5 6.5 24.5 6 26.4 6h6v6.3h-3.9c-.8-.1-1.6.6-1.8 1.4v4.2h5.7c-.1 2-.4 4.1-.7 6.1h-5v18h-7.4V24h-3.6v-6h3.6v-5.9c.1-1.7.7-3.3 1.8-4.3z"/></svg>';
    }
    fb += '</a>';
    fb += '</div>';
    var twi = '';
    twi += '<div class="t-review-social-links__item">';
    twi += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        twi += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 48 48" fill="' + fill + '"><path d="M47.762 24c0 13.121-10.639 23.76-23.761 23.76S.24 37.121.24 24C.24 10.876 10.879.24 24.001.24S47.762 10.876 47.762 24m-9.731-11.625a12.148 12.148 0 0 1-3.87 1.481 6.08 6.08 0 0 0-4.455-1.926 6.095 6.095 0 0 0-6.093 6.095c0 .478.054.941.156 1.388a17.302 17.302 0 0 1-12.559-6.367 6.066 6.066 0 0 0-.825 3.064 6.088 6.088 0 0 0 2.711 5.07 6.075 6.075 0 0 1-2.761-.762v.077c0 2.951 2.1 5.414 4.889 5.975a6.079 6.079 0 0 1-2.752.106 6.104 6.104 0 0 0 5.692 4.232 12.226 12.226 0 0 1-7.567 2.607c-.492 0-.977-.027-1.453-.084a17.241 17.241 0 0 0 9.34 2.736c11.209 0 17.337-9.283 17.337-17.337 0-.263-.004-.527-.017-.789a12.358 12.358 0 0 0 3.039-3.152 12.138 12.138 0 0 1-3.498.958 6.089 6.089 0 0 0 2.686-3.372"/></svg>';
    } else {
        twi += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M41.8 12.7c-1.3.6-2.8 1-4.2 1.1 1.5-1 2.6-2.3 3.2-4-1.5.8-2.9 1.5-4.7 1.8-1.3-1.5-3.2-2.3-5.3-2.3-4 0-7.3 3.2-7.3 7.3 0 .6 0 1.1.2 1.6-6-.3-11.3-3.2-15.1-7.6-.6 1.1-1 2.3-1 3.7 0 2.6 1.3 4.7 3.2 6-1.1 0-2.3-.3-3.2-1v.2c0 3.6 2.4 6.5 5.8 7.1-.6.2-1.3.3-1.9.3-.5 0-1 0-1.3-.2 1 2.9 3.6 5 6.8 5-2.4 1.9-5.7 3.1-9.1 3.1-.6 0-1.1 0-1.8-.2 3.2 2.1 7 3.2 11.2 3.2 13.4 0 20.7-11 20.7-20.7v-1c1.7-.7 2.8-2 3.8-3.4z"/></svg>';
    }
    twi += '</a>';
    twi += '</div>';
    var vk = '';
    vk += '<div class="t-review-social-links__item">';
    vk += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        vk += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 48 48" fill="' + fill + '"><path d="M47.761 24c0 13.121-10.639 23.76-23.76 23.76C10.878 47.76.239 37.121.239 24 .239 10.877 10.878.24 24.001.24c13.121 0 23.76 10.637 23.76 23.76m-12.502 4.999c-2.621-2.433-2.271-2.041.89-6.25 1.923-2.562 2.696-4.126 2.45-4.796-.227-.639-1.64-.469-1.64-.469l-4.71.029s-.351-.048-.609.106c-.249.151-.414.505-.414.505s-.742 1.982-1.734 3.669c-2.094 3.559-2.935 3.747-3.277 3.524-.796-.516-.597-2.068-.597-3.171 0-3.449.522-4.887-1.02-5.259-.511-.124-.887-.205-2.195-.219-1.678-.016-3.101.007-3.904.398-.536.263-.949.847-.697.88.31.041 1.016.192 1.388.699.484.656.464 2.131.464 2.131s.282 4.056-.646 4.561c-.632.347-1.503-.36-3.37-3.588-.958-1.652-1.68-3.481-1.68-3.481s-.14-.344-.392-.527c-.299-.222-.722-.298-.722-.298l-4.469.018s-.674-.003-.919.289c-.219.259-.018.752-.018.752s3.499 8.104 7.573 12.23c3.638 3.784 7.764 3.36 7.764 3.36h1.867s.566.113.854-.189c.265-.288.256-.646.256-.646s-.034-2.512 1.129-2.883c1.15-.36 2.624 2.429 4.188 3.497 1.182.812 2.079.633 2.079.633l4.181-.056s2.186-.136 1.149-1.858c-.197-.139-.715-1.269-3.219-3.591"/></svg>';
    } else {
        vk += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M41.2 22.2c.6-.8 1.1-1.5 1.5-2 2.7-3.5 3.8-5.8 3.5-6.8l-.2-.2c-.1-.1-.3-.3-.7-.4-.4-.1-.9-.1-1.5-.1h-7.2c-.2 0-.3 0-.3.1 0 0-.1 0-.1.1v.1c-.1 0-.2.1-.3.2-.1.1-.2.2-.2.4-.7 1.9-1.5 3.6-2.5 5.2-.6 1-1.1 1.8-1.6 2.5s-.9 1.2-1.2 1.5c-.3.3-.6.6-.9.8-.2.3-.4.4-.5.4-.1 0-.3-.1-.4-.1-.2-.1-.4-.3-.5-.6-.1-.2-.2-.5-.3-.9 0-.4-.1-.7-.1-.9v-4c0-.7 0-1.2.1-1.6v-1.3c0-.4 0-.8-.1-1.1-.1-.3-.1-.5-.2-.7-.1-.2-.3-.4-.5-.6-.2-.1-.5-.2-.8-.3-.8-.2-1.9-.3-3.1-.3-2.9 0-4.7.2-5.5.6-.3.2-.6.4-.9.7-.3.3-.3.5-.1.6.9.1 1.6.5 2 1l.1.3c.1.2.2.6.3 1.1.1.5.2 1.1.2 1.7.1 1.1.1 2.1 0 2.9-.1.8-.1 1.4-.2 1.9-.1.4-.2.8-.3 1.1-.1.3-.2.4-.3.5 0 .1-.1.1-.1.1-.1-.1-.4-.1-.6-.1-.2 0-.5-.1-.8-.3-.3-.2-.6-.5-1-.9-.3-.4-.7-.9-1.1-1.6-.4-.7-.8-1.5-1.3-2.4l-.4-.7c-.2-.4-.5-1.1-.9-1.9-.4-.8-.8-1.6-1.1-2.4-.1-.3-.3-.6-.6-.7l-.1-.1c-.1-.1-.2-.1-.4-.2s-.3-.1-.5-.2H3.2c-.6 0-1.1.1-1.3.4l-.1.1c0 .1-.1.2-.1.4s0 .4.1.6c.9 2.2 1.9 4.3 3 6.3s2 3.6 2.8 4.9c.8 1.2 1.6 2.4 2.4 3.5.8 1.1 1.4 1.8 1.7 2.1.3.3.5.5.6.7l.6.6c.4.4.9.8 1.6 1.3.7.5 1.5 1 2.4 1.5.9.5 1.9.9 3 1.2 1.2.3 2.3.4 3.4.4H26c.5 0 .9-.2 1.2-.5l.1-.1c.1-.1.1-.2.2-.4s.1-.4.1-.6c0-.7 0-1.3.1-1.8s.2-.9.4-1.2c.1-.3.3-.5.5-.7.2-.2.3-.3.4-.3.1 0 .1-.1.2-.1.4-.1.8 0 1.3.4s1 .8 1.4 1.3c.4.5 1 1.1 1.6 1.8.6.7 1.2 1.2 1.6 1.5l.5.3c.3.2.7.4 1.2.5.5.2.9.2 1.3.1l5.9-.1c.6 0 1-.1 1.4-.3.3-.2.5-.4.6-.6.1-.2.1-.5 0-.8-.1-.3-.1-.5-.2-.6-.1-.1-.1-.2-.2-.3-.8-1.4-2.2-3.1-4.4-5.1-1-.9-1.6-1.6-1.9-1.9-.5-.6-.6-1.2-.3-1.9.3-.5 1-1.5 2.2-3z"/></svg>';
    }
    vk += '</a>';
    vk += '</div>';
    var ok = '';
    ok += '<div class="t-review-social-links__item">';
    ok += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        ok += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="' + fill + '"><path d="M15.001 29.997C6.715 29.997 0 23.28 0 14.997 0 6.714 6.715 0 15.001 0 23.282-.001 30 6.713 30 14.998c0 8.283-6.716 14.999-14.998 14.999zM15 9.163c.986 0 1.787.803 1.787 1.787 0 .987-.801 1.788-1.787 1.788a1.788 1.788 0 0 1 0-3.574zm4.317 1.787a4.322 4.322 0 0 0-4.317-4.319 4.323 4.323 0 0 0-4.319 4.32A4.322 4.322 0 0 0 15 15.266a4.32 4.32 0 0 0 4.316-4.317zm-2.57 7.84a8.126 8.126 0 0 0 2.507-1.038 1.266 1.266 0 0 0-1.347-2.143 5.482 5.482 0 0 1-5.816 0 1.265 1.265 0 0 0-1.348 2.143c.78.49 1.628.838 2.507 1.038l-2.414 2.414a1.267 1.267 0 0 0 .894 2.16c.324 0 .649-.125.895-.37l2.371-2.373 2.373 2.373a1.265 1.265 0 0 0 1.788-1.789l-2.41-2.415z"/></svg>';
    } else {
        ok += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="' + fill + '"><path d="M12.498 6.2a1.932 1.932 0 0 0-1.929 1.93 1.932 1.932 0 0 0 3.861 0 1.932 1.932 0 0 0-1.932-1.93zm0 6.592a4.668 4.668 0 0 1-4.66-4.662 4.665 4.665 0 0 1 4.66-4.663 4.666 4.666 0 0 1 4.664 4.663 4.669 4.669 0 0 1-4.664 4.662zm-1.884 3.801a8.75 8.75 0 0 1-2.708-1.12 1.367 1.367 0 1 1 1.455-2.312 5.925 5.925 0 0 0 6.276 0 1.367 1.367 0 1 1 1.456 2.311c-.832.522-1.749.9-2.707 1.12l2.606 2.608a1.367 1.367 0 0 1-1.933 1.931L12.5 18.569l-2.561 2.562a1.368 1.368 0 0 1-1.934-1.934l2.609-2.604z"/></svg>';
    }
    ok += '</a>';
    ok += '</div>';
    var behance = '';
    behance += '<div class="t-review-social-links__item">';
    behance += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        behance += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="-455 257 48 48" fill="' + fill + '"><path d="M-421.541 278.608c-3.095 0-3.526 3.09-3.526 3.09h6.581c0-.001.042-3.09-3.055-3.09zM-436.472 281.697h-5.834v5.356h5.165c.088 0 .219.002.374 0 .831-.02 2.406-.261 2.406-2.604-.001-2.78-2.111-2.752-2.111-2.752z"/><path d="M-430.984 257C-444.248 257-455 267.75-455 281.014c0 13.263 10.752 24.016 24.016 24.016 13.262 0 24.014-10.752 24.014-24.016-.001-13.263-10.754-24.014-24.014-24.014zm5.054 14.756h8.267v2.467h-8.267v-2.467zm-4.179 12.943c0 6.116-6.364 5.914-6.364 5.914h-10.428v-20.038h10.428c3.168 0 5.668 1.75 5.668 5.335s-3.057 3.813-3.057 3.813c4.031 0 3.753 4.976 3.753 4.976zm15.466-.328h-10.384c0 3.722 3.526 3.487 3.526 3.487 3.329 0 3.213-2.156 3.213-2.156h3.527c0 5.722-6.859 5.33-6.859 5.33-8.227 0-7.698-7.661-7.698-7.661s-.008-7.698 7.698-7.698c8.112.001 6.977 8.698 6.977 8.698z"/><path d="M-435.055 276.221c0-2.084-1.417-2.084-1.417-2.084H-442.306v4.471h5.472c.944 0 1.779-.304 1.779-2.387z"/></svg>';
    } else {
        behance += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="' + fill + '"><path d="M22.4 7.5h-5.6V6.1h5.6v1.4zm-9.3 5.7c.4.6.5 1.2.5 2s-.2 1.6-.6 2.2c-.3.4-.6.8-1 1.1-.4.3-1 .6-1.6.7-.6.1-1.3.2-2 .2H2.3V5.6H9c1.7 0 2.9.5 3.6 1.5.4.6.6 1.3.6 2.1s-.2 1.5-.6 2c-.2.3-.6.6-1.1.8.7.2 1.2.7 1.6 1.2zM5.5 11h2.9c.6 0 1.1-.1 1.5-.3.4-.2.6-.6.6-1.2s-.2-1.1-.7-1.3C9.3 8.1 8.7 8 8.1 8H5.5v3zm5.2 4c0-.7-.3-1.2-.9-1.5-.3-.2-.8-.2-1.4-.2h-3V17h2.9c.6 0 1.1-.1 1.4-.2.7-.3 1-.9 1-1.8zm13.8-2.2c.1.5.1 1.1.1 2h-7.2c0 1 .4 1.7 1 2.1.4.3.9.4 1.4.4.6 0 1.1-.1 1.4-.5.2-.2.4-.4.5-.7h2.6c-.1.6-.4 1.2-1 1.8-.9 1-2.1 1.4-3.7 1.4-1.3 0-2.5-.4-3.5-1.2-1-.8-1.5-2.1-1.5-4 0-1.7.5-3 1.4-4 .9-.9 2.1-1.4 3.5-1.4.9 0 1.6.2 2.3.5.7.3 1.3.8 1.7 1.5.6.6.8 1.3 1 2.1zm-2.7.2c0-.7-.3-1.2-.7-1.6-.4-.4-.9-.5-1.5-.5-.7 0-1.2.2-1.5.6-.4.4-.6.9-.7 1.5h4.4z"/></svg>';
    }
    behance += '</a>';
    behance += '</div>';
    var vimeo = '';
    vimeo += '<div class="t-review-social-links__item">';
    vimeo += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        vimeo += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="-455 257 48 48" fill="' + fill + '"><path d="M-431 256.971c13.267 0 24.023 10.755 24.023 24.023s-10.755 24.023-24.023 24.023-24.023-10.755-24.023-24.023 10.756-24.023 24.023-24.023zm-12.262 19.146l.996 1.306s2.054-1.619 2.739-.81c.685.81 3.299 10.584 4.171 12.387.761 1.581 2.862 3.672 5.166 2.179 2.302-1.493 9.959-8.03 11.329-15.749 1.369-7.717-9.213-6.1-10.335.623 2.802-1.682 4.297.683 2.863 3.362-1.431 2.676-2.739 4.421-3.424 4.421-.683 0-1.209-1.791-1.992-4.92-.81-3.236-.804-9.064-4.17-8.403-3.171.623-7.343 5.604-7.343 5.604z"/></svg>';
    } else {
        vimeo += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M2.9 16.5l1.6 2s3.2-2.5 4.3-1.3c1.1 1.3 5.2 16.5 6.5 19.3 1.2 2.5 4.5 5.7 8.1 3.4S39 27.4 41.1 15.3s-14.4-9.5-16.1 1c4.4-2.6 6.7 1.1 4.5 5.3s-4.3 6.9-5.3 6.9c-1.1 0-1.9-2.8-3.1-7.7-1.3-5.1-1.3-14.2-6.5-13.1-5.2 1-11.7 8.8-11.7 8.8z"/></svg>';
    }
    vimeo += '</a>';
    vimeo += '</div>';
    var youtube = '';
    youtube += '<div class="t-review-social-links__item">';
    youtube += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        youtube += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="-455 257 48 48" fill="' + fill + '"><path d="M-431 257.013c13.248 0 23.987 10.74 23.987 23.987s-10.74 23.987-23.987 23.987-23.987-10.74-23.987-23.987 10.739-23.987 23.987-23.987zm11.815 18.08c-.25-1.337-1.363-2.335-2.642-2.458-3.054-.196-6.119-.355-9.178-.357-3.059-.002-6.113.154-9.167.347-1.284.124-2.397 1.117-2.646 2.459a40.163 40.163 0 0 0 0 11.672c.249 1.342 1.362 2.454 2.646 2.577 3.055.193 6.107.39 9.167.39 3.058 0 6.126-.172 9.178-.37 1.279-.124 2.392-1.269 2.642-2.606a39.769 39.769 0 0 0 0-11.654zm-14.591 9.342v-7.115l6.627 3.558-6.627 3.557z"/></svg>';
    } else {
        youtube += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M43.9 15.3c-.4-3.1-2.2-5-5.3-5.3-3.6-.3-11.4-.5-15-.5-7.3 0-10.6.2-14.1.5-3.3.3-4.8 1.8-5.4 4.9-.4 2.1-.6 4.1-.6 8.9 0 4.3.2 6.9.5 9.2.4 3.1 2.5 4.8 5.7 5.1 3.6.3 10.9.5 14.4.5s11.2-.2 14.7-.6c3.1-.4 4.6-2 5.1-5.1 0 0 .5-3.3.5-9.1 0-3.3-.2-6.4-.5-8.5zM19.7 29.8V18l11.2 5.8-11.2 6z"/></svg>';
    }
    youtube += '</a>';
    youtube += '</div>';
    var instagram = '';
    instagram += '<div class="t-review-social-links__item">';
    instagram += ' <a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        instagram += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="' + fill + '"><path d="M15 11.014a3.986 3.986 0 1 0 .001 7.971A3.986 3.986 0 0 0 15 11.014zm0 6.592A2.604 2.604 0 0 1 12.393 15 2.604 2.604 0 0 1 15 12.394 2.614 2.614 0 0 1 17.607 15 2.604 2.604 0 0 1 15 17.606zM19.385 9.556a.915.915 0 0 0-.92.921c0 .512.407.919.92.919a.914.914 0 0 0 .921-.919.916.916 0 0 0-.921-.921z"/><path d="M15.002.15C6.798.15.149 6.797.149 15c0 8.201 6.649 14.85 14.853 14.85 8.199 0 14.85-6.648 14.85-14.85 0-8.203-6.651-14.85-14.85-14.85zm7.664 18.115c0 2.423-1.979 4.401-4.416 4.401h-6.5c-2.438 0-4.417-1.979-4.417-4.386v-6.546c0-2.422 1.978-4.4 4.417-4.4h6.5c2.438 0 4.416 1.978 4.416 4.4v6.531z"/></svg>';
    } else {
        instagram += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25" fill="' + fill + '"><path d="M16.396 3.312H8.604c-2.921 0-5.292 2.371-5.292 5.273v7.846c0 2.886 2.371 5.256 5.292 5.256h7.791c2.922 0 5.292-2.37 5.292-5.274V8.586c.001-2.903-2.37-5.274-5.291-5.274zM7.722 12.5a4.778 4.778 0 1 1 9.554 0 4.778 4.778 0 0 1-9.554 0zm10.034-4.318c-.615 0-1.104-.487-1.104-1.102s.488-1.103 1.104-1.103c.614 0 1.102.488 1.102 1.103s-.488 1.102-1.102 1.102z" /><path d="M12.5 9.376A3.12 3.12 0 0 0 9.376 12.5a3.12 3.12 0 0 0 3.124 3.124 3.12 3.12 0 0 0 3.124-3.124A3.133 3.133 0 0 0 12.5 9.376z"/></svg>';
    }
    instagram += '</a>';
    instagram += '</div>';
    var pinterest = '';
    pinterest += '<div class="t-review-social-links__item">';
    pinterest += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        pinterest += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="-455 257 48 48" fill="' + fill + '"><path d="M-407 281c0 13.254-10.746 24-23.999 24-13.257 0-24.003-10.746-24.003-24 0-13.256 10.746-24 24.003-24 13.253 0 23.999 10.744 23.999 24zm-30.628 14.811c.027.205.29.254.409.1.17-.223 2.365-2.932 3.111-5.639.211-.768 1.212-4.738 1.212-4.738.599 1.145 2.349 2.148 4.21 2.148 5.539 0 9.297-5.049 9.297-11.809 0-5.111-4.329-9.873-10.909-9.873-8.186 0-12.314 5.871-12.314 10.766 0 2.963 1.122 5.6 3.527 6.582.395.162.749.006.863-.43.08-.303.268-1.065.353-1.385.114-.432.07-.582-.248-.959-.694-.818-1.138-1.879-1.138-3.381 0-4.353 3.259-8.252 8.484-8.252 4.627 0 7.169 2.828 7.169 6.603 0 4.969-2.198 9.162-5.461 9.162-1.804 0-3.153-1.49-2.722-3.32.518-2.182 1.522-4.537 1.522-6.113 0-1.41-.758-2.588-2.324-2.588-1.843 0-3.323 1.908-3.323 4.461 0 1.627.55 2.727.55 2.727l-2.217 9.391c-.657 2.787-.099 6.203-.051 6.547z"/></svg>';
    } else {
        pinterest += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M24.9 5.5c4.2 0 7.7 1.3 10.5 3.9s4.1 5.7 4.1 9.2c0 4.6-1.2 8.4-3.5 11.3-2.3 3-5.3 4.5-8.9 4.5-1.2 0-2.3-.3-3.4-.9s-1.8-1.3-2.2-2c-.9 3.5-1.4 5.6-1.6 6.3-.7 2.4-2 4.9-4.1 7.5-.2.2-.4.2-.5-.1-.5-3.5-.5-6.4.1-8.7l2.9-12.6c-.5-.9-.7-2.1-.7-3.6 0-1.7.4-3.1 1.3-4.2s1.9-1.7 3.2-1.7c1 0 1.8.3 2.3 1 .5.6.8 1.5.8 2.4 0 .6-.1 1.4-.3 2.3-.2.9-.5 1.9-.9 3.1s-.6 2.1-.8 2.8c-.3 1.2-.1 2.2.7 3.1.7.9 1.7 1.3 3 1.3 2.1 0 3.8-1.2 5.2-3.6 1.4-2.4 2-5.3 2-8.7 0-2.6-.8-4.7-2.5-6.3-1.7-1.6-4-2.4-7-2.4-3.4 0-6.1 1.1-8.2 3.2S13 17.4 13 20.4c0 1.8.5 3.3 1.5 4.5.3.4.4.8.3 1.2 0 .1-.1.3-.2.6-.1.4-.1.6-.2.7 0 .1-.1.4-.2.6-.1.3-.3.4-.4.4h-.6c-3.1-1.3-4.7-4.2-4.7-8.7 0-3.6 1.5-6.9 4.4-9.9s7-4.3 12-4.3z"/></svg>';
    }
    pinterest += '</a>';
    pinterest += '</div>';
    var linkedin = '';
    linkedin += '<div class="t-review-social-links__item">';
    linkedin += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        linkedin += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="-615 1477 48 48" fill="' + fill + '"><path d="M-566.999 1501c0 13.256-10.746 24-24 24-13.256 0-24.002-10.744-24.002-24 0-13.254 10.746-24 24.002-24 13.254 0 24 10.746 24 24zm-38.507 13.975h6.22v-20.004h-6.22v20.004zm3.11-22.739a3.606 3.606 0 0 0 0-7.211 3.603 3.603 0 0 0-3.604 3.605 3.604 3.604 0 0 0 3.604 3.606zm26.396 11.766c0-5.387-1.163-9.529-7.454-9.529-3.023 0-5.054 1.658-5.884 3.231h-.085v-2.733h-5.964v20.004h6.216v-9.896c0-2.609.493-5.137 3.729-5.137 3.186 0 3.232 2.984 3.232 5.305v9.729h6.21v-10.974z"/></svg>';
    } else {
        linkedin += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M42.3 23.1v13.5h-7.9V24c0-3.2-1.1-5.3-4-5.3-2.2 0-3.5 1.5-4 2.9-.2.5-.3 1.2-.3 1.9v13.2h-7.9s.1-21.4 0-23.6h7.9v3.3s0 .1-.1.1h.1v-.1c1-1.6 2.9-3.9 7.1-3.9 5.2-.1 9.1 3.3 9.1 10.6zM10.1 1.6c-2.7 0-4.4 1.8-4.4 4.1S7.4 9.8 10 9.8h.1c2.7 0 4.4-1.8 4.4-4.1s-1.7-4.1-4.4-4.1zm-3.9 35H14V13H6.2v23.6z"/></svg>';
    }
    linkedin += '</a>';
    linkedin += '</div>';
    var soundcloud = '';
    soundcloud += '<div class="t-review-social-links__item">';
    soundcloud += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        soundcloud += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="30" viewBox="0 0 48 48" fill="' + fill + '"><defs><path id="a" d="M0 0h48v48H0V0z"/></defs><g fill-rule="evenodd"><path d="M38.206 22.171c-.635 0-1.241.129-1.793.36-.369-4.177-3.871-7.453-8.144-7.453a8.28 8.28 0 0 0-2.965.553c-.35.136-.442.275-.446.546v14.709a.545.545 0 0 0 .5.531c.013.002 12.848 0 12.848 0 2.56 0 4.634-2.051 4.634-4.611a4.634 4.634 0 0 0-4.634-4.635zM24.17 16.237a.5.5 0 0 0-.582.043.517.517 0 0 0-.189.392v.053l-.21 10.388.107 1.92.103 1.869a.514.514 0 0 0 .51.505.515.515 0 0 0 .51-.509v.004l.228-3.79-.227-10.44a.517.517 0 0 0-.25-.435zm-1.364 14.72v-.001.001-.002.002zm-.215-13.813a.47.47 0 0 0-.264-.081.486.486 0 0 0-.478.477l-.001.09-.18 9.48.181 3.847.001.016a.48.48 0 0 0 .477.458c.246 0 .248-.054.335-.14a.486.486 0 0 0 .144-.336l.02-.38.181-3.463-.201-9.573a.487.487 0 0 0-.215-.395zm-1.853 1.479a.447.447 0 0 0-.446.444l-.2 8.044.2 3.87c.005.465.2.442.446.442.245 0 .44.023.446-.444v.003l.223-3.87-.223-8.046a.447.447 0 0 0-.446-.443zm-1.577-.273a.416.416 0 0 0-.414.412l-.219 8.349.219 3.895c.005.228.187.41.414.41a.416.416 0 0 0 .413-.412v.002l.246-3.895-.246-8.35a.416.416 0 0 0-.413-.411zm-1.184 12.693v-.002.002zm0-12.5a.385.385 0 0 0-.382-.38.384.384 0 0 0-.381.38l-.238 8.567.238 3.932c.006.21.173.377.381.377a.385.385 0 0 0 .382-.378l.268-3.931-.268-8.568zm-1.935-.069a.354.354 0 0 0-.35.347l-.256 8.288.257 3.957c.006.193.16.346.35.346a.354.354 0 0 0 .349-.347v.002l.29-3.958-.29-8.288a.355.355 0 0 0-.35-.347zm-1.564.691a.322.322 0 0 0-.318.315l-.275 7.65.276 3.998a.321.321 0 0 0 .317.313c.17 0 .31-.138.317-.315v.002l.312-3.999-.312-7.65a.323.323 0 0 0-.317-.314zM12.974 20.6a.29.29 0 0 0-.285.282l-.295 6.226.295 4.023a.29.29 0 0 0 .285.281.292.292 0 0 0 .285-.282v.001l.333-4.023-.333-6.226a.292.292 0 0 0-.285-.282zm-1.264 2.678a.258.258 0 0 0-.253-.248.257.257 0 0 0-.253.25l-.314 3.826.314 4.024c.008.142.117.25.253.25a.259.259 0 0 0 .253-.25l.356-4.024-.356-3.828zm0 7.851v.001zm-1.757-8.366a.226.226 0 0 0-.221.217l-.333 4.126.333 3.99c.009.124.104.217.22.217s.21-.093.221-.216l.4-3.991-.4-4.126c-.01-.123-.105-.217-.22-.217zm-1.493.142c-.1 0-.179.078-.189.185l-.351 4.015.351 3.868c.01.108.09.185.19.185.097 0 .176-.077.188-.184l.4-3.869-.4-4.015c-.012-.107-.091-.185-.189-.185zm-2.873 1.96c-.079 0-.14.06-.151.147l-.276 2.094.276 2.056c.01.086.072.147.15.147s.14-.06.152-.147l.326-2.056-.326-2.095c-.013-.086-.074-.145-.151-.145zM6.98 23.57c-.08 0-.146.064-.157.152l-.25 3.385.25 3.308c.011.088.077.152.157.152s.144-.062.156-.152l.422-3.308-.422-3.385c-.012-.09-.077-.152-.156-.152zM48 24c0 13.255-10.745 24-24 24C10.757 48 0 37.255 0 24 0 10.757 10.757 0 24 0c13.255 0 24 10.757 24 24z"/></g></svg>';
    } else {
        soundcloud += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 48 48" fill="' + fill + '"><path d="M1.6 24.3c-.2 0-.3.1-.3.3l-.5 6.2.5 6c0 .2.2.3.3.3.2 0 .3-.1.3-.3l.6-6-.6-6.2c0-.2-.1-.3-.3-.3zm3.3 12.6zm0-11.8c0-.2-.2-.4-.4-.4s-.4.2-.4.4l-.5 5.8.5 6.1c0 .2.2.4.4.4s.4-.2.4-.4l.5-6.1-.5-5.8zm2.7-6.2c-.3 0-.5.2-.5.5l-.4 11.5.4 6c0 .3.2.5.5.5s.5-.2.5-.5l.5-6-.5-11.6c-.1-.2-.3-.4-.5-.4zm3.8-1c0-.3-.3-.6-.6-.6s-.6.3-.6.6l-.4 13 .4 5.9c0 .3.3.6.6.6s.6-.3.6-.6l.4-5.9-.4-13zm2.7.1c-.4 0-.7.3-.7.7l-.3 12.2.3 5.9c0 .4.3.7.7.7.4 0 .7-.3.7-.7l.3-5.9-.3-12.2c0-.4-.3-.7-.7-.7zm3.7-3.6c-.1-.1-.3-.1-.4-.1-.2 0-.4.1-.5.2-.2.1-.3.4-.3.6v.1l-.3 15.7.2 2.9.2 2.8c0 .4.4.8.8.8s.8-.3.8-.8l.3-5.7-.3-15.8c-.1-.3-.3-.5-.5-.7zm22.4 9c-1 0-1.9.2-2.7.5-.6-6.3-5.9-11.3-12.3-11.3-1.6 0-3.1.3-4.5.8-.5.2-.7.4-.7.8v22.2c0 .4.3.8.8.8h19.4c3.9 0 7-3.1 7-7 0-3.7-3.1-6.8-7-6.8z"/></svg>';
    }
    soundcloud += '</a>';
    soundcloud += ' </div>';
    var telegram = '';
    telegram += '<div class="t-review-social-links__item">';
    telegram += '<a href="' + item + '" target="_blank">';
    if (socialWrapper.hasClass('t-review-social-links__wrapper_round')) {
        telegram += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 60 60" fill="' + fill + '"><path d="M30 0C13.4 0 0 13.4 0 30s13.4 30 30 30 30-13.4 30-30S46.6 0 30 0zm16.9 13.9l-6.7 31.5c-.1.6-.8.9-1.4.6l-10.3-6.9-5.5 5.2c-.5.4-1.2.2-1.4-.4L18 32.7l-9.5-3.9c-.7-.3-.7-1.5 0-1.8l37.1-14.1c.7-.2 1.4.3 1.3 1z"/><path d="M22.7 40.6l.6-5.8 16.8-16.3-20.2 13.3"/></svg>';
    } else {
        telegram += '<svg class="t-review-social-links__svg" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 60 60" fill="' + fill + '"><path d="M52.5 9L6.1 26.9c-.9.4-.9 1.8 0 2.3L18 34.1l4.4 14.1c.3.8 1.2 1 1.8.5l6.8-6.4L43.9 51c.7.4 1.6 0 1.7-.7l8.4-40c.2-.8-.7-1.6-1.5-1.3zm-28 27.7l-.7 7.3-3.6-11.2L45.5 16l-21 20.7z"/></svg>';
    }
    telegram += '</a>';
    telegram += '</div>';
    if (item.indexOf('facebook') != -1) {
        socialWrapper.append(fb);
    }
    if (item.indexOf('twitter') != -1) {
        socialWrapper.append(twi);
    }
    if (item.indexOf('vk.com') != -1) {
        socialWrapper.append(vk);
    }
    if (item.indexOf('ok.ru') != -1) {
        socialWrapper.append(ok);
    }
    if (item.indexOf('behance') != -1) {
        socialWrapper.append(behance);
    }
    if (item.indexOf('vimeo') != -1) {
        socialWrapper.append(vimeo);
    }
    if (item.indexOf('youtube') != -1) {
        socialWrapper.append(youtube);
    }
    if (item.indexOf('instagram') != -1) {
        socialWrapper.append(instagram);
    }
    if (item.indexOf('pinterest') != -1) {
        socialWrapper.append(pinterest);
    }
    if (item.indexOf('linkedin') != -1) {
        socialWrapper.append(linkedin);
    }
    if (item.indexOf('soundcloud') != -1) {
        socialWrapper.append(soundcloud);
    }
    if (item.indexOf('telegram') != -1) {
        socialWrapper.append(telegram);
    }
}

jQuery(document).ready(function ($) {
    t_reviews_social_init('101336447');
});

jQuery(document).ready(function ($) {
    t396_init('101336700');
});
jQuery(document).ready(function ($) {
    t396_init('101333356');
});
jQuery(".tn-atom .js-errorbox-close").click(function () {
    jQuery(this).parent().css("display", "none");
});
jQuery(".tn-atom .js-errorbox-close").click(function () {
    jQuery(this).parent().css("display", "none");
});
jQuery(document).ready(function ($) {
    t396_init('101402729');
});
jQuery(document).ready(function ($) {
    t396_init('101333362');
});
$(".tn-atom .js-errorbox-close").click(function () {
    $(this).parent().css("display", "none");
});
$(".tn-atom .js-errorbox-close").click(function () {
    $(this).parent().css("display", "none");
});
;jQuery(document).ready(function ($) {
    t396_init('101402718');
});
jQuery(document).ready(function ($) {
    t396_init('101333368');
});
$(".tn-atom .js-errorbox-close").click(function () {
    $(this).parent().css("display", "none");
});
$(".tn-atom .js-errorbox-close").click(function () {
    $(this).parent().css("display", "none");
});
jQuery(document).ready(function ($) {
    t396_init('101395579');
});

jQuery(document).ready(function ($) {

    var forms_loaded = false;

    $('.popup-btn').magnificPopup({
        type: 'inline',
        preloader: false,
        focus: '#name',
        mainClass: 'main-Popup',

        callbacks: {
            beforeOpen: function () {

                /*if (!forms_loaded) {

					$('#bx_form_iframe_7').remove();

					var forms = [
						{"id": "5", "lang": "ru", "sec": "90whoj", "type": "button", "click": ""},
						{
							"id"  : "8",
							"lang": "ru",
							"sec" : "3n7ldv",
							"type": "inline",
							"node": document.getElementById( 'howWeDoForm1' )
						},
						{
							"id"  : "9",
							"lang": "ru",
							"sec" : "xsntod",
							"type": "inline",
							"node": document.getElementById( 'howWeDoForm2' )
						},
						{
							"id"  : "6",
							"lang": "ru",
							"sec" : "w14b6m",
							"type": "inline",
							"node": document.getElementById( 'howWeDoForm3' )
						},
						{
							"id"  : "7",
							"lang": "ru",
							"sec" : "ajy69g",
							"type": "inline",
							"node": document.getElementById( 'contact-form' )
						}
					];

					window[window.Bitrix24FormObject].forms = [];

					for ( var form in forms ) {
						b24form( forms[form] );
					}

					Bitrix24FormLoader.init();

					forms_loaded = true;
				}
*/
                if ($(window).width() < 700) {
                    this.st.focus = false;
                } else {
                    this.st.focus = '#name';
                }
            }
        }
    });

    $(window).on("load", function () {

        var $reviewsSlider = $('.reviews-slider');

        $reviewsSlider.on('beforeChange', function () {
            $(this).find('.review-item-text');
            $(".review-item-text").each(function () {
                $(this).removeClass('opened');
                $(this).find('.healper-div:first-child').show();
                $(this).find('.healper-div:nth-child(2)').hide();
            });
        }).on('init', function () {
            addReadMore();

        });

        $reviewsSlider.slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            responsive: [
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        var $qSlider = $('.q-slider');
        $qSlider.on('afterChange', function () {
        });
        $qSlider.slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            dots: true,
            responsive: [
                {
                    breakpoint: 961,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });


    function addReadMore() {
        $(".review-item-text").each(function () {
            var el = $(this);
            var div = $(this).find('.healper-div');
            var clone = div.clone();

            clone.hide();
            clone.appendTo($(this));

            var left;
            var wordArray = div.html().split(' ');

            if ((wordArray.length > 0) && (div.innerHeight() > el.innerHeight())) {
                while (div.innerHeight() > el.innerHeight()) {
                    left = wordArray.pop();
                    div.html(wordArray.join(' '));
                }
                var text = div.text();
                text = text.replace(/^ +/gm, '');
                div.html(text.slice(0, -20) + '...<span class="read-more"><a href="#">читать полностью</a></span>');
                div.find('a').on('click', function (e) {
                    e.preventDefault();
                    div.hide();
                    clone.show();
                    el.addClass('opened');
                });
            }
        });
    }

    setTimeout(function () {
        $('html.modal-new-products').removeClass('modal-new-products');
    }, 4000);
});
