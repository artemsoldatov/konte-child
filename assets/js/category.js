jQuery( document ).ready( function( $ ) {

    // new file
    const filters = {
        init: function() {
            this.displayFilteredField();
            this.actions();
            this.showDefaultCategoryMobile();
            customScroll.init();
        },
        actions: function() {

            $( document.body ).off( 'click', '.filter-swatches .swatch, .filter-list .filter-list-item, .filter-checkboxes .filter-checkboxes-item' );

            $( document.body ).on( 'click', '.action-icon', function( e ) {

                e.stopPropagation();

                var $this = $( this ),
                    id = $this.closest( '.filter-list-item' ).data( 'id' );

                if ( id ) {
                    var $childList = $( '.filter-list-child[data-parent="' + id + '"]' );

                    if ( $childList.length ) {
                        $childList.toggleClass( 'closed' );
                        $this.closest( '.filter-list-item' ).toggleClass( 'children-closed' );
                    }
                }
            } );

            // Redefine konte addons functionality
            $( document.body ).on( 'click', '.filter-swatches .swatch, .filter-list .filter-list-item, .filter-checkboxes .filter-checkboxes-item', function( event ) {
                event.preventDefault();

                var $this = $( this ),
                    $filter = $this.closest( '.filter' ),
                    $input = $this.is('.filter-checkboxes-item') ? $this.closest('.filter-checkboxes').next( 'input[type=hidden]' ) : $this.closest('.filter-list').next( 'input[type=hidden]' ),
                    current = $input.val(),
                    value = $this.data( 'value' ),
                    $form = $( this ).closest( 'form' ),
                    form = $form.get( 0 ),
                    index = -1,
                    $listWrapper = $this.closest( '.filter-list-child' );

                $input = $input.add( 'input[name="' + $input.attr( 'name' ) + '"]' );


                // product category
                var isProductCategory = $filter.hasClass( 'product_cat' );
                if ( isProductCategory ) {
                    $input = $filter.parent().find( 'input[name="product_cat"]' );
                    current = $input.val();
                    $filter.addClass( 'multiple' );
                    current = current ? current.split( ',' ) : [];
                    var $thisCategoryID = $this.data( 'main-parent' )

                    $.each( current, function (i, value) {
                        var $selectedEl = $form.find( '[data-value="'+ value +'"]' );
                        var $selectedElParent = $selectedEl.data( 'main-parent' );
                        if ($selectedElParent !== $thisCategoryID) {
                            current = _.without( current, value );
                            $selectedEl.removeClass( 'selected' );
                        }
                    } );

                    current = current.join(',')
                }

                if ( $filter.hasClass( 'multiple' ) ) {
                    current = current ? current.split( ',' ) : [];
                    index = current.indexOf( value );
                    index = ( -1 !== index ) ? index : current.indexOf( value.toString() );

                    if ( index !== -1 ) {
                        current = _.without( current, value );
                    } else {
                        current.push( value );
                    }

                    $input.val( current.join( ',' ) );
                    $this.toggleClass( 'selected' );

                    if ( current.length === 0 ) {
                        $input.attr( 'disabled', 'disabled' );
                    } else {
                        $input.removeAttr( 'disabled' );
                    }
                } else {

                    if ( $this.hasClass( 'selected' ) ) {
                        $this.removeClass( 'selected' );
                        $input.val( '' ).attr( 'disabled', 'disabled' );

                    } else {
                        $this.addClass( 'selected' ).siblings( '.selected' ).removeClass( 'selected' );
                        $input.val( value ).removeAttr( 'disabled' );
                    }
                }

                /* toggle parent category feature, need to search in current form because of two form filters */
                if ( $listWrapper.length ) {
                    var $parentList = $form.find( '.filter-list-item[data-id="' + $listWrapper.attr( 'data-parent' ) + '"]' );
                    var $grandParentList = $form.find( '.filter-list-item[data-id="' + $listWrapper.attr( 'data-main-parent' ) + '"]' );
                    removeWrapperFromSelection($parentList);
                    removeWrapperFromSelection($grandParentList);
                    function removeWrapperFromSelection ($parentList) {
                        if ( $parentList.hasClass( 'selected' ) ) {
                            var parentValue = $parentList.data( 'value' );
                            current = _.without( current, parentValue );
                            $input.val( current.join( ',' ) );
                            $parentList.removeClass( 'selected' )
                            // $parentList.trigger( 'click' );
                        }
                    }

                }

                $( document.body ).trigger( 'konte_products_filter_change', [form] );
            } );

            // Remove filtered field;
            $( document.body ).on( 'click', 'a.remove-filtered', function( event ) {
                event.preventDefault();

                var $el = $( this ),
                    $widget = $( 'aside section.products-filter-widget' ),
                    name = $el.data( 'name' ),
                    value = $el.data( 'value' );

                $el.remove();

                var $input = $widget.find( ':input[name="' + name + '"]' ),
                    current = $input.val();

                if ( $input.is( 'select' ) ) {
                    $input.prop( 'selectedIndex', 0 );
                    $input.trigger( 'change' );
                } else {
                    current = current.replace( ',' + value, '' );
                    current = current.replace( value, '' );
                    $input.val( current );

                    if ( '' == current ) {
                        $input.prop( 'disabled', true );
                    }

                    $widget.find( '[data-value="' + value + '"]' ).removeClass( 'selected' );
                }

                $widget.find( 'form' ).trigger( 'submit' );
            } );

            $( document.body ).off( 'konte_products_filter_request_success' );

            $( document.body ).on( 'konte_products_filter_request_success', ( event, html, url ) => this.updateFilters( event, html, url ) );

            $( document.body ).on( 'konte_products_filter_request_success', function( e, response, url ) {

                const $content = $( response );

                if ( '?' === url.slice( -1 ) ) {
                    url = url.slice( 0, -1 );
                }

                url = url.replace( /%2C/g, ',' );

                history.pushState( null, '', url );

                $( document ).find( '.woocommerce-pagination' ).remove();
                $( 'ul.products' ).parent().append( $content.find( '.woocommerce-pagination' ) );
            } );

            $( '.reset-button' ).on( 'click.kapsula.category', () => {

                if ( Cookies.get( 'start_category' ) ) {
                    window.location.href = Cookies.get( 'start_category' );
                } else {
                    window.location.href = kapsula.links.wcShopPage;
                }

                return false;
            } );

            $( '.hamburger-menu.button-close' ).on( 'click.kapsula.category', () => this.showDefaultCategoryMobile() );


            this.refreshFilters();
        },
        showDefaultCategoryMobile: function() {
            $( '.filters > .filter > .filter-name' ).not( '.closed' ).find( '.action' ).click();
            $( '.product_cat.filter .action' ).click();
            $( '#products-filter .panel .panel-content' ).scrollTop(0);
        },
        refreshFilters: function() {

            const $selectedParents = $( '.filter-list-item.selected' ).closest( '.filter-list-child.closed, .filter-control.closed' );

            $selectedParents.removeClass( 'closed' ).siblings( '.filter-name.closed' ).removeClass( 'closed' );

            $selectedParents.each( function() {
                $( this ).siblings( '.filter-list-item.children-closed[data-id="' + $( this ).data( 'parent' ) + '"]' ).removeClass( 'children-closed' );
            } );

            $( '.filter-name' ).click( function() {

                const $button = $( this );

                $button.toggleClass( 'closed' ).siblings( '.filter-control' ).toggleClass( 'closed' );
            } );
        },
        /**
         * Display filtered fields.
         */
        displayFilteredField: function () {

            var $toggle = $( '.products-tools .products-filter-toggle' ),
                $widget = $( 'aside section.products-filter-widget' );

            if ( !$widget.length ) {
                return;
            }

            $toggle.find( '.remove-filtered' ).remove();

            var filtered = $widget
                .find( 'form' )
                .find( ':input:not(:checkbox):not(:button)' )
                .filter( function() {
                    return $( this ).val() != '' && $( this ).attr( 'name' ) !== 'filter';
                } )
                .serializeObject();

            $.each( filtered, function( name, value ) {
                if ( 'min_price' === name || 'max_price' === name ) {
                    return;
                }

                var key = name.replace( /^filter_/g, '' ),
                    $filter = $widget.find( '.filter.' + key );

                if ( !$filter.length ) {
                    return;
                }

                if ( typeof value === 'object' ) {
                    value = value.filter( function( val, index, self ) {
                        return self.indexOf( val ) === index;
                    } ).join( ',' );
                }

                value = value.replace( /^,|,$/g, '' );

                $.each( value.split( ',' ), function( index, filtered_value ) {

                    var text = filtered_value;

                    if ( $filter.hasClass( 'list' ) || $filter.hasClass( 'h-list' ) ) {
                        text = $filter.find( '[data-value="' + filtered_value + '"]' ).find( '.name' ).text();
                    } else if ( $filter.hasClass( 'dropdown' ) ) {
                        text = $filter.find( 'select option[value="' + filtered_value + '"]' ).text();
                    } else if ( $filter.hasClass( 'auto' ) ) {
                        if ( $filter.find( '.filter-swatches' ).length ) {
                            var $swatch = $filter.find( '[data-value="' + filtered_value + '"]' );

                            text = $swatch.hasClass( 'swatch-label' ) ? $swatch.attr( 'title' ) : $swatch.text();
                        } else {
                            text = $filter.find( 'select option[value="' + filtered_value + '"]' ).text();
                        }
                    } else if ( $filter.hasClass( 'in_showroom' ) || $filter.hasClass( 'ready_to_delivery' ) ) {
                        text = $filter.find( '.filter-name' ).text().trim();
                    }

                    $toggle.append(
                        '<a href="#" class="remove-filtered" data-name="' + name + '" data-value="' + filtered_value + '">' +
                        text +
                        '<span class="svg-icon icon-close-mini"><svg><use xlink:href="#close-mini"></use></svg></span>' +
                        '</a>'
                    );
                } );
            } );
        },
        updateFilters: function( event, html, url ) {

            $( '.ajax-filter.instant-filter' ).html( $( html ).find( '.ajax-filter.instant-filter' ).html() );
            this.refreshFilters();
            this.displayFilteredField();
            this.initPriceSlider();
            customScroll.init();
        },
        /**
         * Slider initialization.
         *
         * Copied from WooCommerce
         *
         * @see wp-content/plugins/woocommerce/assets/js/frontend/price-slider.js
         */
        initPriceSlider: function() {
            $( 'input#min_price, input#max_price' ).hide();
            $( '.price_slider, .price_label' ).show();

            var min_price         = $( '.price_slider_amount #min_price' ).data( 'min' ),
                max_price         = $( '.price_slider_amount #max_price' ).data( 'max' ),
                step              = $( '.price_slider_amount' ).data( 'step' ) || 1,
                current_min_price = $( '.price_slider_amount #min_price' ).val(),
                current_max_price = $( '.price_slider_amount #max_price' ).val();

            $( '.price_slider:not(.ui-slider)' ).slider({
                range: true,
                animate: true,
                min: min_price,
                max: max_price,
                step: step,
                values: [ current_min_price, current_max_price ],
                create: function() {

                    $( '.price_slider_amount #min_price' ).val( current_min_price );
                    $( '.price_slider_amount #max_price' ).val( current_max_price );

                    $( document.body ).trigger( 'price_slider_create', [ current_min_price, current_max_price ] );
                },
                slide: function( event, ui ) {

                    $( 'input#min_price' ).val( ui.values[0] );
                    $( 'input#max_price' ).val( ui.values[1] );

                    $( document.body ).trigger( 'price_slider_slide', [ ui.values[0], ui.values[1] ] );
                },
                change: function( event, ui ) {

                    $( document.body ).trigger( 'price_slider_change', [ ui.values[0], ui.values[1] ] );
                }
            });
        }
    }

    const videos = {
        init: function() {
            $( window ).scroll( () => this.playVideos() );
        },
        playVideos: function() {

            if ( 768 < $( window ).width() ) {
                return;
            }

            let windowScrollTop = $( window ).scrollTop();

            $( 'li.product video' ).each( function() {

                let $video = $( this ),
                    videoHeight = $video.height(),
                    offsetTop = $video.offset().top;

                if ( this.paused && offsetTop - videoHeight / 2 < windowScrollTop && offsetTop + videoHeight / 2 > windowScrollTop ) {
                    this.play();
                } else if ( ! this.paused && ( offsetTop + videoHeight < windowScrollTop || offsetTop - videoHeight > windowScrollTop ) ) {
                    this.pause();
                }
            } );
        }
    }

    const customScroll = {
        init: function () {
            $("aside section.products-filter-widget .product_brand .filter-list").mCustomScrollbar({
                theme:"dark"
            });
        },
    }

    /**
     * Ajax load more products.
     */
    konte.loadMoreProducts = function() {
        // Handles click load more button.
        $( document.body ).on( 'click', '.woocommerce-navigation.ajax-navigation a', function( event ) {
            event.preventDefault();

            var $el = $( this );

            if ( $el.hasClass( 'loading' ) ) {
                return;
            }

            $el.addClass( 'loading' );

            loadProducts( $( this ) );
        } );

        // Infinite scroll.
        if ( $( document.body ).hasClass( 'woocommerce-nav-infinite' ) ) {
            var waiting = false,
                endScrollHandle;

            $( window ).on( 'scroll', function() {
                if ( waiting ) {
                    return;
                }

                waiting = true;

                // clear previous scheduled endScrollHandle
                clearTimeout( endScrollHandle );

                infiniteScoll();

                setTimeout( function() {
                    waiting = false;
                }, 100 );

                // schedule an extra execution of infiniteScoll() after 200ms
                // in case the scrolling stops in next 100ms.
                endScrollHandle = setTimeout( function() {
                    waiting = false;
                    infiniteScoll();
                }, 200 );
            } );
        }

        /**
         * Infinite scroll handler.
         */
        function infiniteScoll() {
            var $navigation = $( '.woocommerce-navigation.ajax-navigation' ),
                $button = $( 'a', $navigation );

            // When almost reach to the navigation.
            if ( konte.isVisible( $navigation ) && $button.length && !$button.hasClass( 'loading' ) ) {
                $button.addClass( 'loading' );

                loadProducts( $button, function( respond ) {
                    $button = $navigation.find( 'a' );
                } );
            }
        }

        /**
         * Ajax load products.
         *
         * @param jQuery $el Button element.
         * @param function callback The callback function.
         */
        function loadProducts( $el, callback ) {

            var $nav = $el.closest( '.woocommerce-navigation' ),
                url = $el.attr( 'href' );

            $.get( url, function( response ) {
                var $content = $( '#main', response ),
                    $list = $( 'ul.products', $content ),
                    $products = $list.children(),
                    $newNav = $( '.woocommerce-navigation.ajax-navigation', $content );

                $products.each( function( index, product ) {
                    $( product ).css( 'animation-delay', index * 100 + 'ms' );
                } );

                $products.appendTo( $nav.prev( 'ul.products' ) );
                $products.addClass( 'animated konteFadeInUp' );

                if ( $newNav.length ) {
                    $el.replaceWith( $( 'a', $newNav ) );
                } else {
                    $nav.fadeOut( function() {
                        $nav.remove();
                    } );
                }

                $( '.woocommerce-pagination' ).replaceWith( $content.find( '.woocommerce-pagination' ) );

                if ( 'function' === typeof callback ) {
                    callback( response );
                }

                $( document.body ).trigger( 'konte_products_loaded', [$products, true] );

                if ( konte.data.shop_nav_ajax_url_change ) {
                    window.history.pushState( null, '', url );
                }
            } );
        }
    };

    filters.init();
    videos.init();

} );