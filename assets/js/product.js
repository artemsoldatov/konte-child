jQuery( document ).ready( function( $ ) {

    const $document = $( document );

    if ( typeof konte !== 'undefined' ) {
        /**
         * Related & ppsell products carousel.
         *
         * Added recently viewed products. Changed amount of slides per view.
         */
        konte.relatedProductsCarousel = function() {

            var $related = $( '.products.related, .products.upsells, .recent_products' );

            if ( ! $related.length ) {
                return;
            }

            var $products = $related.find( 'ul.products' );

            $products.wrap( '<div class="swiper-container linked-products-carousel" style="opacity: 0;"></div>' );
            $products.after( '<div class="swiper-pagination"></div>' );
            $products.addClass( 'swiper-wrapper' );
            $products.find( 'li.product' ).addClass( 'swiper-slide' );

            var carousel = new Swiper( '.linked-products-carousel', {
                loop: false,
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 50,
                speed: 800,
                watchOverflow: true,
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                    renderBullet: function( index, className ) {
                        return '<span class="' + className + '"><span></span></span>';
                    }
                },
                on: {
                    init: function() {
                        this.$el.css( 'opacity', 1 );
                    }
                },
                breakpoints: {
                    360: {
                        spaceBetween: 10,
                        slidesPerView: 2,
                        slidesPerGroup: 2
                    },
                    768: {
                        spaceBetween: 20,
                        slidesPerView: 2,
                        slidesPerGroup: 2
                    },
                    992: {
                        spaceBetween: 30,
                        slidesPerView: 3,
                        slidesPerGroup: 3
                    },
                    1200: {
                        spaceBetween: 30,
                        slidesPerView: 5,
                        slidesPerGroup: 5
                    },
                    1440: {
                        spaceBetween: 50,
                        slidesPerView: 5,
                        slidesPerGroup: 5
                    }
                }
            } );
        };

        /**
         * Special functions for product layout v5.
         */
        konte.singleProductV5 = function() {
            var $product = $( 'div.product.layout-v5' ),
                $summary = $product.find( '.summary' ),
                $summaryInner = $summary.children( '.summary-inner' ),
                $window = $( window ),
                headerHeight = $( '#masthead' ).height(),
                summarySticky = false;

            if ( !$product.length ) {
                return;
            }

            // Make product fullwidth
            productWidth();

            $window.on( 'resize', function() {
                productWidth();
            } );

            // Set top padding of product
            $summary.css( {paddingTop: headerHeight} );

            // Init zoom for product gallery thumbnails
            if ( konte.data.product_image_zoom ) {
                $product.find( '.product-gallery-thumbnails .woocommerce-product-gallery__image' ).each( function() {
                    konte.zoomSingleProductImage( this );
                } );
            }

            // Sticky summary
            if ( $.fn.stick_in_parent ) {
                $summaryInner
                    .on( 'sticky_kit:bottom', function() {
                        $( this ).closest( '.product-gallery-summary' ).addClass( 'summary-sticky-bottom' );
                    } )
                    .on( 'sticky_kit:unbottom', function() {
                        $( this ).closest( '.product-gallery-summary' ).removeClass( 'summary-sticky-bottom' );
                    } );
            }

            function productWidth() {
                var width = $window.width(),
                    bonus = width > 1440 ? 60 : 0;

                width -= konte.getVerticalHeaderWidth();

                $product.width( width );

                if ( konte.data.rtl ) {
                    $product.css( 'margin-right', -width / 2 );
                    $summary.css( 'padding-left', width / 2 - $( '.konte-container' ).width() / 2 + bonus );
                } else {
                    $product.css( 'margin-left', -width / 2 );
                    $summary.css( 'padding-right', width / 2 - $( '.konte-container' ).width() / 2 + bonus );
                }
            }
        };

    }
} );

if(window.matchMedia('(min-width: 992px)').matches) {
    var margin = -100;

    if(jQuery(window).width() <= 1440) {
        margin = -60;
    }

    (function () {
        var a = document.querySelector('.summary-inner'), b = null, K = null, Z = 0, P = margin, N = 0;  // если у P ноль заменить на число, то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента, если у N — нижний край дойдёт до нижнего края элемента. Может быть отрицательным числом
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);

        function Ascroll() {
            var Ra = a.getBoundingClientRect(),
                R1bottom = document.querySelector('.woocommerce-product-gallery').getBoundingClientRect().bottom;
            if (Ra.bottom < R1bottom) {
                if (b == null) {
                    var Sa = getComputedStyle(a, ''), s = '';
                    for (var i = 0; i < Sa.length; i++) {
                        if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                            s += Sa[i] + ': ' + Sa.getPropertyValue(Sa[i]) + '; '
                        }
                    }
                    b = document.createElement('div');
                    b.className = "stop";
                    b.style.cssText = s + ' box-sizing: border-box; width: ' + a.offsetWidth + 'px;';
                    a.insertBefore(b, a.firstChild);
                    var l = a.childNodes.length;
                    for (var i = 1; i < l; i++) {
                        b.appendChild(a.childNodes[1]);
                    }
                    a.style.height = b.getBoundingClientRect().height + 'px';
                    a.style.padding = '0';
                    a.style.border = '0';
                }
                var Rb = b.getBoundingClientRect(),
                    Rh = Ra.top + Rb.height,
                    W = document.documentElement.clientHeight,
                    R1 = Math.round(Rh - R1bottom),
                    R2 = Math.round(Rh - W);
                if (Rb.height > W) {
                    if (Ra.top < K) {  // скролл вниз
                        if (R2 + N > R1) {  // не дойти до низа
                            if (Rb.bottom - W + N <= 0) {  // подцепиться
                                b.className = 'sticky';
                                b.style.top = W - Rb.height - N + 'px';
                                Z = N + Ra.top + Rb.height - W;
                            } else {
                                b.className = 'stop';
                                b.style.top = -Z + 'px';
                            }
                        } else {
                            b.className = 'stop';
                            b.style.top = -R1 + 'px';
                            Z = R1;
                        }
                    } else {  // скролл вверх
                        if (Ra.top - P < 0) {  // не дойти до верха
                            if (Rb.top - P >= 0) {  // подцепиться
                                b.className = 'sticky';
                                b.style.top = P + 'px';
                                Z = Ra.top - P;
                            } else {
                                b.className = 'stop';
                                b.style.top = -Z + 'px';
                            }
                        } else {
                            b.className = '';
                            b.style.top = '';
                            Z = 0;
                        }
                    }
                    K = Ra.top;
                } else {
                    if ((Ra.top - P) <= 0) {
                        if ((Ra.top - P) <= R1) {
                            b.className = 'stop';
                            b.style.top = -R1 + 'px';
                        } else {
                            b.className = 'sticky';
                            b.style.top = P + 'px';
                        }
                    } else {
                        b.className = '';
                        b.style.top = '';
                    }
                }
                window.addEventListener('resize', function () {
                    a.children[0].style.width = getComputedStyle(a, '').width
                }, false);
            }
        }
    })()
}