jQuery( document ).ready( function( $ ) {

    if ( typeof kapsula === 'undefined' ) {
        return;
    }

    const $document = $( document );

    $.ajaxPrefilter( ( options, originalOptions) => {
        if ( options.type.toLowerCase() === 'post' ) {
            options.data = options.data.replace(/&lang=(\w){2}/, '') + '&lang=' + kapsula.language;
        }
    } );

    const cart = {
        sizesSelect: '.woocommerce-mini-cart-item__summary .pa_size-select',
        init: function() {
            this.events();
            this.refreshMiniIcons();
        },
        events: function() {

            const self = this;

            $document.on( 'change.kapsula.sizes', self.sizesSelect, function( e ) {
                self.sendSizeChangeRequest( $( this ) );
            } );

            $( document.body ).on( 'wc_fragments_refreshed added_to_wishlist removed_from_wishlist', () => self.refreshMiniIcons() );
        },
        refreshMiniIcons: function() {
            $( 'span.counter' ).each( function() {

                const $this = $( this );

                parseInt( $this.text() ) > 0 ? $this.show() : $this.hide();
            } );
        },
        sendSizeChangeRequest: function( $select ) {

            const name = $select.attr('name');

            this.block( $( '.widget_shopping_cart_content' ).parent() );

            $.post( kapsula.ajaxURL, {
                'nonce': kapsula.nonce,
                'update_cart': true,
                'action': 'change_size_in_cart',
                [name]: $select.val()
            }, response => {
                if ( response.hasOwnProperty( 'success' ) && response.success ) {
                    if ( response.data.hasOwnProperty( 'update' ) ) {
                        for ( let i in response.data.update ) {
                            $( i ).html( response.data.update[ i ] );
                        }
                    }
                }

                this.unblock( $( '.widget_shopping_cart_content' ).parent() );
            } );
        },
        /**
         * Init jQuery.BlockUI
         */
        block: function( element ) {
            $( element ).block({
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            });
        },

        /**
         * Remove jQuery.BlockUI
         */
        unblock: function( element ) {
            $( element ).unblock();
        },
    }

    cart.init();

    const product = {
        init: function() {
            this.events();
            this.hideColorSelect();
        },
        events: function() {
            $( document.body ).on( 'konte_product_quickview_loaded', () => this.hideColorSelect() );
        },
        hideColorSelect: function() {
            $( '.pa_color-select' ).closest( 'tr' ).hide().closest( '.variations' ).show();
            $( '.pa_size-select' ).parent().siblings( '.label' ).remove();
        }
    }

    product.init();

    const promo = {
        closePromoButtonSelector: '.konte-promotion .close-banner',
        init: function() {
            this.$closePromoButton = $( this.closePromoButtonSelector );
            this.events();
        },
        events: function() {
            this.$closePromoButton.on( 'click.kapsula.promo', () => this.closePromoBanner() );
        },
        closePromoBanner: function() {

            const $campaignBar = $( '.campaign-bar__campaigns' );

            $campaignBar
                .animate( {
                    'height': 0
                }, 500, () => $campaignBar.hide() );
            document.cookie = "promo_disabled=1;path=/;";
        }
    }

    promo.init();

    const productEvents = {

        init: function() {

            const self = this;

            // Remove WooCommerce updating form variation as we will have only sizes in selects
            $( '#pa_size' ).closest( 'form' ).off( 'update_variation_values.wc-variation-form' );

            $( document )
                .on( 'click.kapsula.add', '.single_add_to_cart_button', function( e ) {
                    self.onAddToCart( e, $( this ) );
                } )
                .on( 'change.kapsula.select', '.variations select.pa_size-select', function() {
                    self.onSelectVariation( $( this ) );
                } );

            $( '.variations select.pa_size-select' ).trigger( 'change.kapsula.select' );
        },
        onSelectVariation: function ( $select ) {

            let inCart = $select.find( 'option[value="' + $select.val() + '"]' ).hasClass( 'in-cart' ),
                text = inCart ? kapsula.translations.inCart : kapsula.translations.addToCart;

            $select.parents( '.variations_form' ).find( '.single_add_to_cart_button' ).toggleClass( 'in-cart', inCart ).text( text );
        },
        onAddToCart: function( e, $button ) {

            const product_id = $button.parent().find( 'input[name=product_id]' ).val(),
                variation_id = $button.parent().find( 'input[name=variation_id]' ).val(),
                name = $( '.product_title' ).text().trim(),
                brand = $( '.brand' ).find( 'a' ).text(),
                price = $button.closest( '.entry-summary' ).find( '.product-price' ).text();

            let $category = $( '.category' ),
                category = '';

            if ( ! $category.length ) {
                $category = $button.closest( '.entry-summary' ).find( '.posted_in a' ).last();
            }

            if ( $category.length ) {
                category = $category.text();
            }

            const product = {
                id : product_id,
                name : name,
                category : category,
                brand : brand,
                variant : variation_id,
                price : price,
                qty : 1
            };

            this.addToCartFacebookAction( product );
            this.addToCartGoogleAction( product );

            if ( $button.hasClass( 'in-cart' ) ) {
                e.stopPropagation();
                e.preventDefault();
                window.location.href = kapsula.links.wcCartPage;
            } else {
                const $select = $button.closest( 'form' ).find( 'select.pa_size-select' );

                $select.find( 'option[value="' + $select.val() + '"]' ).addClass( 'in-cart' );
                $select.trigger( 'change.kapsula.select' );
            }
        },
        addToCartGoogleAction: function( product ) {

            if ( typeof ga !== 'function' ) {
                return;
            }

            ga( 'ec:addProduct', {
                'id' : product.id,
                'name' : product.name,
                'category' : product.category,
                'brand' : product.brand,
                'variant' : product.variant,
                'price' : product.price,
                'quantity' : product.qty
            } );
            ga( 'ec:setAction', 'add' );
            ga( 'send', 'event', 'UX', 'click', 'add to cart' );     // Send data using an event.
        },
        addToCartFacebookAction: function( product ) {

            if ( typeof fbq !== 'function' ) {
                return;
            }

            let $currency_input = $( '[name="woocommerce-currency-switcher"]' ),
                currency = 'UAH';

            if ( $currency_input.length ) {
                currency = $currency_input.first().val();
            }

            fbq( 'track', 'AddToCart', {
                content_type : 'product',
                content_ids : [product.id],
                content_name : product.name,
                content_category : product.category,
                value : product.price,
                currency : currency
            } );
        }
    }

    productEvents.init();

    /* theme overwrite */
    const konte = window.konte || {};
    konte.mobileMenu = function() {
        var $mobileMenu = $( '#mobile-menu' );

        // Add class 'open' to current menu item.
        $mobileMenu.find( '.menu > .menu-item-has-children, .menu > li > ul > .menu-item-has-children' ).filter( function() {
            return $( this ).hasClass( 'current-menu-item' ) || $( this ).hasClass( 'current-menu-ancestor' );
        } ).addClass( 'open' );

        // Toggle submenu.
        $mobileMenu.on( 'click', '.menu-item-has-children', function( event ) {
            event.stopPropagation();
            var $li = $( this );
            $li.addClass( 'clicked' );
            $li.toggleClass( 'open' ).children( 'ul' ).slideToggle();
            $li.siblings( '.open' ).removeClass( 'open clicked' ).children( 'ul' ).slideUp();
        } ).on( 'click', '.menu-item-has-children > .toggle', function( event ) {
            event.stopPropagation();
            event.preventDefault();
            var $li = $( this ).parent();

            $li.toggleClass( 'open' ).children( 'ul' ).slideToggle();
            $li.siblings( '.open' ).removeClass( 'open' ).children( 'ul' ).slideUp();
        } ).on( 'click', '.menu-item-has-children > a', function( event ) {
            event.stopPropagation();
        });

        // Close other panels.
        $mobileMenu.on( 'click', '[data-toggle="off-canvas"], [data-toggle="modal"]', function() {
            if ( 'mobile-menu' !== $( this ).data( 'target' ) ) {
                konte.closeModal();
                konte.closeOffCanvas();
            }
        } );
    };

    var $window = $( window ),
        $header = $( 'header#masthead' );

    if(window.matchMedia('(max-width: 767px)').matches) {
        $window.on('scroll', function () {
            var scrollPosition = $window.scrollTop();

            if (scrollPosition > 100) {
                $header.addClass('fixed');
            } else {
                $header.removeClass('fixed');
            }

            if (scrollPosition > 300) {
                $header.addClass('sticky');
            } else {
                $header.removeClass('sticky');
            }

        });
    }
} );