jQuery( document ).ready( function( $ ) {

    if ( typeof kapsula === 'undefined' ) {
        return;
    }

    const $document = $( document );

    const cart = {
        sizesSelect: '.cart .product-size select',
        shippingOptions: '#shipping_method input[type="radio"]',
        init: function() {
            this.events();
        },
        events: function() {

            const self = this;

            $document.off( 'change', 'select.shipping_method, :input[name^=shipping_method]' );

            $document.on( 'change.kapsula.sizes', self.sizesSelect, function( e ) {
                $( 'button[name="update_cart"]' ).click();
            } );

            $document.on( 'change.kapsula.shipping', self.shippingOptions, function() {
                self.sendShippingChangeRequest( $( this ) );
            } );
        },
        sendShippingChangeRequest: function( $input ) {
            $.post( kapsula.ajaxURL, {
                'nonce': kapsula.nonce,
                'action': 'change_shipping_in_cart',
                'value': $input.val()
            } );
        }
    }

    cart.init();
} );