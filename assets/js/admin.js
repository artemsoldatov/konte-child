jQuery( document ).ready( function( $ ) {

    var loading = false;

    $( '.apply-sales' ).click( function() {

        if ( loading ) {
            return;
        }

        loading = true;

        $( '.ajax-loader' ).addClass( 'loading' );

        var $button = $( this );

        $button.css( {'opacity': 0.3} );

        $.ajax( {
            method: 'POST',
            data: {
                action: 'manually_do_scheduled_sales',
                security: '123456789'
            },
            url: ajaxurl,
            success: function( responce ) {
                $button.css( {'opacity': 1} ).after( '<p>' + responce + '</p>' );
                $( '.ajax-loader' ).removeClass( 'loading' );
                loading = false;
            },
            error: function() {
                $( '.ajax-loader' ).removeClass( 'loading' );
                loading = false;
            }
        } )
    } );
} );

// Functions for managing banner settings
// /wp-admin/admin.php?page=wc-settings&tab=promo_banner
jQuery( document ).ready( function( $ ) {

    var $banner;

    $( document ).on( 'click', '.wc_button_promo_banner_image', function() {

        $banner = $( this ).closest( '.promo-banner-settings' );

        tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
        return false;
    } );

    window.send_to_editor = function( html ) {

        var imgurl = $( 'img', html ).attr( 'src' );

        $banner.find( 'button.remove_image_banner_button' ).show();
        $banner.find( '.wc_settings_promo_banner_image' ).val( imgurl );
        $banner.find( 'img.promo-banner-img' ).attr( 'src', imgurl );
        tb_remove();
    };

    $( document ).on( 'click', '.wc_remove_promo_banner_image', function() {

        $banner = $( this ).closest( '.promo-banner-settings' );

        $banner.find( '.wc_settings_promo_banner_image' ).val( '' );
        $banner.find( 'img.promo-banner-img' ).attr( 'src', '/wp-content/plugins/woocommerce/assets/images/placeholder.png' );
        $( this ).hide();
    } );

    $( '.add-promo-banner' ).click( function( e ) {

        e.preventDefault();

        var $promoSettingsAll = $( '.promo-banner-settings' ),
            $promoSettingsNew = $promoSettingsAll.first().clone();

        $promoSettingsAll.last().after( $promoSettingsNew );

        $promoSettingsNew.find( 'input' ).val( '' ).filter( '[type="checkbox"]' ).removeAttr( 'checked' );
        $promoSettingsNew.find( 'img.promo-banner-img' ).attr( 'src', '/wp-content/plugins/woocommerce/assets/images/placeholder.png' );
        $promoSettingsNew.find( 'button.remove_image_banner_button' ).remove();
    } );

    $( document ).on( 'click', '.remove-banner', function( e ) {

        e.preventDefault();

        $( this ).closest( '.promo-banner-settings' ).remove();
    } );

    // Only show the "remove image" button when needed
    $( '.category-banner-field' ).each( ( index, el ) => {
        if ( ! parseInt( $( el ).find( 'input' ).val() ) ) {
            $( el ).find( '.remove_image_banner_button' ).hide();
        }
    } );

    // Uploading files
    var file_frame,
        button;

    $( document ).on( 'click', '.upload_image_banner_button', function( event ) {

        event.preventDefault();

        button = this;

        // If the media frame already exists, reopen it.
        if ( file_frame ) {
            file_frame.open();
            return;
        }

        // Create the media frame.
        file_frame = wp.media.frames.downloadable_file = wp.media( {
            title: 'Выбрать изображение',
            button: {
                text: 'Использовать изображение'
            },
            multiple: false
        } );

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {

            let attachment = file_frame.state().get( 'selection' ).first().toJSON(),
                $wrapper = $( button ).closest( '.category-banner-field' );

            $wrapper.find( 'input' ).val( attachment.id );
            $wrapper.find( 'img' ).attr( 'src', attachment.sizes.thumbnail.url );
            $wrapper.find( '.remove_image_banner_button' ).show();
        } );

        // Finally, open the modal.
        file_frame.open();
    } );

    $( document ).on( 'click', '.remove_image_banner_button', function() {

        let $wrapper = $( this ).closest( '.category-banner-field' );

        $wrapper.find( 'img' ).attr( 'src', '/wp-content/uploads/product_images/woocommerce-placeholder-680x920.png' );
        $wrapper.find( 'input' ).val( '' );
        $wrapper.find( '.remove_image_banner_button' ).hide();
        return false;
    } );

    $( '.wc_settings_promo_banner_activate' ).each( ( index, el ) => {
        $( el ).siblings( 'input' ).val( $( el ).is( ':checked' ) ? 'on' : 'off' );
    } ).change( function() {
        $( this ).siblings( 'input' ).val( $( this ).is( ':checked' ) ? 'on' : 'off' );
    } );

    //Add select2 to Brand list in settings general options
    $('.option-tree-ui-select').select2();

    //Save brands of AJAX
    $('#save-brand-list').click(function (e) {

        e.preventDefault();

        let ajaxurl = $( this ).attr('data-page-url')+'/wp-admin/admin-ajax.php';
        let ajaxnonce = $( this ).attr('data-page-nonce');
        let select_brands = $(".select2-search-choice div").map( function( i, el ){
            return $( el ).text();
        }).get();
        let data = {
            'action': 'addselectbrands',
            'select_brands': select_brands,
            'security': ajaxnonce,
        };

        $( '.brand-save-notice' ).remove();

        $.ajax( {
            url: ajaxurl,
            data: data,
            type: 'POST',
            success: function ( data ) {

                let resultText = '';

                if ( data && data.success ) {
                    resultText = 'Список обновлён!';
                } else {
                    resultText = 'Произошла ошибка, попробуйте перезагрузить страницу и сохранить ещё раз';
                }

                $( '#brandsform' ).after( `<div class="notice brand-save-notice">${resultText}</div>` );
            }
        } );
    } );
} );