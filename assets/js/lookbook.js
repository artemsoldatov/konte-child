jQuery( document ).ready( function( $ ) {

    if ( typeof konte !== 'undefined' ) {
        /**
         * Related & ppsell products carousel.
         *
         * Added recently viewed products. Changed amount of slides per view.
         */
        konte.relatedProductsCarousel = function() {

            var $featured = $( '.wpb_wrapper .woocommerce' );

            if ( ! $featured.length ) {
                return;
            }

            var $products = $featured.find( 'ul.products' );

            $products.wrap( '<div class="swiper-container linked-products-carousel" style="opacity: 0;"></div>' );
            $products.after( '<div class="swiper-pagination"></div>' );
            $products.addClass( 'swiper-wrapper' );
            $products.find( 'li.product' ).addClass( 'swiper-slide' );

            var carousel = new Swiper( '.linked-products-carousel', {
                loop: false,
                slidesPerView: 1,
                slidesPerGroup: 1,
                spaceBetween: 30,
                speed: 800,
                watchOverflow: true,
                autoplay: {
                    delay: 5000,
                },
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                    renderBullet: function( index, className ) {
                        return '<span class="' + className + '"><span></span></span>';
                    }
                },
                on: {
                    init: function() {
                        this.$el.css( 'opacity', 1 );
                    }
                },
                breakpoints: {
                    360: {
                        spaceBetween: 10,
                        slidesPerView: 2,
                        slidesPerGroup: 2
                    },
                    768: {
                        spaceBetween: 20,
                        slidesPerView: 2,
                        slidesPerGroup: 2
                    },
                    992: {
                        slidesPerView: 3,
                        slidesPerGroup: 3
                    },
                    1200: {
                        slidesPerView: 4,
                        slidesPerGroup: 4
                    }
                }
            } );
        };
    }

} );