jQuery( document ).ready( function( $ ) {

    const $document = $( document ),
        $documentBody = $( document.body );

    const checkout = {
        currentStep: 1,
        init: function() {
            this.events();

            if ( typeof $.fn.select2 !== 'undefined' ) {
                $( '#shipping_country' ).select2();
            }
        },
        events: function() {

            $documentBody.on( 'update_checkout', () => {
                $( '.checkout-sidebar' ).block( {
                    message: null,
                    overlayCSS: {
                        background: '#fff',
                        opacity: 0.6
                    }
                } );
            } );

            $( 'form.woocommerce-checkout' ).on( 'change.kapsula.checkout', '.woocommerce-checkout-payment .payment_methods input', function() {
                $documentBody.trigger( 'update_checkout' );
            } );

            if ( $documentBody.hasClass( 'woocommerce-order-pay' ) ) {

                $documentBody.on( 'payment_method_selected', () => {

                    const paymentMethod = $( '.woocommerce-checkout input[name="payment_method"]:checked' ).attr( 'id' );

                    $.post( wc_checkout_params.ajax_url, {
                        action : 'update_order_pay_form',
                        security : wc_checkout_params.update_order_review_nonce,
                        payment_method : paymentMethod.replace( 'payment_method_', '' ),
                        path : window.location.pathname
                    }, data => {
                        if ( typeof( data.data.order_review ) !== 'undefined' ) {
                            $( '#order_review' ).html( $( data.data.order_review ).html() );
                        }
                    } );
                } );

                $documentBody.trigger( 'payment_method_selected' );
            }
        }
    }

    const formFields = {
        billingCountryField: '#billing_country',
        cityFields: '#justin_shipping_method_city, #shipping_nova_poshta_for_woocommerce_city, #billing_city',
        init: function() {
            this.$billingCountryField = $( this.billingCountryField );
            this.$cityFields = $( this.cityFields );

            this.events();

            // Move address fields after shipping methods selection
            $( '.woocommerce-checkout-review-order-table' )
                .after( $( '#justin_shipping_method_warehouse_field' ) );
        },
        events: function() {

            const self = this;

            self.$cityFields.on( 'change.kapsula.checkout', function( e ) {
                self.updateCheckoutCityFields( e, this );
            } );

            $document.on( 'change.kapsula.checkout', 'input.shipping_method', function( e ) {
                self.updateCheckoutAddressFields();
                self.updateCheckoutCityFields( e, self.$cityFields.filter( ':visible' ).get( 0 ) );
            } );

            $document.on( 'focusout.kapsula.checkout', '#shipping_city', () => $documentBody.trigger( 'update_checkout' ) );

            this.$billingCountryField.on( 'change.kapsula.checkout', () => {
                $( '[name="shipping_method[0]"]:checked' ).trigger( 'change.kapsula.checkout' );
            } );

            $( document.body ).on( 'updated_checkout', e => {
                this.shippingFieldsManipulation();
                this.updateCallbackCheckboxVisibility();
                this.updateCheckoutAddressFields();
            } );
        },
        updateCheckoutAddressFields: function() {

            let $shippingMethodField = $( '[name="shipping_method[0]"]' ),
                $addressField = $( '#billing_address_1_field' );

            if ( $shippingMethodField.filter( ':checked' ).length ) {
                $shippingMethodField = $shippingMethodField.filter( ':checked' );
            } else {
                $shippingMethodField = $shippingMethodField.first();
            }

            if ( this.$billingCountryField.val() !== 'UA' ) {
                $( '#billing_state_field' ).after( $addressField.show() );
                return;
            }

            $( 'table.woocommerce-checkout-review-order-table' ).after( $addressField );

            if ( $shippingMethodField.val().split( ':' )[0] === 'free_shipping' ) {
                $addressField.show();
            } else {
                $addressField.hide();
            }
        },
        updateCheckoutCityFields: function ( e, field ) {

            const self = this,
                $fields = this.$cityFields.not( '#' + field.getAttribute( 'id' ) );

            let value = $( field ).is( 'select' ) ? $( field ).find( 'option:selected' ).text() : $( field ).val();

            value = value.trim();

            this.$cityFields.off( 'change.kapsula.checkout' );

            $fields.each( function() {

                const $select = $( this );

                if ( $select.val() === value ) {
                    return;
                }

                if ( $select.is( '#billing_city' ) ) {
                    $select.val( value );
                } else if ( $select.is( '#justin_shipping_method_city' ) ) {

                    let found = false;

                    $select.find( 'option' ).removeAttr('selected').each( function() {
                        if ( $( this ).text() === value ) {
                            $( this ).prop( 'selected', true );
                            found = true;
                            return true;
                        }
                    } );

                    if ( e.type === 'updated_checkout' ) {
                        self.toggleShippingMethodVisibility( $( '#shipping_method li input[value^="justin_shipping_method"]' ).parent(), !found );
                    }

                    $( document.body ).off( 'updated_checkout.kapsula.shippingVisibility' ).on( 'updated_checkout.kapsula.shippingVisibility', () => {
                        self.toggleShippingMethodVisibility( $( '#shipping_method li input[value^="justin_shipping_method"]' ).parent(), !found );
                    } );

                } else if ( $select.is( '#shipping_nova_poshta_for_woocommerce_city' ) ) {
                    let options = $select.find('option'),
                        found = false;

                    $( options ).each( function() {
                        $( this ).prop( 'selected', false );
                        if ( $( this ).text() === value ) {
                            $( this ).prop( 'selected', true );
                            found = true;
                        }
                    } );

                    if ( ! found ) {
                        $select.append( '<option selected="selected">' + value + '</option>' );
                    }

                }

                $select.trigger( 'change' );
            } );

            $documentBody.trigger( 'update_checkout' );

            this.$cityFields.on( 'change.kapsula.checkout', function( e ) {
                self.updateCheckoutCityFields( e, this );
            } );
        },
        toggleShippingMethodVisibility: function( $method, visibility ) {
            $method.toggleClass( 'hidden', visibility );
        },
        shippingFieldsManipulation: function() {

            const $shippingNovaPoshtaCity = $( '#shipping_nova_poshta_for_woocommerce_city_field' ),
                $shippingNovaPoshtaWarehouse = $( '.shipping-nova-poshta-warehouse' ),
                $novaPoshtaCheckbox = $( '#shipping_method_0_shipping_nova_poshta_for_woocommerce' ),
                $billingCityField = $( '#billing_city_field' ),
                $billingStateField = $( '#billing_state_field' );

            if ( this.$billingCountryField.val() === 'UA' ) {
                $billingCityField.hide();
                $billingStateField.hide();
                if ( $novaPoshtaCheckbox.is( ':checked' ) || ( $( '[name="shipping_method[0]"]' ).length === 1 && $novaPoshtaCheckbox.length ) ) {
                    $shippingNovaPoshtaCity.show();
                    $shippingNovaPoshtaWarehouse.show();
                } else {
                    if (  $( '#justin_shipping_method_fields' ).is( ':visible' ) ) {
                        $shippingNovaPoshtaCity.hide();
                    } else {
                        $shippingNovaPoshtaCity.show();
                    }
                    $shippingNovaPoshtaWarehouse.hide();
                }
            } else {
                $billingCityField.show();
                if ( $billingStateField.find( 'input' ).attr( 'type' ) !== 'hidden' ) {
                    $billingStateField.show();
                }
                $shippingNovaPoshtaCity.hide();
                $shippingNovaPoshtaWarehouse.hide();
            }
        },
        updateCallbackCheckboxVisibility: function() {

            const $callbackCheckbox = $( '#billing_connection_no_field' );

            let $paymentMethod = $( '#payment_method_fondy' );

            if ( ! $paymentMethod.length ) {
                $paymentMethod = $( '#payment_method_platononline' );
            }

            $paymentMethod.is( ':checked' ) ? $callbackCheckbox.show() : $callbackCheckbox.hide().find( 'input' ).removeAttr( 'checked' );
        }
    }

    formFields.init();
    checkout.init();

    if ( typeof intlTelInput !== 'undefined' ) {
        intlTelInput( $( '#billing_phone' ).get( 0 ), {
            initialCountry: 'UA',
            separateDialCode: true,
            formatOnDisplay: false
        } );
    }

    if ( typeof $.fn.mask !== 'undefined' ) {
        $( '#billing_phone' ).mask( "+000000000009", {
            placeholder: "+__________"
        } );
    }

} );

(function($) {

    var Defaults = $.fn.select2.amd.require('select2/defaults');

    $.extend(Defaults.defaults, {
        dropdownPosition: 'auto'
    });

    var AttachBody = $.fn.select2.amd.require('select2/dropdown/attachBody');

    var _positionDropdown = AttachBody.prototype._positionDropdown;

    AttachBody.prototype._positionDropdown = function() {

        var $window = $(window);

        var isCurrentlyAbove = this.$dropdown.hasClass('select2-dropdown--above');
        var isCurrentlyBelow = this.$dropdown.hasClass('select2-dropdown--below');

        var newDirection = null;

        var offset = this.$container.offset();

        offset.bottom = offset.top + this.$container.outerHeight(false);

        var container = {
            height: this.$container.outerHeight(false)
        };

        container.top = offset.top;
        container.bottom = offset.top + container.height;

        var dropdown = {
            height: this.$dropdown.outerHeight(false)
        };

        var viewport = {
            top: $window.scrollTop(),
            bottom: $window.scrollTop() + $window.height()
        };

        var enoughRoomAbove = viewport.top < (offset.top - dropdown.height);
        var enoughRoomBelow = viewport.bottom > (offset.bottom + dropdown.height);

        var css = {
            left: offset.left,
            top: container.bottom
        };

        // Determine what the parent element is to use for calciulating the offset
        var $offsetParent = this.$dropdownParent;

        // For statically positoned elements, we need to get the element
        // that is determining the offset
        if ($offsetParent.css('position') === 'static') {
            $offsetParent = $offsetParent.offsetParent();
        }

        var parentOffset = $offsetParent.offset();

        css.top -= parentOffset.top
        css.left -= parentOffset.left;

        var dropdownPositionOption = this.options.get('dropdownPosition');

        if (dropdownPositionOption === 'above' || dropdownPositionOption === 'below') {

            newDirection = dropdownPositionOption;

        } else {

            if (!isCurrentlyAbove && !isCurrentlyBelow) {
                newDirection = 'below';
            }

            if (!enoughRoomBelow && enoughRoomAbove && !isCurrentlyAbove) {
                newDirection = 'above';
            } else if (!enoughRoomAbove && enoughRoomBelow && isCurrentlyAbove) {
                newDirection = 'below';
            }

        }

        if (newDirection == 'above' ||
            (isCurrentlyAbove && newDirection !== 'below')) {
            css.top = container.top - parentOffset.top - dropdown.height;
        }

        if (newDirection != null) {
            this.$dropdown
                .removeClass('select2-dropdown--below select2-dropdown--above')
                .addClass('select2-dropdown--' + newDirection);
            this.$container
                .removeClass('select2-container--below select2-container--above')
                .addClass('select2-container--' + newDirection);
        }

        this.$dropdownContainer.css(css);

    };

})(window.jQuery);


jQuery(document).ready(function() {
    $("#billing_country").select2({
        dropdownPosition: 'below'
    });
});
