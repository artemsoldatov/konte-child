<div class="col-md-3 checkout-sidebar">
    <div class="checkout-side-info">

        <div class="checkout-product-item-wrap">
            <?php

            foreach ( WC()->cart->get_cart_contents() as $content ) {

                if ( ! isset( $content['data'] ) || ! is_a( $content['data'], '\WC_Product' ) ) {
                    continue;
                }

                $product = $content['data'];

                ?>

                <div class="checkout-product-item">
                    <div class="checkout-product-image"><?php echo $product->get_image( 'thumbnail' ); ?></div>
                    <div class="checkout-product-description">
                        <div class="checkout-product-title"><?php echo $product->get_title(); ?></div>
                        <div class="checkout-product-attributes"><?php _e( 'Размер:', 'kapsula' ); ?>
                            <span><?php echo $product->get_attribute( 'size' ); ?></span>
                        </div>
                        <div class="checkout-product-price"><?php echo $product->get_price_html(); ?></div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

        <?php Konte_WooCommerce_Template_Checkout::checkout_coupon_form(); ?>

        <hr>

        <div class="checkout-side-item">
            <div class="checkout-side-subtotal-title -label"><?php _e( 'Сумма заказа', 'kapsula' ); ?></div>
            <div class="checkout-side-subtotal -value"><?php wc_cart_totals_subtotal_html(); ?></div>
        </div>

        <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

            <div class="checkout-side-item checkout-side-shipping">
                <?php $shipping_total = WC()->cart->get_shipping_total(); ?>
                <div class="-label">
                    <?php _e( 'Shipping:', 'woocommerce' ); ?>
                </div>
                <div class="-value">
                    <?php echo $shipping_total ? wc_price( $shipping_total ) : __( 'Бесплатно', 'kapsula' ); ?>
                </div>
            </div>

        <?php endif; ?>

        <hr>

        <div class="checkout-side-item -total">
            <div class="checkout-side-total-title -label"><?php esc_html_e( 'Total', 'woocommerce' ); ?></div>
            <div class="checkout-side-total order-total -value"><?php wc_cart_totals_order_total_html(); ?></div>
        </div>

        <?php

        if ( isset( $_POST['payment_method'] )
            && in_array( $_POST['payment_method'], [ 'platononline', 'fondy' ] )
            && WC()->customer->get_billing_country() == 'UA' ) :

            ?>

            <span class="checkout-side-discount">
                <?php _e( 'Скидка 2%', 'kapsula' ); ?>
            </span>

        <?php endif; ?>

    </div>
</div>
