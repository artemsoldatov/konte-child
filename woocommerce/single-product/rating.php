<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

global $product;

// Remove this variable when WooCommerce 3.7 release. Use function instead.
$review_ratings_enabled = function_exists( 'wc_review_ratings_enabled' ) ? wc_review_ratings_enabled() : ( 'yes' === get_option( 'woocommerce_enable_reviews' ) && 'yes' === get_option( 'woocommerce_enable_review_rating' ) );

if ( ! $review_ratings_enabled || ! is_product() ) {
    return;
}

$rating_count = $product->get_rating_count();
$review_count = $product->get_review_count();
$average = $product->get_average_rating();

?>

<div class="woocommerce-product-rating">

    <?php if ( $rating_count > 0 ) : ?>

        <?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>
        <?php if ( comments_open() ) : ?>
            <?php //phpcs:disable ?>
            <a href="#reviews" class="woocommerce-review-link" rel="nofollow"><?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'konte' ), '<span class="count">' . esc_html( $review_count ) . '</span>' ); ?></a>
            <?php // phpcs:enable ?>
        <?php endif ?>

    <?php else : ?>

        <?php if ( comments_open() ) : ?>

            <a href="#reviews" class="woocommerce-review-link" rel="nofollow">
                <div class="star-rating" role="img">
                    <span class="max-rating rating-stars">
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                    </span>
                    <span class="user-rating rating-stars" style="width:0">
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                        <span class="svg-icon star-icon"><svg><use xlink:href="#star"></use></svg></span>
                    </span>
                </div>

                <?php //phpcs:disable ?>
                    <?php _e( 'Оставить отзыв', 'kapsula' ); ?>
                <?php // phpcs:enable ?>
            </a>
        <?php endif ?>

    <?php endif; ?>

</div>

