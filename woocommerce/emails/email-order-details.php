<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see           http://docs.woothemes.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates/Emails
 * @version       2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>

<?php if ( ! $sent_to_admin ) : ?>
    <h2><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>
<?php else : ?>
    <h2>
        <a class="link" href="<?php echo esc_url( admin_url( 'post.php?post=' . $order->get_id() . '&action=edit' ) ); ?>"><?php printf( __( 'Order #%s', 'woocommerce' ), $order->get_order_number() ); ?></a>
        (<?php printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order->get_date_created() ) ), date_i18n( wc_date_format(), strtotime( $order->get_date_created() ) ) ); ?>)
    </h2>
<?php endif; ?>

<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
    <thead>
    <tr>
        <th class="td" scope="col" style="text-align:left;"><?php _e( 'Товар', 'woocommerce' ); ?></th>
        <th class="td" scope="col" style="text-align:left;"><?php _e( 'Количество', 'woocommerce' ); ?></th>
        <th class="td" scope="col" style="text-align:left;"><?php _e( 'Цена', 'woocommerce' ); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php echo wc_get_email_order_items(
        $order, [
        'show_sku' => true,
        'show_image' => false,
        '$image_size' => [ 32, 32 ],
        'plain_text' => $plain_text
    ]
    );

    ?>
    </tbody>
    <tfoot>
    <?php

    if ( get_post_meta( $order->get_id(), '_shipping_consultation', true ) ) {
        ?>
        <tr>
            <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Требуется консультация:
            </th>
            <td class="td" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Да
            </td>
        </tr>
        <?php
    }
    if ( get_post_meta( $order->get_id(), '_shipping_try_it_on', true ) ) {
        ?>
        <tr>
            <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Требуется примерка:
            </th>
            <td class="td" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Да
            </td>
        </tr>
        <?php
    }
    if ( get_post_meta( $order->get_id(), '_shipping_present', true ) ) {
        ?>
        <tr>
            <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Подарок - подписать открытку:
            </th>
            <td class="td" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Да
            </td>
        </tr>
        <?php
    }
    if ( get_post_meta( $order->get_id(), '_shipping_present_text', true ) ) {
        ?>
        <tr>
            <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Текст открытки:
            </th>
            <td class="td" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>"><?php echo get_post_meta( $order->get_id(), '_shipping_present_text', true ); ?></td>
        </tr>
        <?php
    }
    $phone = get_post_meta( $order->get_id(), '_billing_connection_phone', true ) ? 'Телефон' : '';
    $viber = get_post_meta( $order->get_id(), '_billing_connection_viber', true ) ? 'Viber' : '';
    $email = get_post_meta( $order->get_id(), '_billing_connection_email', true ) ? 'Email' : '';
    $no_connection = get_post_meta( $order->get_id(), '_billing_connection_no', true ) ? 'Можно не перезванивать' : '';
    $connection_types = [ $phone, $viber, $email, $no_connection ];
    $connection_types = array_filter(
        $connection_types, function ( $value ) {
        return $value !== '';
    }
    );
    $connection_types_text = implode( ", ", $connection_types );

    if ( $connection_types_text != '' ) {
        ?>
        <tr>
            <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>">Предпочитаемый способ связи:
            </th>
            <td class="td" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>"><?php echo $connection_types_text; ?></td>
        </tr>
        <?php
    }

    ?>
    <tr>
        <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
            echo 'border-top-width: 4px;';
        } ?>">Заметки пользователя:
        </th>
        <td class="td" style="text-align:left; <?php if ( isset( $i ) && $i === 1 ) {
            echo 'border-top-width: 4px;';
        } ?>"><?php echo get_post( $order->get_id() )->post_excerpt; ?></td>
    </tr>
    <?php

    if ( $totals = $order->get_order_item_totals() ) {
        $i = 0;
        foreach ( $totals as $total ) {
            $i++;
            ?>
            <tr>
            <th class="td" scope="row" colspan="2" style="text-align:left; <?php if ( $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>"><?php echo $total['label']; ?></th>
            <td class="td" style="text-align:left; <?php if ( $i === 1 ) {
                echo 'border-top-width: 4px;';
            } ?>"><?php echo $total['value']; ?></td>
            </tr><?php
        }
    }
    ?>
    </tfoot>
</table>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
