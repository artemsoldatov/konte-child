<?php
/**
 * Cart totals
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-totals.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see           https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       2.3.6
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

?>
<div class="cart_totals <?php echo ( WC()->customer->has_calculated_shipping() ) ? 'calculated_shipping' : ''; ?>">

    <?php do_action( 'woocommerce_before_cart_totals' ); ?>

    <table cellspacing="0" class="shop_table shop_table_responsive">

        <tr class="cart-subtotal">
            <th><?php _e( 'Сумма заказа', 'kapsula' ); ?></th>
            <td data-title="<?php esc_attr_e( 'Subtotal', 'konte' ); ?>"><?php wc_cart_totals_subtotal_html(); ?></td>
        </tr>

        <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
            <tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                <th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
                <td data-title="<?php echo esc_attr( wc_cart_totals_coupon_label( $coupon, false ) ); ?>"><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
            </tr>
        <?php endforeach; ?>

        <?php $shipping = WC()->customer->get_shipping_country(); ?>

        <tr class="shipping">
            <th><?php _e( 'Shipping', 'woocommerce' ); ?></th>
            <td>
                <ul id="shipping_method" class="woocommerce-shipping-methods">
                    <li>
                        <input type="radio" id="shipping-method-ukraine" name="shipping_method_cart" value="UA" class="shipping_method" <?php checked( $shipping, 'UA' ); ?>>
                        <label for="shipping-method-ukraine"><?php _e( 'Бесплатно по Украине', 'kapsula' ); ?></label>
                    </li>
                    <li>
                        <input type="radio" id="shipping-method-international" name="shipping_method_cart" value="International" class="shipping_method" <?php echo $shipping != '' && $shipping != 'UA' ? 'checked="checked"' : ''; ?>>
                        <label for="shipping-method-international"><?php _e( 'Международная доставка (стоимость зависит от страны назначения)', 'kapsula' ); ?></label>
                    </li>
                </ul>
            </td>
        </tr>
    </table>

    <table cellspacing="0" class="shop_table shop_table_responsive order-total-table">
        <?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>


        <?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>
    </table>

    <div class="wc-proceed-to-checkout">
        <?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
    </div>

    <?php do_action( 'woocommerce_after_cart_totals' ); ?>

</div>
