<?php
/**
 * Class for working with media files.
 *
 * @author     artem <artem@dev.com>
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Media
 *
 * @package dev\Kapsula
 */
class Media {

    public function __construct() {
    }

    /**
     * All needed for theme mime types.
     *
     * @param $mimes
     *
     * @return mixed
     */
    public function allow_mime_types( $mimes ) {

        $mimes['svg'] = 'image/svg+xml';

        return $mimes;
    }

    /**
     * Svg files may have mime type 'image/svg'. Need to change it to 'image/svg+xml' to allow using in media.
     *
     * @param        $data
     * @param        $file
     * @param        $filename
     * @param        $mimes
     * @param string $real_mime
     *
     * @return mixed
     */
    function fix_svg_mime_type( $data, $file, $filename, $mimes, $real_mime = '' ) {

        if ( in_array( $real_mime, [ 'image/svg', 'image/svg+xml' ] ) && current_user_can( 'manage_options' ) ) {
            $data['ext'] = 'svg';
            $data['type'] = 'image/svg+xml';
        }

        return $data;
    }
}
