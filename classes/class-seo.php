<?php
/**
 * Class for SEO changes.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class SEO
 *
 * @package dev\Kapsula\Theme
 */
class SEO {

    /**
     * Add alt and title attributes to images
     *
     * @param $attr
     *
     * @return mixed
     */
    public function add_kapsula_image_attributes( $attr ) {

        global $product, $counter;

        if ( $product && is_a( $product, 'WC_Product' ) ) {
            $number = $counter ? $counter : 1;
            // translators: %1$s - название продукта, %2$s - артикул, %3$d - порядковый номер
            $attr['title'] = sprintf( __( '%1$s %2$s, фото %3$d', 'kapsula' ), $product->get_name(), $product->get_sku(), $number );
            $attr['alt'] = $attr['title'] . __( ' - в интернет магазине KAPSULA' );
        }

        return $attr;
    }

    /**
     * Add headers for static pages
     */
    function set_kapsula_seo_headers() {

        global $post;

        if ( function_exists( 'WC' ) && ( is_cart() || is_checkout() ) ) {
            return;
        }

        if ( function_exists( 'soow_is_wishlist' ) && soow_is_wishlist() ) {
            header( 'Cache-Control: no-cache, no-store, must-revalidate' );
            header( 'Pragma: no-cache' );
            header( 'Expires: 0' );

            return;
        }

        header( 'Cache-Control: must-revalidate, max-age=86400' );
        header( 'Expires: ' . gmdate( 'D, d M Y H:m:i', time() + DAY_IN_SECONDS ) . ' GMT' );
        header( 'Vary: User-Agent' );
        header( 'Etag: "' . md5( time() ) . '"' );
        if ( isset( $post ) ) {
            header( 'Last-Modified: ' . gmdate( 'D, d M Y H:m:i', strtotime( $post->post_modified ) - 10800 ) . ' GMT' );
        }
        header_remove( 'Pragma' );
    }
}
