<?php
/**
 * Autoloader class for Kapsula child theme.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Autoloader class for Kapsula child theme
 *
 * @version 1.0.0
 * @since 1.0.0
 * @package dev\Kapsula\Theme
 */
class Autoloader {

    /**
     * Custom class map
     *
     * @var array
     */
    private static $class_map = [];

    /**
     * Base directory
     *
     * @var string $dir
     */
    private static $dir;

    /**
     * AutoLoader constructor.
     */
    public function __construct() {

        self::$dir = get_stylesheet_directory() . '/classes';
        $this->register_auto_loader();
    }

    /**
     * Register auto loader
     */
    public function register_auto_loader() {
        spl_autoload_register( [ __CLASS__, 'autoload' ] );
    }

    /**
     * Implementations of PSR-4
     *
     * @link: https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-4-autoloader-examples.md
     * @var string $class_name
     */
    public static function autoload( $class_name ) {

        $prefix = __NAMESPACE__;
        // does the class use the namespace prefix?
        $len = strlen( $prefix );

        if ( strncmp( $prefix, $class_name, $len ) !== 0 ) {
            // no, move to the next registered autoloader
            return;
        }

        // trying map autoloader first
        if ( isset( self::$class_map[ $class_name ] ) ) {
            include( self::$dir . self::$class_map[ $class_name ] );
        }

        $path_array = explode( '\\', $class_name );
        // get the relative class
        $relative_class = end( $path_array );

        // get the path
        $relative_path = substr( substr( $class_name, $len ), 0, -strlen( $relative_class ) );

        // replace the namespace prefix with the base directory, replace namespace
        // separators with directory separators in the relative class name, append
        // with .php
        $file = self::$dir . strtolower( str_replace( '\\', '/', $relative_path . 'class-' . str_replace( '_', '-', $relative_class ) ) . '.php' );

        // if the file exists, require it
        if ( file_exists( $file ) ) {
            require $file;
        }
    }
}

new Autoloader();


