<?php
/**
 * Main theme class for adding events.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Theme main class
 *
 * @version    1.0.0
 * @since      1.0.0
 * @subpackage dev\Kapsula\Theme
 */
class Theme {

    /**
     * @var null|Theme $instance for theme functionality
     */
    private static $instance = null;

    /**
     * Theme constructor.
     *
     * Should contain all actions and filters.
     * Need to have them all in one place to be easily managed.
     */
    private function __construct() {

        add_action( 'wp', [ $this, 'on_wp_actions' ] );

        $assets = new Assets();
        add_action( 'wp_enqueue_scripts', [ $assets, 'enqueue_child_assets' ] );
        add_action( 'admin_enqueue_scripts', [ $assets, 'enqueues_admin_assets' ] );
        add_action( 'login_enqueue_scripts', [ $assets, 'enqueues_login_assets' ] );
        add_action( 'wp_footer', [ $assets, 'add_bitrix_form_code' ] );
        add_action( 'wp_print_scripts', [ $assets, 'disable_scripts' ] );

        $meta_fields = new Meta_Fields();
        add_filter( 'rwmb_meta_boxes', [ $meta_fields, 'register_meta_boxes' ] );

        $seo = new SEO();
        add_filter( 'wp_get_attachment_image_attributes', [ $seo, 'add_kapsula_image_attributes' ] );
        add_action( 'template_redirect', [ $seo, 'set_kapsula_seo_headers' ], 99 );
        // Remove shortlinks from header for SEO
        remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
        remove_action( 'template_redirect', 'wp_shortlink_header', 11 );

        $performance = new Performance();
        add_filter( 'heartbeat_settings', [ $performance, 'set_heartbeat_timeout' ], 50 );

        $product = new Woocommerce\Product();
        add_action( 'init', [ $product, 'add_characteristics_taxonomy' ] );
        add_action( 'woocommerce_single_product_summary', [ $product, 'show_delivery_time' ], 16 );
        add_action( 'woocommerce_single_product_summary', [ $product, 'add_form_ask_question' ], 18 );
        add_action( 'woocommerce_product_meta_start', [ $product, 'add_product_meta_start' ] );
        add_action( 'woocommerce_product_meta_end', [ $product, 'add_product_meta' ] );
        // Do not show quantity inputs
        add_filter( 'woocommerce_quantity_input_max', '__return_one', 99 );
        // Do not show reset link as we will have only size options
        add_filter( 'woocommerce_reset_variations_link', '__return_empty_string', 99 );
        add_filter( 'woocommerce_product_single_add_to_cart_text', [ $product, 'change_woocommerce_product_single_add_to_cart_text' ] );
        add_filter( 'term_links-brand', [ $product, 'wrap_term_links' ] );
        add_filter( 'term_links-product_cat', [ $product, 'wrap_term_links' ] );
        add_filter( 'woocommerce_product_reviews_tab_title', [ $product, 'add_review_rating_to_title' ] );
        add_action( 'woocommerce_after_single_product', [ $product, 'show_recently_viewed_products' ], 25 );
        add_action( 'wpla_product_has_changed', [ $product, 'run_woocommerce_sync' ] );
        add_filter( 'woocommerce_dropdown_variation_attribute_options_args', [ $product, 'set_selected_attribute_options' ] );
        add_filter( 'woocommerce_available_variation', [ $product, 'add_variation_args' ], 10, 3 );
        add_filter( 'woocommerce_get_price_html', [ $product, 'add_sale_date_end' ], 20, 2 );
        add_filter( 'woocommerce_single_product_summary', [ $product, 'add_price_attribute_for_pixel' ], 1 );
        add_filter( 'konte_woocommerce_product_quickview_summary', [ $product, 'add_price_attribute_for_pixel' ], 1 );
        add_filter( 'woocommerce_product_related_posts_query', [ $product, 'product_related_posts_query' ], 10, 3 );
        add_filter( 'woocommerce_product_backorders_allowed', '__return_false' );
        add_filter( 'woocommerce_get_children', [ $product, 'show_all_children_on_admin' ], 10, 3 );

        $brands = new Woocommerce\Brands();
        add_filter( 'single_term_title', [ $brands, 'change_brands_term_title' ] );
        add_shortcode( 'kapsula_brands', [ $brands, 'get_kapsula_brands' ] );
        add_action( 'save_post', [ $brands, 'save_brand_category_size_guide' ], 99, 2 );
        add_action( 'init', [ $brands, 'register_brands_for_size_guide' ] );
        add_filter( 'get_post_metadata', [ $brands, 'get_brand_size_guide' ], 10, 3 );
        add_action( 'admin_menu', [ $brands, 'register_brand_list_page' ], 99 );
        //Add brands function AJAX
        if ( wp_doing_ajax() ) {
            add_action( 'wp_ajax_addselectbrands', [ $brands, 'add_select_brands' ] );
            add_action( 'wp_ajax_nopriv_addselectbrands', [ $brands, 'add_select_brands' ] );
        }

        $promo_banner = new Woocommerce\Promo_Banner();
        add_filter( 'woocommerce_settings_tabs_array', [ $promo_banner, 'add_settings_tab' ], 100 );
        add_action( 'woocommerce_settings_tabs_promo_banner', [ $promo_banner, 'promo_banner_content' ] );
        add_action( 'woocommerce_update_options_promo_banner', [ $promo_banner, 'promo_banner_update' ] );
        add_action( 'woocommerce_admin_field_promo_banner_settings', [ $promo_banner, 'promo_banner_settings' ] );
        add_filter( 'woocommerce_admin_settings_sanitize_option_promo_banner_settings', [ $promo_banner, 'update_option_promo_banner_settings' ] );
        add_action( 'admin_print_scripts', [ $promo_banner, 'promo_banner_scripts' ] );
        add_action( 'admin_print_styles', [ $promo_banner, 'promo_banner_styles' ] );
        add_action( 'product_cat_add_form_fields', [ $promo_banner, 'add_category_banner' ] );
        add_action( 'product_brand_add_form_fields', [ $promo_banner, 'add_category_banner' ] );
        add_action( 'product_cat_edit_form_fields', [ $promo_banner, 'edit_category_banner' ], 11 );
        add_action( 'product_brand_edit_form_fields', [ $promo_banner, 'edit_category_banner' ], 11 );
        add_action( 'created_term', [ $promo_banner, 'save_category_fields' ], 10, 3 );
        add_action( 'edit_term', [ $promo_banner, 'save_category_fields' ], 10, 3 );
        add_action( 'konte_after_header', [ $promo_banner, 'show_category_promo_banner' ] );
        // Remove archive description as we will show it in banner section
        remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );

        $woocommerce = new Woocommerce\Woocommerce();
        add_filter( 'woocommerce_get_price_html', [ $woocommerce, 'add_sale_percent_text' ], 99, 2 );
        add_filter( 'woocommerce_variable_sale_price_html', [ $woocommerce, 'add_sale_percent_text' ], 99, 2 );
        add_filter( 'woocommerce_sale_price_html', [ $woocommerce, 'add_sale_percent_text' ], 99, 2 );
        add_filter( 'woocommerce_shop_loop_item_title', [ $woocommerce, 'add_product_sizes_html' ], 5 );
        add_action( 'woocommerce_before_shop_loop_item', [ $woocommerce, 'add_wishlist_button' ], 15 );
        add_filter( 'woocommerce_checkout_fields', [ $woocommerce, 'change_checkout_fields' ], 40 );
        add_filter( 'woocommerce_shipping_fields', [ $woocommerce, 'override_shipping_fields' ], 99 );
        if ( function_exists( 'add_kapsula_showroom_label' ) ) {
            add_action( 'woocommerce_after_shop_loop_item', 'add_kapsula_showroom_label' );
        }
        add_action( 'woocommerce_before_checkout_form', [ $woocommerce, 'checkout_form_col_open' ], 11 );
        add_action( 'woocommerce_after_checkout_form', [ $woocommerce, 'checkout_form_col_close' ], 98 );
        add_action( 'woocommerce_after_checkout_form', [ $woocommerce, 'show_checkout_products' ], 99 );
        add_action( 'woocommerce_checkout_before_order_review', [ $woocommerce, 'add_checkout_order_review_titles' ] );
        add_action( 'woocommerce_checkout_after_order_review', [ $woocommerce, 'add_checkout_order_review_buttons' ] );
        add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' );
        add_filter( 'woocommerce_form_field', [ $woocommerce, 'change_form_field' ], 20, 3 );
        add_filter( 'woocommerce_show_page_title', [ $woocommerce, 'show_page_title' ] );
        add_filter( 'posts_clauses', [ 'dev\Kapsula\Theme\Woocommerce\Woocommerce', 'filter_in_stock_variations' ], 99, 2 );
        add_action( 'woocommerce_admin_order_totals_after_discount', [ $woocommerce, 'show_discounts_in_admin' ], 10, 1 );
        add_action( 'woocommerce_checkout_create_order_line_item', [ $woocommerce, 'add_line_item_regula_price' ], 10 );
        add_action( 'woocommerce_admin_order_item_values', [ $woocommerce, 'woocommerce_admin_order_item_values' ], 10, 2 );
        add_action( 'woocommerce_admin_order_item_headers', [ $woocommerce, 'woocommerce_admin_order_item_headers' ], 10, 1 );
        add_action( 'woocommerce_checkout_process', [ $woocommerce, 'checkout_custom_validation' ] );
        add_filter( 'posts_where', [ $woocommerce, 'sale_percent_price_filter' ], 998, 2 );
        add_filter( 'posts_join', [ $woocommerce, 'join_in_showroom' ], 10, 2 );
        add_filter( 'posts_where', [ $woocommerce, 'where_in_showroom' ], 10, 2 );
        add_filter( 'woocommerce_update_order_review_fragments', [ $woocommerce, 'add_order_review_fragments' ] );
        add_filter( 'woocommerce_checkout_registration_required', '__return_true' );
        add_action( 'pre_get_posts', [ $woocommerce, 'add_vars_to_request' ] );
        add_filter( 'woocommerce_checkout_get_value', [ $woocommerce, 'get_custom_form_values_for_checkout' ], 10, 2 );
        add_filter( 'woocommerce_catalog_orderby', [ $woocommerce, 'change_orderby_catalog_filter' ] );
        add_filter( 'woocommerce_default_catalog_orderby', [ $woocommerce, 'set_default_orderby' ] );
        add_filter( 'woocommerce_product_query_tax_query', [ $woocommerce, 'add_pa_taxonomies_to_request' ] );
        add_action( 'woocommerce_product_duplicate', [ $woocommerce, 'copy_product_taxonomies_on_duplication' ], 10, 2 );
        add_action( 'pre_get_posts', [ $woocommerce, 'add_search_by_id' ], 1 );
        add_action( 'wp', [ $woocommerce, 'catalog_start_category_cookie' ] );

        if ( wp_is_mobile() ) {
            add_action( 'konte_woocommerce_products_toolbar', [ 'Konte_WooCommerce_Template_Catalog', 'products_filter' ], 10 );
        } else {
            add_action( 'konte_woocommerce_products_toolbar', [ $woocommerce, 'add_remove_filters_wrapper' ], 60 );
        }

        $nova_poshta = new Woocommerce\Nova_Poshta();
        add_filter( 'shipping_nova_poshta_for_woocommerce_default_city_id', [ $nova_poshta, 'get_customer_city_id' ], 20, 2 );
        add_action( 'woocommerce_review_order_after_shipping', [ $nova_poshta, 'show_nova_poshta_branch_select' ] );
        add_filter( 'shipping_nova_poshta_for_woocommerce_default_city_id', [ $nova_poshta, 'return_chosen_city_id' ] );
        add_filter( 'shipping_nova_poshta_for_woocommerce_default_warehouse_id', [ $nova_poshta, 'return_chosen_warehouse_id' ], 15 );
        add_filter( 'woocommerce_form_field_args', [ $nova_poshta, 'clear_default_warehouse' ], 10, 3 );
        remove_class_filter( 'woocommerce_after_shipping_rate', 'Nova_Poshta\Core\Checkout', 'fields' );
        remove_class_filter( 'woocommerce_checkout_process', 'Nova_Poshta\Core\Checkout', 'validate' );
        add_filter( 'woocommerce_checkout_process', [ $nova_poshta, 'validate_branch' ] );

        add_action( 'widgets_init', [ __NAMESPACE__ . '\Widgets\Products_Filter_Widget', 'init_widget' ] );

        $cart = new Woocommerce\Cart();
        add_action( 'wp_loaded', [ $cart, 'change_size_in_cart' ], 20 );
        add_action( 'wp_ajax_change_size_in_cart', [ $cart, 'change_size_in_cart' ], 20 );
        add_action( 'wp_ajax_nopriv_change_size_in_cart', [ $cart, 'change_size_in_cart' ], 20 );
        add_action( 'woocommerce_after_cart_item_name', [ $cart, 'add_in_showroom_label' ], 15 );
        add_action( 'woocommerce_after_cart_item_name', [ $cart, 'add_wishlist_button_to_cart' ], 20 );
        add_action( 'woocommerce_proceed_to_checkout', [ $cart, 'add_back_to_shop_button' ], 5 );
        add_filter( 'woocommerce_widget_cart_item_quantity', [ $cart, 'change_widget_cart_quantity_block' ], 20, 3 );
        add_filter( 'woocommerce_is_sold_individually', '__return_true' );

        $payments = new Woocommerce\Payments();
        add_action( 'woocommerce_saved_order_items', [ $payments, 'woocommerce_before_save_order_items' ], 10, 2 );
        add_action( 'woocommerce_cart_calculate_fees', [ $payments, 'woocommerce_custom_surcharge' ] );
        add_action( 'woocommerce_checkout_update_order_meta', [ $payments, 'remove_html_for_payment_method' ], 20 );
        // Keep this for change price in checkout when platononline payment selected
        // add_filter( 'woocommerce_update_order_review_fragments', [ $payments, 'update_order_review_custom' ] );
        add_action( 'wp_ajax_update_order_pay_form', [ $payments, 'update_order_pay_form' ] );
        add_action( 'wp_ajax_nopriv_update_order_pay_form', [ $payments, 'update_order_pay_form' ] );
        add_action( 'woocommerce_before_pay_action', [ $payments, 'manipulate_order_discount_online_payment' ] );
        add_action( 'woocommerce_thankyou', [ $payments, 'manipulate_order_discount_online_payment' ], 10, 1 );
        add_filter( 'woocommerce_available_payment_gateways', [ $payments, 'filter_available_payment_gateways' ] );
        add_filter( 'woocommerce_checkout_update_order_review', [ $payments, 'change_requested_payment_gateway' ] );
        add_action( 'woocommerce_admin_order_data_after_billing_address', [ $payments, 'show_choosen_custom_billing_fields_in_admin' ], 10, 1 );
        add_filter( 'woocommerce_gateway_description', [ $payments, 'change_gateway_description' ], 99, 2 );
        add_filter( 'woocommerce_gateway_title', [ $payments, 'change_gateway_title' ], 10, 2 );

        $shipping = new Woocommerce\Shipping();
        add_filter( 'woocommerce_shipping_packages', [ $shipping, 'filter_shipping_methods' ] );
        add_action( 'woocommerce_admin_order_data_after_shipping_address', [ $shipping, 'show_choosen_custom_shipping_fields_in_admin' ], 10, 1 );
        add_action( 'wp_ajax_change_shipping_in_cart', [ $shipping, 'change_shipping_country' ] );
        add_action( 'wp_ajax_nopriv_change_shipping_in_cart', [ $shipping, 'change_shipping_country' ] );
        add_filter( 'woocommerce_get_country_locale', [ $shipping, 'set_country_locale_rules' ], 99 );
        add_filter( 'woocommerce_get_country_locale_default', [ $shipping, 'set_default_locale_rules' ] );
        add_action( 'init', [ $shipping, 'set_default_country_locale' ], 9 );
        add_filter( 'woocommerce_shipping_rate_label', [ $shipping, 'change_shipping_labels' ] );
        add_action( 'woocommerce_checkout_create_order', [ $shipping, 'set_selected_shipping_method_to_order' ], 99 );

        $product_video = new Woocommerce\Product_Video();
        add_action( 'add_meta_boxes', [ $product_video, 'add_meta_box' ] );
        add_action( 'save_post', [ $product_video, 'save_product_video' ], 1, 2 );
        add_action( 'woocommerce_before_shop_loop_item_title', [ $product_video, 'add_video_to_catalog_product' ], 15 );
        add_action( 'woocommerce_product_thumbnails', [ $product_video, 'add_product_video' ], 10 );

        $mobile = new Mobile();
        add_filter( 'the_posts', [ $mobile, 'sort_for_mobile_catalog' ], 99 );
        add_filter( 'woocommerce_post_class', [ $mobile, 'add_catalog_post_class' ], 10, 2 );

        $media = new Media();
        add_filter( 'upload_mimes', [ $media, 'allow_mime_types' ] );
        add_filter( 'wp_check_filetype_and_ext', [ $media, 'fix_svg_mime_type' ], 10, 5 );

        $parent_theme = new Parent_Theme();
        add_filter( 'konte_prebuild_header', [ $parent_theme, 'add_currency_to_header' ], 10, 2 );
        add_filter( 'konte_content_container_class', [ $parent_theme, 'add_fluid_container_class' ], 25 );
        add_action( 'wp_head', [ $parent_theme, 'add_head_tags' ], 99 );
        add_filter( 'konte_social_media', [ $parent_theme, 'add_social_items' ] );
        add_filter( 'konte_get_option', [ $parent_theme, 'replace_option_placeholders' ], 10, 2 );

        $currency = new Currency();
        add_filter( 'woocommerce_after_order_notes', [ $currency, 'add_order_currency_field' ], 99 );
        add_action( 'woocommerce_checkout_update_order_meta', [ $currency, 'save_order_currency' ], 10, 2 );

        add_filter( 'woocommerce_create_order', [ $currency, 'recalc_order' ], 99 );
        add_filter( 'woocommerce_order_get_currency', [ $currency, 'change_currency' ], 10, 2 );

        add_filter( 'woocommerce_order_formatted_line_subtotal', [ $currency, 'filter_order_item_subtotal' ], 10, 3 );
        add_filter( 'woocommerce_order_subtotal_to_display', [ $currency, 'filter_order_subtotal' ], 20, 3 );
        add_filter( 'woocommerce_order_get_total_discount', [ $currency, 'filter_order_discount' ], 20, 2 );
        add_filter( 'woocommerce_order_shipping_to_display', [ $currency, 'filter_order_shipping' ], 20, 3 );
        add_filter( 'woocommerce_get_formatted_order_total', [ $currency, 'filter_order_total' ], 20, 2 );
        add_filter( 'woocommerce_order_item_get_total', [ $currency, 'filter_order_discount' ], 20, 2 );

        // WPBakery
        new WPBakery\Home_Video_Item( 'video_item' );
        new WPBakery\Team_Member( 'team_member' );

        if ( function_exists( 'vc_manager' ) ) {
            vc_manager()->disableUpdater();
        }
    }

    public function on_wp_actions() {

        $product = new Woocommerce\Product();
        add_filter( 'woocommerce_product_tabs', [ $product, 'set_product_tabs' ], 98 );
        remove_shortcode( 'wrvp_recently_viewed_products' );
        add_shortcode( 'wrvp_recently_viewed_products', [ $product, 'recently_viewed_products' ] );
        // Re-order reviews
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15 );
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
        add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 32 );
        // Re-order the description.
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 7 );
        add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 35 );
        remove_action( 'konte_woocommerce_product_quickview_summary', 'woocommerce_template_single_excerpt', 20 );
        add_action( 'konte_woocommerce_product_quickview_summary', 'woocommerce_template_single_excerpt', 55 );
        remove_action( 'konte_woocommerce_after_product_quickview_summary', [ 'Konte_WooCommerce_Template_Product', 'add_to_wishlist_button' ], 10 );
        remove_action( 'woocommerce_single_product_summary', [ 'Konte_WooCommerce_Template_Product', 'add_to_wishlist_button' ], 34 );
        remove_action( 'woocommerce_after_add_to_cart_button', [ 'Soo_Wishlist_Frontend', 'single_product_button' ] );

        $woocommerce = new Woocommerce\Woocommerce();
        remove_action( 'woocommerce_before_checkout_form', [ 'Konte_WooCommerce_Template_Checkout', 'checkout_coupon_form' ], 15 );
        remove_filter( 'woocommerce_widget_cart_item_quantity', [ 'Konte_WooCommerce_Template_Cart', 'widget_cart_item_quantity' ], 10 );
        add_action( 'woocommerce_after_add_to_cart_button', [ $woocommerce, 'add_wishlist_button' ] );

        remove_action( 'konte_after_header', 'konte_campaign_bar', 10 );
        if ( ! isset( $_COOKIE['promo_disabled'] ) ) {
            add_action( 'konte_before_site', 'konte_campaign_bar', 10 );
        }

    }

    /**
     * Close cloning functionality.
     */
    final public function __clone() {
        trigger_error( 'Singleton. No cloning allowed!', E_USER_ERROR );
    }

    /**
     * Close serialize functionality.
     */
    final public function __wakeup() {
        trigger_error( 'Singleton. No cloning allowed!', E_USER_ERROR );
    }

    /**
     * Initialize theme.
     *
     * @return Theme
     */
    public static function init() {
        return is_null( self::$instance ) ? self::$instance = new self() : self::$instance;
    }
}
