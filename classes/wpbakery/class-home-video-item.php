<?php
/**
 * Class for home video element.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\WPBakery;

defined( 'ABSPATH' ) || die( 'Not allowed' );

/**
 * Class Home_Video_Item
 *
 * @package dev\Kapsula\Theme\WPBakery
 */
class Home_Video_Item extends WPBakery_Support {

    /**
     * Elements' settings.
     *
     * @throws \Exception
     */
    protected function get_mapping() {

        return [
            'name' => __( 'Видео блок', 'kapsula' ),
            'base' => 'kapsula_' . $this->name,
            'category' => __( 'Kapsula', 'kapsula' ),
            'icon' => THEME_ASSETS . '/img/kapsula-logo-round.png',
            'params' => [
                [
                    'type' => 'vc_link',
                    'heading' => __( 'Ссылка на видео', 'kapsula' ),
                    'param_name' => 'link',
                ],
                [
                    'type' => 'textfield',
                    'param_name' => 'heading',
                    'heading' => __( 'Заголовок', 'kapsula' ),
                ],
            ],
        ];
    }
}
