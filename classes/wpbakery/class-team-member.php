<?php
/**
 * Class for home video element.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\WPBakery;

defined( 'ABSPATH' ) || die( 'Not allowed' );

/**
 * Class Home_Video_Item
 *
 * @package dev\Kapsula\Theme\WPBakery
 */
class Team_Member extends WPBakery_Support {

    /**
     * Elements' settings.
     *
     * @throws \Exception
     */
    protected function get_mapping() {

        return [
            'name' => __( 'Участник команды', 'kapsula' ),
            'base' => 'kapsula_' . $this->name,
            'category' => __( 'Kapsula', 'kapsula' ),
            'icon' => THEME_ASSETS . '/img/kapsula-logo-round.png',
            'params' => [
                [
                    'heading' => esc_html__( 'Image', 'konte-addons' ),
                    'description' => esc_html__( 'Member photo', 'konte-addons' ),
                    'param_name' => 'image',
                    'type' => 'attach_image',
                ],
                [
                    'heading' => esc_html__( 'Member link', 'konte-addons' ),
                    'description' => esc_html__( 'Member link', 'konte-addons' ),
                    'param_name' => 'link',
                    'type' => 'vc_link',
                ],
                [
                    'heading' => esc_html__( 'Image Size', 'konte-addons' ),
                    'description' => esc_html__( 'Enter image size (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme). Alternatively enter size in pixels (Example: 200x100 (Width x Height)). Leave empty to use "thumbnail" size.', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'image_size',
                    'value' => 'full',
                ],
                [
                    'heading' => esc_html__( 'Full Name', 'konte-addons' ),
                    'description' => esc_html__( 'Member name', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'name',
                    'admin_label' => true,
                ],
                [
                    'heading' => esc_html__( 'Job', 'konte-addons' ),
                    'description' => esc_html__( 'The job/position name of member in your team', 'konte-addons' ),
                    'param_name' => 'job',
                    'type' => 'textfield',
                    'admin_label' => true,
                ],
                [
                    'heading' => esc_html__( 'Facebook', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'facebook',
                ],
                [
                    'heading' => esc_html__( 'Twitter', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'twitter',
                ],
                [
                    'heading' => esc_html__( 'Google Plus', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'google',
                ],
                [
                    'heading' => esc_html__( 'Pinterest', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'pinterest',
                ],
                [
                    'heading' => esc_html__( 'Linkedin', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'linkedin',
                ],
                [
                    'heading' => esc_html__( 'Youtube', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'youtube',
                ],
                [
                    'heading' => esc_html__( 'Instagram', 'konte-addons' ),
                    'type' => 'textfield',
                    'param_name' => 'instagram',
                ],
                vc_map_add_css_animation(),
                [
                    'heading' => esc_html__( 'Extra class name', 'konte-addons' ),
                    'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'konte-addons' ),
                    'param_name' => 'el_class',
                    'type' => 'textfield',
                ],
            ],
        ];
    }
}
