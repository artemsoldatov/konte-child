<?php
/**
 * Class for adding custom WPBakery blocks.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\WPBakery;

defined( 'ABSPATH' ) || die( 'Not allowed' );

/**
 * Class WPBakery_Support
 *
 * @package dev\Kapsula\Theme\WPBakery
 */
abstract class WPBakery_Support {

    /**
     * Name of shortcode.
     *
     * @var $name
     */
    protected $name;

    /**
     * Wp_Bakery_Support constructor.
     *
     * @param $name
     */
    public function __construct( $name ) {

        // The name should always be with underscores
        $this->name = str_replace( '-', '_', $name );
        $this->map();

        add_shortcode( 'kapsula_' . $name, [ $this, 'get_template' ] );
    }

    /**
     * Get WPBakery mapping.
     *
     * @return mixed
     */
    abstract protected function get_mapping();

    /**
     * Map element to WPBakery plugin.
     */
    public function map() {
        vc_map( $this->get_mapping() );
    }

    /**
     * Prints element on the front.
     *
     * @param $atts
     * @param $content
     *
     * @return false|string
     */
    public function get_template( $atts, $content ) {

        // Template names should be with hyphens so replace underscores
        $file = get_stylesheet_directory() . '/template-parts/wpbakery/' . str_replace( '_', '-', $this->name ) . '.php';

        ob_start();

        if ( is_file( $file ) ) {
            include $file;
        }

        return ob_get_clean();

    }

    /**
     * Checks if WPBakery plugin is active or if the main shortcode class is exists.
     *
     * @return bool
     */
    public function is_shortcode_exists() {
        return class_exists( 'WPBakeryShortCode' );
    }

    /**
     * Checks if WPBakery plugin is active or if the main shortcode container class is exists.
     *
     * @return bool
     */
    public function is_container_exists() {
        return class_exists( 'WPBakeryShortCodesContainer' );
    }
}
