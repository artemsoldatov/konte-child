<?php
/**
 * Class for manipulations with brands.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Brands
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Brands {

    /**
     * Change result of get_post_meta for size guide if there is a brand attached.
     *
     * @param $data
     * @param $post_id
     * @param $meta_key
     *
     * @return array[]
     */
    public function get_brand_size_guide( $data, $post_id, $meta_key ) {

        global $wpdb;

        if ( $meta_key != 'konte_size_guide' ) {
            return $data;
        }

        if ( is_admin() ) {
            return $data;
        }

        if ( ! function_exists( 'konte_addons_get_translated_object_id' ) ) {
            return $data;
        }

        $brands = get_terms(
            [
                'taxonomy' => 'product_brand',
                'hide_empty' => true,
                'object_ids' => $post_id,
                'orderby' => 'name',
                'fields' => 'ids'
            ]
        );

        $results = $wpdb->get_results( "SELECT * FROM {$wpdb->postmeta} pm WHERE pm.`meta_key` = 'size_guide_brands' " );

        foreach ( $results as $result ) {

            $brands_guides = unserialize( $result->meta_value );

            if ( is_array( $brands_guides ) && count( array_intersect( $brands_guides, $brands ) ) ) {
                // Override single meta value check with array of arrays
                return [ [ 'guide' => konte_addons_get_translated_object_id( $result->post_id ) ] ];
            }
        }

        return $data;
    }

    /**
     * Register brands taxonomy for size guides.
     */
    public function register_brands_for_size_guide() {
        register_taxonomy_for_object_type( 'product_brand', 'konte_size_guide' );
    }

    /**
     * Add name of category to title in brand pages
     *
     * @param $title
     *
     * @return mixed
     */
    public function change_brands_term_title( $title ) {

        if ( is_tax( 'product_brand' ) && isset( $_GET['product_cat'] ) ) {
            $term = get_term_by( 'slug', $_GET['product_cat'], 'product_cat' );

            if ( $term ) {
                return $term->name . ' ' . $title;
            }
        }

        return $title;
    }

    /**
     * Retrieve all brands for brands page
     *
     * @param $args
     *
     * @return string
     */
    public function get_kapsula_brands( $args ) {

        $brands = get_terms(
            [
                'taxonomy' => 'product_brand',
                'hide_empty' => true,
                'orderby' => 'name'
            ]
        );

        $brands_list = '<p style="text-align: left;">';

        foreach ( $brands as $brand ) {
            $brands_list .= '<a href="' . get_term_link( $brand->term_id ) . '" target="_blank" rel="noopener">' . mb_strtoupper( $brand->name ) . '</a><br />';
        }

        $brands_list .= '</p>';

        return $brands_list;
    }

    /**
     * Save meta box content.
     *
     * @param int $post_id
     * @param object $post
     */
    public function save_brand_category_size_guide( $post_id, $post ) {

        // If not the flex post.
        if ( 'konte_size_guide' != $post->post_type ) {
            return;
        }

        // Check if user has permissions to save data.
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }

        // Check if not an autosave.
        if ( wp_is_post_autosave( $post_id ) ) {
            return;
        }

        if ( ! empty( $_POST['tax_input'] ) && ! empty( $_POST['tax_input']['product_brand'] ) ) {
            $cat_ids = array_filter( array_map( 'intval', $_POST['tax_input']['product_brand'] ) );
            update_post_meta( $post_id, 'size_guide_brands', $cat_ids );
        }
    }

    //Add brands list option to settings woocommerce submenu page
    public function register_brand_list_page() {

        $brand_list_page_callback = function () {
            echo '<h3>Select the brands you need</h3>';

            $brands_selected = explode( ',', get_option( 'brand_names_for_jewellery', [] ) );
            $product_brands = get_terms( 'product_brand' );
            $brands_list = [];
            $ajax_nonce = wp_create_nonce( 'security-nonce' );

            foreach ( $product_brands as $item ) {
                $brands_list[] = $item->name;
            }
            ?>

            <form id="brandsform" method="POST" action="">
                <select name="brands" size="3" multiple="multiple" tabindex="1" class="option-tree-ui-select">

                    <?php foreach ( $brands_list as $item ) : ?>

                        <option value="<?php echo $item; ?>" <?php selected( in_array( $item, $brands_selected ), true ); ?>><?php echo $item; ?></option>

                    <?php endforeach; ?>

                </select>
                <button id="save-brand-list" data-page-url="<?php echo site_url(); ?>" data-page-nonce="<?php echo $ajax_nonce; ?>">
                    <?php _e( 'Save', 'woocommerce' ); ?>
                </button>
            </form>

            <?php
        };

        add_submenu_page( 'woocommerce', 'Brands list', 'Brands list', 'manage_options', 'brands-list', $brand_list_page_callback );
    }

    function brand_list_page_callback() {
        echo '<h3>Select the brands you need</h3>';

        $brands_selected = explode( ',', get_option( 'brand_names_for_jewellery', [] ) );
        $product_brands = get_terms( 'product_brand' );
        $brands_list = [];
        $ajax_nonce = wp_create_nonce( 'security-nonce' );

        foreach ( $product_brands as $item ) {
            $brands_list[] = $item->name;
        }
        ?>

        <form id="brandsform" method="POST" action="">
            <select name="brands" size="3" multiple="multiple" tabindex="1" class="option-tree-ui-select">

                <?php foreach ( $brands_list as $item ) : ?>

                    <option value="<?php echo $item; ?>" <?php selected( in_array( $item, $brands_selected ), true ); ?>>
                        <?php echo $item; ?>
                    </option>

                <?php endforeach; ?>
            </select>
            <button id="save-brand-list" data-page-url="<?php echo site_url(); ?>" data-page-nonce="<?php echo $ajax_nonce; ?>">
                <?php _e( 'Save', 'woocommerce' ); ?>
            </button>
        </form>

        <?php
    }

    function add_select_brands() {
        if ( check_ajax_referer( 'security-nonce', 'security' ) != false ) {
            update_option( 'brand_names_for_jewellery', implode( ',', $_POST['select_brands'] ) );
            wp_send_json_success();
        }

        wp_send_json_error();
    }
}
