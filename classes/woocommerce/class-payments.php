<?php
/**
 * Class for payments calculations.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Payments
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Payments {

    /**
     * Change gateway description to show in checkout.
     *
     * @param $description
     * @param $method_id
     *
     * @return string
     */
    public function change_gateway_description( $description, $method_id ) {

        if ( in_array( $method_id, [ 'platononline', 'fondy' ] ) && WC()->customer && WC()->customer->get_billing_country() == 'UA' ) {

            $discount_description = '';
            foreach ( WC()->cart->get_fees() as $fee ) {

                if ( $fee->name == 'Оплата онлайн -2%' ) {
                    ob_start();
                    wc_cart_totals_fee_html( $fee );
                    $discount_description = ob_get_clean();
                    $discount_description = '<br />' . $fee->name . ': ' . $discount_description;
                }
            }

            $description .= $discount_description;
        }

        return $description;
    }

    /**
     * Add discount description to Fondy title.
     *
     * @param $title
     * @param $id
     *
     * @return mixed|string
     */
    public function change_gateway_title( $title, $id ) {

        if ( $id == 'platononline' && WC()->customer && WC()->customer->get_billing_country() == 'UA' ) {
            $title .= ' <span class="mobile-br">(Скидка 2%)</span>';
        }

        return $title;
    }

    /**
     * Show additional order information in admin order page.
     *
     * @param $order
     */
    public function show_choosen_custom_billing_fields_in_admin( $order ) {

        $phone = get_post_meta( $order->get_id(), '_billing_connection_phone', true ) ? 'Телефон' : '';
        $sms = get_post_meta( $order->get_id(), '_billing_connection_viber', true ) ? 'Viber' : '';
        $email = get_post_meta( $order->get_id(), '_billing_connection_email', true ) ? 'Email' : '';
        $no_call = get_post_meta( $order->get_id(), '_billing_connection_no', true ) ? 'Можно не перезванивать' : '';

        $connection_types = [ $phone, $sms, $email, $no_call ];

        $connection_types = array_filter(
            $connection_types,
            function ( $value ) {
                return $value !== '';
            }
        );

        $connection_types_text = implode( ', ', $connection_types );

        if ( $connection_types_text != '' ) {
            echo '<p><strong>' . __( 'Предпочитаемый способ связи', 'kapsula' ) . ':</strong> ' . $connection_types_text . '</p>';
        }
    }

    /**
     * Add negative fee item to order for discount.
     *
     * This functionality is obsolete and need to be replaced.
     *
     * @depreacted
     * @param $meta_id
     * @param $post_id
     * @param $meta_key
     * @param $meta_value
     *
     * @throws \Exception
     */
    public function after_update_order_meta( $meta_id, $post_id, $meta_key, $meta_value ) {

        $post = get_post( $post_id );

        if ( $post->post_type == 'shop_order' && '_payment_method' == $meta_key ) {
            $this->manipulate_order_discount_online_payment( $post_id );
        }
    }

    /**
     * Add negative fee item to order for discount.
     *
     * This functionality is obsolete and need to be replaced.
     *
     * @param $order_id
     * @param $items
     *
     * @throws \Exception
     */
    public function woocommerce_before_save_order_items( $order_id, $items ) {

        $order = new \WC_Order( $order_id );

        if ( in_array( $order->get_payment_method(), [ 'platononline', 'fondy' ] ) && $order->get_billing_country() == 'UA' ) {
            if ( ! get_feed_id( $order, 'Оплата онлайн -2%' ) ) {
                $discount = -round( $order->get_total() * 0.02, wc_get_price_decimals() );
                $item_id = wc_add_order_item(
                    $order->get_id(),
                    [
                        'order_item_name' => 'Оплата онлайн -2%',
                        'order_item_type' => 'fee'
                    ]
                );

                wc_add_order_item_meta( $item_id, '_line_total', wc_format_decimal( $discount ) );
            }
        } else {
            wc_delete_order_item( get_feed_id( $order, 'Оплата онлайн -2%' ) );
        }

        $order->calculate_totals();
    }

    /**
     * Add or remove order discount.
     *
     * @param int|\WC_Order $order
     *
     * @throws \Exception
     */
    public function manipulate_order_discount_online_payment( $order ) {

        $order = is_numeric( $order ) ? new \WC_Order( $order ) : $order;

        if ( ! is_a( $order, '\WC_Order' ) ) {
            return;
        }

        if ( in_array( $order->get_payment_method(), [ 'platononline', 'fondy' ] ) && $order->get_billing_country() == 'UA' ) {
            $this->add_online_discount( $order );
        } else {
            wc_delete_order_item( get_feed_id( $order, 'Оплата онлайн -2%' ) );
        }

        $order->calculate_totals();
    }

    /**
     * Add discount 2% for order for online payments if one not exists.
     *
     * @param $order
     *
     * @throws \Exception
     */
    public function add_online_discount( $order ) {

        if ( ! get_feed_id( $order, 'Оплата онлайн -2%' ) ) {
            $discount = -round( $order->get_total() * 0.02, wc_get_price_decimals() );
            $item_id = wc_add_order_item(
                $order->get_id(),
                [
                    'order_item_name' => 'Оплата онлайн -2%',
                    'order_item_type' => 'fee'
                ]
            );
            wc_add_order_item_meta( $item_id, '_line_total', wc_format_decimal( $discount ) );
        }
    }

    /**
     * Add negative fee item to order for discount.
     *
     * This functionality is obsolete and need to be replaced
     *
     * @param $cart
     */
    public function woocommerce_custom_surcharge( $cart ) {

        $cart_total = $cart->cart_contents_total;
        $post_params = [ 'payment_method', 'payment_name', 'payment' ];
        $payment_method = '';
        foreach ( $post_params as $param ) {
            if ( isset( $_POST[ $param ] ) ) {
                $payment_method = $_POST[ $param ];
                break;
            }
        }

        if ( isset( $_POST['post_data'] ) && empty( $payment_method ) ) {
            parse_str( $_POST['post_data'], $params );
            if ( isset( $params['payment_method'] ) ) {
                $payment_method = $params['payment_method'];
            }
        }

        WC()->cart->fees_api()->remove_all_fees();

        if ( in_array( $payment_method, [ 'platononline', 'fondy', 'online' ] ) && WC()->customer->get_billing_country() == 'UA' ) {
            $discount = -round( $cart_total * 0.02, wc_get_price_decimals() );
            WC()->cart->add_fee( 'Оплата онлайн -2%', $discount, false, '' );
        }
    }

    public function update_order_pay_form() {

        check_ajax_referer( 'update-order-review', 'security' );

        wc_maybe_define_constant( 'WOOCOMMERCE_CHECKOUT', true );

        preg_match( '/(\d+)/', $_POST['path'], $matches );

        if ( ! isset( $matches[0] ) ) {
            wp_send_json_error( __( 'Неправильный ID заказа', 'kapsula' ) );
        }

        $order = wc_get_order( $matches[0] );

        if ( ! is_a( $order, '\WC_Order' ) ) {
            wp_send_json_error( __( 'Неправильный ID заказа', 'kapsula' ) );
        }

        WC()->session->set( 'chosen_payment_method', empty( $_POST['payment_method'] ) ? '' : wc_clean( wp_unslash( $_POST['payment_method'] ) ) );
        $available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
        WC()->payment_gateways()->set_current_gateway( $available_gateways );
        $order->set_payment_method( $_POST['payment_method'] );

        $this->manipulate_order_discount_online_payment( $order );

        ob_start();

        wc_get_template(
            'checkout/form-pay.php',
            [
                // Reload order object for getting changes in payment gateways
                'order'              => wc_get_order( $order->get_id() ),
                'available_gateways' => $available_gateways,
                'order_button_text'  => apply_filters( 'woocommerce_pay_order_button_text', __( 'Pay for order', 'woocommerce' ) ),
            ]
        );

        wp_send_json_success( [ 'order_review' => ob_get_clean() ] );
    }

    /**
     * Add negative fee item to order for discount.
     *
     * This functionality is obsolete and need to be replaced.
     *
     * @param $update_order_fragments
     *
     * @return mixed
     */
    public function update_order_review_custom( $update_order_fragments ) {

        $current_total = WC()->cart->total;
        $old_total = intval( preg_replace( '#[^\d.]#', '', WC()->cart->get_cart_subtotal() ) );
        $old_price = '';
        if ( $current_total != $old_total ) {
            $old_price = '<del>' . WC()->cart->get_cart_subtotal() . '</del>';
        }
        $update_order_fragments['price'] = '<strong>' . apply_filters( 'woocommerce_cart_total', wc_price( WC()->cart->total ) ) . '</strong>' . $old_price;
        $update_order_fragments['fees'] = '';
        foreach ( WC()->cart->get_fees() as $fee ) {
            $update_order_fragments['fees'] .= '<tr class="fee">' .
                '<td>' . $fee->name . '</td>' .
                '<td>' . $fee->amount . ' ' . get_woocommerce_currency() . '</td>' .
                '</tr>';
        }

        return $update_order_fragments;
    }

    /**
     * Change payment gateways for some countries.
     *
     * @param $gateways
     *
     * @return mixed
     */
    public function filter_available_payment_gateways( $gateways ) {

        if ( is_admin() && ! wp_doing_ajax() ) {
            return $gateways;
        }

        if ( WC()->customer && WC()->customer->get_shipping_country() == 'UA' ) {
            unset( $gateways['platononline_fullprice'] );

            $session_shipping = WC()->session->get( 'chosen_shipping_methods' );
            $shipping = $session_shipping ? reset( $session_shipping ) : '';
            $city = mb_strtolower( trim( WC()->customer->get_shipping_city() ) );

            if ( ! in_array( $city, [ 'kiev', 'киев', 'київ' ] ) ) {

                // Remove pay to currier
                unset( $gateways['cheque'] );

                if ( strpos( $shipping, 'free_shipping' ) === 0 ) {
                    unset( $gateways['cod'] );
                }
            } elseif ( strpos( $shipping, 'shipping_nova_poshta_for_woocommerce' ) === 0 || strpos( $shipping, 'justin_shipping_method' ) === 0 ) {
                // Remove pay to currier for delivery companies methods in Kiev
                unset( $gateways['cheque'] );
            } else {
                // Remove pay on delivery for currier delivery in Kiev
                unset( $gateways['cod'] );
            }
        } else {
            unset( $gateways['platononline'] );
            unset( $gateways['bacs'] );
            unset( $gateways['cheque'] );
            unset( $gateways['cod'] );
            unset( $gateways['fondy'] );
        }

        return $gateways;
    }

    /**
     * Modify platononline payment as for orders from Ukraine it has 2% discount.
     */
    public function change_requested_payment_gateway() {

        $requested_country = $_POST['country'];

        if ( isset( $_POST['post_data'] ) ) {
            parse_str( $_POST['post_data'], $params );
            if ( isset( $params['ship_to_different_address'] ) && $params['ship_to_different_address'] ) {
                $requested_country = $params['shipping_country'];
            }
        }

        if ( $requested_country == 'UA' && $_POST['payment_method'] == 'platononline_fullprice' ) {
            $_POST['payment_method'] = 'platononline';
            if ( isset( $params ) ) {
                $params['payment_method'] = 'platononline';
            }
        } elseif ( $requested_country != 'UA' && $_POST['payment_method'] == 'platononline' ) {
            $_POST['payment_method'] = 'platononline_fullprice';
            if ( isset( $params ) ) {
                $params['payment_method'] = 'platononline_fullprice';
            }
        }

        if ( isset( $params ) ) {
            $_POST['post_data'] = http_build_query( $params );
        }
    }

    /**
     * Remove the <span> wrapper for the discount
     *
     * @param $order_id
     */
    public function remove_html_for_payment_method( $order_id ) {
        if ( empty( $order_id ) ) {
            return;
        }

        $payment_method = get_post_meta( $order_id, '_payment_method_title', true );

        update_post_meta( $order_id, '_payment_method_title', strip_tags( $payment_method ) );
    }
}
