<?php
/**
 * Class for adding video to products.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Product_Video
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Product_Video {

    public function add_product_video() {

        global $product;

        $video = get_post_meta( $product->get_id(), '_video_id', true );

        if ( $video ) :

            ?>
            <div class="woocommerce-product-gallery__image">
            <video class="product-video" poster="<?php echo isset( $image_link ) ? $image_link : ''; ?>" autoplay loop muted playsinline>
                <source src="<?php echo $video; ?>" type="video/mp4">
            </video>
            </div>
        <?php
        endif;
    }

    public function add_meta_box() {
        add_meta_box( 'product-video', __( 'Видео к товару' ), [ $this, 'product_video_meta_box' ], 'product', 'normal', 'high' );
    }

    public function product_video_meta_box() {
        global $post;

        $image_src = '';

        $video_src = get_post_meta( $post->ID, '_video_id', true );

        ?>
        <div id="video-block">
            <?php if ( $video_src ) : ?>
                <video controls="" id="video" class="video-prd-block" name="media">
                    <source src="<?php echo $video_src; ?>" type="video/webm">
                </video>
            <?php endif; ?>
        </div>

        <input type="hidden" name="upload_video_id" id="upload_video_id" value="<?php echo $video_src; ?>" />
        <p>
            <a title="<?php esc_attr_e( 'Set product video' ); ?>" href="#" id="set-product-video"><?php _e( 'Добавить видео к товару' ); ?></a>
            <br>
            <a title="<?php esc_attr_e( 'Remove product video' ); ?>" href="#" id="remove-product-video" style="<?php echo( ! $video_src ? 'display:none;' : '' ); ?>"><?php _e( 'Удалить видео с товара' ); ?></a>
        </p>

        <script type="text/javascript">
            jQuery(document).ready(function ($) {

                // save the send_to_editor handler function
                window.send_to_editor_default = window.send_to_editor;

                $('#set-product-video').click(function () {

                    // replace the default send_to_editor handler function with our own
                    window.send_to_editor = window.attach_image;
                    tb_show('', 'media-upload.php?post_id=<?php echo $post->ID; ?>&amp;type=video&amp;TB_iframe=true');

                    return false;
                });

                $('#remove-product-video').click(function () {

                    $('#upload_video_id').val('');
                    $('#video-block video').remove();
                    $(this).hide();

                    return false;
                });

                // handler function which is invoked after the user selects an image from the gallery popup.
                // this function displays the image and sets the id so it can be persisted to the post meta
                window.attach_image = function (html) {
                    // turn the returned image html into a hidden image element so we can easily pull the relevant attributes we need
                    $('body').append('<div id="temp_image">' + html + '</div>');

                    var videoSrc = $('#temp_image').find('a').attr('href');

                    $('#upload_video_id').val(videoSrc);
                    $('#remove-book-image').show();
                    var videoBlock = document.getElementById('video-block');
                    var video = document.createElement('video');
                    var source = document.createElement('source');
                    videoBlock.innerHTML = '';
                    source.setAttribute('src', videoSrc);
                    source.setAttribute('type', "video/webm");
                    videoBlock.appendChild(video);
                    video.appendChild(source);
                    video.play();
                    $('#remove-product-video').show();
                    try {
                        tb_remove();
                    } catch (e) {
                    }
                    ;
                    $('#temp_image').remove();

                    // restore the send_to_editor handler function
                    window.send_to_editor = window.send_to_editor_default;

                }

            });
        </script>
        <?php
    }

    public function save_product_video( $post_id, $post ) {
        if ( empty( $post_id ) || empty( $post ) || empty( $_POST ) ) {
            return;
        }
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
        }
        if ( is_int( wp_is_post_revision( $post ) ) ) {
            return;
        }
        if ( is_int( wp_is_post_autosave( $post ) ) ) {
            return;
        }
        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
        if ( $post->post_type != 'product' || ! isset( $_POST['upload_video_id'] ) ) {
            return;
        }

        update_post_meta( $post_id, '_video_id', $_POST['upload_video_id'] );
    }

    public function add_video_to_catalog_product() {

        global $post;

        $video = get_post_meta( $post->ID, '_video_id', true );

        if ( ! empty( $video ) && wp_is_mobile() && $post->display == 'video' ) :

            ?>
            <a href="<?php echo get_permalink( $post ); ?>">
                <video src="<?php echo $video; ?>" autoplay loop muted playsinline></video>
            </a>
            <?php
        endif;
    }
}
