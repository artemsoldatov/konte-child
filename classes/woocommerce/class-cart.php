<?php
/**
 * Class for manipulations with cart page.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Cart
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Cart {

    /**
     * Change product variations on cart
     */
    public function change_size_in_cart() {

        if ( ! isset( $_REQUEST['update_cart'] ) || ! isset( $_REQUEST['attribute_pa_size'] ) ) {
            return;
        }

        $cart_contents = WC()->cart->cart_contents;

        if ( empty( $cart_contents ) ) {
            return;
        }

        foreach ( $_REQUEST['attribute_pa_size'] as $cart_item_key => $size ) {

            if ( ! isset( $cart_contents[ $cart_item_key ] ) ) {
                continue;
            }

            $cart_item = $cart_contents[ $cart_item_key ];

            if ( empty( $cart_item['variation'] ) || empty( $cart_item['variation']['attribute_pa_size'] ) || $cart_item['variation']['attribute_pa_size'] == $size ) {
                continue;
            }

            $product = wc_get_product( $cart_item['data']->get_parent_id() );

            $available_variations = $product->get_available_variations();
            $search_attributes = [];
            foreach ( $cart_item['data']->get_attributes() as $attribute => $value ) {
                $search_attributes[ 'attribute_' . $attribute ] = $value;
            }
            $search_attributes['attribute_pa_size'] = $size;

            foreach ( $available_variations as $item ) {
                if ( $item['attributes'] == $search_attributes ) {
                    WC()->cart->add_to_cart( $product->get_id(), 1, $item['variation_id'], $search_attributes );
                    WC()->cart->remove_cart_item( $cart_item_key );
                }
            }
        }

        if ( wp_doing_ajax() ) {

            ob_start();

            woocommerce_mini_cart();

            $cart = ob_get_clean();

            wp_send_json_success( [ 'update' => [ '.widget_shopping_cart_content' => $cart ] ] );
        }
    }

    /**
     * Change the quantity HTML of widget cart.
     *
     * @param string $product_quantity
     * @param array  $cart_item
     * @param string $cart_item_key
     *
     * @return string
     */
    public static function change_widget_cart_quantity_block( $product_quantity, $cart_item, $cart_item_key ) {

        $_product      = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_price = apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
        $select = '';

        if ( $_product->is_type( 'variation' ) ) {

            $parent_product = wc_get_product( $_product->get_parent_id() );

            $args = [
                'product' => $parent_product,
                'show_option_none' => false,
                'attribute' => 'pa_size',
                'name' => 'attribute_pa_size[' . $cart_item_key . ']',
                'selected' => $_product->get_attribute( 'pa_size' ),
                'id' => 'pa_size_' . $cart_item_key . rand( 10, 99 )
            ];

            ob_start();

            wc_dropdown_variation_attribute_options( $args ); // PHPCS: XSS ok.

            $select = ob_get_clean();
        }

        $in_showroom = apply_filters( 'is_in_showroom', false, $_product->get_id() ) ? '<span class="in-showroom">' . __( 'Сейчас в шоуруме', 'kapsula' ) . '</span>' : '';
        if ( ! $in_showroom ) {
            $in_showroom = apply_filters( 'is_in_shop', false, $_product->get_id() ) ? '<span class="in-showroom">' . __( 'Готов к отправке', 'kapsula' ) . '</span>' : '';
        }

        ob_start();

        Woocommerce::add_wishlist_button( $_product->get_id() );

        $add_to_favorites = ob_get_clean();

        return sprintf(
            '<div class="woocommerce-mini-cart-item__qty" data-nonce="%s"><span class="label">%s</span>%s<span class="price">%s</span></div>%s<div class="add-to-favorites">%s</div>',
            esc_attr( wp_create_nonce( 'konte-update-mini-cart-qty--' . $cart_item_key ) ),
            esc_html__( 'Размер:', 'kapsula' ),
            $select,
            $product_price,
            $in_showroom,
            $add_to_favorites
        );
    }

    /**
     * Add wishlist button to the cart.
     *
     * @param $cart_item
     */
    public function add_wishlist_button_to_cart( $cart_item ) {

        echo '<div class="cart-wishlist">';

        Woocommerce::add_wishlist_button( $cart_item['product_id'] );

        echo '<span>';

        _e( 'Перенести в избранное', 'kapsula' );

        echo '</span></div>';
    }

    /**
     * Add label in showroom to item in the cart.
     *
     * @param $cart_item
     */
    public function add_in_showroom_label( $cart_item ) {

        if ( ! isset( $cart_item['variation_id'] ) || ! $cart_item['variation_id'] ) {
            return;
        }

        if ( apply_filters( 'is_in_showroom', false, $cart_item['variation_id'] ) ) {
            echo '<span class="in-showroom">' . __( 'Сейчас в шоуруме', 'kapsula' ) . '</span>';
        } elseif ( apply_filters( 'is_in_shop', false, $cart_item['variation_id'] ) ) {
            echo '<span class="in-showroom">' . __( 'Готов к отправке', 'kapsula' ) . '</span>';
        }
    }

    /**
     * Add back to shop button to cart page.
     */
    public function add_back_to_shop_button() {
        ?>

        <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" class="checkout-button button alt wc-back button-back">
            <?php _e( 'Продолжить покупки', 'kapsula' ); ?>
        </a>

        <?php
    }
}
