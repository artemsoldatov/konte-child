<?php
/**
 * Class for shipping configurations.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Shipping
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Shipping {

    /**
     * Set shipping address as selected warehouses of post services.
     *
     * @param $order
     */
    public function set_selected_shipping_method_to_order( $order ) {

        if ( ! isset( $_POST['shipping_method'][0] ) || strstr( $_POST['shipping_method'][0], 'justin_shipping_method' ) !== false ) {
            return;
        }

        $ship_to_different_address = isset( $_POST['ship_to_different_address'] ) && $_POST['ship_to_different_address'];

        if ( $_POST['shipping_method'][0] == 'shipping_nova_poshta_for_woocommerce' && ! empty( $_POST['shipping_nova_poshta_for_woocommerce_warehouse'] ) ) {

            $address = Nova_Poshta::get_warehouse_data_by_id( $_POST['shipping_nova_poshta_for_woocommerce_warehouse'] );

            $order->set_billing_address_1( $address );
            $order->set_shipping_address_1( $address );
        } elseif ( strpos( $_POST['shipping_method'][0], 'local_pickup' ) === 0 ) {

            $address = 'Жилянская 54, Киев';

            $order->set_billing_address_1( $address );
            $order->set_shipping_address_1( $address );
        } else {
            $order->set_billing_address_1( $_POST['billing_address_1'] );
            if ( isset( $_POST['ship_to_different_address'] ) && $_POST['ship_to_different_address'] ) {
                $order->set_shipping_address_1( $_POST['shipping_address_1'] );
            } else {
                $order->set_shipping_address_1( $_POST['billing_address_1'] );
            }
        }

        $order->set_billing_city( $_POST['billing_city'] );

        if ( $ship_to_different_address ) {
            $order->set_shipping_city( $_POST['shipping_city'] );
        } else {
            $order->set_shipping_city( $_POST['billing_city'] );
        }

        if ( $ship_to_different_address ) {
            $order->set_shipping_address_1( $_POST['shipping_address_1'] );
        }
    }

    /**
     * Change shipping label names.
     *
     * @param $name
     *
     * @return string|void
     */
    public function change_shipping_labels( $name ) {

        if ( $name == 'Justin' ) {
            $name = __( 'В отдение Justin', 'kapsula' );
        } elseif ( $name == 'Nova Poshta delivery' ) {
            $name = __( 'В отделение Новой Почты', 'kapsula' );
        }

        return $name;
    }
    /**
     * Set country specific rules to show fields in checkout.
     *
     * @param $locales
     *
     * @return mixed
     */
    public function set_country_locale_rules( $locales ) {

        if ( ! WC()->session ) {
            return $locales;
        }

        array_map(
            function ( $locale ) use ( &$locales ) {
                $locales[ $locale ]['address_1'] = [
                    'required' => true,
                    'hidden'   => false,
                    'priority' => 82
                ];
                $locales[ $locale ]['address_2'] = [
                    'required' => false,
                    'hidden'   => true,
                ];
            },
            array_keys( $locales )
        );

        $locales['UA'] = [
            'postcode' => [
                'required' => false,
                'hidden'   => true,
            ],
            'state' => [
                'required' => false,
                'hidden'   => true,
            ],
            'address_1' => [
                'required' => false,
                'hidden'   => true,
                'priority' => 82
            ],
            'address_2' => [
                'required' => false,
                'hidden'   => true,
            ],
        ];

        $shipping = WC()->session->get( 'chosen_shipping_methods' );

        if ( isset( $shipping[0] ) && strstr( $shipping[0], 'free_shipping' ) !== false ) {
            $locales['UA']['address_1']['hidden'] = false;
            $locales['UA']['address_1']['required'] = true;
        }

        return $locales;
    }

    /**
     * Change default settings for locales.
     *
     * @param $locale
     *
     * @return mixed
     */
    public function set_default_locale_rules( $locale ) {

        $locale['address_1']['priority'] = 82;
        $locale['address_1']['required'] = true;

        return $locale;
    }

    /**
     * Change shipping on updating country or city.
     *
     * @param $packages
     *
     * @return mixed
     */
    public function filter_shipping_methods( $packages ) {

        if ( WC()->customer->get_shipping_country() != 'UA' ) {
            return $packages;
        }

        $city = mb_strtolower( trim( WC()->customer->get_shipping_city() ) );
        $has_free_shipping = false;

        if ( ! in_array( $city, [ 'kiev', 'киев', 'київ' ] ) ) {

            foreach ( $packages as $k => $package ) {

                foreach ( $packages[ $k ]['rates'] as $rate_id => $rate ) {

                    if ( $rate->method_id == 'free_shipping' ) {
                        if ( $has_free_shipping ) {
                            unset( $packages[ $k ]['rates'][ $rate_id ] );
                        } else {
                            $has_free_shipping = true;
                            $packages[ $k ]['rates'][ $rate_id ]->label = __( 'Курьером по адресу', 'kapsula' );
                        }
                    }

                    if ( $rate->method_id == 'local_pickup' ) {
                        unset( $packages[ $k ]['rates'][ $rate_id ] );
                    }
                }
            }
        }

        // Remove delivery methods if cart contents <= 1000 UAH
        // We have infinite loop on calculate_totals when calculating shipping. So remove shipping calculations to prevent it.
        add_filter( 'woocommerce_cart_ready_to_calc_shipping', '__return_false', 99 );
        // Block currency conversion. We need UAH price
        $_REQUEST['woocs_block_price_hook'] = true;
        $cart = clone WC()->cart;
        $cart->calculate_totals();
        $cart_total = $cart->get_cart_contents_total();
        unset( $_REQUEST['woocs_block_price_hook'] );

        if ( $cart_total <= 1000 ) {
            foreach ( $packages as $k => $package ) {
                foreach ( $packages[ $k ]['rates'] as $rate_id => $rate ) {
                    if ( ! in_array( $rate->method_id, [ 'shipping_nova_poshta_for_woocommerce', 'local_pickup' ] ) ) {
                        unset( $packages[ $k ]['rates'][ $rate_id ] );
                    }
                }
            }
        }
        // Return shipping calculation settings
        remove_filter( 'woocommerce_cart_ready_to_calc_shipping', '__return_false', 99 );

        // Reset shipping methods
        $chosen_shipping_methods = WC()->session->get( 'chosen_shipping_methods' );
        $posted_shipping_methods = isset( $_POST['shipping_method'] ) ? wc_clean( wp_unslash( $_POST['shipping_method'] ) ) : [];

        if ( is_array( $posted_shipping_methods ) ) {
            foreach ( $posted_shipping_methods as $i => $value ) {
                $chosen_shipping_methods[ $i ] = $value;
            }
        }

        WC()->session->set( 'chosen_shipping_methods', $chosen_shipping_methods );

        return $packages;
    }

    /**
     * Show additional order information in admin order page.
     *
     * @param $order
     */
    public function show_choosen_custom_shipping_fields_in_admin( $order ) {

        if ( get_post_meta( $order->get_id(), '_shipping_consultation', true ) ) {
            echo '<p><strong>' . __( 'Требуется консультация', 'kapsula' ) . '</strong></p>';
        }

        if ( get_post_meta( $order->get_id(), '_shipping_try_it_on', true ) ) {
            echo '<p><strong>' . __( 'Требуется примерка', 'kapsula' ) . '</strong></p>';
        }

        if ( get_post_meta( $order->get_id(), '_shipping_present', true ) ) {
            echo '<p><strong>' . __( 'Подарок - подписать открытку', 'kapsula' ) . '</strong></p>';
        }

        if ( get_post_meta( $order->get_id(), '_shipping_present_text', true ) ) {
            echo '<p><strong>' . __( 'Текст открытки', 'kapsula' ) . ':</strong>' . get_post_meta( $order->get_id(), '_shipping_present_text', true ) . '</p>';
        }
    }

    /**
     * If no country was set than use UA as default country.
     */
    public function set_default_country_locale() {
        if ( WC()->customer && ! WC()->customer->get_billing_country() ) {
            WC()->customer->set_shipping_country( 'UA' );
            WC()->customer->set_billing_country( 'UA' );
        }
    }

    /**
     * For changing delivery region on the cart.
     */
    public function change_shipping_country() {

        if ( ! wp_doing_ajax() ) {
            return;
        }

        if ( ! isset( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'kapsula' ) ) {
            return;
        }

        if ( empty( $_POST['value'] ) ) {
            return;
        }

        if ( $_POST['value'] == 'UA' ) {
            WC()->customer->set_shipping_country( 'UA' );
            WC()->customer->set_billing_country( 'UA' );
        } elseif ( $_POST['value'] == 'International' ) {
            WC()->customer->set_shipping_country( 'GB' );
            WC()->customer->set_billing_country( 'GB' );
        }

        wp_send_json_success( 'updated' );
    }
}
