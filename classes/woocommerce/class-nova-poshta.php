<?php
/**
 * Class for manipulations with Nova Poshta delivery.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

use Nova_Poshta\Admin\Notice\Notice;
use Nova_Poshta\Core\API;
use Nova_Poshta\Core\Cache\Factory_Cache;
use Nova_Poshta\Core\Cache\Object_Cache;
use Nova_Poshta\Core\Cache\Transient_Cache;
use Nova_Poshta\Core\DB;
use Nova_Poshta\Core\Language;
use Nova_Poshta\Core\Settings;
use dev\Kapsula\Logger;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Nova_Poshta
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Nova_Poshta {

    /**
     * Get already chosen method from post_data.
     *
     * @param $city
     *
     * @return mixed
     */
    public function return_chosen_city_id( $city ) {

        if ( isset( $_POST['post_data'] ) ) {

            parse_str( $_POST['post_data'], $params );

            if ( ! empty( $params['shipping_nova_poshta_for_woocommerce_city'] ) ) {
                return $params['shipping_nova_poshta_for_woocommerce_city'];
            }
        }

        return $city;
    }

    /**
     * Get already chosen method from post_data.
     *
     * @param $warehouse
     *
     * @return mixed
     */
    public function return_chosen_warehouse_id( $warehouse ) {

        $warehouse = '';

        if ( isset( $_POST['post_data'] ) ) {

            parse_str( $_POST['post_data'], $params );

            if ( ! empty( $params['shipping_nova_poshta_for_woocommerce_warehouse'] ) ) {
                $warehouse = $params['shipping_nova_poshta_for_woocommerce_warehouse'];
            }
        } else {
            $user_id = get_current_user_id();

            if ( $user_id ) {
                $warehouse = get_user_meta( $user_id, 'shipping_nova_poshta_for_woocommerce_warehouse', true );
            }
        }

        return $warehouse;
    }

    /**
     * Get NP city id by currently chosen user city.
     *
     * @param $city_id
     * @param $user_id
     *
     * @return int|string|null
     */
    public function get_customer_city_id( $city_id, $user_id ) {

        $city_data = self::get_city_data_by_name( WC()->customer->get_shipping_city() );

        if ( is_array( $city_data ) ) {
            return array_key_first( $city_data );
        }

        return '';
    }

    /**
     * NP branch show.
     */
    public function show_nova_poshta_branch_select() {

        if ( ! is_checkout() ) {
            return;
        }

        ?>

        <tr class="shipping-nova-poshta-warehouse">
            <th><?php _e( 'Отделение Новой Почты', 'kapsula' ); ?></th>
            <td>

                <?php do_action( 'shipping_nova_poshta_for_woocommerce_user_fields' ); ?>

            </td>
        </tr>

        <?php
    }

    /**
     * Check if branch selected.
     */
    public function validate_branch() {

        if ( ! WC()->customer || WC()->customer->get_shipping_country() != 'UA' ) {
            return;
        }

        if ( $_POST['shipping_method'][0] == 'shipping_nova_poshta_for_woocommerce' && empty( $_POST['shipping_nova_poshta_for_woocommerce_warehouse'] ) ) {
            wc_add_notice( __( 'Choose branch', 'shipping-nova-poshta-for-woocommerce' ), 'error' );
        }
    }

    /**
     * Add NP city field to checkout fieldset.
     *
     * @param $fields
     */
    public static function update_checkout_fields( &$fields ) {

        if ( ! is_plugin_active( 'shipping-nova-poshta-for-woocommerce/shipping-nova-poshta-for-woocommerce.php' ) ) {
            return;
        }

        if ( isset( $_POST['city'] ) ) {
            $city_name = sanitize_text_field( $_POST['city'] );
        } else {

            if ( ! WC()->customer ) {
                return;
            }

            $city_name = WC()->customer->get_shipping_city();
        }

        $fields['billing']['shipping_nova_poshta_for_woocommerce_city'] = [
            'type' => 'select',
            'initiator' => 'theme',
            'label' => __( 'City', 'woocommerce' ),
            'required' => true,
            'options' => [ 0 => '' ],
            'default' => 0,
            'class' => [ 'form-row-wide', 'address-field' ],
            'priority' => 75,
        ];

        if ( ! empty( $city_name ) ) {
            $city = self::get_city_data_by_name( $city_name );

            if ( is_array( $city ) && ! empty( $city ) ) {
                $city_id = array_keys( $city )[0];
                $city = array_pop( $city );

                $fields['billing']['shipping_nova_poshta_for_woocommerce_city']['options'] = [ $city_id => $city ];
                $fields['billing']['shipping_nova_poshta_for_woocommerce_city']['default'] = $city_id;
            }
        }

        $default_cities = [''];
        foreach ( get_sort_cities_order() as $city ) {
            $city_data = self::get_city_data_by_name( $city );

            if ( ! empty( $city_data ) ) {
                $default_cities = array_merge( $default_cities, $city_data );
            }
        }

        $fields['billing']['shipping_nova_poshta_for_woocommerce_city']['options'] = $default_cities;
        $fields['billing']['billing_city']['class'] = [ 'form-row-wide', 'address-field' ];
    }

    /**
     * Get API instance.
     *
     * @return API|\WP_Error
     */
    public static function get_api() {

        try {
            $transient_cache = new Transient_Cache();
            $object_cache = new Object_Cache();
            $factory_cache = new Factory_Cache( $transient_cache, $object_cache );
            $notice = new Notice( $transient_cache );
            $language = new Language();
            $db = new DB( $language );
            $settings = new Settings( $notice );
            $api = new API( $db, $factory_cache, $settings );

            return $api;
        } catch ( \Error $e ) {
            return new \WP_Error( '10', $e->getMessage() );
        }
    }
    /**
     * Get NP plugin city data by its name.
     *
     * @param $name
     *
     * @return array
     */
    public static function get_city_data_by_name( $name ) {

        $api = self::get_api();

        if ( is_wp_error( $api ) ) {
            // ( new Logger() )->add_log( $api );
            return [];
        }

        return $api->cities( $name, 1 );
    }

    /**
     * Get NP plugin warehouse data by its id.
     *
     * @param $id
     *
     * @return string
     */
    public static function get_warehouse_data_by_id( $id ) {

        $api = self::get_api();

        if ( ! $api || is_wp_error( $api ) ) {
            // ( new Logger() )->add_log( $api );
            return '';
        }

        return $api->warehouse( $id );
    }

    /**
     * Clear default warehouse from the list and force user to choose warehouse.
     *
     * @param $args
     * @param $key
     * @param $value
     *
     * @return mixed
     */
    public function clear_default_warehouse( $args, $key, $value ) {

        if ( $key == 'shipping_nova_poshta_for_woocommerce_warehouse' ) {
            $args['custom_attributes']['data-placeholder'] = __( 'Выберите номер отделения из списка', 'kapsula' );
            $args['custom_attributes']['data-allowClear'] = true;
            $args['options'] = array_merge( [ '' => __( 'Выберите номер отделения из списка', 'kapsula' ) ], $args['options'] );
        }

        return $args;
    }
}
