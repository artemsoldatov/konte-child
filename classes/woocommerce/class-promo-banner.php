<?php
/**
 * Class for banners on catalog pages.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Theme main class
 *
 * @subpackage dev\Kapsula\Theme\Woocommerce
 * @version 1.0.0
 * @since 1.0.0
 */
class Promo_Banner {

    public function __construct(){}
    
    public function add_settings_tab( $settings_tabs ) {

        $settings_tabs['promo_banner'] = __( 'Promo Banner', 'kapsula' );

        return $settings_tabs;
    }

    public function promo_banner_content() {
        woocommerce_admin_fields( $this->get_promo_banner_settings() );
    }

    public function promo_banner_update() {
        woocommerce_update_options( $this->get_promo_banner_settings() );
    }

    public function get_promo_banner_settings() {

        $settings = [
            'section_basic_title'   => [
                'name' => __( 'Settings Promo Banner', 'kapsula' ),
                'type' => 'title',
                'id'   => 'wc_settings_promo_banner_title'
            ],
            'promo_banner_settings' => [
                'type' => 'promo_banner_settings',
                'id'   => 'promo_banner_settings',
            ],
            'section_end'           => [
                'type' => 'sectionend',
                'id'   => 'wc_settings_promo_banner_section_end'
            ]
        ];

        return apply_filters( 'wc_settings_promo_banner_settings', $settings );
    }

    public function promo_banner_settings() {

        add_option( 'promo_banner_settings', [] );

        ?>

        </table>
        <button class="add-promo-banner button"><?php _e( 'Добавить промо баннер', 'kapsula' ); ?></button>

        <?php

        $promo_banner_settings = get_option( 'promo_banner_settings', [] );

        foreach ( $promo_banner_settings as $data ) {
            ?>
            <div class="promo-banner-settings">
                <table class="form-table">
                    <tr valign="top">
                        <th scope="row"><?php _e( 'Promo Banner активация', 'kapsula' ); ?></th>
                        <td>
                            <fieldset>
                                <label for="wc_settings_promo_banner_activate">
                                    <input type="checkbox" <?php checked( $data['activate'] ?? '', 'on' ); ?> class="wc_settings_promo_banner_activate" />
                                    <input type="hidden" name="promo_banner_settings[activate][]" value="<?php echo $data['activate']; ?>" />
                                    <?php _e( 'Поставьте отметку, чтобы использовать Promo Banner для всех категорий', 'kapsula' ); ?>
                                </label>
                            </fieldset>
                        </td>
                    </tr>

                    <?php if ( function_exists( 'PLL' ) ) : ?>
                        <tr valign="top">
                            <th scope="row"><?php _e( 'Выберите язык для которого показывать баннер', 'kapsula' ); ?></th>
                            <td>
                                <fieldset>
                                    <label for="wc_settings_promo_banner_activate">
                                        <select name="promo_banner_settings[banner_language][]" id="promo-banner-language">
                                            <?php foreach ( array_combine( pll_languages_list( [ 'fields' => 'slug' ] ), pll_languages_list( [ 'fields' => 'name' ] ) ) as $slug => $language ) : ?>
                                                <option value="<?php echo $slug; ?>" <?php selected( $data['banner_language'] ?? '', $slug ); ?>><?php echo $language; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                        <?php _e( 'Язык для которого будет выводиться баннер', 'kapsula' ); ?>
                                    </label>
                                </fieldset>
                            </td>
                        </tr>
                    <?php endif; ?>

                    <tr valign="top">
                        <th scope="row"><?php _e( 'Promo Banner destination url', 'kapsula' ); ?></th>
                        <td>
                            <fieldset>
                                <label for="wc_settings_promo_banner_url">
                                    <?php $url = $data['url'] ?? ''; ?>
                                    <input type="text" name="promo_banner_settings[url][]" value="<?php echo $url; ?>" class="wc_settings_promo_banner_url" />
                                    <?php _e( 'Укажите url для баннера(опционально)', 'kapsula' ); ?>
                                </label>
                            </fieldset>
                        </td>
                    </tr>
                    <tr valign="top">
                        <th scope="row"><?php _e( 'Загрузите Promo Banner', 'kapsula' ); ?></th>
                        <td><label for="upload_image">
                                <?php if ( ! $data['image'] ) : ?>
                                    <img class="promo-banner-img" src="<?php echo site_url(); ?>/wp-content/plugins/woocommerce/assets/images/placeholder.png" width="60px" height="60px">
                                <?php else : ?>
                                    <img class="promo-banner-img" src="<?php echo esc_attr( $data['image'] ); ?>" width="60px" height="60px">
                                <?php endif; ?>

                                <?php $image_src = $data['image'] ?? ''; ?>
                                <input class="wc_settings_promo_banner_image" type="hidden" size="36" name="promo_banner_settings[image][]" value="<?php echo $image_src; ?>" />
                                <button type="button" class="wc_button_promo_banner_image upload_image_button button"><?php _e( 'Загрузить изображение', 'kapsula' ); ?></button>
                                <button type="button" class="wc_remove_promo_banner_image remove_image_banner_button button"
                                <?php
                                if ( ! $data['image'] ) :
                                    ?>
                                    style="display: none;"<?php endif; ?>><?php _e( 'Удалить изображение', 'kapsula' ); ?></button>
                            </label></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <button class="remove-banner button"><?php _e( 'Удалить баннер', 'kapsula' ); ?></button>
                        </td>
                    </tr>
                </table>
            </div>
        <?php } ?>

            <table>

        <?php
    }

    public function update_option_promo_banner_settings( $value ) {

        $promo_banner_settings = [];

        foreach ( $value as $fields => $settings ) {
            foreach ( $settings as $key => $setting ) {
                $promo_banner_settings[ $key ][ $fields ] = trim( $setting );
            }
        }

        return $promo_banner_settings;
    }

    public function promo_banner_scripts() {
        wp_enqueue_script( 'media-upload' );
        wp_enqueue_script( 'thickbox' );
    }

    public function promo_banner_styles() {
        wp_enqueue_style( 'thickbox' );
    }

    public function add_category_banner() {
        ?>

        <div class="form-field">
            <label><?php _e( 'Banner', 'woocommerce' ); ?></label>
            <div id="product_cat_banner" style="float: left; margin-right: 10px;">
                <img src="<?php echo esc_url( wc_placeholder_img_src() ); ?>" width="60px" height="60px" /></div>
            <div style="line-height: 60px;">
                <input type="hidden" id="product_cat_banner_id" name="product_cat_banner_id" />
                <button type="button" class="upload_image_banner_button button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                <button type="button" class="remove_image_banner_button button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
            </div>
            <div class="clear"></div>
        </div>
        <?php
    }


    public function edit_category_banner( $term ) {

        $banner_id = absint( get_term_meta( $term->term_id, 'banner_id', true ) );

        if ( $banner_id ) {
            $image = wp_get_attachment_thumb_url( $banner_id );
        } else {
            $image = wc_placeholder_img_src();
        }

        $banner_id_1 = absint( get_term_meta( $term->term_id, 'banner_id_1', true ) );

        if ( $banner_id_1 ) {
            $image_1 = wp_get_attachment_thumb_url( $banner_id_1 );
        } else {
            $image_1 = wc_placeholder_img_src();
        }


        ?>
        <tr class="form-field category-banner-field">
            <th scope="row" valign="top"><label><?php _e( 'Banner', 'woocommerce' ); ?></label></th>
            <td>
                <div id="product_cat_banner" style="float: left; margin-right: 10px;">
                    <img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" />
                </div>
                <div style="line-height: 60px;">
                    <input type="hidden" id="product_cat_banner_id" name="product_cat_banner_id" value="<?php echo $banner_id; ?>" />
                    <button type="button" class="upload_image_banner_button button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                    <button type="button" class="remove_image_banner_button button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
                </div>
                <div class="clear"></div>
            </td>
        </tr>

        <tr class="form-field category-banner-field">
            <th scope="row" valign="top"><label><?php _e( 'Second banner', 'woocommerce' ); ?></label></th>
            <td>
                <div id="product_cat_banner" style="float: left; margin-right: 10px;">
                    <img src="<?php echo esc_url( $image_1 ); ?>" width="60px" height="60px" />
                </div>
                <div style="line-height: 60px;">
                    <input type="hidden" id="product_cat_banner_id_1" name="product_cat_banner_id_1" value="<?php echo $banner_id_1; ?>" />
                    <button type="button" class="upload_image_banner_button button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                    <button type="button" class="remove_image_banner_button button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
                </div>
                <div class="clear"></div>
            </td>
        </tr>
        <?php
    }

    public function save_category_fields( $term_id, $tt_id = '', $taxonomy = '' ) {
        if ( isset( $_POST['product_cat_banner_id'] ) && in_array( $taxonomy, [ 'product_cat', 'product_brand' ] ) ) {
            update_term_meta( $term_id, 'banner_id', absint( $_POST['product_cat_banner_id'] ) );
        }

        if ( isset( $_POST['product_cat_banner_id_1'] ) && in_array( $taxonomy, [ 'product_cat', 'product_brand' ] ) ) {
            update_term_meta( $term_id, 'banner_id_1', absint( $_POST['product_cat_banner_id_1'] ) );
        }
    }

    /**
     * Show custom category banners on category and brand pages.
     */
    public function show_category_promo_banner() {

        $promo_banner = get_option( 'promo_banner_settings' );
        $current_language = kapsula_get_current_language();
        $cat_banner = '<div class="category-banners">';
        $banners = 0;

        foreach ( $promo_banner as $k => $banner ) {
            if ( ! isset( $banner['activate'] ) || $banner['activate'] != 'on' || ! isset( $banner['image'] ) ) {
                unset( $promo_banner[ $k ] );
            }

            if ( ! empty( $banner['banner_language'] ) && $current_language != $banner['banner_language'] ) {
                unset( $promo_banner[ $k ] );
            }
        }

        $promo_banner = array_values( $promo_banner );

        $allowed_taxonomies = [ 'product_cat', 'product_tag', 'product_brand', 'characteristics' ];

        if ( ! is_tax( $allowed_taxonomies ) ) {
            return;
        }

        $description = wc_format_content( term_description() );
        $term = false;

        foreach ( $allowed_taxonomies as $taxonomy ) {
            $current_tax = get_query_var( $taxonomy );
            $term = get_term_by( 'slug', $current_tax, $taxonomy );

            if ( $term ) {
                break;
            }
        }

        if ( $term ) {
            $category_banner = wp_get_attachment_url( get_term_meta( $term->term_id, 'banner_id', true ) );
            if ( $category_banner ) {
                $cat_banner .= '<div class="promo-banner"><img src=' . $category_banner . ' width=100% height="auto" /></div>';
                $banners++;
            }

            $category_banner_1 = wp_get_attachment_url( get_term_meta( $term->term_id, 'banner_id_1', true ) );
            if ( $category_banner_1 ) {
                $cat_banner .= '<div class="promo-banner"><img src=' . $category_banner_1 . ' width=100% height="auto" /></div>';
                $banners++;
            }
        }

        if ( is_tax( 'product_brand' ) ) {
            $term = get_queried_object();
            if ( $term ) {
                $description = '<h1>' . $term->name . '</h1>' . $description;
            }
        }

        if ( $description ) {
            $cat_banner .= '<div class="promo-banner"><div class="term-description brand-description">' . $description . '</div></div>';
            $banners++;
        }

        if ( ! is_tax( 'product_brand' ) ) {
            $promo_banner = array_slice( $promo_banner, 0, 2 - $banners );

            foreach ( $promo_banner as $banner ) {
                if ( $banner['url'] ) {
                    $cat_banner .= '<div class="promo-banner"><a href="' . $banner['url'] . '"><img src=' . $banner['image'] . ' /></a></div>';
                } else {
                    $cat_banner .= '<div class="promo-banner"><img src=' . $banner['image'] . ' width=100% height="auto" /></div>';
                }
            }
        }

        $cat_banner .= '</div>';

        echo $cat_banner;
    }
}
