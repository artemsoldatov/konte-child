<?php
/**
 * Class for WooCommerce changes.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Woocommerce
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Woocommerce {

    /**
     * Change search query to use product filter and other needed functionality
     *
     * @param $query
     */
    public function add_search_by_id( $query ) {

        if ( isset( $_GET['s'] ) && ! is_admin() && $query->is_main_query() && is_numeric( $_GET['s'] ) ) {

            $query->set( 'p', $_GET['s'] );
            add_filter( 'posts_search', [ $this, 'use_search_with_post_request' ] );
        }
    }

    /**
     * Change search SQL request from 'AND' to 'OR'
     *
     * @param $search
     *
     * @return mixed
     */
    public function use_search_with_post_request( $search ) {
        return substr_replace( $search, ' OR', 0, 4 );
    }

    /**
     * Copy product brand and product category to new product on duplicate action
     *
     * @param $duplicate
     * @param $product
     */
    public function copy_product_taxonomies_on_duplication( $duplicate, $product ) {

        $brands = wp_get_object_terms( $product->get_id(), 'product_brand' );
        $categories = wp_get_object_terms( $product->get_id(), 'product_cat' );

        $brand_terms = [];
        $category_terms = [];
        foreach ( $brands as $brand ) {
            $brand_terms[] = $brand->term_id;
        }

        foreach ( $categories as $category ) {
            $category_terms[] = $category->term_id;
        }

        wp_set_object_terms( $duplicate->get_id(), $brand_terms, 'product_brand' );
        wp_set_object_terms( $duplicate->get_id(), $category_terms, 'product_cat' );
    }

    /**
     * Add taxonomies with pa_ prefix to products request.
     *
     * @param $tax_query
     *
     * @see wp-content/plugins/woocommerce/includes/class-wc-query.php
     *
     * @return mixed
     */
    public function add_pa_taxonomies_to_request( $tax_query ) {

        foreach ( $this->get_attributes_from_request( $tax_query ) as $taxonomy => $data ) {

            $tax_query[] = [
                'taxonomy'         => $taxonomy,
                'field'            => 'slug',
                'terms'            => $data['terms'],
                'operator'         => 'and' === $data['query_type'] ? 'AND' : 'IN',
                'include_children' => false,
            ];
        }

        return $tax_query;
    }

    /**
     * Get attributes from GET request.
     *
     * @param $tax_query
     *
     * @return array
     */
    public function get_attributes_from_request( $tax_query ) {
        // phpcs:disable WordPress.Security.NonceVerification.Recommended
        $chosen_attributes = [];
        unset( $tax_query['relation'] );
        $taxonomies = wp_list_pluck( $tax_query, 'taxonomy' );

        if ( ! empty( $_GET ) ) {
            foreach ( $_GET as $key => $value ) {
                if ( 0 === strpos( $key, 'pa_' ) ) {
                    $attribute    = wc_sanitize_taxonomy_name( str_replace( 'pa_', '', $key ) );
                    $taxonomy     = wc_attribute_taxonomy_name( $attribute );
                    $filter_terms = ! empty( $value ) ? explode( ',', wc_clean( wp_unslash( $value ) ) ) : [];

                    if ( in_array( $taxonomy, $taxonomies ) ) {
                        continue;
                    }
                    if ( empty( $filter_terms ) || ! taxonomy_exists( $taxonomy ) || ! wc_attribute_taxonomy_id_by_name( $attribute ) ) {
                        continue;
                    }

                    $query_type = ! empty( $_GET[ 'query_type_' . $attribute ] ) && in_array( $_GET[ 'query_type_' . $attribute ], [ 'and', 'or' ], true ) ? wc_clean( wp_unslash( $_GET[ 'query_type_' . $attribute ] ) ) : '';
                    $chosen_attributes[ $taxonomy ]['terms'] = array_map( 'sanitize_title', $filter_terms ); // Ensures correct encoding.
                    $chosen_attributes[ $taxonomy ]['query_type'] = $query_type ? $query_type : apply_filters( 'woocommerce_layered_nav_default_query_type', 'and' );
                }
            }
        }
        return $chosen_attributes;
        // phpcs:disable WordPress.Security.NonceVerification.Recommended
    }

    /**
     * Set default catalog order by date.
     *
     * @return string
     */
    public function set_default_orderby() {
        return 'date';
    }

    /**
     * Change order by filters in catalog pages.
     *
     * @param $order_by
     *
     * @return array
     */
    public function change_orderby_catalog_filter( $order_by ) {

        return [
            'menu_order' => __( 'Default sorting (custom ordering + name)', 'woocommerce' ),
            'date'       => __( 'По новизне', 'kapsula' ),
            'rating'     => __( 'Average rating', 'woocommerce' ),
            'price'      => __( 'Sort by price (asc)', 'woocommerce' ),
            'price-desc' => __( 'Sort by price (desc)', 'woocommerce' ),
        ];
    }

    /**
     * Get filled values when updating checkout via AJAX.
     *
     * @param $value
     * @param $input
     *
     * @return array|false|string
     */
    public function get_custom_form_values_for_checkout( $value, $input ) {

        if ( isset( $_POST['post_data'] ) ) {

            parse_str( $_POST['post_data'], $params );

            if ( isset( $params[ $input ] ) ) {
                return wc_clean( wp_unslash( $params[ $input ] ) );
            } elseif ( in_array( $input, [ 'billing_connection_phone', 'billing_connection_viber', 'billing_connection_email', 'billing_connection_no', 'shipping_consultation' ] ) ) {
                return false;
            }
        }

        return $value;
    }
    /**
     * If we have additional parameters in request add them to global query.
     *
     * @param $wp_query
     */
    public function add_vars_to_request( $wp_query ) {

        if ( ! $wp_query->is_main_query() ) {
            return;
        }

        if ( isset( $_REQUEST['characteristics'] ) ) {
            $wp_query->query_vars['tax_query'][] = [
                'taxonomy' => 'characteristics',
                'field' => 'slug',
                'terms' => esc_sql( $_REQUEST['characteristics'] )
            ];
        }
    }
    /**
     * Validate checkout fields.
     */
    public function checkout_custom_validation() {
        if ( isset( $_POST['billing_phone'] ) && ( strlen( $_POST['billing_phone'] ) < 8 || strlen( $_POST['billing_phone'] ) > 13 ) ) {
            wc_add_notice( __( 'Неправильный номер телефона', 'kapsula' ), 'error' );
        }
    }

    /**
     * Show discount table in admin if we have discounts in order.
     *
     * @param $order_id
     */
    public function woocommerce_admin_order_item_headers( $order_id ) {

        if ( $this->get_order_sale_amount( $order_id ) ) {
            // set the column name
            $column_name = __( 'Сезонная скидка', 'woocommerce' );
            // display the column name
            echo '<th>' . $column_name . '</th>';
        }
    }

    /**
     * Discount calculations for products on sale.
     *
     * @param $order_id
     *
     * @return false|float|int
     */
    public function get_order_sale_amount( $order_id ) {

        $order = new \WC_Order( $order_id );

        if ( ! $order ) {
            return false;
        }

        $sale_amount = 0;
        $items = $order->get_items();

        foreach ( $items as $item ) {
            $sale_amount += $this->get_order_item_sale_amount( $item );
        }

        return $sale_amount;
    }

    /**
     * Add regular price for line item if it on sale.
     *
     * @param $item
     */
    public function add_line_item_regula_price( $item ) {

        $product_obj = $item->get_product();

        if ( ! $product_obj || ! $product_obj->is_on_sale() ) {
            return;
        }

        $item->update_meta_data( '_regular_price', $product_obj->get_regular_price() );
    }

    /**
     * Get sale amount of order item.
     *
     * @param $item
     *
     * @return false|float|int
     */
    private function get_order_item_sale_amount( $item ) {

        $id = $item->get_id();

        $regular_price = get_metadata( 'order_item', $id, '_regular_price', true );
        $line_total = get_metadata( 'order_item', $id, '_line_subtotal', true );

        if ( (int) $regular_price && $regular_price != $line_total ) {
            return ceil( $regular_price - $line_total );
        }

        return 0;
    }

    /**
     * Show order cost in admin.
     *
     * @param $product
     * @param $item
     */
    public function woocommerce_admin_order_item_values( $product, $item ) {

        $order_item_sale_amount = $this->get_order_item_sale_amount( $item );

        $currency = get_woocommerce_currency_symbol();

        if ( $product && $order_item_sale_amount ) {
            echo '<td class="line_cost" width="5%"><div class="view">';
            echo '<del>' . get_metadata( 'order_item', $item->get_id(), '_regular_price', true ) . ' ' . $currency . '</del> ';
            echo get_metadata( 'order_item', $item->get_id(), '_line_subtotal', true ) . ' ' . $currency;
            echo '</div></td>';
        } else {
            echo '<td class="line_cost" width="5%"></td>';
        }
    }

    /**
     * Show order discounts in admin.
     *
     * @param $order_id
     */
    public function show_discounts_in_admin( $order_id ) {

        $order = new \WC_Order( $order_id );
        $currency = get_woocommerce_currency_symbol();
        $sale_amount = $this->get_order_sale_amount( $order_id );

        if ( $sale_amount > 0 ) {
            ?>
            <tr>
                <td class="label"><?php echo wc_help_tip( __( 'Общая сезоннная скидка товаров из заказа', 'woocommerce' ) ); ?><?php echo __( 'Общая сезонная скидка', 'woocommerce' ); ?></td>
                <td class="total">
                    <span class="amount"><?php echo $sale_amount . ' ' . $currency; ?> </span>
                </td>
                <td width="1%"></td>
            </tr>
            <?php
        }

        if ( in_array( $order->get_payment_method(), [ 'platononline', 'fondy' ] ) && $order->get_billing_country() == 'UA' ) {
            ?>
            <tr>
                <td class="label"><?php echo wc_help_tip( __( 'Данная скидка предоставляется при оплате заказа онлайн используя платежную систему Platon или Fondy', 'woocommerce' ) ); ?><?php echo __( 'Скидка за оплату онлайн -2%:', 'woocommerce' ); ?></td>
                <td class="total">
                    <span class="amount">
                        <?php echo json_decode( $order->get_item( get_feed_id( $order, 'Оплата онлайн -2%' ) ), true )['amount'] . $currency; ?>
                    </span>
                </td>
                <td width="1%"></td>
            </tr>
            <?php
        }
    }

    /**
     * Show only products with in stock variation when used filter.
     *
     * @param $args
     * @param $wp_query
     *
     * @return mixed
     */
    public static function filter_in_stock_variations( $args, $wp_query ) {

        global $wpdb;

        if ( ! isset( $_REQUEST['filter_size'] ) ) {
            return $args;
        }

        $filter_size = array_filter( explode( ',', $_REQUEST['filter_size'] ) );

        if ( $wp_query->is_main_query() ) {

            if ( ! strstr( $args['join'], 'children_lookup' ) ) {
                $args['join'] .= " LEFT JOIN {$wpdb->posts} children ON $wpdb->posts.`ID` = children.`post_parent` ";
                $args['join'] .= " LEFT JOIN {$wpdb->postmeta} children_meta ON children.`ID` = children_meta.`post_id` ";
                $args['join'] .= " LEFT JOIN {$wpdb->wc_product_meta_lookup} children_lookup ON children.ID = children_lookup.product_id ";
            }

            $args['where'] .= $wpdb->prepare( ' AND children_lookup.stock_status = %s ', 'instock' );
            $args['where'] .= $wpdb->prepare( " AND children_meta.`meta_key` = 'attribute_pa_size' AND children_meta.`meta_value` IN (" . implode( ',', array_fill( 0, count( $filter_size ), '%s' ) ) . ') ', $filter_size );

            remove_filter( 'posts_clauses', [ __NAMESPACE__ . '\Woocommerce', 'filter_in_stock_variations' ], 99 );
        }

        return $args;
    }

    /**
     * Don't show title on product brand pages as we will show it in header description.
     *
     * @param $show
     *
     * @return false
     */
    public function show_page_title( $show ) {

        if ( is_tax( 'product_brand' ) ) {
            return false;
        }

        return $show;
    }

    /**
     * Adding sale percent to products on sale.
     *
     * @param $price_html
     * @param $product
     *
     * @return string
     */
    public function add_sale_percent_text( $price_html, $product ) {

        if ( is_product() && $product->get_id() === get_queried_object_id() ) {
            return $price_html;
        }

        if ( $product->is_on_sale() && $product->is_type( 'variable' ) ) {

            $prices = $product->get_variation_prices( true );
            $prices = array_map( 'array_unique', $prices );

            // Check if we have the same price on all variations
            if (
                isset( $prices['regular_price'] )
                && isset( $prices['sale_price'] )
                && count( $prices['regular_price'] ) === 1
                && count( $prices['sale_price'] ) === 1 ) {
                $price_html .= '<span class="sale-percent">(-';

                $price_html .= round( 100 - reset( $prices['sale_price'] ) / reset( $prices['regular_price'] ) * 100 );

                $price_html .= '%)</span>';
            }
        }

        return $price_html;
    }

    /**
     * Product sizes sorting function.
     */
    public function add_product_sizes_html() {

        global $product;

        if ( $product && $product->is_type( 'variable' ) ) {
            $children = $product->get_children();

            $size_list = [];
            foreach ( $children as $child ) {
                $variation = wc_get_product( $child );

                if ( ! $variation || ! $variation->is_type( 'variation' ) || ! $variation->is_in_stock() ) {
                    continue;
                }

                $size_list[] = strtolower( $variation->get_attribute( 'pa_size' ) );
            }

            // First sort by usual sizes from low to high
            asort( $size_list );
            // Second user defined sort by sizes like 'xxs', 'xs'
            usort( $size_list, 'sort_size_attributes' );

            echo '<span class="product-sizes">' . implode( ' ', $size_list ) . '</span>';
        }
    }

    /**
     * Wishlist button.
     */
    public static function add_wishlist_button( $product_id = 0 ) {

        $atts = $product_id ? 'id="' . $product_id . '"' : '';

        echo do_shortcode( '[add_to_wishlist ' . $atts . ']' );
    }

    /**
     * Wrapper for remove filters functionality.
     *
     * Used in parent theme for filters in topbar.
     * To use remove filters with sidebar filters need to add it to the page.
     *
     * @see wp-content/themes/konte/inc/woocommerce/template-catalog.php
     */
    public function add_remove_filters_wrapper() {
        echo '<p class="products-filter-toggle"></p>';
    }

    /**
     * Change checkout form fields for nova poshta plugin.
     *
     * The field to show configured in Nova_Poshta class
     *
     * @see wp-content/themes/konte-child/classes/woocommerce/class-nova-poshta.php
     *
     * @param $field
     * @param $key
     * @param $args
     *
     * @return string
     */
    public function change_form_field( $field, $key, $args ) {

        if ( $key == 'shipping_nova_poshta_for_woocommerce_city'
            && ( ! isset( $args['initiator'] ) || $args['initiator'] !== 'theme' ) ) {
            return '';
        }

        return $field;
    }

    /**
     * Remove shipping state and postcode to allow show delivery by choosing country only.
     *
     * @param $fields
     *
     * @return mixed
     */
    public function override_shipping_fields( $fields ) {

        unset( $fields['shipping_state'] );
        unset( $fields['shipping_postcode'] );

        return $fields;
    }

    /**
     * Changes in checkout fields according to theme requirements.
     *
     * @param $fields
     *
     * @return mixed
     */
    public function change_checkout_fields( $fields ) {

        unset( $fields['billing']['billing_company'] );
        unset( $fields['billing']['billing_address_2'] );
        unset( $fields['shipping']['shipping_company'] );
        unset( $fields['shipping']['shipping_city'] );
        unset( $fields['shipping']['shipping_state'] );
        unset( $fields['shipping']['shipping_postcode'] );

        Nova_Poshta::update_checkout_fields( $fields );

        $fields['billing']['billing_phone']['priority'] = 35;
        $fields['billing']['billing_email']['priority'] = 38;
        $fields['billing']['billing_address_1']['priority'] = 82;

        WC()->shipping()->load_shipping_methods();
        $shipping_methods = WC()->shipping->get_shipping_methods();

        if ( ! empty( $shipping_methods ) ) {
            foreach ( $shipping_methods as $id => $shipping_method ) {
                if ( isset( $shipping_method->enabled ) && $shipping_method->enabled == 'yes' ) {
                    $active_methods[ $id ] = $shipping_method->title;
                }
            }
        }

        $countries = WC()->countries->get_allowed_countries();
        $first_countries = [ '' => 'Выберите страну доставки' ];
        foreach ( $countries as $key => $value ) {
            if ( $value == 'Азербайджан' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Армения' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Беларусь' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Грузия ' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Казахстан' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Кыргызстан' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Молдова' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            } elseif ( $value == 'Россия' ) {
                $first_countries[ $key ] = $value;
                unset( $countries[ $key ] );
            }
        }
        $first_countries['separator'] = '_______________________________________________________';

        asort( $countries );

        $countries = array_merge( $first_countries, $countries );

        $fields['shipping']['shipping_country']['required'] = true;
        $fields['shipping']['shipping_country']['class'] = [ 'form-row-wide', 'address-field', 'update_totals_on_change' ];
        $fields['shipping']['shipping_country']['type'] = 'select';
        $fields['shipping']['shipping_country']['autocomplete'] = 'country';
        $fields['shipping']['shipping_country']['priority'] = 30;
        $fields['shipping']['shipping_country']['options'] = $countries;

        $fields['shipping']['shipping_city']['label'] = __( 'Город', 'kapsula' );
        $fields['shipping']['shipping_city']['priority'] = 35;
        $fields['shipping']['shipping_city']['required'] = true;
        $fields['shipping']['shipping_city']['placeholder'] = __( 'Укажите город', 'kapsula' );

        $fields['shipping']['shipping_address_1']['required'] = true;
        $fields['shipping']['shipping_address_1']['label'] = __( 'Адрес', 'kapsula' );
        $fields['shipping']['shipping_address_1']['placeholder'] = __( 'Укажите адрес', 'kapsula' );

        $fields['connection']['billing_connection_phone']['label'] = __( 'Телефон', 'kapsula' );
        $fields['connection']['billing_connection_phone']['type'] = 'checkbox';
        $fields['connection']['billing_connection_phone']['priority'] = 5;
        $fields['connection']['billing_connection_viber']['label'] = 'Viber';
        $fields['connection']['billing_connection_viber']['type'] = 'checkbox';
        $fields['connection']['billing_connection_viber']['priority'] = 5;
        $fields['connection']['billing_connection_email']['label'] = 'Email';
        $fields['connection']['billing_connection_email']['type'] = 'checkbox';
        $fields['connection']['billing_connection_email']['priority'] = 5;
        $fields['connection']['billing_connection_no']['label'] = __( 'Мне можно не перезванивать для подтверждения заказа', 'kapsula' );
        $fields['connection']['billing_connection_no']['type'] = 'checkbox';
        $fields['connection']['billing_connection_no']['priority'] = 5;

        $fields['order']['shipping_consultation']['label'] = __( 'Необходима консультация по размерам', 'kapsula' );
        $fields['order']['shipping_consultation']['type'] = 'checkbox';
        $fields['order']['shipping_consultation']['priority'] = 10;

        $fields['order']['order_comments']['placeholder'] = __( 'Комментарий', 'kapsula' );
        $fields['order']['order_comments']['label'] = __( 'Комментарий', 'kapsula' );
        $fields['order']['order_comments']['placeholder'] = __( 'Укажите ваши мерки фигуры для уточнения размеров, и другие пожелания к заказу', 'kapsula' );
        $fields['order']['order_comments']['priority'] = 15;

        $fields['shipping']['shipping_order_table'] = [
            'type' => 'order_table',
            'label' => 'Heading here',
        ];

        return $fields;
    }

    /**
     * Start checkout form column wrapper.
     */
    public function checkout_form_col_open() {
        echo '<div class="col-md-9 checkout-form-column">';
    }

    /**
     * End checkout column wrapper.
     */
    public function checkout_form_col_close() {
        echo '</div>';
    }

    /**
     * Show products in the cart on checkout page.
     */
    public function show_checkout_products() {

        ob_start();

        get_template_part( 'woocommerce/checkout/checkout', 'side' );

        echo ob_get_clean();
    }

    /**
     * Add sidebar items for update on AJAX action.
     *
     * @param $fragments
     *
     * @return mixed
     */
    public function add_order_review_fragments( $fragments ) {

        ob_start();

        get_template_part( 'woocommerce/checkout/checkout', 'side' );

        $fragments['.checkout-sidebar'] = ob_get_clean();

        return $fragments;
    }

    /**
     * Titles for steps functionality in checkout.
     */
    public function add_checkout_order_review_titles() {

        ?>

        <h3 class="checkout-order-review-shipping"><?php _e( 'Способ доставки', 'kapsula' ); ?></h3>

        <?php
    }

    /**
     * Buttons for steps functionality in checkout.
     */
    public function add_checkout_order_review_buttons() {

        ?>

        <div class="checkout-step-buttons step-2">
            <a href="<?php echo wc_get_cart_url(); ?>" class="button go-cart -first"><?php _e( 'Назад', 'kapsula' ); ?></a>
            <button><?php _e( 'Оформить заказ', 'kapsula' ); ?></button>
        </div>

        <?php

        if ( wp_is_mobile() ) {
            wc_get_template( 'checkout/terms.php' );
        }
    }

    /**
     * Filter products in catalog by sale percent.
     *
     * @param $where
     * @param $wp_query
     *
     * @return string
     */
    public function sale_percent_price_filter( $where, $wp_query ) {

        global $wpdb;

        if ( ! is_main_query() || ! isset( $_GET['sale_products'] ) ) {
            return $where;
        }

        $and = '';

        if ( isset( $_GET['sale_from'] ) ) {
            $and .= ' AND 100 - pm.`meta_value` / pm1.`meta_value` * 100 >= ' . abs( intval( $_GET['sale_from'] ) );
        }

        if ( isset( $_GET['sale_to'] ) ) {
            $and .= ' AND 100 - pm.`meta_value` / pm1.`meta_value` * 100 <= ' . abs( intval( $_GET['sale_to'] ) );
        }

        $where .= " AND {$wpdb->posts}.`ID` IN ( SELECT p.post_parent FROM {$wpdb->posts} p 
				JOIN {$wpdb->postmeta} pm ON p.`ID` = pm.`post_id`
				JOIN {$wpdb->postmeta} pm1 ON p.`ID` = pm1.`post_id`
				WHERE p.`post_type` = 'product_variation'
				AND pm.`meta_key` = '_price' 
				AND pm1.`meta_key` = '_regular_price'
				AND pm.`meta_value` != pm1.`meta_value`
				AND pm.`meta_value` > 0
				AND p.`post_parent` > 0
				$and
				) ";

        remove_class_filter( 'posts_where', 'dev\Kapsula\Theme\Woocommerce\Woocommerce', 'sale_percent_price_filter' );

        return $where;
    }

    /**
     * Filter products in catalog by in showroom availability.
     *
     * @param $join
     * @param $wp_query
     *
     * @return string
     */
    public function join_in_showroom( $join, $wp_query ) {

        global $wpdb;

        if ( ! $wp_query->is_main_query() ) {
            return $join;
        }

        if ( ( ! isset( $_GET['in_showroom'] ) || $_GET['in_showroom'] != 'in_showroom' ) && ( ! isset( $_GET['ready_to_delivery'] ) || $_GET['ready_to_delivery'] != 'ready_to_delivery' ) ) {
            return $join;
        }

        $join .= " JOIN {$wpdb->posts} as pv ON {$wpdb->posts}.ID = pv.post_parent
		    JOIN {$wpdb->prefix}warehouses as showroom ON showroom.product = pv.ID";

        return $join;
    }

    /**
     * Filter products in catalog by in showroom availability.
     *
     * @param $where
     * @param $wp_query
     *
     * @return string
     */
    public function where_in_showroom( $where, $wp_query ) {

        if ( ! $wp_query->is_main_query() ) {
            return $where;
        }

        if ( ( ! isset( $_GET['in_showroom'] ) || $_GET['in_showroom'] != 'in_showroom' ) && ( ! isset( $_GET['ready_to_delivery'] ) || $_GET['ready_to_delivery'] != 'ready_to_delivery' ) ) {
            return $where;
        }

        if ( isset( $_GET['ready_to_delivery'] ) && $_GET['ready_to_delivery'] == 'ready_to_delivery' ) {
            $where .= ' AND showroom.amount_showroom + showroom.amount_shop > 0';
        } elseif ( $_GET['in_showroom'] ) {
            $where .= ' AND showroom.amount_showroom > 0';
        }

        return $where;
    }

    /**
     * Set cookie for catalog slug from which user came to the catalog.
     * After user press reset filters button he will be redirected to this catalog page.
     */
    public function catalog_start_category_cookie() {

        if ( wp_doing_ajax() ) {
            return;
        }

        if ( function_exists( 'is_shop' ) && is_shop() ) {
            return;
        }

        if ( is_tax( 'product_cat' ) || is_tax( 'product_brand' ) ) {
            setcookie( 'start_category', $_SERVER['REQUEST_URI'], 0, '/' );
        } else {
            setcookie( 'start_category', '/shop', time() - DAY_IN_SECONDS, '/' );
        }
    }
}
