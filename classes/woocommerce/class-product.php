<?php
/**
 * Class for manipulations with product page.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Woocommerce;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Product
 *
 * @package dev\Kapsula\Theme\Woocommerce
 */
class Product {

    function product_related_posts_query( $query, $product_id, $args ) {

        global $wpdb;

        $related_terms = wc_get_product_terms(
            $product_id,
            'product_cat',
            [
                'menu_order'   => 'ASC',
                'fields' => 'ids',
                'hierarchical' => true
            ]
        );

        // Remove top level categories
        foreach ( $related_terms as $k => $term ) {
            foreach ( $related_terms as $term1 ) {
                if ( term_is_ancestor_of( $term, $term1, 'product_cat' ) ) {
                    unset( $related_terms[ $k ] );
                }
            }
        }

        $related_colors = wc_get_product_terms( $product_id, 'pa_hidden-color', [ 'fields' => 'ids' ] );

        $data_store    = \WC_Data_Store::load( 'product' );
        $query = $data_store->get_related_products_query( [], [], $args['exclude_ids'], $args['limit'], $product_id );


        if ( ! empty( $related_terms ) ) {
            $query['join'] .= " INNER JOIN (
                            SELECT object_id FROM {$wpdb->term_relationships}
                                INNER JOIN {$wpdb->term_taxonomy} using( term_taxonomy_id )
                                    WHERE term_id IN ( " . implode( ',', array_map( 'absint', $related_terms ) ) . ' )
                            )
                                AS include_join ON include_join.object_id = p.ID';
        }

        if ( ! empty( $related_colors ) ) {
            $query['join'] .= " INNER JOIN (
                            SELECT object_id FROM {$wpdb->term_relationships}
                                INNER JOIN {$wpdb->term_taxonomy} using( term_taxonomy_id )
                                    WHERE term_id IN ( " . implode( ',', array_map( 'absint', $related_colors ) ) . ' )
                            )
                                AS include_join1 ON include_join1.object_id = p.ID';
        }

        return $query;
    }

    /**
     * Add end sale date string.
     *
     * @param $price
     * @param $product
     *
     * @return string
     */
    public function add_sale_date_end( $price, $product ) {

        if ( is_product() && $product->is_type( 'variable' ) && $product->is_on_sale() ) {
            foreach ( $product->get_visible_children() as $child_id ) {

                $child_product = wc_get_product( $child_id );

                if ( $child_product && $child_product->get_date_on_sale_to() ) {
                    $price .= '<span class="sale-date-to">(' . __( 'акция до', 'kapsula' ) . $child_product->get_date_on_sale_to()->date_i18n( ' d.m' ) . ')</span>';
                    break;
                }
            }
        }

        return $price;
    }

    public function add_price_attribute_for_pixel() {

        global $product;

        echo '<div class="product-price hidden">' . $product->get_price() . '</div>';
    }

    /**
     * Add quantity to variation args.
     *
     * @param $args
     * @param $product
     * @param $variation
     *
     * @return mixed
     */
    public function add_variation_args( $args, $product, $variation ) {

        $args['quantity'] = $variation->get_stock_quantity();

        return $args;
    }

    /**
     * Set preselected attribute for product add to cart block.
     *
     * @param $args
     *
     * @return mixed
     */
    public function set_selected_attribute_options( $args ) {

        $args['class'] = $args['attribute'] . '-select';

        if ( $args['attribute'] == 'pa_color' ) {
            $args['selected'] = $args['options'][0];
            $args['show_option_none'] = false;
        } elseif ( $args['attribute'] == 'pa_size' && is_product() ) {
            $args['show_option_none'] = __( 'Выбрать размер', 'kapsula' );

            if ( $args['product'] && $args['product']->is_type( 'variable' ) ) {
                $product = $args['product'];

                foreach ( $product->get_available_variations() as $variation ) {

                    if ( is_variation_in_cart( $variation['variation_id'] ) ) {
                        continue;
                    }

                    if ( $variation['is_in_stock'] && isset( $variation['attributes']['attribute_pa_size'] ) ) {
                        $args['selected'] = $variation['attributes']['attribute_pa_size'];
                        break;
                    }
                }
            }
        }

        return $args;
    }

    /**
     * Add additional meta to product page.
     */
    public function add_product_meta_start() {

        global $product;

        echo '<span class="product-id-wrapper"><span class="label">' . __( 'Цвет', 'kapsula' ) . '</span><span class="product-id">' . $product->get_attribute( 'color' ) . '</span></span>';
        echo '<span class="product-id-wrapper"><span class="label">' . __( 'Код товара', 'kapsula' ) . '</span><span class="product-id">' . $product->get_id() . '</span></span>';
    }

    /**
     * Add additional meta to product page.
     */
    public function add_product_meta() {

        global $product;

        echo get_the_term_list( $product->get_id(), 'product_brand', '<span class="posted_in"><span class="label">' . __( 'Бренд', 'kapsula' ) . '</span>', ', ', '</span>' );
    }

    /**
     * Wrap term links with span element.
     *
     * @param $links
     *
     * @return array
     */
    public function wrap_term_links( $links ) {

        if ( ! is_array( $links ) || count( $links ) < 2 ) {
            return $links;
        }

        $first_element = reset( $links );
        $links[ key( $links ) ] = '<span class="term-links">' . $first_element;

        $last_element = end( $links );
        $links[ key( $links ) ] = $last_element . '</span>';

        return $links;
    }

    /**
     * Configure tabs on product page.
     *
     * @param $tabs
     *
     * @return mixed
     */
    public function set_product_tabs( $tabs ) {

        unset( $tabs['description'] );
        unset( $tabs['additional_information'] );

        $tabs['delivery'] = [
            'title' => __( 'Доставка и возврат', 'kapsula' ),
            'priority' => 10,
            'callback' => [ $this, 'get_delivery_text' ]
        ];

        $tabs['brand'] = [
            'title' => __( 'О бренде', 'kapsula' ),
            'priority' => 20,
            'callback' => [ $this, 'get_brand_text' ]
        ];

        return $tabs;
    }

    /**
     * The text for delivery tab.
     */
    public function get_delivery_text() {

        ?>

        <div class="panel-body">
            <ul>
                <li><?php _e( 'Бесплатная доставка по всей Украине', 'kapsula' ); ?></li>
                <li><?php _e( 'По Киеву доставка с примеркой до 30 мин', 'kapsula' ); ?></li>
                <li><?php _e( 'После оформления заказа клиент-менеджер согласует с Вами удобную дату и время доставки.', 'kapsula' ); ?></li>
                <li><?php _e( 'Ориентировочные сроки доставки указаны для каждого изделия на странице описания товара.', 'kapsula' ); ?></li>
                <li><?php _e( 'В день доставки курьер свяжется с Вами для подтверждения времени и адреса доставки.', 'kapsula' ); ?></li>
                <li><?php _e( 'Вы можете вернуть или обменять заказ полностью или частично в случае, если вещь вам не понравилась или не подошла в течение 14 календарных дней. ', 'kapsula' ); ?></li>
                <li><?php _e( 'Вы можете примерить изделия категорий нижнего белья, перчаток и ювелирных изделий перед покупкой. После покупки эти изделия не подлежат обмену или возврату. ', 'kapsula' ); ?></li>
                <li><?php _e( 'Мы гарантируем 100% возврат денежных средств на банковскую карту в течение 5 рабочих дней.', 'kapsula' ); ?></li>
            </ul>
        </div>

        <?php
    }

    /**
     * The text for brand tab.
     */
    public function get_brand_text() {

        global $product;

        if ( ! $product ) {
            return;
        }

        $terms = get_the_terms( $product->get_id(), 'product_brand' );

        if ( ! $terms ) {
            return;
        }

        foreach ( $terms as $term ) {
            echo '<h2>' . $term->name . '</h2>';
            echo '<p>' . $term->description . '</p>';
        }
    }

    public function show_recently_viewed_products() {
        echo do_shortcode( '[rvp_show no_products="12" slider_use="false"]' );
    }

    /**
     * Add stars to rating tab in mobile.
     *
     * @param $title
     *
     * @return string
     */
    public function add_review_rating_to_title( $title ) {

        global $product;

        if ( ! wp_is_mobile() ) {
            return $title;
        }

        return $title . wc_get_rating_html( $product->get_average_rating() );
    }

    /**
     * Changed add to cart text.
     *
     * @param $text
     *
     * @return string|void
     */
    public function change_woocommerce_product_single_add_to_cart_text( $text ) {
        return __( 'Добавить в корзину', 'kapsula' );
    }

    /**
     * Show delivery time custom field.
     */
    public function show_delivery_time() {

        global $product;

        echo '<span class="delivery-note-short"><i class="fa fa-truck"></i>';
        // translators: %s: delivery text
        printf( __( 'Доставка %s', 'kapsula' ), get_post_meta( $product->get_id(), 'delivery_' . kapsula_get_current_language(), 1 ) );
        echo '</span>';
    }

    /**
     * Bitrix form loader button for mobile devices.
     */
    public function add_form_ask_question() {

        if ( wp_is_mobile() ) :
            ?>

            <span class="ask-button b24-web-form-popup-btn-23"><?php _e( 'Задать вопрос о товаре', 'kapsula' ); ?></span>

            <?php
        endif;
    }

    /**
     * Recently viewed products shortcode.
     * Refactored from recently viewed plugin.
     *
     * @see wp-content/plugins/recently-viewed-and-most-viewed-products/recently-viewed-and-most-viewed-products.php
     * @depreacted
     *
     * @param $atts
     *
     * @return false|string
     */
    public function recently_viewed_products( $atts ) {

        global $number_of_products_in_row_in_recent;

        $atts = shortcode_atts(
            [
                'number_of_products_in_row' => '',
                'posts_per_page' => get_option( CED_RVMV_PREFIX . '_wrvp_total_items_display' ),
            ],
            $atts,
            'wrvp_recently_viewed_products'
        );

        $number_of_products_in_row_in_recent = $atts['number_of_products_in_row'];

        $posts_per_page = isset( $atts['posts_per_page'] ) ?? 1;

        if ( $posts_per_page > 20 ) {
            $posts_per_page = 20;
        }

        $args = [
            'post_type' => 'product',
            'ignore_sticky_posts' => 1,
            'no_found_rows' => 1,
            'posts_per_page' => $posts_per_page,
            'orderby' => 'recent_view_time',
            'order' => 'DESC'
        ];

        $wrvp_instock = get_option( CED_RVMV_PREFIX . '_wrvp_instock' );

        if ( $wrvp_instock == 'yes' ) {
            $args['meta_query'] = [
                'relation' => 'AND',
                [
                    'key' => 'recent_view_time',
                    'value' => '',
                    'compare' => '!='
                ],
                [
                    'key' => '_stock_status',
                    'value' => 'instock',
                    'compare' => '=',
                ],
            ];
        } else {
            $args['meta_query'] = [
                [
                    'key' => 'recent_view_time',
                    'value' => '',
                    'compare' => '!='
                ],
            ];
        }

        $products = new \WP_Query( $args );
        ob_start();

        if ( $products->have_posts() ) :

            // Changed output for recent viewed products to use theme slider and styles
            ?>
            <section class="viewed products" data-rows="<?php echo $atts['number_of_products_in_row']; ?>">
                <h2>

                    <?php
                    $wrvp_viewed_title = get_option( CED_RVMV_PREFIX . '_wrvp_viewed_title', null );
                    if ( $wrvp_viewed_title ) {
                        echo $wrvp_viewed_title;
                    } else {
                        echo __( 'Recently viewed products', 'kapsula' );
                    }

                    ?>

                </h2>

                <?php woocommerce_product_loop_start(); ?>

                <?php while ( $products->have_posts() ) : ?>

                    <?php $products->the_post(); ?>

                    <?php wc_get_template_part( 'content', 'product' ); ?>

                <?php endwhile; // end of the loop. ?>

                <?php woocommerce_product_loop_end(); ?>
            </section>

            <?php
        endif;
        $content = ob_get_clean();
        wp_reset_postdata();

        return $content;
    }

    /**
     * Sync variation params with parent product on update in bulk edit plugin.
     *
     * @param $product_id
     */
    public function run_woocommerce_sync( $product_id ) {

        $product = wc_get_product( $product_id );

        if ( ! $product || $product->get_type() != 'variable' ) {
            return;
        }

        \WC_Product_Variable::sync( $product_id );
    }

    /**
     * Add characteristics taxonomy for new arrivals products.
     */
    public function add_characteristics_taxonomy() {

        $labels = [
            'name'                       => _x( 'Characteristics', 'taxonomy general name', 'prdctfltr' ),
            'singular_name'              => _x( 'Characteristic', 'taxonomy singular name', 'prdctfltr' ),
            'search_items'               => __( 'Search Characteristics', 'prdctfltr' ),
            'popular_items'              => __( 'Popular Characteristics', 'prdctfltr' ),
            'all_items'                  => __( 'All Characteristics', 'prdctfltr' ),
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'edit_item'                  => __( 'Edit Characteristics', 'prdctfltr' ),
            'update_item'                => __( 'Update Characteristics', 'prdctfltr' ),
            'add_new_item'               => __( 'Add New Characteristic', 'prdctfltr' ),
            'new_item_name'              => __( 'New Characteristic Name', 'prdctfltr' ),
            'separate_items_with_commas' => __( 'Separate Characteristics with commas', 'prdctfltr' ),
            'add_or_remove_items'        => __( 'Add or remove Characteristics', 'prdctfltr' ),
            'choose_from_most_used'      => __( 'Choose from the most used Characteristics', 'prdctfltr' ),
            'not_found'                  => __( 'No Characteristics found', 'prdctfltr' ),
            'menu_name'                  => __( 'Characteristics', 'prdctfltr' ),
        ];

        $args = [
            'hierarchical'          => false,
            'labels'                => $labels,
            'show_ui'               => true,
            'show_admin_column'     => true,
            'update_count_callback' => '_update_post_term_count',
            'query_var'             => true,
            'rewrite'               => [ 'slug' => 'characteristics' ],
        ];

        register_taxonomy( 'characteristics', [ 'product' ], $args );
    }

    /**
     * Show all product variable children on admin side to allow see prices on out-of-stock products.
     *
     * @param $children
     * @param $variable_product
     * @param $visible_only
     *
     * @return mixed
     */
    public function show_all_children_on_admin( $children, $variable_product, $visible_only ) {

        if ( is_admin() && ! wp_doing_ajax() && $visible_only ) {
            return $variable_product->get_children();
        }

        return $children;
    }
}
