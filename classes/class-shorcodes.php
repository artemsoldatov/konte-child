<?php
/**
 * Class for adding theme shortcodes.
 *
 * @author     artem <artem@dev.com>
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Shortcodes
 *
 * @package dev\Kapsula\Theme
 */
class Shortcodes {

    private $shortcodes = [];

    public function __construct() {
        foreach ( $this->shortcodes as $shortcode => $callback ) {
            add_shortcode( $shortcode, [ $this, $callback ] );
        }
    }
}
