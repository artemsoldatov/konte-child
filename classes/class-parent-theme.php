<?php
/**
 * Class for changes in parent theme.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Parent_Theme
 *
 * @package dev\Kapsula\Theme
 */
class Parent_Theme {

    /**
     * Add code with bitrix form
     */
    public function add_head_tags() {

        ?>

        <meta name=“p:domain_verify” content=“384b7e78b1c095235326c6c66f57e740"/>

        <script>
            (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://crm.kapsula.com.ua/upload/crm/site_button/loader_1_yss73p.js');
        </script>

        <?php

    }

    /**
     * Add currency switcher on v3 header template.
     *
     * @param $sections
     * @param $version
     *
     * @return mixed
     */
    public function add_currency_to_header( $sections, $version ) {

        if ( $version != 'v3' ) {
            return $sections;
        }

        $sections['main']['right'] = [
            [ 'item' => 'account' ],
            [ 'item' => 'currency' ],
            [ 'item' => 'language' ],
            [ 'item' => 'search' ],
            [ 'item' => 'wishlist' ],
            [ 'item' => 'cart' ],
        ];

        return $sections;
    }

    /**
     * Added fluid container class to pages.
     *
     * @param $class
     *
     * @return string
     */
    public function add_fluid_container_class( $class ) {

        if ( is_product() || is_front_page() ) {
            $class = 'product-content-container konte-container-fluid';
        }

        return $class;
    }

    /**
     * Add social items to theme widget.
     *
     * @param $socials
     *
     * @return mixed
     */
    public function add_social_items( $socials ) {

        $socials['telegram'] = esc_html__( 'Telegram', 'kapsula' );

        return $socials;
    }

    /**
     * Replace placeholders from options to real values.
     *
     * @param $value
     * @param $name
     *
     * @return string|string[]
     */
    public function replace_option_placeholders( $value, $name ) {

        if ( $name == 'footer_copyright' ) {
            return str_replace( '%year%', gmdate( 'Y' ), $value );
        }

        return $value;
    }
}
