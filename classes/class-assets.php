<?php
/**
 * Class for assets manipulations.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Assets
 *
 * @package dev\Kapsula
 */
class Assets {

    /**
     * Current theme assets version.
     */
    const VERSION = '1.1.14';

    /**
     * Register and enqueue frontend assets.
     */
    public function enqueue_child_assets() {

        $localize_args = [
            'ajaxURL' => admin_url( 'admin-ajax.php' ),
            'nonce' => wp_create_nonce( 'kapsula' ),
            'language' => kapsula_get_current_language(),
            'translations' => [
                'inCart' => __( 'Товар в корзине. Перейти?', 'kapsula' ),
                'addToCart' => __( 'Add to cart', 'woocommerce' )
            ],
            'links' => [
                'wcCartPage' => '',
                'wcCheckoutPage' => '',
                'wcShopPage' => '',
                'home' => home_url( '/' )
            ]
        ];

        if ( function_exists( 'WC' ) ) {
            $localize_args['links']['wcCartPage'] = wc_get_cart_url();
            $localize_args['links']['wcCheckoutPage'] = wc_get_checkout_url();

            if ( wc_get_page_id( 'shop' ) > 0 ) {
                $localize_args['links']['wcShopPage'] = site_url( get_permalink( wc_get_page_id( 'shop' ) ) );
            } else {
                $localize_args['links']['wcShopPage'] = site_url( '/' . kapsula_get_current_language() . '/shop/' );
            }
        }

        wp_register_style( 'kapsula-common', THEME_ASSETS . '/css/common.css', [ 'konte', 'konte-woocommerce' ], self::VERSION );
        wp_register_style( 'kapsula-home', THEME_ASSETS . '/css/home.css', [ 'kapsula-common' ], self::VERSION );
        wp_register_style( 'kapsula-category', THEME_ASSETS . '/css/category.css', [ 'kapsula-common', 'konte-woocommerce' ], self::VERSION );
        wp_register_style( 'kapsula-checkout', THEME_ASSETS . '/css/checkout.css', [ 'kapsula-common', 'konte-woocommerce' ], self::VERSION );
        wp_register_style( 'kapsula-product', THEME_ASSETS . '/css/product.css', [ 'kapsula-common', 'konte-woocommerce' ], self::VERSION );
        wp_register_style( 'kapsula-cart', THEME_ASSETS . '/css/cart.css', [ 'kapsula-common', 'konte-woocommerce' ], self::VERSION );
        wp_register_style( 'custom-scroll', THEME_ASSETS . '/custom-scroll/jquery.mCustomScrollbar.css', self::VERSION );
        wp_register_style( 'intl-tel-input', THEME_ASSETS . '/intl-tel-input/css/intlTelInput.min.css', [ 'kapsula-common' ], self::VERSION );
        wp_register_style( 'lookbook-page', THEME_ASSETS . '/css/lookbook.css', [ 'kapsula-common', 'kapsula-category' ], self::VERSION );

        wp_register_script( 'kapsula-common', THEME_ASSETS . '/js/common.js', [ 'jquery' ], self::VERSION, true );
        wp_localize_script( 'kapsula-common', 'kapsula', $localize_args );
        wp_register_script( 'kapsula-home', THEME_ASSETS . '/js/home.js', [ 'jquery', 'kapsula-common', 'swiper' ], self::VERSION, true );
        wp_register_script( 'kapsula-category', THEME_ASSETS . '/js/category.js', [ 'jquery', 'kapsula-common', 'konte-products-filter' ], self::VERSION, true );
        wp_register_script( 'kapsula-checkout', THEME_ASSETS . '/js/checkout.js', [ 'jquery', 'jquery-mask', 'kapsula-common' ], self::VERSION, true );
        wp_register_script( 'kapsula-product', THEME_ASSETS . '/js/product.js', [ 'jquery', 'kapsula-common' ], self::VERSION, true );
        wp_register_script( 'kapsula-cart', THEME_ASSETS . '/js/cart.js', [ 'jquery', 'kapsula-common' ], self::VERSION, true );
        wp_register_script( 'kapsula-account', THEME_ASSETS . '/js/my-account.js', [ 'jquery', 'kapsula-common' ], self::VERSION, true );
        wp_register_script( 'jquery-mask', THEME_ASSETS . '/js/jquery.mask.min.js', [ 'jquery' ], self::VERSION, true );
        wp_register_script( 'custom-scroll', THEME_ASSETS . '/custom-scroll/jquery.mCustomScrollbar.concat.min.js', [ 'jquery', 'kapsula-category' ], self::VERSION );
        wp_register_script( 'phone-utils', THEME_ASSETS . '/intl-tel-input/js/utils.js', [], self::VERSION, true );
        wp_register_script( 'intl-tel-input', THEME_ASSETS . '/intl-tel-input/js/intlTelInput.min.js', [ 'phone-utils' ], self::VERSION, true );
        wp_register_script( 'lookbook-page', THEME_ASSETS . '/js/lookbook.js', [ 'jquery', 'kapsula-common', 'swiper' ], self::VERSION, true );

        $enqueue_styles = [ 'kapsula-common' ];
        $enqueue_scripts = [ 'kapsula-common' ];

        // Dequeue select2 from recently viewed products as it conflicted with WooCommerce select2
        wp_dequeue_script( 'rvmv-select2-js' );

        if ( is_front_page() ) {
            $enqueue_styles = array_merge( $enqueue_styles, [ 'kapsula-home', 'kapsula-category' ] );
            $enqueue_scripts = array_merge( $enqueue_scripts, [ 'kapsula-home' ] );
        } elseif ( is_page_template( 'templates/lookbook-container.php' ) ) {
            $enqueue_styles = array_merge( $enqueue_styles, [ 'lookbook-page' ] );
            $enqueue_scripts = array_merge( $enqueue_scripts, [ 'lookbook-page' ] );
        }

        if ( function_exists( 'WC' ) ) {
            if ( is_product_taxonomy() || is_shop() ) {
                $enqueue_styles = array_merge( $enqueue_styles, [ 'custom-scroll', 'kapsula-category' ] );
                $enqueue_scripts = array_merge( $enqueue_scripts, [ 'custom-scroll', 'kapsula-category' ] );
            } elseif ( is_checkout() ) {
                $enqueue_styles = array_merge( $enqueue_styles, [ 'kapsula-checkout' ] );
                $enqueue_scripts = array_merge( $enqueue_scripts, [ 'kapsula-checkout' ] );
            } elseif ( is_product() ) {
                $enqueue_styles = array_merge( $enqueue_styles, [ 'kapsula-product', 'kapsula-category' ] );
                $enqueue_scripts = array_merge( $enqueue_scripts, [ 'kapsula-product' ] );
                // For related products
            } elseif ( is_cart() ) {
                $enqueue_styles = array_merge( $enqueue_styles, [ 'kapsula-cart' ] );
                $enqueue_scripts = array_merge( $enqueue_scripts, [ 'kapsula-cart' ] );
            } elseif ( is_account_page() ) {
                $enqueue_scripts = array_merge( $enqueue_scripts, [ 'kapsula-account' ] );
            }
        }

        wp_enqueue_style( $enqueue_styles );
        wp_enqueue_script( $enqueue_scripts );
    }

    /**
     * Register and enqueue backend assets.
     */
    public function enqueues_admin_assets() {
        wp_enqueue_script( 'kapsula-admin', THEME_ASSETS . '/js/admin.js', [ 'jquery' ], self::VERSION, true );
        wp_enqueue_style( 'kapsula-admin', THEME_ASSETS . '/css/admin.css', [], self::VERSION );
        wp_register_style( 'select2css', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.css', false, '1.0', 'all' );
        wp_register_script( 'select2', '//cdnjs.cloudflare.com/ajax/libs/select2/3.4.8/select2.js', [ 'jquery' ], '1.0', true );
        wp_enqueue_style( 'select2css' );
        wp_enqueue_script( 'select2' );
    }

    /**
     * Register and enqueue login page assets.
     */
    public function enqueues_login_assets() {
        wp_enqueue_style( 'kapsula-login', THEME_ASSETS . '/css/login.css', [], self::VERSION );
    }

    /**
     * Add code for bitrix form from product page.
     */
    public function add_bitrix_form_code() {

        ?>

        <script>
            (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://crm.kapsula.com.ua/upload/crm/site_button/loader_1_yss73p.js');
        </script>

        <?php

        if ( is_product() ) :
            ?>
            <script id="bx24_form_button" data-skip-moving="true">
                (function(w,d,u,b){w['Bitrix24FormObject']=b;w[b] = w[b] || function()

                {arguments[0].ref=u; (w[b].forms=w[b].forms||[]).push(arguments[0])}
                ;
                    if(w[b]['forms']) return;
                    var s=d.createElement('script');s.async=1;s.src=u+'?'+(1*new Date());
                    var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
                })(window,document,'https://crm.kapsula.com.ua/bitrix/js/crm/form_loader.js','b24form');

                b24form(

                    {"id":"23","lang":"ru","sec":"nr2bq4","type":"button","click":""}
                );
            </script>
            <?php
        endif;
    }

    /**
     * Function for disabling scripts and styles
     */
    public function disable_scripts() {
        global $post;
     
        if ( is_page() && $post->post_name == 'showroom-kapsula' ) {
            return;
        }

        wp_dequeue_script( 'google-recaptcha' );
        wp_dequeue_script( 'wpcf7-recaptcha' );
        wp_dequeue_style( 'wpcf7-recaptcha' );

    }
}
