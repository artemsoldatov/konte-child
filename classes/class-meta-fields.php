<?php
/**
 * Class for working with meta boxes and custom fields.
 *
 * @author     artem <artem@dev.com>
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Meta_Fields
 *
 * @package dev\Kapsula\Theme
 */
class Meta_Fields {

    /**
     * Add product meta boxes.
     *
     * @param $meta_boxes
     *
     * @return mixed
     */
    public function register_meta_boxes( $meta_boxes ) {

        $product_fields = [
            [
                'type' => 'text',
                // translators: %s - language name
                'name' => sprintf( esc_html__( 'Доставка %s', 'kapsula' ), 'ru' ),
                'id' => 'delivery_ru',
                'desc' => esc_html__( 'Строка доставки на странице продукта', 'kapsula' ),
            ],
        ];

        if ( function_exists( 'pll_languages_list' ) ) {

            $language_list = pll_languages_list( [ 'fields' => '' ] );

            foreach ( $language_list as $lang ) {

                if ( $lang->slug === pll_default_language() ) {
                    continue;
                }

                $product_fields[] = [
                    'type' => 'text',
                    // translators: %s - language name
                    'name' => sprintf( esc_html__( 'Доставка %s', 'kapsula' ), $lang->slug ),
                    'id' => 'delivery_' . $lang->slug,
                ];
            }
        }

        $meta_boxes[] = [
            'title' => esc_html__( 'Дополнительная информация', 'kapsula' ),
            'id' => 'kapsula_product',
            'post_types' => [ 'product' ],
            'context' => 'normal',
            'priority' => 'high',
            'fields' => $product_fields,
        ];

        return $meta_boxes;
    }
}
