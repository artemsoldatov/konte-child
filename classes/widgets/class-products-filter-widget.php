<?php
/**
 * Class for widgets creations.
 *
 * Copied Konte_Addons_Products_Filter_Widget class
 *
 * @see        wp-content/plugins/konte-addons/includes/widgets/products-filter.php
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme\Widgets;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Products_Filter_Widget
 *
 * @package dev\Kapsula\Theme\Widgets
 */
class Products_Filter_Widget extends \WP_Widget {

    /**
     * Default widget settings
     *
     * @var array
     */
    protected $defaults;

    /**
     * Store active filter field to avoid duplicate fields
     *
     * @var array
     */
    protected $active_fields;

    /**
     * Store other filters from URL
     *
     * @var array
     */
    protected $current_filters;

    /**
     * Filter sources.
     *
     * @var array
     */
    protected $sources;

    /**
     * All product attributes.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Display type of sources.
     *
     * @var array
     */
    protected $display;

    /**
     * Widget constructor.
     */
    public function __construct() {

        $this->defaults = [
            'title' => '',
            'ajax' => true,
            'instant' => false,
            'filter' => [],
        ];

        $this->active_fields = [];
        $this->current_filters = [];

        parent::__construct(
            'kapsula-products-filter',
            esc_html__( 'Kapsula - Products Filter', 'kapsula' ),
            [
                'classname' => 'woocommerce products-filter-widget',
                'description' => esc_html__( 'WooCommerce products filter.', 'konte-addons' ),
            ],
            [ 'width' => 780 ]
        );

        add_action( 'admin_init', [ $this, 'get_sources_display' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'scripts' ] );
        add_action( 'admin_print_scripts', [ $this, 'admin_scripts' ] );
        add_action( 'customize_controls_print_footer_scripts', [ $this, 'admin_form_templates' ] );
        add_action( 'admin_footer', [ $this, 'admin_form_templates' ] );
    }

    /**
     * Widget initialization.
     */
    public static function init_widget() {
        if ( class_exists( 'WooCommerce' ) ) {
            register_widget( __CLASS__ );
        }
    }

    /**
     * Enqueue scripts on the frontend
     */
    public function scripts() {
        wp_enqueue_script(
            'konte-products-filter',
            KONTE_ADDONS_URL . 'assets/js/products-filter.js',
            [
                'jquery',
                'wp-util',
                'jquery-serialize-object',
            ],
            '20180527',
            true
        );
    }

    /**
     * Enqueue scripts in the backend.
     */
    public function admin_scripts() {
        global $pagenow;

        if ( 'widgets.php' != $pagenow && 'customize.php' != $pagenow ) {
            return;
        }

        wp_enqueue_style( 'konte-products-filter-admin', KONTE_ADDONS_URL . 'assets/css/products-filter-admin.css', [], '20190107' );
        wp_enqueue_script( 'konte-products-filter-admin', KONTE_ADDONS_URL . 'assets/js/products-filter-admin.js', [ 'wp-util' ], '20190107', true );

        wp_localize_script(
            'konte-products-filter-admin',
            'konte_products_filter_params',
            [
                'sources' => $this->sources,
                'display' => $this->display,
                'attributes' => $this->attributes,
            ]
        );
    }

    /**
     * Get values of sources, attributes and display.
     */
    public function get_sources_display() {
        $sources = [
            'products_group' => esc_html__( 'Group', 'konte-addons' ),
            'price' => esc_html__( 'Price', 'konte-addons' ),
            'attribute' => esc_html__( 'Attributes', 'konte-addons' ),
            'in_showroom' => esc_html__( 'В шоуруме', 'kapsula' ),
            'ready_to_delivery' => esc_html__( 'Готово к отправке', 'kapsula' )
        ];
        $display = [];
        $attributes = [];

        // Getting attributes.
        $attribute_taxonomies = wc_get_attribute_taxonomies();
        foreach ( $attribute_taxonomies as $taxonomy ) {
            $attributes[ $taxonomy->attribute_name ] = $taxonomy->attribute_label;
        }

        // Getting sources.
        $product_taxonomies = get_object_taxonomies( 'product', 'objects' );
        foreach ( $product_taxonomies as $name => $taxonomy ) {
            if ( ! $taxonomy->public || ! $taxonomy->publicly_queryable ) {
                continue;
            }

            if ( 'product_shipping_class' == $name || array_key_exists( $name, $attributes ) ) {
                continue;
            }

            $sources[ $name ] = $taxonomy->label;
        }

        // Getting display types.
        foreach ( $sources as $source => $label ) {
            switch ( $source ) {
                case 'price':
                    $display[ $source ] = [
                        'slider' => esc_html__( 'Slider', 'konte-addons' ),
                    ];
                    break;

                case 'attribute':
                    $display[ $source ] = [
                        'auto' => esc_html__( 'Auto', 'konte-addons' ),
                        'dropdown' => esc_html__( 'Dropdown', 'konte-addons' ),
                        'list' => esc_html__( 'Vertical List', 'konte-addons' ),
                        'h-list' => esc_html__( 'Horizontal List', 'konte-addons' ),
                        'checkboxes' => esc_html__( 'Checkboxes', 'konte-addons' ),
                    ];
                    break;

                case 'in_showroom':
                case 'ready_to_delivery':
                    $display[ $source ] = [
                        'checkbox-kapsula' => esc_html__( 'Kapsula checkbox', 'konte-addons' ),
                    ];
                    break;
                default:
                    $display[ $source ] = [
                        'dropdown' => esc_html__( 'Dropdown', 'konte-addons' ),
                        'list' => esc_html__( 'Vertical List', 'konte-addons' ),
                        'h-list' => esc_html__( 'Horizontal List', 'konte-addons' ),
                        'checkboxes' => esc_html__( 'Checkboxes', 'konte-addons' ),
                    ];
                    break;
            }
        }

        $this->sources = $sources;
        $this->attributes = $attributes;
        $this->display = $display;
    }

    /**
     * Add Underscore template to footer.
     */
    public function admin_form_templates() {
        global $pagenow;

        if ( 'widgets.php' != $pagenow && 'customize.php' != $pagenow ) {
            return;
        }
        ?>

        <script type="text/template" id="tmpl-konte-products-filter">
            <div class="konte-products-filter-fields">
                <div class="name">
                    <label>
                        <?php esc_html_e( 'Filter Name', 'konte-addons' ); ?>
                        <input type="text" name="{{data.name}}[{{data.count}}][name]" class="widefat">
                    </label>
                </div>
                <div class="source">
                    <label>
                        <?php esc_html_e( 'Filter By', 'konte-addons' ); ?>
                        <select name="{{data.name}}[{{data.count}}][source]" class="widefat filter-by">
                            <# _.each( data.sources, function( name, source ) { #>
                            <option value="{{source}}">{{name}}</option>
                            <# } ); #>
                        </select>
                        <select name="{{data.name}}[{{data.count}}][attribute]" class="widefat filter-attribute hidden">
                            <# _.each( data.attributes, function( name, slug ) { #>
                            <option value="{{slug}}">{{name}}</option>
                            <# } ); #>
                        </select>
                    </label>
                </div>
                <div class="display">
                    <label>
                        <?php esc_html_e( 'Display Type', 'konte-addons' ); ?>
                        <select name="{{data.name}}[{{data.count}}][display]" class="widefat display-type">
                            <# _.each( data.display[_.keys(data.sources)[0]], function( name, type ) { #>
                            <option value="{{type}}">{{name}}</option>
                            <# } ); #>
                        </select>
                        <select name="{{data.name}}[{{data.count}}][multiple]" class="widefat multiple-select hidden">
                            <option value="1"><?php esc_html_e( 'Multiple select', 'konte-addons' ); ?></option>
                            <option value="0"><?php esc_html_e( 'Single select', 'konte-addons' ); ?></option>
                        </select>
                        <select name="{{data.name}}[{{data.count}}][query_type]" class="widefat query-type hidden">
                            <option value="and"><?php esc_html_e( 'Query type: AND', 'konte-addons' ); ?></option>
                            <option value="or"><?php esc_html_e( 'Query type: OR', 'konte-addons' ); ?></option>
                        </select>
                    </label>
                </div>
                <div class="actions">
                    <a href="#" class="remove-filter dashicons dashicons-no-alt">
                        <span class="screen-reader-text"><?php esc_html_e( 'Remove filter', 'konte-addons' ); ?></span>
                    </a>
                </div>
            </div>
        </script>

        <script type="text/template" id="tmpl-konte-products-filter-display-options">
            <# _.each( data.options, function( name, type ) { #>
            <option value="{{type}}">{{name}}</option>
            <# } ); #>
        </script>

        <?php
    }

    /**
     * Echoes the widget content.
     *
     * @param array $args     Widget arguments
     * @param array $instance Saved values from database
     *
     * @see WP_Widget::widget()
     *
     */
    public function widget( $args, $instance ) {

        if ( ! is_post_type_archive( 'product' ) && ! is_tax( get_object_taxonomies( 'product' ) ) ) {
            return;
        }

        $instance = wp_parse_args( $instance, $this->defaults );

        if ( empty( $instance['filter'] ) ) {
            return;
        }

        // Remember current filters.
        $this->get_current_filters( $instance['filter'] );

        // Get form action url.
        $form_action = wc_get_page_permalink( 'shop' );

        // CSS classes.
        $classes = [];

        if ( $instance['ajax'] ) {
            $classes[] = 'ajax-filter';

            if ( $instance['instant'] ) {
                $classes[] = 'instant-filter';
            }
        }

        echo $args['before_widget'];

        if ( $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base ) ) {
            echo $args['before_title'] . esc_html( $title ) . $args['after_title'];
        }

        echo '<form action="' . esc_url( $form_action ) . '" method="get" class="' . esc_attr( implode( ' ', $classes ) ) . '">';
        echo '<div class="filters">';

        // Reset active fields
        $this->active_fields = [];

        foreach ( (array) $instance['filter'] as $filter ) {
            $this->display_filter( $filter );
        }

        foreach ( $this->current_filters as $name => $value ) {
            if ( 'filter' == $name || array_key_exists( $name, $this->active_fields ) ) {
                continue;
            }

            printf( '<input type="hidden" name="%s" value="%s">', esc_attr( $name ), esc_attr( $value ) );
        }

        // Add param post_type when the shop page is home page
        if ( trailingslashit( $form_action ) == trailingslashit( home_url() ) ) {
            echo '<input type="hidden" name="post_type" value="product">';
        }

        echo '<input type="hidden" name="filter" value="1">';
        echo '</div>';

        echo '<button type="submit" value="' . esc_attr__( 'Filter', 'konte-addons' ) . '" class="button filter-button">' . esc_html__( 'Filter', 'konte-addons' ) . '</button>';
        echo '<button type="reset" value="' . esc_attr__( 'Reset Filter', 'konte-addons' ) . '" class="button alt reset-button">' . esc_html__( 'Reset Filter', 'konte-addons' ) . '</button>';

        if ( $instance['ajax'] ) {
            echo '<span class="products-loader"><span class="spinner"></span></span>';
        }

        echo '</form>';

        echo $args['after_widget'];
    }

    /**
     * Updates a particular instance of a widget.
     *
     * This function should check that `$new_instance` is set correctly. The newly-calculated
     * value of `$instance` should be returned. If false is returned, the instance won't be
     * saved/updated.
     *
     * @param array $new_instance New settings for this instance as input by the user via WP_Widget::form().
     * @param array $old_instance Old settings for this instance.
     *
     * @return array Settings to save or bool false to cancel saving.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = $new_instance;
        $instance['title'] = strip_tags( $instance['title'] );
        $instance['ajax'] = isset( $instance['ajax'] );
        $instance['instant'] = isset( $instance['instant'] );

        // Reorder filters
        if ( isset( $instance['filter'] ) ) {
            unset( $instance['filter'] );

            $index = 0;
            foreach ( $new_instance['filter'] as $filter ) {
                $instance['filter'][ $index ] = $filter;
                $index++;
            }
        }

        return $instance;
    }

    /**
     * Outputs the settings update form.
     *
     * @param array $instance Current settings.
     *
     * @return string|void
     */
    public function form( $instance ) {
        $instance = wp_parse_args( $instance, $this->defaults );
        ?>

        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'konte-addons' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>">
        </p>

        <p>
            <input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'ajax' ) ); ?>" class="konte-products-filter__ajax-option" name="<?php echo esc_attr( $this->get_field_name( 'ajax' ) ); ?>" value="1" <?php checked( 1, $instance['ajax'] ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id( 'ajax' ) ); ?>"><?php esc_html_e( 'Use ajax for filtering', 'konte-addons' ); ?></label>
        </p>

        <p style="display: <?php echo ! $instance['ajax'] ? 'none;' : 'block'; ?>">
            <input type="checkbox" id="<?php echo esc_attr( $this->get_field_id( 'instant' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'instant' ) ); ?>" value="1" <?php checked( 1, $instance['instant'] ); ?>>
            <label for="<?php echo esc_attr( $this->get_field_id( 'instant' ) ); ?>"><?php esc_html_e( 'Filtering products instantly (no buttons required)', 'konte-addons' ); ?></label>
        </p>

        <hr>
        <p></p>

        <div class="konte-products-filters">
            <p class="no-filter <?php echo empty( $instance['filter'] ) ? '' : 'hidden'; ?>"><?php esc_html_e( 'There is no filter yet.', 'konte-addons' ); ?></p>

            <?php foreach ( (array) $instance['filter'] as $index => $filter ) : ?>

                <div class="konte-products-filter-fields">
                    <div class="name">
                        <label>
                            <?php esc_html_e( 'Filter Name', 'konte-addons' ); ?>
                            <input type="text" name="<?php echo esc_attr( $this->get_field_name( "filter[$index]" ) ); ?>[name]" value="<?php echo esc_attr( $filter['name'] ); ?>" class="widefat">
                        </label>
                    </div>
                    <div class="source">
                        <label>
                            <?php esc_html_e( 'Filter By', 'konte-addons' ); ?>
                            <select name="<?php echo esc_attr( $this->get_field_name( "filter[$index]" ) ); ?>[source]" class="widefat filter-by">
                                <?php
                                foreach ( $this->sources as $source => $name ) {
                                    printf( '<option value="%s" %s>%s</option>', esc_attr( $source ), selected( $source, $filter['source'], false ), esc_html( $name ) );
                                }
                                ?>
                            </select>
                            <select name="<?php echo esc_attr( $this->get_field_name( "filter[$index]" ) ); ?>[attribute]" class="widefat filter-attribute <?php echo 'attribute' == $filter['source'] ? '' : 'hidden'; ?>">
                                <?php
                                foreach ( $this->attributes as $attribute => $name ) {
                                    printf( '<option value="%s" %s>%s</option>', esc_attr( $attribute ), selected( $attribute, $filter['attribute'], false ), esc_html( $name ) );
                                }
                                ?>
                            </select>
                        </label>
                    </div>
                    <div class="display">
                        <label>
                            <?php esc_html_e( 'Display Type', 'konte-addons' ); ?>
                            <select name="<?php echo esc_attr( $this->get_field_name( "filter[$index]" ) ); ?>[display]" class="widefat display-type">
                                <?php
                                foreach ( $this->display[ $filter['source'] ] as $display => $name ) {
                                    printf( '<option value="%s" %s>%s</option>', esc_attr( $display ), selected( $display, $filter['display'], false ), esc_html( $name ) );
                                }
                                ?>
                            </select>
                            <select name="<?php echo esc_attr( $this->get_field_name( "filter[$index]" ) ); ?>[multiple]" class="widefat multiple-select <?php echo 'attribute' != $filter['source'] || 'dropdown' == $filter['display'] ? 'hidden' : ''; ?>">
                                <option value="1" <?php selected( 1, $filter['multiple'] ); ?>><?php esc_html_e( 'Multiple select', 'konte-addons' ); ?></option>
                                <option value="0" <?php selected( 0, $filter['multiple'] ); ?>><?php esc_html_e( 'Single select', 'konte-addons' ); ?></option>
                            </select>
                            <select name="<?php echo esc_attr( $this->get_field_name( "filter[$index]" ) ); ?>[query_type]" class="widefat query-type <?php echo 'attribute' != $filter['source'] || 'dropdown' == $filter['display'] || ( 'dropdown' != $filter['display'] && ! $filter['multiple'] ) ? 'hidden' : ''; ?>">
                                <option value="and" <?php selected( 'and', $filter['query_type'] ); ?>><?php esc_html_e( 'Query type: AND', 'konte-addons' ); ?></option>
                                <option value="or" <?php selected( 'or', $filter['query_type'] ); ?>><?php esc_html_e( 'Query type: OR', 'konte-addons' ); ?></option>
                            </select>
                        </label>
                    </div>
                    <div class="actions">
                        <a href="#" class="remove-filter dashicons dashicons-no-alt">
                            <span class="screen-reader-text"><?php esc_html_e( 'Remove filter', 'konte-addons' ); ?></span>
                        </a>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>

        <p style="text-align: center">
            <a href="#" class="konte-products-filter-add-new" data-number="<?php echo esc_attr( $this->number ); ?>" data-name="<?php echo esc_attr( $this->get_field_name( 'filter' ) ); ?>" data-count="<?php echo count( $instance['filter'] ); ?>">+ <?php esc_html_e( 'Add new filter', 'konte-addons' ); ?></a>
        </p>

        <?php

    }

    /**
     * Remember current filters/search
     */
    protected function get_current_filters( $active_filter = [] ) {
        $request = $_GET;

        if ( get_search_query() ) {
            $this->current_filters['s'] = get_search_query();
            if ( isset( $request['s'] ) ) {
                unset( $request['s'] );
            }
        }

        if ( ! empty( $request['post_type'] ) ) {
            $this->current_filters['post_type'] = $request['post_type'];
            unset( $request['post_type'] );
        }

        if ( ! empty( $request['product_cat'] ) ) {
            $this->current_filters['product_cat'] = $request['product_cat'];
            unset( $request['product_cat'] );
        }

        if ( ! empty( $request['product_tag'] ) ) {
            $this->current_filters['product_tag'] = $request['product_tag'];
            unset( $request['product_tag'] );
        }

        if ( ! empty( $request['orderby'] ) ) {
            $this->current_filters['orderby'] = $request['orderby'];
            unset( $request['orderby'] );
        }

        if ( ! empty( $request['min_rating'] ) ) {
            $this->current_filters['min_rating'] = $request['min_rating'];
            unset( $request['min_rating'] );
        }

        if ( $_chosen_attributes = \WC_Query::get_layered_nav_chosen_attributes() ) {
            foreach ( $_chosen_attributes as $attribute => $data ) {
                $taxonomy_filter = 'filter_' . str_replace( 'pa_', '', $attribute );
                if ( isset( $request[ $taxonomy_filter ] ) ) {
                    unset( $request[ $taxonomy_filter ] );
                }
                $this->current_filters[ $taxonomy_filter ] = implode( ',', $data['terms'] );

                if ( 'or' == $data['query_type'] ) {
                    $query_type = str_replace( 'pa_', 'query_type_', $attribute );
                    $this->current_filters[ $query_type ] = 'or';

                    if ( isset( $request[ $query_type ] ) ) {
                        unset( $request[ $query_type ] );
                    }
                }
            }
        }

        if ( is_product_taxonomy() ) {
            $taxonomy = get_queried_object()->taxonomy;
            $term = get_query_var( $taxonomy );
            $this->current_filters[ $taxonomy ] = $term;
        }

        // Remove other active filters
        foreach ( $active_filter as $filter ) {
            if ( 'slider' == $filter['display'] ) {
                $min_name = 'min_' . $filter['source'];
                $max_name = 'max_' . $filter['source'];

                if ( isset( $request[ $min_name ] ) ) {
                    unset( $request[ $min_name ] );
                }

                if ( isset( $request[ $max_name ] ) ) {
                    unset( $request[ $max_name ] );
                }
            }
        }

        foreach ( $request as $name => $value ) {
            $this->current_filters[ $name ] = $value;
        }
    }

    /**
     * Print HTML for single filter
     *
     * @param array $filter
     */
    protected function display_filter( $filter = [] ) {

        $filter = wp_parse_args(
            $filter,
            [
                'name' => '',
                'source' => 'price',
                'display' => 'slider',
                'attribute' => '',
                'multiple' => false, // Use for attribute only
            ]
        );

        // Build filter args
        $options = $this->get_filter_options( $filter );
        $args = [
            'name' => 'attribute' == $filter['source'] ? 'filter_' . $filter['attribute'] : $filter['source'],
            'current' => [],
            'options' => $options,
        ];



        if ( 'attribute' == $filter['source'] ) {
            $attr = $this->get_tax_attribute( $filter['attribute'] );

            // Stop if attribute isn't exists
            if ( ! $attr ) {
                return;
            }

            $args['type'] = $attr->attribute_type;
            $args['multiple'] = $filter['multiple'];
            $args['all'] = sprintf( esc_html__( 'Any %s', 'konte-addons' ), wc_attribute_label( $attr->attribute_label ) );
            $args['query_type'] = $filter['query_type'];
            $args['attribute'] = $filter['attribute'];
        } elseif ( taxonomy_exists( $filter['source'] ) ) {
            $taxonomy = get_taxonomy( $filter['source'] );
            $args['all'] = sprintf( esc_html__( 'Select a %s', 'konte-addons' ), $taxonomy->labels->singular_name );
        } elseif ( $filter['source'] == 'in_showroom' || $filter['source'] == 'ready_to_delivery' ) {
            $args['attribute'] = '';
            $args['multiple'] = false;
            $args['query_type'] = 'and';
        } else {
            $args['all'] = esc_html__( 'All Products', 'konte-addons' );
        }

        if ( $filter['source'] == 'in_showroom' || $filter['source'] == 'ready_to_delivery' ) {
            $args['current']['in_showroom'] = isset( $_REQUEST['in_showroom'] ) && $_REQUEST['in_showroom'] == 'in_showroom';
            $args['current']['ready_to_delivery'] = isset( $_REQUEST['ready_to_delivery'] ) && $_REQUEST['ready_to_delivery'] == 'ready_to_delivery';
        } elseif ( 'slider' == $filter['display'] ) {
            $args['current']['min'] = isset( $_GET[ 'min_' . $args['name'] ] ) ? $_GET[ 'min_' . $args['name'] ] : '';
            $args['current']['max'] = isset( $_GET[ 'max_' . $args['name'] ] ) ? $_GET[ 'max_' . $args['name'] ] : '';
        } elseif ( 'attribute' == $filter['source'] ) {
            $args['current'] = isset( $_GET[ 'filter_' . $filter['attribute'] ] ) ? explode( ',', $_GET[ 'filter_' . $filter['attribute'] ] ) : [];
        } elseif ( isset( $this->current_filters[ $args['name'] ] ) ) {
            $args['current'] = explode( ',', $this->current_filters[ $args['name'] ] );
        }

        // Only apply multiple select to attributes
        if ( 'attribute' != $filter['source'] || 'dropdown' == $filter['display'] ) {
            $args['multiple'] = false;
        }

        // Don't duplicate fields
        if ( array_key_exists( $args['name'], $this->active_fields ) ) {
            return;
        } else {
            $this->active_fields[ $args['name'] ] = isset( $_GET[ $args['name'] ] ) ? $_GET[ $args['name'] ] : $args['current'];

            // Always add 'query_type_xx' field for attributes.
            if ( 'attribute' == $filter['source'] ) {
                $field = 'query_type_' . $filter['attribute'];
                $this->active_fields[ $field ] = isset( $_GET[ $field ] ) ? $_GET[ $field ] : $filter['query_type'];
            }
        }

        $classes = [];
        $classes[] = ! empty( $args['name'] ) ? sanitize_title( $args['name'], '', 'query' ) : '';
        $classes[] = $filter['source'];
        $classes[] = $filter['display'];
        $classes[] = 'attribute' == $filter['source'] ? $filter['attribute'] : '';
        $classes[] = $args['multiple'] ? 'multiple' : '';
        $classes = array_unique( $classes );

        $filter_control_class = $filter['display'] == 'list' ? 'closed' : '';
        ?>

        <div class="filter <?php echo join( ' ', $classes ); ?>">
            <?php if ( ! empty( $filter['name'] ) ) : ?>
                <span class="filter-name <?php echo $filter_control_class; ?>">
                    <?php echo esc_html( $filter['name'] ); ?>
                    <span class="action"></span>
                </span>
            <?php endif; ?>

            <div class="filter-control <?php echo $filter_control_class; ?>">
                <?php
                switch ( $filter['display'] ) {
                    case 'slider':
                        ob_start();
                        the_widget( 'WC_Widget_Price_Filter' );
                        $html = ob_get_clean();
                        $html = preg_replace( '/<form[^>]*>(.*?)<\/form>/msi', '$1', $html );
                        echo $html;
                        break;

                    case 'list':
                    case 'h-list':
                        $this->display_list( $args );
                        break;

                    case 'checkboxes':
                        $this->display_checkboxes( $args );
                        break;

                    case 'checkbox-kapsula':
                        $this->display_checkbox_kapsula( $args );
                        break;

                    case 'auto':
                        $this->display_auto( $args );
                        break;

                    default:
                        $this->display_dropdown( $args );
                        break;
                }
                ?>
            </div>
        </div>

        <?php
    }

    /**
     * Get filter options
     *
     * @param array $filter
     *
     * @return array
     */
    protected function get_filter_options( $filter ) {

        global $wp_query;

        $options = [];

        switch ( $filter['source'] ) {
            case 'price':
                // Use the default price slider.
                break;

                // Find min and max price in current result set
                $prices = $this->get_filtered_price();
                $min = floor( $prices->min_price );
                $max = ceil( $prices->max_price );

                /**
                 * Adjust max if the store taxes are not displayed how they are stored.
                 * Min is left alone because the product may not be taxable.
                 * Kicks in when prices excluding tax are displayed including tax.
                 */
                if ( wc_tax_enabled() && 'incl' === get_option( 'woocommerce_tax_display_shop' ) && ! wc_prices_include_tax() ) {
                    $tax_classes = array_merge( [ '' ], WC_Tax::get_tax_classes() );
                    $class_max = $max;

                    foreach ( $tax_classes as $tax_class ) {
                        if ( $tax_rates = WC_Tax::get_rates( $tax_class ) ) {
                            $class_max = $max + WC_Tax::get_tax_total( WC_Tax::calc_exclusive_tax( $max, $tax_rates ) );
                        }
                    }

                    $max = $class_max;
                }
                $options['min'] = $min;
                $options['max'] = $max;

                break;

            case 'in_showroom':
                $options['in_showroom'] = [
                    'name' => __( 'В шоуруме', 'kapsula' ),
                    'count' => 0
                ];
                break;

            case 'ready_to_delivery':
                $options['ready_to_delivery'] = [
                    'name' => __( 'Готово к отправке', 'kapsula' ),
                    'count' => 0
                ];
                break;

            case 'attribute':
                $taxonomy = 'pa_' . $filter['attribute'];

                $term_query_params = [
                    'taxonomy' => $taxonomy,
                    'hide_empty' => true,
                    'parent' => 0
                ];

                if ( $filter['attribute'] == 'size' || $filter['attribute'] == 'hidden-color' ) {
                    $args = $wp_query->query_vars;
                    $args['nopaging'] = true;
                    $args['fields'] = 'ids';

                    unset( $args['taxonomy'] );
                    unset( $args['term'] );

                    foreach ( $args['tax_query'] as $key => $tax_query ) {
                        if ( is_array( $tax_query ) && $tax_query['taxonomy'] == $taxonomy ) {
                            unset( $args['tax_query'][ $key ] );
                        }
                    }

                    $filters_query = new \WP_Query( $args );
                    $term_query_params['object_ids'] = $filters_query->posts;
                }

                $terms = get_terms( $term_query_params );

                usort( $terms, 'sort_terms_attributes' );

                if ( is_wp_error( $terms ) ) {
                    break;
                }

                $terms = $this->get_terms_hierarchy( $terms, $filter['source'] );

                foreach ( $terms as $term ) {
                    $options[ $term->slug ] =
                        [
                            'name' => $term->name,
                            'count' => $term->count,
                            'id' => $term->term_id,
                            'parent' => $term->parent,
                            'children' => $term->children
                        ];
                }
                break;

            case 'products_group':
                $filter_groups = apply_filters(
                    'konte_products_filter_groups',
                    [
                        'best_sellers' => esc_attr__( 'Best Sellers', 'konte-addons' ),
                        'new' => esc_attr__( 'New Products', 'konte-addons' ),
                        'sale' => esc_attr__( 'Sale Products', 'konte-addons' ),
                        'featured' => esc_attr__( 'Hot Products', 'konte-addons' ),
                    ]
                );
                $options = [];

                if ( 'dropdown' != $filter['display'] ) {
                    $options[''] = [
                        'name' => esc_attr__( 'All Products', 'konte-addons' ),
                        'count' => 0,
                        'id' => 0,
                    ];
                }
                foreach ( $filter_groups as $group_name => $group_label ) {
                    $options[ $group_name ] = [
                        'name' => $group_label,
                        'count' => 0,
                        'id' => 0,
                    ];
                }
                break;

            default:
                if ( ! taxonomy_exists( $filter['source'] ) ) {
                    break;
                }

                $term_query_params = [
                    'taxonomy' => $filter['source'],
                    'hide_empty' => true,
                    'parent' => 0,
                    'exclude_tree' => [ 2146, 4361, 1930, 1933, 1691, 4133 ]
                ];

                $args = $wp_query->query_vars;

                if ( $filter['source'] == 'product_brand' ) {

                    $args['nopaging'] = true;
                    $args['fields'] = 'ids';

                    unset( $args['product_brand'] );
                    unset( $args['taxonomy'] );
                    unset( $args['term'] );

                    if ( isset( $args['product_cat'] ) ) {
                        $args['tax_query'][] = [
                            'taxonomy' => 'product_cat',
                            'field' => 'slug',
                            'terms' => explode( ',', $args['product_cat'] ),
                            'operator' => 'IN',
                            'include_children' => false
                        ];

                        unset( $args['product_cat'] );
                    }

                    $filters_query = new \WP_Query( $args );
                    $term_query_params['object_ids'] = $filters_query->posts;
                }

                $terms = get_terms( $term_query_params );

                if ( $filter['source'] == 'product_cat' ) {
                    $category_order = [ 'clothing', 'shoes', 'accessories', 'home' ];

                    usort(
                        $terms,
                        function ( $term1, $term2 ) use ( $category_order ) {
                            return array_search( $term1->slug, $category_order ) <=> array_search( $term2->slug, $category_order );
                        }
                    );
                } elseif ( $filter['source'] == 'product_brand' ) {
                    usort(
                        $terms,
                        function( $term1, $term2 ) {
                            return $term1->name <=> $term2->name;
                        }
                    );
                }

                $terms = $this->get_terms_hierarchy( $terms, $filter['source'] );

                foreach ( $terms as $term ) {
                    $options[ $term->slug ] = [
                        'name' => $term->name,
                        'count' => $term->count,
                        'id' => $term->term_id,
                        'parent' => $term->parent,
                        'children' => $term->children,
                        'main_parent' => $term->main_parent
                    ];
                }
                break;
        }

        return $options;
    }

    /**
     * Get children for all terms and add parent id to each one.
     *
     * @param       $terms
     * @param       $taxonomy
     * @param array $sorted_terms
     *
     * @return array|mixed
     */
    public function get_terms_hierarchy( $terms, $taxonomy, &$sorted_terms = [] ) {

        static $main_parent = 0;

        foreach ( $terms as $term ) {

            if ( $term->parent === 0 ) {
                $main_parent = $term->term_id;
            }

            $child_terms = get_terms(
                [
                    'taxonomy' => $taxonomy,
                    'hide_empty' => true,
                    'parent' => $term->term_id
                ]
            );

            $term->children = is_array( $child_terms ) && ! empty( $child_terms );
            $term->main_parent = $main_parent;

            $sorted_terms[] = $term;

            if ( $term->children ) {
                $this->get_terms_hierarchy( $child_terms, $taxonomy, $sorted_terms );
            }
        }

        return $sorted_terms;
    }

    /**
     * Print HTML of slider
     *
     * @param array $args
     */
    protected function display_slider( $args ) {
        $args = wp_parse_args(
            $args,
            [
                'name' => '',
                'current' => [
                    'min' => '',
                    'max' => '',
                ],
                'options' => [
                    'min' => 0,
                    'max' => 0,
                ],
            ]
        );

        $min_price = apply_filters( 'woocommerce_price_filter_widget_min_amount', $args['current']['min'] );
        $max_price = apply_filters( 'woocommerce_price_filter_widget_max_amount', $args['current']['max'] );
        $min = apply_filters( 'woocommerce_price_filter_widget_min_amount', $args['options']['min'] );
        $max = apply_filters( 'woocommerce_price_filter_widget_max_amount', $args['options']['max'] );

        if ( $min == $max ) {
            return;
        }

        /**
         * Adjust max if the store taxes are not displayed how they are stored.
         * Min is left alone because the product may not be taxable.
         * Kicks in when prices excluding tax are displayed including tax.
         */
        if ( wc_tax_enabled() && 'incl' === get_option( 'woocommerce_tax_display_shop' ) && ! wc_prices_include_tax() ) {
            $tax_classes = array_merge( [ '' ], WC_Tax::get_tax_classes() );
            $class_max = $max;

            foreach ( $tax_classes as $tax_class ) {
                if ( $tax_rates = WC_Tax::get_rates( $tax_class ) ) {
                    $class_max = $max + WC_Tax::get_tax_total( WC_Tax::calc_exclusive_tax( $max, $tax_rates ) );
                }
            }

            $max = $class_max;
        }
        ?>

        <div class="filter-slider price_slider hidden"></div>
        <div class="slider-amount">
            <input type="text" name="min_<?php echo esc_attr( $args['name'] ); ?>" value="<?php echo esc_attr( $min_price ); ?>" data-min="<?php echo esc_attr( $min ); ?>" />
            <input type="text" name="max_<?php echo esc_attr( $args['name'] ); ?>" value="<?php echo esc_attr( $max_price ); ?>" data-max="<?php echo esc_attr( $max ); ?>" />
            <div class="slider-label hidden">
                <span class="range"><?php esc_html_e( 'Range:', 'konte-addons' ); ?></span><span class="from"></span> &mdash;
                <span class="to"></span>
            </div>
            <div class="clear"></div>
        </div>

        <?php

    }

    /**
     * Print HTML of dropdown
     *
     * @param array $args
     */
    protected function display_dropdown( $args ) {
        $args = wp_parse_args(
            $args,
            [
                'name' => '',
                'current' => [],
                'options' => [],
                'all' => esc_html__( 'Any', 'konte-addons' ),
            ]
        );

        if ( empty( $args['options'] ) ) {
            return;
        }

        echo '<select name="' . esc_attr( $args['name'] ) . '">';

        echo '<option value="">' . $args['all'] . '</option>';
        foreach ( $args['options'] as $slug => $option ) {
            $slug = urldecode( $slug );
            printf(
                '<option value="%s" %s>%s</option>',
                esc_attr( $slug ),
                selected( true, in_array( $slug, (array) $args['current'] ), false ),
                esc_html( $option['name'] )
            );
        }

        echo '</select>';
    }

    /**
     * Print HTML of list
     *
     * @param array $args
     */
    protected function display_list( $args ) {
        $args = wp_parse_args(
            $args,
            [
                'name' => '',
                'current' => [],
                'options' => [],
                'attribute' => '',
                'multiple' => '',
            ]
        );

        if ( empty( $args['options'] ) ) {
            return;
        }

        $opened_terms = [];
        ob_start();

        echo '<ul class="filter-list filter-list-parent">';

        foreach ( $args['options'] as $slug => $option ) {
            if ( ! isset( $option['parent'] ) ) {
                continue;
            }

            $main_parent = $option['main_parent'] ?? 0;

            if ( count( $opened_terms ) ) {

                foreach ( $opened_terms as $k => $opened_term ) {
                    if ( $option['parent'] === $opened_term['id'] ) {
                        break;
                    }

                    unset( $opened_terms[ $k ] );
                    echo '</ul></li>';
                }
            }

            $children = $option['children'] ? '<span class="action-icon fa fa-angle-up"></span>' : '';
            $child_name = function_exists( 'pll__' ) ? pll__( $option['name'] ) : $option['name'];

            printf(
                '<li class="children-closed filter-list-item %s" data-value="%s" data-id="%s" data-main-parent="%s"><span class="name">%s</span><span class="count">%d</span>%s</li>',
                in_array( $slug, (array) $args['current'] ) ? 'selected' : '',
                esc_attr( $slug ),
                $option['id'],
                $main_parent,
                esc_html( $child_name ),
                esc_html( $option['count'] ),
                $children
            );

            if ( $option['children'] ) {

                printf(
                    '<li class="closed filter-list-child" data-parent="%d" data-main-parent="%d"><ul class="filter-list">',
                    $option['id'],
                    $main_parent
                );

                array_unshift( $opened_terms, $option );
            }
        }

        foreach ( $opened_terms as $k => $opened_term ) {
            echo '</ul></li>';
        }

        echo '</ul>';

        $layout = ob_get_clean();

        echo $layout;

        printf(
            '<input type="hidden" name="%s" value="%s" %s>',
            esc_attr( $args['name'] ),
            esc_attr( implode( ',', $args['current'] ) ),
            empty( $args['current'] ) ? 'disabled' : ''
        );

        if ( $args['attribute'] && $args['multiple'] && 'or' == $args['query_type'] ) {
            printf( '<input type="hidden" name="query_type_%s" value="or">', esc_attr( $args['attribute'] ) );
        }
    }

    protected function display_checkbox_kapsula( $args ) {

        $args = wp_parse_args(
            $args,
            [
                'name' => '',
                'current' => [],
                'options' => [],
                'attribute' => '',
                'multiple' => '',
            ]
        );

        if ( empty( $args['options'] ) ) {
            return;
        }

        echo '<ul class="filter-checkboxes">';
        foreach ( $args['options'] as $slug => $option ) {
            printf(
                '<li class="filter-checkboxes-item %s" data-value="%s"><span class="name">%s</span><span class="count">%d</span></li>',
                in_array( $slug, (array) $args['current'] ) && ! empty( $args['current'][ $slug ] ) ? 'selected' : '',
                esc_attr( $slug ),
                esc_html( $option['name'] ),
                esc_html( $option['count'] )
            );
        }
        echo '</ul>';

        printf(
            '<input type="hidden" name="%s" value="%s" %s>',
            esc_attr( $args['name'] ),
            isset( $args['current'][ $args['name'] ] ) && $args['current'][ $args['name'] ] ? $args['name'] : '',
            empty( $args['current'] ) ? 'disabled' : ''
        );
    }

    /**
     * Print HTML of checkboxes
     *
     * @param array $args
     */
    protected function display_checkboxes( $args ) {
        $args = wp_parse_args(
            $args,
            [
                'name' => '',
                'current' => [],
                'options' => [],
                'attribute' => '',
                'multiple' => '',
            ]
        );

        if ( empty( $args['options'] ) ) {
            return;
        }

        echo '<ul class="filter-checkboxes">';
        foreach ( $args['options'] as $slug => $option ) {
            printf(
                '<li class="filter-checkboxes-item %s" data-value="%s"><span class="name">%s</span><span class="count">%d</span></li>',
                in_array( $slug, (array) $args['current'] ) ? 'selected' : '',
                esc_attr( $slug ),
                esc_html( $option['name'] ),
                esc_html( $option['count'] )
            );
        }
        echo '</ul>';

        printf(
            '<input type="hidden" name="%s" value="%s" %s>',
            esc_attr( $args['name'] ),
            esc_attr( implode( ',', array_filter( $args['current'] ) ) ),
            empty( $args['current'] ) ? 'disabled' : ''
        );

        if ( $args['attribute'] && $args['multiple'] && 'or' == $args['query_type'] ) {
            printf( '<input type="hidden" name="query_type_%s" value="or">', esc_attr( $args['attribute'] ) );
        }
    }

    /**
     * Display attribute filter automatically
     *
     * @param array $args
     */
    protected function display_auto( $args ) {
        $args = wp_parse_args(
            $args,
            [
                'name' => '',
                'type' => 'select',
                'current' => [],
                'options' => [],
                'attribute' => '',
                'multiple' => false,
            ]
        );

        if ( empty( $args['options'] ) ) {
            return;
        }

        if ( ! class_exists( 'TA_WC_Variation_Swatches' ) ) {
            $args['type'] = 'select';
        }

        switch ( $args['type'] ) {
            case 'color':
                echo '<div class="filter-swatches">';
                foreach ( $args['options'] as $slug => $option ) {
                    $color = get_term_meta( $option['id'], 'color', true );

                    printf(
                        '<span class="swatch swatch-color swatch-%s %s" data-value="%s" style="background-color:%s;" title="%s">%s</span>',
                        esc_attr( $slug ),
                        in_array( $slug, (array) $args['current'] ) ? 'selected' : '',
                        esc_attr( $slug ),
                        esc_attr( $color ),
                        esc_attr( $option['name'] ),
                        esc_html( $option['name'] )
                    );
                }
                echo '</div>';

                printf(
                    '<input type="hidden" name="%s" value="%s" %s>',
                    esc_attr( $args['name'] ),
                    esc_attr( implode( ',', $args['current'] ) ),
                    empty( $args['current'] ) ? 'disabled' : ''
                );

                if ( $args['attribute'] && $args['multiple'] && 'or' == $args['query_type'] ) {
                    printf( '<input type="hidden" name="query_type_%s" value="or">', esc_attr( $args['attribute'] ) );
                }
                break;

            case 'image':
                echo '<div class="filter-swatches">';
                foreach ( $args['options'] as $slug => $option ) {
                    $image = get_term_meta( $option['id'], 'image', true );
                    $image = $image ? wp_get_attachment_image_src( $image ) : '';
                    $image = $image ? $image[0] : WC()->plugin_url() . '/assets/images/placeholder.png';

                    printf(
                        '<span class="swatch swatch-image swatch-%s %s" data-value="%s" title="%s"><img src="%s" alt="%s"></span>',
                        esc_attr( $slug ),
                        in_array( $slug, (array) $args['current'] ) ? 'selected' : '',
                        esc_attr( $slug ),
                        esc_attr( $option['name'] ),
                        esc_url( $image ),
                        esc_attr( $option['name'] )
                    );
                }
                echo '</div>';

                printf(
                    '<input type="hidden" name="%s" value="%s" %s>',
                    esc_attr( $args['name'] ),
                    esc_attr( implode( ',', $args['current'] ) ),
                    empty( $args['current'] ) ? 'disabled' : ''
                );

                if ( $args['attribute'] && $args['multiple'] && 'or' == $args['query_type'] ) {
                    printf( '<input type="hidden" name="query_type_%s" value="or">', esc_attr( $args['attribute'] ) );
                }
                break;

            case 'label':
                echo '<div class="filter-swatches">';
                foreach ( $args['options'] as $slug => $option ) {
                    $label = get_term_meta( $option['id'], 'label', true );
                    $label = $label ? $label : $option['name'];

                    printf(
                        '<span class="swatch swatch-label swatch-%s %s" data-value="%s" title="%s">%s</span>',
                        esc_attr( $slug ),
                        in_array( $slug, (array) $args['current'] ) ? 'selected' : '',
                        esc_attr( $slug ),
                        esc_attr( $option['name'] ),
                        esc_html( $label )
                    );
                }
                echo '</div>';

                printf(
                    '<input type="hidden" name="%s" value="%s" %s>',
                    esc_attr( $args['name'] ),
                    esc_attr( implode( ',', $args['current'] ) ),
                    empty( $args['current'] ) ? 'disabled' : ''
                );

                if ( $args['attribute'] && $args['multiple'] && 'or' == $args['query_type'] ) {
                    printf( '<input type="hidden" name="query_type_%s" value="or">', esc_attr( $args['attribute'] ) );
                }
                break;

            default:
                $this->display_dropdown( $args );
                break;
        }
    }

    /**
     * Get filtered min price for current products.
     *
     * @return object
     */
    protected function get_filtered_price() {
        global $wpdb, $wp_the_query;

        $args = $wp_the_query->query_vars;
        $tax_query = isset( $args['tax_query'] ) ? $args['tax_query'] : [];
        $meta_query = isset( $args['meta_query'] ) ? $args['meta_query'] : [];

        if ( ! empty( $args['taxonomy'] ) && ! empty( $args['term'] ) ) {
            $tax_query[] = [
                'taxonomy' => $args['taxonomy'],
                'terms' => [ $args['term'] ],
                'field' => 'slug',
            ];
        }

        foreach ( $meta_query as $key => $query ) {
            if ( ! empty( $query['price_filter'] ) || ! empty( $query['rating_filter'] ) ) {
                unset( $meta_query[ $key ] );
            }
        }

        $meta_query = new \WP_Meta_Query( $meta_query );
        $tax_query = new \WP_Tax_Query( $tax_query );

        $meta_query_sql = $meta_query->get_sql( 'post', $wpdb->posts, 'ID' );
        $tax_query_sql = $tax_query->get_sql( $wpdb->posts, 'ID' );

        $sql = "SELECT min( CAST( price_meta.meta_value AS UNSIGNED ) ) as min_price, max( CAST( price_meta.meta_value AS UNSIGNED ) ) as max_price FROM {$wpdb->posts} ";
        $sql .= " LEFT JOIN {$wpdb->postmeta} as price_meta ON {$wpdb->posts}.ID = price_meta.post_id " . $tax_query_sql['join'] . $meta_query_sql['join'];
        $sql .= " 	WHERE {$wpdb->posts}.post_type = 'product'
						AND {$wpdb->posts}.post_status = 'publish'
						AND price_meta.meta_key IN ('" . implode( "','", array_map( 'esc_sql', apply_filters( 'woocommerce_price_filter_meta_keys', [ '_price' ] ) ) ) . "')
						AND price_meta.meta_value > '' ";
        $sql .= $tax_query_sql['where'] . $meta_query_sql['where'];

        return $wpdb->get_row( $sql );
    }

    /**
     * Get attribute's properties
     *
     * @param string $attribute
     *
     * @return object
     */
    protected function get_tax_attribute( $attribute ) {
        global $wpdb;

        $attr = $wpdb->get_row( "SELECT * FROM " . $wpdb->prefix . "woocommerce_attribute_taxonomies WHERE attribute_name = '$attribute'" );

        return $attr;
    }

}