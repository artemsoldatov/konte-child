<?php
/**
 * Class for mobile changes.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Assets
 *
 * @package dev\Kapsula\Theme
 */
class Mobile {

    /**
     * Add catalog classes to show in grid.
     *
     * @param $classes
     * @param $product
     *
     * @return mixed
     */
    public function add_catalog_post_class( $classes, $product ) {

        global $post;

        if ( property_exists( $post, 'display' ) ) {
            $classes[] = 'display-' . $post->display;
        }

        return $classes;
    }

    /**
     * Sort posts for output on mobile devices.
     *
     * Some products may have video and such products should be displayed in catalog 1 on the line
     * and don't have 2 or more products with video in row
     *
     * @param $posts
     *
     * @return array
     */
    public function sort_for_mobile_catalog( $posts ) {

        if ( is_main_query() && wp_is_mobile() && strpos( $_SERVER['HTTP_USER_AGENT'], 'iPad' ) === false ) {

            $sorted_posts = [];
            $delayed_posts = [];
            // Count for post index on the page started from 0
            $shift = isset( $_GET['lazy'] ) && isset( $_GET['shift'] ) ? intval( $_GET['shift'] ) : 0;

            foreach ( $posts as $k => $post ) {

                if ( ! empty( get_post_meta( $post->ID, '_video_id', true ) ) ) {

                    $post->display = 'video';

                    // Only show one per row after 2 products with images
                    if ( ( $shift + 1 ) % 3 != 0 ) {
                        $delayed_posts[] = $post;
                        continue;
                    }

                    if ( count( $delayed_posts ) ) {
                        $sorted_posts[] = array_shift( $delayed_posts );
                        $delayed_posts[] = $post;
                    } else {
                        $sorted_posts[] = $post;
                    }

                    $shift++;

                    // Flow for product without video
                } else {

                    if ( count( $delayed_posts ) && count( $sorted_posts ) && ( $shift + 1 ) % 3 == 0 ) {
                        // Delayed posts can be only video type so do not set display
                        $sorted_posts[] = array_shift( $delayed_posts );
                        $shift++;
                    }

                    if ( ( $shift + 1 ) % 3 == 0 ) {
                        $post->display = 'full-width';
                    } else {
                        $post->display = 'normal';
                    }

                    $shift++;
                    $sorted_posts[] = $post;
                }
            }

            // If we have delayed videos sort them also
            if ( count( $delayed_posts ) ) {
                foreach ( $delayed_posts as $delayed_post ) {

                    if ( ( $shift + 1 ) % 3 != 0 ) {
                        $delayed_post->display = 'normal-video';
                    }

                    $sorted_posts[] = $delayed_post;
                    $shift++;
                }
            }

            $posts = $sorted_posts;
        }

        return $posts;
    }
}
