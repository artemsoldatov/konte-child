<?php
/**
 * Class for improving total site performance.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Performance
 *
 * @package dev\Kapsula\Theme
 */
class Performance {

    public function __construct() {}

    /**
     * Increase heartbeat interval.
     *
     * @param $settings
     *
     * @return mixed
     */
    public function set_heartbeat_timeout( $settings ) {

        $settings['interval'] = 90;
        $settings['minimalInterval'] = 90;

        return $settings;
    }
}