<?php
/**
 * Class for currency.
 *
 * @author     artem
 * @package    dev
 * @subpackage Kapsula
 * @version    1.0.0
 * @since      1.0.0
 */

namespace dev\Kapsula\Theme;

defined( 'ABSPATH' ) || die( 'What are you looking for?' );

/**
 * Class Currency
 *
 * @package dev\Kapsula
 */
class Currency {

    private $order;

    private $order_id;

    private $order_currency;

    private $default_currency;

    private $woocs;

    private function init_order( $order ) {
        global $WOOCS;

        if ( is_a( $order, 'WC_Order_Item' ) && $order->get_order() ) {
            $order = $order->get_order();
        }

        if ( $order->get_id() !== $this->order_id ) {
            $this->order = $order;
            $this->order_id = $order->get_id();
            $this->order_currency = get_post_meta( $this->order_id, '_selected_currency_order', true );
            $this->default_currency = $WOOCS->default_currency;
            $this->woocs = $WOOCS;
        }
    }

    public function recalc_order( $order_id ) {
        global $WOOCS;

        if ( is_null( $order_id ) ) {
            $_REQUEST['woocs_block_price_hook'] = true;

            $WOOCS->current_currency = $WOOCS->default_currency;

            WC()->cart->calculate_shipping();
            WC()->cart->calculate_totals();
        }

        return $order_id;
    }

    public function change_currency( $order_currency, $order ) {
        global $WOOCS;

        if ( ! is_admin() && is_object( $order ) && ! wp_doing_ajax() ) {

            if ( method_exists( $order, 'get_id' ) ) {
                $order_id = $order->get_id();
            } else {
                $order_id = $order->id;
            }

            $currency = get_post_meta( $order_id, '_selected_currency_order', true );

            if ( ! empty( $currency ) ) {
                $order_currency = $currency;
                $WOOCS->set_currency( $currency );
            }
        }

        return $order_currency;
    }

    public function add_order_currency_field( $fields ) {
        global $WOOCS;
        ?>

        <input type="hidden" id="selected_currency_order" name="selected_currency_order" value="<?php echo $WOOCS->current_currency; ?>">

        <?php
    }

    public function save_order_currency( $order_id ) {
        if ( isset( $_POST['selected_currency_order'] ) ) {
            update_post_meta( $order_id, '_selected_currency_order', sanitize_text_field( $_POST['selected_currency_order'] ) );
        }
    }

    public function filter_order_item_subtotal( $subtotal, $item, $order ) {
        $this->init_order( $order );

        if ( $this->need_convert_price() ) {
            $subtotal = $this->convert_wc_price( (float) $item->get_subtotal() );
        }

        return $subtotal;
    }

    public function filter_order_subtotal( $subtotal, $compound, $order ) {
        $this->init_order( $order );

        if ( $this->need_convert_price() ) {
            $subtotal = $this->convert_wc_price( (float) $this->order->get_subtotal() );
        }

        return $subtotal;
    }

    public function filter_order_discount( $discount, $order ) {
        $this->init_order( $order );

        if ( $this->need_convert_price() ) {
            $discount = $this->woocs->woocs_exchange_value( $discount );
        }

        return $discount;
    }

    public function filter_order_shipping( $shipping, $order, $tax_display ) {
        $this->init_order( $order );

        if ( $this->need_convert_price() ) {
            $shipping_total = (float) ( $this->order->get_shipping_total() + $this->order->get_shipping_tax() );
            $shipping = $this->convert_wc_price( $shipping_total );
        }

        return $shipping;
    }

    public function filter_order_total( $total, $order ) {
        $this->init_order( $order );

        if ( $this->need_convert_price() ) {
            $total = $this->convert_wc_price( (float) $this->order->get_total() );
        }

        return $total;
    }

    private function convert_wc_price( $price ) {
        $converted_price = (float) $this->woocs->woocs_exchange_value( $price );

        return wc_price( $converted_price, [ 'currency' => $this->order_currency ] );
    }

    private function need_convert_price() {
        if ( $this->order_currency == $this->default_currency || empty( $this->order_currency ) ) {
            return false;
        }
        
        return true;
    }
}
