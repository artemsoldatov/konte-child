<?php
/**
 * The template for checkout page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Konte
 */

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main">

            <?php
            while ( have_posts() ) :

                the_post();
                ?>

                <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if ( ( empty( $GLOBALS['konte']['single_page'] ) || empty( $GLOBALS['konte']['single_page']['featured_image'] ) ) && 'none' != get_post_meta( get_the_ID(), 'page_title_display', true ) ) : ?>
                        <header class="entry-header">
                            <?php get_template_part( 'template-parts/header/logo' ); ?>
                            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
                        </header><!-- .entry-header -->
                    <?php endif; ?>

                    <div class="breadcrumbs">
                        <ul>
                            <li><?php _e( 'Cart', 'woocommerce' ); ?></li>
                            <li><?php _e( 'Данные о доставке и оплата', 'kapsula' ); ?></li>
                        </ul>
                    </div>

                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div><!-- .entry-content -->
                </div><!-- #post-<?php the_ID(); ?> -->
                <?php

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
