<?php

$atts = shortcode_atts(
    [
        'image' => '',
        'image_size' => 'full',
        'name' => '',
        'link' => '',
        'job' => '',
        'facebook' => '',
        'twitter' => '',
        'google' => '',
        'pinterest' => '',
        'linkedin' => '',
        'youtube' => '',
        'instagram' => '',
        'css_animation' => '',
        'el_class' => '',
    ],
    $atts
);

$css_class = [
    'konte-team-member',
    Konte_Addons_Shortcodes::get_css_animation( $atts['css_animation'] ),
    $atts['el_class'],
];

if ( $atts['image'] ) {
    $image = Konte_Addons_Shortcodes::get_image( $atts['image'], $atts['image_size'] );
} else {
    $image = KONTE_ADDONS_URL . 'assets/images/person.jpg';
    $image = sprintf(
        '<img src="%s" alt="%s" width="288" height="352">',
        esc_url( $image ),
        esc_attr( $atts['name'] )
    );
}

$socials = [ 'facebook', 'twitter', 'google', 'pinterest', 'linkedin', 'youtube', 'instagram' ];
$links = [];

foreach ( $socials as $social ) {
    if ( empty( $atts[ $social ] ) ) {
        continue;
    }

    $icon = str_replace(
        [ 'google', 'pinterest', 'youtube' ],
        [ 'google-plus', 'pinterest-p', 'youtube-play' ],
        $social
    );

    $links[] = sprintf( '<a href="%s" target="_blank"><i class="fa fa-%s"></i></a>', esc_url( $atts[ $social ] ), esc_attr( $icon ) );
}

$link = vc_build_link( $atts['link'] );

if ( ! empty( $link['url'] ) ) {
    $image = vsprintf( '<a href="%1$s" title="%2$s" target="%3$s" rel="%4$s">' . $image . '</a>', $link );
    $atts['name'] = vsprintf( '<a href="%1$s" title="%2$s" target="%3$s" rel="%4$s">' . $atts['name'] . '</a>', $link );
}

printf(
    '<div class="%s">
				%s
				<div class="konte-team-member__info">
					<h5 class="konte-team-member__name">%s</h5>
					<span class="konte-team-member__job">%s</span>
					<span class="konte-team-member__socials">%s</span>
				</div>
			</div>',
    esc_attr( implode( ' ', $css_class ) ),
    $image,
    $atts['name'],
    esc_html( $atts['job'] ),
    implode( '', $links )
);
