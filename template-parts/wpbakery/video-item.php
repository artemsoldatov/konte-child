<?php

$atts = shortcode_atts(
    [
        'link' => '',
        'heading' => ''
    ],
    $atts
);

$url = explode( '|', $atts['link'] );

if ( isset( $url[0] ) ) {
    $url = urldecode( str_replace( 'url:', '', $url[0] ) );
}

?>

<div class="video-element">
    <video autoplay loop muted playsinline>
        <source src="<?php echo $url; ?>" type="video/mp4">
    </video>

    <div class="video-preview">
        <div class="play-button"></div>
        <h4><?php echo $atts['heading']; ?></h4>
    </div>
</div>
