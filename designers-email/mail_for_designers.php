<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
<!--    <link rel="stylesheet" href="mail_for_designers-styles.php">-->
    <title>Email for Designer</title>
</head>
<body class="body">
<table class="email-container" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <center>
                <table cellspacing="0" cellpadding="0" class="email-wrapper">
                    <tr>
                        <td>

                            <table class="first-block" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <span class="header-text">Обновление стоков KAPSULA</span>
                                        <a class="email-logo" href="https://kapsula.com.ua/">
                                            <svg width="225" height="40" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 478.31 83.58"><path d="M77.19,80.81H67.34l29.11-78h13.28l29.12,78H129l-5.87-15.94h-40ZM103.2,11,85.38,57.67h35.53Z"></path><path d="M154.57,80.81h-9v-78h27.68c18,0,30.88,11.07,30.88,26.57s-12.84,26.45-31,26.45h-18.6Zm0-69.85V47.49H172c13.5,0,23-7.53,22.91-18.15S185.45,11,172,11Z"></path><path d="M211,55.57h9.19c0,11.51,9.85,19.82,23.91,19.82,12.84,0,20.7-6.76,20.7-16.17,0-10.4-11.07-11.95-23.24-13.5-14.39-2-29.34-4.32-29.34-21.7,0-13.84,11.85-24,29.45-24,17.82,0,30.77,11.07,30.77,26.46h-9c.11-10.63-9.08-18.27-21.81-18.27-12.29,0-20.26,6.31-20.26,15.06,0,10.4,10.52,12.06,22.69,13.72C258.47,39,274,41.29,274,58.12c0,14.83-11.73,25.46-30.11,25.46C224.71,83.58,211,71.84,211,55.57Z"></path><path d="M334.81,2.77H344V49.15c0,20.15-13.06,34.43-31.55,34.43S280.79,69.3,280.79,49.15V2.77H290V49.15c0,14.28,8.86,26.24,22.47,26.24,13.4,0,22.36-10.85,22.36-26.24Z"></path><path d="M350.82,80.81v-78H360V72.62h40v8.19Z"></path><path d="M416.65,80.81H406.8l29.11-78h13.28l29.12,78h-9.85l-5.87-15.94h-40Zm26-69.85L424.84,57.67h35.54Z"></path><path d="M60.51,80.81H51.39c0-12.37-3.6-23.53-9-31.7-6.6,6-14.4,9.13-22.33,9.13a19,19,0,0,1-10.92-3V80.81H0v-78H9.13V29.9a19.9,19.9,0,0,1,10.92-3c8.17,0,15.85,3.24,22.45,8.89C47.79,27.38,51,15.85,51,2.77h9.12c0,16.45-4.56,30-11.64,39.5C55.83,51.87,60.51,65.2,60.51,80.81ZM37,42.75c-5-4.92-10.93-7.8-16.93-7.8C13,35,9.25,38,9.13,42.15V43c.12,4.2,3.84,7.2,10.92,7.2C26.29,50.19,32.18,47.43,37,42.75Z"></path></svg>
                                        </a>
                                    </td>
                                </tr>
                            </table>

                            <table class="second-block"  cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="main-text">
                                                        <p>Добрый день, партнеры!</p>
                                                        <p>Вы получили это письмо, так как зарегистрированы на сайте KAPSULA, как бренд-партнер.</p>
                                                        <p>Напоминаем вам о необходимости обновить наличие товаров вашего бренда на сайте KAPSULA.<br>
                                                            Перейти в личный кабинет вы можете по этой <a href="<?php echo site_url(); ?>/wp-admin/">ссылке</a><br>
                                                            Инструкцию по обновлению остатков вы найдете <a href="https://drive.google.com/drive/folders/1GqOExaitrR7TQnrdzOSWEl2cSLkG0jaq?usp=sharing">здесь: https://drive.google.com/drive/folders/1GqOExaitrR7TQnrdzOSWEl2cSLkG0jaq?usp=sharing</a><br>
                                                            Если у вас не получается войти в кабинет или возникли вопросы - напишите аккаунт-менеджеру account@kapsula.com.ua<br>
                                                            Обращаем ваше внимание, что согласно условий сотрудничества, бренды-партнеры обязаны своевременно обновлять наличие товаров во избежание штрафов за нереализованные заказы.<br>
                                                            Пожалуйста, не игнорируйте обновление остатков в личном кабинете!<br>
                                                            Это письмо отправлено автоматически и не требует ответа.<br>
                                                            Чтобы отписаться от рассылки - отправьте письменный запрос на care@kapsula.com.ua.<br>
                                                            Продуктивной недели!</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                            <table class="third-block" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <img src="<?php echo site_url() ?>/wp-content/themes/konte-child/designers-email/images-email/phone-icon.png" alt="Phone">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="address">
                                                        <a href="tel:+3800444991190">+38 044 499 11 90</a>
                                                        Жилянская 54, Киев
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="unsubscribe">
                                                        Вы получили данное письмо, так как зарегистрированы на kapsula.com.ua
                                                        Как дизайнер. Чтобы отписаться от рассылки отправьте нам письмо на <a href="mailto:care@kapsula.com.ua">care@kapsula.com.ua</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
</body>
</html>