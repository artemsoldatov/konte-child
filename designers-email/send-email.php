<?php

if ( ! defined( 'ABSPATH' ) ) {
    require( dirname(__FILE__) . '/../../../../wp-blog-header.php' );
}

if ( class_exists( 'DOMDocument' ) && ! class_exists( 'Emogrifier' ) ) {
    include_once( dirname(__FILE__) . '/Emogrifier.php' );
}

$users = get_users( array( 'role' => 'designer' ) );

$subject = "Обновление стоков KAPSULA";
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
$headers .= 'From: KAPSULA <robot@kapsula.com.ua>' . "\r\n";

ob_start();
include( 'mail_for_designers-styles.php' );
$styles = ob_get_clean();

ob_start();
include( 'mail_for_designers.php' );

if ( class_exists( 'Pelago\Emogrifier' ) ) {
    $emogrifier = new Pelago\Emogrifier( ob_get_clean(), $styles );
    $message = $emogrifier->emogrify();
} else {
    $message = ob_get_clean();
}

foreach ( $users as $user ) {
    $to[] = $user->user_email;
}

$headers .= 'Bcc: ' . implode( ',', $to ) . "\r\n";

wp_mail( 'stock@kapsula.com.ua', $subject, $message, $headers );
