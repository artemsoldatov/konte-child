@import url('https://fonts.googleapis.com/css?family=Andika&subset=cyrillic');

body {
    font-family: 'Andika', sans-serif;;
    background: #fff;
    font-size: 16px;
    color: #000;
    width: 100%;
    height: 100%;
    text-align: center;
    padding: 0;
    margin: 0;
    display: block;
    -webkit-text-size-adjust:none;
}
td, table, h1 {
    text-align: center;
    width: 100%;
}
h1 {
    font-size: initial;
}
.email-container a {
    color: #000;
}
.email-wrapper {
    max-width: 620px;
    padding: 0 20px;
}
.first-block,
.second-block,
.third-block,
.email-container {
    padding: 15px 0;
}
.first-block {
    border-bottom: 2px solid #000;
}
.second-block {
    border-bottom: 2px solid #000;
}
.third-block {
    text-align: center;
    background: rgba(100, 100, 100, .04);
    font-size: 14px;
    color: #646464;
    letter-spacing: -.2px;
}
.third-block img {
    width: 30px;
    height: 30px;
}
.header-text {
    display: block;
    font-size: 12px;
}
.email-logo {
    margin-top: 10px;
    display: inline-block;
}
.email-logo img {
    width: 230px;
    height: 35px;
}
.personal-cabinet {
    text-align: center;
    text-transform: uppercase;
    margin: 20px auto 10px;
}
.unsubscribe {
    padding: 20px 50px;
    margin: 20px 80px;
    background: rgba(0, 0, 0, .15);
    font-family: sans-serif;
    font-size: 12px;
}
.address {
    margin-top: 10px;
    line-height: 20px;
}
.address a {
    display: table;
    margin: 0 auto;
}

/*MEDIA*/
@media screen and (max-width: 600px) {
    .unsubscribe {
        margin: 20px 0px;
    }
}
@media screen and (max-width: 420px) {
    .main-text {
        text-align: justify;
    }
}