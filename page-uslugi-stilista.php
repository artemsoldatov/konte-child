<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

<!--    <link rel="stylesheet" href="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/slick/slick.css">-->
    <link rel="stylesheet" href="/wp-content/plugins/woo-brand/css/carousel/slick.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/slick/slick-theme.css">
    <link rel="stylesheet" href="/wp-content/plugins/ct-size-guide/assets/css/magnific.popup.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/wardrobe.css">
    <link rel="shortcut icon" href="/wp-content/uploads/product_img/2019/03/Logo-Round-1.png">

    <!--noptimize-->
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function()
        {n.callMethod? n.callMethod.apply(n,arguments):n.queue.push(arguments)}
        ;if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','//connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1491716344460784');
        fbq('track', "PageView");
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="https://www.facebook.com/tr?id=1491716344460784&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Facebook Pixel Code -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-65544326-1"></script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5QJ8QG');</script>
    <!-- End Google Tag Manager -->
    <script>window.dataLayer=window.dataLayer||[];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config','UA-65544326-1',{'optimize_id':'GTM-MZRK8V2'});
    </script>

    <style>.async-hide {
            opacity: 0 !important
        }</style>
    <script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;
            h.start=1*new Date;h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
            (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
        })(window,document.documentElement,'async-hide','dataLayer',2000,{'GTM-5QJ8QG': true});</script>

    <!-- Hotjar Tracking Code -->
    <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1440529,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
    </script>
    <!-- End Hotjar Tracking Code -->
    <!--/noptimize-->


<!--    --><?php //wp_head(); ?>

</head>
<body class="t-body ver-a" style="margin: 0px;"><!--allrecords-->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QJ8QG"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="allrecords" class="t-records t-records_animated t-records_visible" data-hook="blocks-collection-content-node"
     data-tilda-project-id="1159705" data-tilda-page-id="5656591" data-tilda-page-alias="main3_for_dev"
     data-tilda-formskey="eb772ffc5b3952d8812ee53a8eac3075">
    <div id="rec101333342" class="r t-rec" style="background-color:#ffffff; " data-animationappear="off"
         data-record-type="257" data-bg-color="#ffffff"><!-- T228 -->
        <div id="nav101333342marker"></div>
        <div class="t228__mobile">
            <div class="t228__mobile_container">
                <div class="t228__mobile_text t-name t-name_md" field="text">&nbsp;</div>
                <div class="t228__burger"><span></span> <span></span> <span></span> <span></span></div>
            </div>
        </div>
        <div id="nav101333342" class="t228 t228__hidden t228__positionabsolute "
             style="background-color: rgba(255,255,255,1); height:80px; box-shadow: 0px 1px 3px rgba(0,0,0,0.20);"
             data-bgcolor-hex="#ffffff" data-bgcolor-rgba="rgba(255,255,255,1)" data-navmarker="nav101333342marker"
             data-appearoffset="" data-bgopacity-two="" data-menushadow="20" data-bgopacity="1"
             data-menu-items-align="right" data-menu="yes">
            <div class="t228__maincontainer " style="height:80px;">
                <div class="t228__padding40px"></div>
                <div class="t228__leftside">
                    <div class="t228__leftcontainer">
                        <a href="<?php echo site_url(); ?>" target="_blank"
                           style="color:#ffffff;font-size:18px;font-weight:700;letter-spacing:1.5px;"><img
                                    src="https://static.tildacdn.com/tild6431-6539-4638-b863-306236396134/Kapsula-logo_300.png"
                                    class="t228__imglogo t228__imglogomobile no-lazy-load" imgfield="img"
                                    style="max-width: 150px;width: 150px; height: auto; display: block;" alt=""></a>
                    </div>
                </div>
                <div class="t228__centerside t228__menualign_right">
                    <div class="t228__centercontainer">
                        <ul class="t228__list ">
                            <li class="t228__list_item">
                                <a class="t-menu__link-item" href="#rec101334156"
                                   style="color:#000000;font-size:14px;font-weight:400;font-family:'Roboto';text-transform:uppercase;"
                                   data-menu-item-number="1">Услуги и стоимость</a>
                            </li>
                            <li class="t228__list_item">
                                <a class="t-menu__link-item" href="#how-we-do-it"
                                   style="color:#000000;font-size:14px;font-weight:400;font-family:'Roboto';text-transform:uppercase;"
                                   data-menu-item-number="2">Как мы работаем</a>
                            </li>
                            <li class="t228__list_item">
                                <a class="t-menu__link-item" href="#rec101336447"
                                   style="color:#000000;font-size:14px;font-weight:400;font-family:'Roboto';text-transform:uppercase;"
                                   data-menu-item-number="3">Отзывы клиентов</a>
                            </li>
                            <li class="t228__list_item">
                                <a class="t-menu__link-item" href="#rec101334323"
                                   style="color:#000000;font-size:14px;font-weight:400;font-family:'Roboto';text-transform:uppercase;"
                                   data-menu-item-number="4">Стилисты</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="t228__rightside">
                    <div class="t228__rightcontainer">
                        <div class="t228__right_descr" style="">+38 044 499 1190</div>
                        <div class="t228__right_buttons">
                            <div class="t228__right_buttons_wrap">
                                <div class="t228__right_buttons_but">
                                    <a class="t-btn b24-web-form-popup-btn-7"
                                       style="color:#e95027;border:1px solid #e95027;background-color:#ffffff;border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Оставить заявку</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t228__padding40px"></div>
            </div>
        </div>

        <div class="youtube-section">
            <iframe width="100%" height="816" src="https://www.youtube.com/embed/wpIqwmRrRrA?rel=0&vq=hd1080" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div id="rec101333343" class="r t-rec new-mobile-first-section" data-animationappear="off"
             data-record-type="205"><!-- cover -->
            <div class="t-cover no-lazy-load" id="recorddiv101333343">
                <div class="t-container">
                    <div class="t-width t-width_12" style="margin:0 auto;">
                        <div class="t-cover__wrapper t-valign_middle">
                            <div class="t182">
                                <div data-hook-content="covercontent">
                                    <div class="t182__wrapper">
                                        <div class="t182__title t-title t-title_xl t-animate t-animate_started"
                                             data-animate-style="fadeinup" data-animate-group="yes"
                                             style="font-size: 56px; font-weight: 700; padding-top: 55px; font-family: Roboto; transition-delay: 0s;"
                                             field="title">
                                            <span style="font-weight: 700;">Создаем стильный гардероб <br/>без утомительных походов по магазинам</span> <br>
                                        </div>
                                        <div class="t182__descr t-descr t-descr_lg t-animate t-animate_started"
                                             data-animate-style="fadeinup" data-animate-group="yes"
                                             style="color: #000; font-size: 30px; font-weight: 300; padding-top: 50px; font-family: Roboto; transition-delay: 0.3s;"
                                             field="descr">Быть уверенной в себе легко, <br />
                                            когда есть эффективный гардероб.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- arrow -->
                <div class="t-cover__arrow">
                    <div class="t-cover__arrow-wrapper t-cover__arrow-wrapper_animated">
                        <div class="t-cover__arrow_mobile">
                            <a class="t412__btn t-btn b24-web-form-popup-btn-7" style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                <table style="width:100%; height:100%;">
                                    <tbody>
                                    <tr>
                                        <td>Узнать больше про услугу</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </a>
                        </div>
                    </div>
                </div> <!-- arrow --> </div>

        </div>

        <div id="rec101333344" class="r t-rec r_anim r_showed" style=" " data-record-type="270">
            <div class="t270"></div>

        </div>
        <div class="block-custom block-3">
            <div class="t-container">
                <div class="block-3-bg"></div>
                <div class="t-col t-col_6">
                    <div class="t-title t-title_lg title-mb">Подбираем вам индивидуальный гардероб</div>
                    <ul class="list-custom">
                        <li>
                            Все вещи сочетаются между собой в капсульный гардероб
                        </li>
                        <li>
                            Готовые комплекты на весь сезон и любые события
                        </li>
                        <li>
                            Все вещи подобраны индивидуально для вашей фигуры и образа жизни
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="how-we-do-it" class="block-custom block-4">
            <div class="t-container">
                <div class="t-title t-title_lg t-align_center">Как мы это делаем?</div>
                <div class="popup-steps four-steps">
                    <div class="popup-steps-item">
                        <div class="popup-steps-title">Знакомство со стилистом</div>
                        <div class="popup-steps-numb"><span>1</span></div>
                        <div class="popup-steps-description">
                            Заполняете онлайн-анкету с указанием ваших предпочтений по гардеробу.
                            После чего знакомитесь со стилистом на звонке или на личной  встрече для согласования нюансов гардероба.
                        </div>
                    </div>
                    <div class="popup-steps-item">
                        <div class="popup-steps-title">Подбор гардероба стилистом</div>
                        <div class="popup-steps-numb"><span>2</span></div>
                        <div class="popup-steps-description">
                            Стилист подбирает одежду, обувь и аксессуары среди 100+ дизайнерских брендов в KAPSULA, учитывая ваши пожелания и бюджет.
                        </div>
                    </div>
                    <div class="popup-steps-item">
                        <div class="popup-steps-title">Примерка за 2 часа</div>
                        <div class="popup-steps-numb"><span>3</span></div>
                        <div class="popup-steps-description">
                            Приезжаете в комфортный шоурум KAPSULA в Киеве, где вас ждет более 30 готовых комплектов вещей на примерку.
                            Пробуете разные сочетания вещей и покупаете только то, что нравится.
                        </div>
                    </div>
                    <div class="popup-steps-item">
                        <div class="popup-steps-title">Сезонная поддержка гардероба</div>
                        <div class="popup-steps-numb"><span>4</span></div>
                        <div class="popup-steps-description">
                            Мы храним информацию о ваших размерах и предпочтениях.
                            В течение сезона стилист помогает подбирать новые вещи по запросу.
                        </div>
                    </div>
                </div>
                <div style="text-align: center">
                    <a class="t412__btn t-btn b24-web-form-popup-btn-7" style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                        <table style="width:100%; height:100%;">
                            <tbody>
                            <tr>
                                <td>Получить консультацию</td>
                            </tr>
                            </tbody>
                        </table>
                    </a>
                </div>
            </div>
        </div>
        <div id="rec101334156" class="r t-rec t-rec_pt_90 t-rec_pb_0 r_anim r_showed"
             style="padding-top:90px;padding-bottom:0px; " data-record-type="30"><!-- T015 -->
            <div class="t015">
                <div class="t-container t-align_center">
                    <div class="t-col t-col_10 t-prefix_1">
                        <div class="t015__title t-title t-title_lg" field="title" style="">Заказывайте от одного образа до гардероба на сезон</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rec101334179" class="r t-rec t-rec_pt_45 t-rec_pb_15 r_anim r_showed"
             style="padding-top:45px;padding-bottom:15px; " data-record-type="412"><!-- T412 -->
            <div class="t412">
                <div class="t-container">
                    <div class="t412__col t-col t-col_4 t412__col"><a href="#obraz">
                        </a>
                        <div class="t412__content" style="border-color: #000000;border-width: 2px;"><a href="#obraz">
                            </a>
                            <div class="t412__wrapper" style="height: 677px;">
                                <a href="#obraz"><img class="t412__img t412__img_circle t-img loaded no-lazy-load"
                                                      src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Nashi_yslugi_3.png"
                                                      data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Nashi_yslugi_3.png"
                                                      data-tu-max-width="300" data-tu-max-height="300" alt="" style=""
                                                      imgfield="img3">
                                    <div class="t412__title t-name t-name_xl" style="height: 64px;" field="title3">
                                        Подбор
                                        готового образа
                                    </div>
                                    <div class="price-shape">500 грн</div>
                                    <div class="t412__descr t-text" style="height: 350px;" field="descr3">
                                        <ul class="list-custom list-custom-small">
                                            <li>Подбор по вашему запросу нескольких вариантов вещей с обувью и
                                                аксессуарами
                                            </li>
                                            <li>Доставка выбранных вещей курьером или примерка в шоуруме KAPSULA со
                                                стилистом
                                            </li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="t412__buttonwrapper"><a href="#obraz">
                                    </a>
                                    <a class="t412__btn t-btn popup-btn hide-mobile"
                                       href="#howWeDo3"
                                       style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Записаться <br/> к стилисту</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                    <a class="t412__btn t-btn popup-btn popup-mobile-btn show-mobile"
                                       href="#howWeDo3"
                                       style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Записаться <br/> к стилисту</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="t412__col t-col t-col_4 t412__col"><a href="#wardrobe">
                        </a>
                        <div class="t412__content" style="border-color: #000000;border-width: 2px;"><a href="#wardrobe">
                            </a>
                            <div class="t412__wrapper" style="height: 677px;">
                                <a href="#wardrobe"><img class="t412__img t412__img_circle t-img loaded no-lazy-load"
                                                         src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Nashi_yslugi_1_3.png"
                                                         data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Nashi_yslugi_1_3.png"
                                                         data-tu-max-width="300" data-tu-max-height="300" alt=""
                                                         style="" imgfield="img">
                                    <div class="t412__title t-name t-name_xl" style="height: 64px;" field="title">
                                        Подбор капсульного гардероба
                                    </div>
                                    <div class="price-shape">3000 грн</div>
                                    <div class="t412__descr t-text" style="height: 350px;" field="descr">
                                        <ul class="list-custom list-custom-small">
                                            <li>Предварительная встреча-знакомство со стилистом</li>
                                            <li>Индивидуальный подбор гардероба стилистом</li>
                                            <li>Примерка в шоуруме KAPSULA с персональным стилистом</li>
                                            <li>Любое количество «докупок» в течение сезона</li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="t412__buttonwrapper"><a href="#wardrobe">
                                    </a>
                                    <a class="t412__btn t-btn popup-btn hide-mobile"
                                       href="#howWeDo2"
                                       style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Записаться <br/> к стилисту</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                    <a class="t412__btn t-btn popup-btn popup-mobile-btn show-mobile"
                                       href="#howWeDo2"
                                       style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Записаться <br/> к стилисту</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                </div>
                                <p class="tooltip-price-text mobile">* есть услуга рассрочки и скидки от 40 тыс грн</p>
                            </div>
                        </div>
                    </div>
                    <div class="t412__col t-col t-col_4 t412__col"><a href="#analysis">
                        </a>
                        <div class="t412__content" style="border-color: #000000;border-width: 2px;"><a href="#analysis">
                            </a>
                            <div class="t412__wrapper" style="height: 677px;">
                                <a href="#analysis"><img class="t412__img t412__img_circle t-img loaded no-lazy-load"
                                                         src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Nashi_yslugi_2_2.png"
                                                         data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Nashi_yslugi_2_2.png"
                                                         data-tu-max-width="300" data-tu-max-height="300" alt=""
                                                         style="" imgfield="img2">
                                    <div class="t412__title t-name t-name_xl" style="height: 64px;" field="title2">
                                        Разбор +
                                        подбор капсульного гардероба
                                    </div>
                                    <div class="price-shape">4700 грн</div>
                                    <div class="t412__descr t-text" style="height: 350px;" field="descr2">
                                        <ul class="list-custom list-custom-small">
                                            <li>Разбор гардероба со стилистом у вас дома</li>
                                            <li>Список приоритетных покупок</li>
                                            <li>Индивидуальный подбор нового гардероба стилистом с учетом текущего
                                                гардероба
                                            </li>
                                            <li>Примерка в шоуруме KAPSULA с персональным стилистом</li>
                                            <li>Любое количество «докупок» в течение сезона</li>
                                        </ul>
                                    </div>
                                </a>
                                <div class="t412__buttonwrapper"><a href="#analysis">
                                    </a>
                                    <a class="t412__btn t-btn popup-btn hide-mobile"
                                       href="#howWeDo1"
                                       style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Записаться <br/> к стилисту</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                    <a class="t412__btn t-btn popup-btn popup-mobile-btn show-mobile"
                                       href="#howWeDo1"
                                       style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                        <table style="width:100%; height:100%;">
                                            <tbody>
                                            <tr>
                                                <td>Записаться <br/> к стилисту</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </a>
                                </div>
                                <p class="tooltip-price-text mobile">* есть услуга рассрочки и скидки от 40 тыс грн</p>
                            </div>
                        </div>
                    </div>
                    <p class="tooltip-price-text hide-mob">* есть услуга рассрочки и скидки от 40 тыс грн</p>
                </div>
            </div>

        </div>
        <div id="rec101334323" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->
            <div class="t396__elem tn-elem tn-elem__1013343231556183472179" data-elem-id="1556183472179"
                 data-elem-type="text" data-field-top-value="80" data-field-top-res-480-value="50"
                 data-field-top-res-320-value="40" data-field-left-value="0" data-field-left-res-480-value="0"
                 data-field-left-res-320-value="0" data-field-width-value="560" data-field-axisy-value="top"
                 data-field-axisx-value="center" data-field-container-value="grid" data-field-topunits-value=""
                 data-field-leftunits-value="" data-field-heightunits-value="" data-field-widthunits-value=""
                 data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                 style="top: 80px; left: 433px; width: 560px;">
                <div class="tn-atom" field="tn_text_1556183472179">Наша команда</div>
            </div>

            <div class="t396">
                <div class="t396__artboard rendered" data-artboard-recid="101334323" data-artboard-height="570"
                     data-artboard-height-res-640="410" data-artboard-height-res-480="590"
                     data-artboard-height-res-320="640" data-artboard-height_vh="" data-artboard-valign="center"
                     data-artboard-ovrflw="" data-artboard-proxy-min-offset-top="0" data-artboard-proxy-min-height="570"
                     data-artboard-proxy-max-height="570">
                    <div class="t396__carrier" data-artboard-recid="101334323"></div>
                    <div class="t396__filter" data-artboard-recid="101334323"></div>
                    <div class="t396__elem tn-elem tn-elem__1013343231470210033144 img-team-wrapper" data-elem-id="1470210033144"
                         data-elem-type="image" data-field-top-value="178" data-field-top-res-640-value="129"
                         data-field-top-res-480-value="119" data-field-top-res-320-value="109"
                         data-field-left-value="20" data-field-left-res-640-value="10" data-field-left-res-480-value="0"
                         data-field-left-res-320-value="0" data-field-width-value="360"
                         data-field-width-res-640-value="220" data-field-axisy-value="top" data-field-axisx-value="left"
                         data-field-axisx-res-480-value="center" data-field-container-value="grid"
                         data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                         data-field-widthunits-value=""
                         data-fields="img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="left: 133px; top: 178px; width: 360px;">
                        <div class="tn-atom">
                            <img class="tn-atom__img t-img loaded no-lazy-load"
                                 data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_natasha_1.png"
                                 imgfield="tn_img_1470210033144"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_natasha_1.png">
                        </div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013343231470210128180 team-status-prof" data-elem-id="1470210128180"
                         data-elem-type="text" data-field-top-value="250" data-field-top-res-960-value="170"
                         data-field-top-res-640-value="130" data-field-top-res-480-value="379"
                         data-field-top-res-320-value="359" data-field-left-value="421"
                         data-field-left-res-960-value="411" data-field-left-res-640-value="251"
                         data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                         data-field-width-value="740" data-field-width-res-960-value="540"
                         data-field-width-res-640-value="380" data-field-width-res-320-value="300"
                         data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                         data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                         data-field-widthunits-value=""
                         data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="top: 250px; left: 534px; width: 740px;">
                        <div class="tn-atom" field="tn_text_1470210128180">Со-основатель и персональный стилист</div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013343231470210011265 team-status-desc" data-elem-id="1470210011265"
                         data-elem-type="text" data-field-top-value="290" data-field-top-res-960-value="210"
                         data-field-top-res-640-value="170" data-field-top-res-480-value="410"
                         data-field-top-res-320-value="410" data-field-left-value="421"
                         data-field-left-res-960-value="411" data-field-left-res-640-value="251"
                         data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                         data-field-width-value="760" data-field-width-res-960-value="530"
                         data-field-width-res-640-value="380" data-field-width-res-480-value="460"
                         data-field-width-res-320-value="300" data-field-axisy-value="top" data-field-axisx-value="left"
                         data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                         data-field-heightunits-value="" data-field-widthunits-value=""
                         data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="top: 290px; left: 534px; width: 760px;">
                        <div class="tn-atom" field="tn_text_1470210011265">Наталья - наш со-основатель и идейный
                            вдохновитель. Более 5 лет опыта в роли персонального стилиста и более 100 довольных клиентов
                            в 5
                            странах. Именно она отбирает в KAPSULA особенные дизайнерские вещи и обучает команду
                            стилистов.
                            Весной и осенью попасть на примерку к Наталье сложно, но возможно.
                        </div>
                    </div>
                </div>
                <div class="team-slider">
                    <div id="rec101334444" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->
                        <div class="t396">
                            <div class="t396__artboard rendered" data-artboard-recid="101334444" data-artboard-height="300"
                                 data-artboard-height-res-640="280" data-artboard-height-res-480="450"
                                 data-artboard-height-res-320="460" data-artboard-height_vh="" data-artboard-valign="center"
                                 data-artboard-ovrflw="" data-artboard-proxy-min-offset-top="0" data-artboard-proxy-min-height="300"
                                 data-artboard-proxy-max-height="300">
                                <div class="t396__carrier" data-artboard-recid="101334444"></div>
                                <div class="t396__filter" data-artboard-recid="101334444"></div>
                                <div class="t396__elem tn-elem tn-elem__1013344441470210033144 img-team-wrapper" data-elem-id="1470210033144"
                                     data-elem-type="image" data-field-top-value="18" data-field-top-res-960-value="18"
                                     data-field-top-res-640-value="29" data-field-top-res-480-value="29"
                                     data-field-top-res-320-value="29" data-field-left-value="920"
                                     data-field-left-res-960-value="690" data-field-left-res-640-value="410"
                                     data-field-left-res-480-value="0" data-field-left-res-320-value="0"
                                     data-field-width-value="260" data-field-width-res-640-value="220" data-field-axisy-value="top"
                                     data-field-axisx-value="left" data-field-axisx-res-480-value="center"
                                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                                     data-field-heightunits-value="" data-field-widthunits-value=""
                                     data-fields="img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="left: 1028.5px; top: 18px; width: 260px;">
                                    <div class="tn-atom">
                                        <img class="tn-atom__img t-img loaded no-lazy-load"
                                             data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_lena_1.png"
                                             imgfield="tn_img_1470210033144"
                                             src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_lena_1.png">
                                    </div>
                                </div>
                                <div class="t396__elem tn-elem tn-elem__1013344441470210128180 team-status-prof" data-elem-id="1470210128180"
                                     data-elem-type="text" data-field-top-value="81" data-field-top-res-960-value="50"
                                     data-field-top-res-640-value="30" data-field-top-res-480-value="289"
                                     data-field-top-res-320-value="279" data-field-left-value="21"
                                     data-field-left-res-960-value="11" data-field-left-res-640-value="11"
                                     data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                                     data-field-width-value="740" data-field-width-res-960-value="540"
                                     data-field-width-res-640-value="380" data-field-width-res-320-value="300"
                                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                                     data-field-widthunits-value=""
                                     data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="top: 81px; left: 129.5px; width: 740px;">
                                    <div class="tn-atom" field="tn_text_1470210128180">Персональный стилист</div>
                                </div>
                                <div class="t396__elem tn-elem tn-elem__1013344441470210011265 team-status-desc" data-elem-id="1470210011265"
                                     data-elem-type="text" data-field-top-value="121" data-field-top-res-960-value="90"
                                     data-field-top-res-640-value="70" data-field-top-res-480-value="320"
                                     data-field-top-res-320-value="310" data-field-left-value="21"
                                     data-field-left-res-960-value="11" data-field-left-res-640-value="11"
                                     data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                                     data-field-width-value="860" data-field-width-res-960-value="640"
                                     data-field-width-res-640-value="380" data-field-width-res-480-value="460"
                                     data-field-width-res-320-value="300" data-field-axisy-value="top" data-field-axisx-value="left"
                                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                                     data-field-heightunits-value="" data-field-widthunits-value=""
                                     data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="top: 121px; left: 129.5px; width: 860px;">
                                    <div class="tn-atom" field="tn_text_1470210011265">А это Лена - опытный персональный стилист и
                                        главный трендсеттер в нашей команде. Лена умело внедряет самые смелые модные тренды в
                                        повседневный гардероб, при этом ты чувствуешь себя комфортно и стильно.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="rec101334528" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->

                        <div class="t396">
                            <div class="t396__artboard rendered" data-artboard-recid="101334528" data-artboard-height="300"
                                 data-artboard-height-res-640="270" data-artboard-height-res-480="470"
                                 data-artboard-height-res-320="490" data-artboard-height_vh="" data-artboard-valign="center"
                                 data-artboard-ovrflw="" data-artboard-proxy-min-offset-top="0" data-artboard-proxy-min-height="300"
                                 data-artboard-proxy-max-height="300">
                                <div class="t396__carrier" data-artboard-recid="101334528"></div>
                                <div class="t396__filter" data-artboard-recid="101334528"></div>
                                <div class="t396__elem tn-elem tn-elem__1013345281470210033144 img-team-wrapper" data-elem-id="1470210033144"
                                     data-elem-type="image" data-field-top-value="18" data-field-top-res-640-value="29"
                                     data-field-top-res-480-value="29" data-field-top-res-320-value="29" data-field-left-value="20"
                                     data-field-left-res-640-value="10" data-field-left-res-480-value="0"
                                     data-field-left-res-320-value="0" data-field-width-value="260"
                                     data-field-width-res-640-value="220" data-field-axisy-value="top" data-field-axisx-value="left"
                                     data-field-axisx-res-480-value="center" data-field-container-value="grid"
                                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                                     data-field-widthunits-value=""
                                     data-fields="img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="left: 133px; top: 18px; width: 260px;">
                                    <div class="tn-atom">
                                        <img class="tn-atom__img t-img loaded no-lazy-load"
                                             data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_Olya_1.png"
                                             imgfield="tn_img_1470210033144"
                                             src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_Olya_1.png">
                                    </div>
                                </div>
                                <div class="t396__elem tn-elem tn-elem__1013345281470210128180 team-status-prof" data-elem-id="1470210128180"
                                     data-elem-type="text" data-field-top-value="70" data-field-top-res-960-value="40"
                                     data-field-top-res-640-value="30" data-field-top-res-480-value="289"
                                     data-field-top-res-320-value="279" data-field-left-value="321"
                                     data-field-left-res-960-value="331" data-field-left-res-640-value="251"
                                     data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                                     data-field-width-value="740" data-field-width-res-960-value="540"
                                     data-field-width-res-640-value="380" data-field-width-res-320-value="300"
                                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                                     data-field-widthunits-value=""
                                     data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="top: 70px; left: 434px; width: 740px;">
                                    <div class="tn-atom" field="tn_text_1470210128180">Персональный стилист</div>
                                </div>
                                <div class="t396__elem tn-elem tn-elem__1013345281470210011265 team-status-desc" data-elem-id="1470210011265"
                                     data-elem-type="text" data-field-top-value="110" data-field-top-res-960-value="80"
                                     data-field-top-res-640-value="70" data-field-top-res-480-value="320"
                                     data-field-top-res-320-value="310" data-field-left-value="321"
                                     data-field-left-res-960-value="331" data-field-left-res-640-value="251"
                                     data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                                     data-field-width-value="860" data-field-width-res-960-value="620"
                                     data-field-width-res-640-value="380" data-field-width-res-480-value="460"
                                     data-field-width-res-320-value="300" data-field-axisy-value="top" data-field-axisx-value="left"
                                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                                     data-field-heightunits-value="" data-field-widthunits-value=""
                                     data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="top: 110px; left: 434px; width: 860px;">
                                    <div class="tn-atom" field="tn_text_1470210011265">Это Оля и одевать людей - ее любимая работа.
                                        Оля
                                        классно разбирается в типах фигур, идеально подбирает посадку вещей. Если хочешь обновить
                                        гардероб в стиле непринужденной легкости, необязывающей классики и со 100% комфортом - это к
                                        Оле!
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="rec101334601" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->

                        <div class="t396">
                            <div class="t396__artboard rendered" data-artboard-recid="101334601" data-artboard-height="360"
                                 data-artboard-height-res-960="360" data-artboard-height-res-640="310"
                                 data-artboard-height-res-480="490" data-artboard-height-res-320="500" data-artboard-height_vh=""
                                 data-artboard-valign="center" data-artboard-ovrflw="" data-artboard-proxy-min-offset-top="0"
                                 data-artboard-proxy-min-height="360" data-artboard-proxy-max-height="360">
                                <div class="t396__carrier" data-artboard-recid="101334601"></div>
                                <div class="t396__filter" data-artboard-recid="101334601"></div>
                                <div class="t396__elem tn-elem tn-elem__1013346011470210033144 img-team-wrapper" data-elem-id="1470210033144"
                                     data-elem-type="image" data-field-top-value="18" data-field-top-res-960-value="18"
                                     data-field-top-res-640-value="29" data-field-top-res-480-value="29"
                                     data-field-top-res-320-value="29" data-field-left-value="920"
                                     data-field-left-res-960-value="690" data-field-left-res-640-value="410"
                                     data-field-left-res-480-value="0" data-field-left-res-320-value="0"
                                     data-field-width-value="260" data-field-width-res-640-value="220" data-field-axisy-value="top"
                                     data-field-axisx-value="left" data-field-axisx-res-480-value="center"
                                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                                     data-field-heightunits-value="" data-field-widthunits-value=""
                                     data-fields="img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="left: 1033px; top: 18px; width: 260px;">
                                    <div class="tn-atom">
                                        <img class="tn-atom__img t-img loaded no-lazy-load"
                                             data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_Marjana_1.png"
                                             imgfield="tn_img_1470210033144"
                                             src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Stylist_Marjana_1.png">
                                    </div>
                                </div>
                                <div class="t396__elem tn-elem tn-elem__1013346011470210128180 team-status-prof" data-elem-id="1470210128180"
                                     data-elem-type="text" data-field-top-value="81" data-field-top-res-960-value="50"
                                     data-field-top-res-640-value="30" data-field-top-res-480-value="289"
                                     data-field-top-res-320-value="279" data-field-left-value="21"
                                     data-field-left-res-960-value="11" data-field-left-res-640-value="11"
                                     data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                                     data-field-width-value="740" data-field-width-res-960-value="540"
                                     data-field-width-res-640-value="380" data-field-width-res-320-value="300"
                                     data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                                     data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                                     data-field-widthunits-value=""
                                     data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="top: 81px; left: 134px; width: 740px;">
                                    <div class="tn-atom" field="tn_text_1470210128180">Персональный стилист</div>
                                </div>
                                <div class="t396__elem tn-elem tn-elem__1013346011470210011265 team-status-desc" data-elem-id="1470210011265"
                                     data-elem-type="text" data-field-top-value="121" data-field-top-res-960-value="90"
                                     data-field-top-res-640-value="70" data-field-top-res-480-value="320"
                                     data-field-top-res-320-value="310" data-field-left-value="21"
                                     data-field-left-res-960-value="11" data-field-left-res-640-value="11"
                                     data-field-left-res-480-value="11" data-field-left-res-320-value="11"
                                     data-field-width-value="860" data-field-width-res-960-value="640"
                                     data-field-width-res-640-value="380" data-field-width-res-480-value="460"
                                     data-field-width-res-320-value="300" data-field-axisy-value="top" data-field-axisx-value="left"
                                     data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                                     data-field-heightunits-value="" data-field-widthunits-value=""
                                     data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                                     style="top: 121px; left: 134px; width: 860px;">
                                    <div class="tn-atom" field="tn_text_1470210011265">Марьяна - наш незаменимый стилист по базовому
                                        гардеробу с индивидуальным подходом. Марьяна идеально чувствует вашу потребность и чутко
                                        подбирает под ваш образ жизни ключевые вещи для гардероба.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rec101334698" class="r t-rec" style=" " data-animationappear="off" data-record-type="396"><!-- T396 -->
        </div>
        <div id="rec101334763" class="r t-rec" style=" " data-animationappear="off" data-record-type="179">
            <!-- cover -->
            <div class="t-cover no-lazy-load" id="recorddiv101334763" bgimgfield="img"
                 style="height:500px; background-image:url('https://static.tildacdn.com/tild3463-3539-4062-b733-326462663139/-/resize/20x/Kapsula_email_back3.png');">
                <div class="t-cover__carrier loaded no-lazy-load" id="coverCarry101334763" data-content-cover-id="101334763"
                     data-content-cover-bg="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Kapsula_email_back3.png"
                     data-content-cover-height="500px" data-content-cover-parallax=""
                     style="height: 500px; background-attachment: scroll; background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/Kapsula_email_back3.png');"
                     src=""></div>
                <div class="t-cover__filter no-lazy-load"
                     style="height:500px;background-image: -moz-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: -webkit-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: -o-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: -ms-linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));background-image: linear-gradient(top, rgba(0,0,0,0.0), rgba(0,0,0,0.0));filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#fe000000', endColorstr='#fe000000');"></div>
                <!-- T164 -->
                <div class="t164">
                    <div class="t-container">
                        <div class="t-cover__wrapper t-valign_middle" style="height:500px;">
                            <div class="t-col t-col_10 t-prefix_1 t-align_center">
                                <div data-hook-content="covercontent">
                                    <div class="t164__wrapper">
                                        <div class="t164__descr t-descr t-descr_xxxl"
                                             style="color:#000000;font-size:24px;font-weight:300;font-family:'Roboto';"
                                             field="descr">А если я не могу оплатить сразу все желаемые покупки?
                                        </div>
                                        <div class="t164__text t-text t-text_md"
                                             style="color:#000000;font-size:24px;font-weight:300;font-family:'Roboto';"
                                             field="text">
                                            <span style="font-weight: 700;"><span data-redactor-style="font-size: 30px;"
                                                                                  style="font-size: 30px;">Понимаем, поэтому теперь для вас есть услуга рассрочки.</span></span><br><br>При
                                            подборе гардероба со стилистом вам не обязательно оплачивать все покупки
                                            сразу —
                                            вы можете сделать это постепенно, разбив недостающую сумму оплаты от 2 до 24
                                            месяцев.<br></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rec101336217" class="r t-rec t-rec_pt_90 t-rec_pb_0 r_anim r_showed"
             style="padding-top:60px;padding-bottom:0px; " data-record-type="30"><!-- T015 -->
            <div class="t015">
                <div class="t-container t-align_center">
                    <div class="t-col t-col_10 t-prefix_1">
                        <div class="t015__title t-title t-title_lg" field="title" style=""> Готовы попробовать?</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rec101336270" class="r t-rec r_anim r_showed" style=" " data-record-type="396"><!-- T396 -->

            <div class="t396">
                <div class="t396__artboard rendered" data-artboard-recid="101336270" data-artboard-height="710"
                     data-artboard-height-res-960="560" data-artboard-height-res-640="1110"
                     data-artboard-height-res-480="924" data-artboard-height-res-320="720" data-artboard-height_vh=""
                     data-artboard-valign="center" data-artboard-ovrflw="" data-artboard-proxy-min-offset-top="0"
                     data-artboard-proxy-min-height="710" data-artboard-proxy-max-height="710">
                    <div class="t396__carrier" data-artboard-recid="101336270"></div>
                    <div class="t396__filter" data-artboard-recid="101336270"></div>
                    <div class="t396__elem tn-elem tn-elem__1013362701466084312673" data-elem-id="1466084312673"
                         data-elem-type="text" data-field-top-value="230" data-field-top-res-960-value="191"
                         data-field-top-res-640-value="818" data-field-top-res-480-value="659"
                         data-field-top-res-320-value="465" data-field-left-value="621"
                         data-field-left-res-960-value="489" data-field-left-res-640-value="20"
                         data-field-left-res-480-value="10" data-field-left-res-320-value="10"
                         data-field-width-value="560" data-field-width-res-960-value="460"
                         data-field-width-res-640-value="600" data-field-width-res-480-value="460"
                         data-field-width-res-320-value="300px" data-field-axisy-value="top"
                         data-field-axisx-value="left" data-field-container-value="grid" data-field-topunits-value=""
                         data-field-leftunits-value="" data-field-heightunits-value="" data-field-widthunits-value=""
                         data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="top: 230px; left: 734px; width: 560px;">
                        <div class="tn-atom" field="tn_text_1466084312673">Доверьте и то, и другое нашему стилисту</div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013362701469216533139" data-elem-id="1469216533139"
                         data-elem-type="image" data-field-top-value="46" data-field-top-res-960-value="40"
                         data-field-top-res-640-value="55" data-field-top-res-480-value="63"
                         data-field-top-res-320-value="30" data-field-left-value="20" data-field-left-res-960-value="11"
                         data-field-left-res-640-value="20" data-field-left-res-480-value="10"
                         data-field-left-res-320-value="10" data-field-width-value="560"
                         data-field-width-res-960-value="460" data-field-width-res-640-value="600"
                         data-field-width-res-480-value="460" data-field-width-res-320-value="300"
                         data-field-axisy-value="top" data-field-axisx-value="left" data-field-container-value="grid"
                         data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                         data-field-widthunits-value=""
                         data-fields="img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="left: 133px; top: 46px; width: 560px;">
                        <div class="tn-atom">
                            <img class="tn-atom__img t-img loaded no-lazy-load"
                                 data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/_MG_0875_5.jpg"
                                 imgfield="tn_img_1469216533139"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/_MG_0875_5.jpg">
                        </div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013362701469216647562" data-elem-id="1469216647562"
                         data-elem-type="text" data-field-top-value="152" data-field-top-res-960-value="110"
                         data-field-top-res-640-value="737" data-field-top-res-480-value="589"
                         data-field-top-res-320-value="373" data-field-left-value="611"
                         data-field-left-res-960-value="490" data-field-left-res-640-value="20"
                         data-field-left-res-480-value="10" data-field-left-res-320-value="10"
                         data-field-width-value="560" data-field-width-res-960-value="460"
                         data-field-width-res-640-value="600" data-field-width-res-480-value="460"
                         data-field-width-res-320-value="300px" data-field-axisy-value="top"
                         data-field-axisx-value="left" data-field-container-value="grid" data-field-topunits-value=""
                         data-field-leftunits-value="" data-field-heightunits-value="" data-field-widthunits-value=""
                         data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="top: 152px; left: 724px; width: 560px;">
                        <div class="tn-atom" field="tn_text_1469216647562">Шопинг должен быть удобным, а гардероб –
                            стильным.
                        </div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013362701473937931492" data-elem-id="1473937931492"
                         data-elem-type="shape" data-field-top-value="207" data-field-top-res-960-value="179"
                         data-field-top-res-640-value="796" data-field-top-res-480-value="637"
                         data-field-top-res-320-value="443" data-field-left-value="867"
                         data-field-left-res-960-value="700" data-field-left-res-640-value="295"
                         data-field-left-res-480-value="215" data-field-left-res-320-value="137"
                         data-field-height-value="3" data-field-width-value="50" data-field-axisy-value="top"
                         data-field-axisx-value="left" data-field-container-value="grid" data-field-topunits-value=""
                         data-field-leftunits-value="" data-field-heightunits-value="" data-field-widthunits-value=""
                         data-fields="width,height,top,left,container,axisx,axisy,widthunits,heightunits,leftunits,topunits"
                         style="width: 50px; left: 980px; top: 207px; height: 3px;">
                        <div class="tn-atom"></div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013362701554299386610" data-elem-id="1554299386610"
                         data-elem-type="button" data-field-top-value="398" data-field-top-res-960-value="338"
                         data-field-top-res-640-value="958" data-field-top-res-480-value="778"
                         data-field-top-res-320-value="558" data-field-left-value="700"
                         data-field-left-res-960-value="530" data-field-left-res-640-value="120"
                         data-field-left-res-480-value="40" data-field-left-res-320-value="10"
                         data-field-height-value="55" data-field-height-res-320-value="56" data-field-width-value="400"
                         data-field-width-res-320-value="300" data-field-axisy-value="top" data-field-axisx-value="left"
                         data-field-container-value="grid" data-field-topunits-value="" data-field-leftunits-value=""
                         data-field-heightunits-value="" data-field-widthunits-value=""
                         data-fields="top,left,width,height,container,axisx,axisy,caption,leftunits,topunits"
                         style="top: 398px; left: 813px; width: 400px; height: 55px;">
                        <button class="b24-web-form-popup-btn-5 tn-atom popup-btn">ЗАПИСАТЬСЯ НА УСЛУГИ СТИЛИСТА
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="block-custom block-5">
            <div class="t-container">
                <div class="t-title t-title_lg t-align_center title-mb">Почему нам доверяют?</div>
                <div class="t-col t-col_6 block-5-list">
                    <div class="list-item">
                        <i class="kaps kaps-icon-team"></i>Опытная команда персональных стилистов, прошедшая кастинг и
                        обучение
                    </div>
                    <div class="list-item">
                        <i class="kaps kaps-icon-money"></i>
                        Только индивидуальный подбор гардероба по вашему запросу
                    </div>
                    <div class="list-item">
                        <i class="kaps kaps-icon-indv"></i>
                        Гибкая система оплаты. Возможность рассрочки
                    </div>
                    <div class="list-item">
                        <i class="kaps kaps-icon-time"></i>
                        Вы не тратите время на магазины. Только 2 часа на примерку готовых комплектов
                    </div>
                    <div class="list-item">
                        <i class="kaps kaps-icon-goods"></i>
                        Подбор вещей из 4000+ изделий одежды, обуви и аксессуаров в KAPSULA
                    </div>
                </div>
                <div class="t-col t-col_6">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-title">
                                340+ клиентов уже попробовали KAPSULA WARDROBE
                            </div>
                            <div class="card-text">
                                97% клиентов отмечают «100% рекомендую попробовать»
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rec101336700" class="r t-rec r_anim r_showed" style=" " data-record-type="396"><!-- T396 -->

            <div class="t396">
                <div class="t396__artboard rendered" data-artboard-recid="101336700" data-artboard-height="770"
                     data-artboard-height-res-960="560" data-artboard-height-res-640="1340"
                     data-artboard-height-res-480="1174" data-artboard-height-res-320="1100"
                     data-artboard-height_vh="" data-artboard-valign="center" data-artboard-ovrflw=""
                     data-artboard-proxy-min-offset-top="0" data-artboard-proxy-min-height="770"
                     data-artboard-proxy-max-height="770">
                    <div class="t396__carrier" data-artboard-recid="101336700"></div>
                    <div class="t396__filter" data-artboard-recid="101336700"></div>
                    <div class="t396__elem tn-elem tn-elem__1013367001466084312673" data-elem-id="1466084312673"
                         data-elem-type="text" data-field-top-value="100" data-field-top-res-960-value="49"
                         data-field-top-res-640-value="718" data-field-top-res-480-value="579"
                         data-field-top-res-320-value="426" data-field-left-value="621"
                         data-field-left-res-960-value="489" data-field-left-res-640-value="25"
                         data-field-left-res-480-value="10" data-field-left-res-320-value="10"
                         data-field-width-value="560" data-field-width-res-960-value="460"
                         data-field-width-res-640-value="600" data-field-width-res-480-value="460"
                         data-field-width-res-320-value="300px" data-field-axisy-value="top"
                         data-field-axisx-value="left" data-field-container-value="grid"
                         data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                         data-field-widthunits-value=""
                         data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="top: 200px; left: 734px; width: 560px;">
                        <div class="tn-atom" field="tn_text_1466084312673">Готовы попробовать?</div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013367001469216533139" data-elem-id="1469216533139"
                         data-elem-type="image" data-field-top-value="96" data-field-top-res-960-value="48"
                         data-field-top-res-640-value="55" data-field-top-res-480-value="63"
                         data-field-top-res-320-value="81" data-field-left-value="20"
                         data-field-left-res-960-value="11" data-field-left-res-640-value="20"
                         data-field-left-res-480-value="10" data-field-left-res-320-value="10"
                         data-field-width-value="560" data-field-width-res-960-value="460"
                         data-field-width-res-640-value="600" data-field-width-res-480-value="460"
                         data-field-width-res-320-value="300" data-field-axisy-value="top"
                         data-field-axisx-value="left" data-field-container-value="grid"
                         data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                         data-field-widthunits-value=""
                         data-fields="img,width,filewidth,fileheight,top,left,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="left: 133px; top: 96px; width: 560px;">
                        <div class="tn-atom">
                            <img class="tn-atom__img t-img loaded no-lazy-load"
                                 data-original="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/_MG_0875_5.jpg"
                                 imgfield="tn_img_1469216533139"
                                 src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/_MG_0875_5.jpg">
                        </div>
                    </div>
                    <div class="t396__elem tn-elem tn-elem__1013367001469216647562" data-elem-id="1469216647562"
                         data-elem-type="text" data-field-top-value="162" data-field-top-res-960-value="108"
                         data-field-top-res-640-value="777" data-field-top-res-480-value="669"
                         data-field-top-res-320-value="494" data-field-left-value="651"
                         data-field-left-res-960-value="489" data-field-left-res-640-value="25"
                         data-field-left-res-480-value="10" data-field-left-res-320-value="10"
                         data-field-width-value="500" data-field-width-res-960-value="460"
                         data-field-width-res-640-value="600" data-field-width-res-480-value="460"
                         data-field-width-res-320-value="300px" data-field-axisy-value="top"
                         data-field-axisx-value="left" data-field-container-value="grid"
                         data-field-topunits-value="" data-field-leftunits-value="" data-field-heightunits-value=""
                         data-field-widthunits-value=""
                         data-fields="top,left,width,container,axisx,axisy,widthunits,leftunits,topunits"
                         style="top: 312px; left: 764px; width: 500px;">
                        <div id="contact-form"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="rec101336447" class="r t-rec t-rec_pt_90 t-rec_pb_75"
             style="padding-top:90px;padding-bottom:75px;background-color:#fcdee5; " data-record-type="605"
             data-bg-color="#fcdee5" data-animationappear="off"><!-- t605 -->
            <div class="t605 t605__witharrows">
                <div class="t-section__container t-container">
                    <div class="t-col t-col_12">
                        <div class="t-section__topwrapper t-align_center">
                            <div class="t-section__title t-title t-title_xs" field="btitle">
                                <div style="line-height:48px;" data-customstyle="yes">340+ клиентов уже попробовали</div>
                                <p class="after-title-text-custom">97% отмечают « 100% рекомендую попробовать »</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="t-container">
                    <div class="reviews-slider clearfix">
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_7.png"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    «На днях мне посчастливилось быть клиенткой суперского сервиса по подбору гардероба Kapsula. Девочки, если хочется чего-то новенького в своем стиле, или решили обновить гардероб, вам к экспертам в Kapsula. За пару часов работы со стилистами Kapsula я получила массу удовольствия, внимания, заботы и классные новые вещи. Открыла для себя новые образы. Очень всё понравилось. Шопинг в кайф. Стилисты Kapsula подберут вам подходящий гардероб на ваш бюджет, помогут навести порядок в вашем гардеробе, избавиться от ненужных вещей, подобрать образ на мероприятие ну и многое другое. Обращайтесь!)»
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Оксана Соколова</span>
                                <span>Нутрициолог, ASICS Frontrunner, основатель Health is Wealth </span>
                                <span>Стилист Наталья</span>
                            </div>
                        </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_8.png"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    Я очень долго ждала, созревала, чтобы поработать со стилистом. Останавливали меня страхи, которые, возможно, знакомы большинству — «а вдруг он подберет мне совсем не то, что я могу на себя надеть», «а вдруг мне вообще ничего не понравится, как я тогда скажу нет», «а если мне не понравится сам стилист, я же не могу развернуться и просто так уйти». Все произошло как обычно — мне припекло. За несколько месяцев работы с диетологом я потеряла пару размеров и при входе в осень обнаружила, что мне нечего надеть от слова «совсем». При этом лишних ни времени, ни денег на глупые эксперименты у меня не было. Так я встретилась с Наташей.
                                    Что мне понравилось? Первое — комфортные условия. Один на один. Когда вы со стилистом как с подружкой. Можете и о личном, и о страхах, и об опасениях, и покружиться во всем, на что раньше не отваживались. Второе — Наташа поддерживала мои пожелания и искала решения на пересечении моих «хочу» и ее профессиональных «это оно». Так мы нашли именно то, что подходит именно мне и что важнее всего — в чем мне комфортно. Меня не сделали кем-то другим, пусть даже стильным.
                                    Теперь я точно знаю, что хороший стилист — это подружка, которая чувствует тебя и при этом может грамотно, профессионально рассказать твою историю, кто ты — через одежду. Наташа, спасибо, обнимаю!
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Анна Чаплыгина</span>
                                <span>Специалист по этикету и моделированию поведения </span>
                                <span>Стилист Наталья</span>
                            </div>
                        </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_1.jpg"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    «Спасибо за услугу капсульный гардероб. Экономит часы, проведенные в магазинах.
                                    Заказываешь услуги стилиста, приходишь в назначенное время на примерки готовых образов и
                                    все. Очень удобно! Вернусь к вам в декабре)»
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Марина Гранкина</span>
                                <span>Продюсер at Орел и Решка. Шопинг </span>
                                <span>Стилист Наталья</span>
                            </div>
                        </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_2.jpg"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    «Воспользоваться услугой подбора капсульного гардероба решила по рекомендации подруги.
                                    Мне предстояла смена работы - и еще больше встреч и публичных мероприятий. Хотелось
                                    качественного обновления. Весь процесс, начиная с заполнения заявки на сайте, в которой
                                    нужно было рассказать о своих предпочтениях прошел легко и приятно. Вещи, подобранные
                                    Натальей, сильно отличались от того, что я обычно носила - и цветовой гаммой, и общим
                                    настроением. Но именно за этим я и пришла - изменениями! Кроме того, что я очень
                                    довольна своим новым гардеробом, я и сама стала обращать больше внимания на сочетания
                                    вещей и продумывать луки.»
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Валерия Селезнева</span>
                                <span>Должность Национальный банк Украины, связи с общественностью.</span>
                                <span>Стилист Наталья</span>
                            </div>
                        </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_3.jpg"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    «Обратилась за подборкой капсульного гардероба. Мне помогала стилист Леночка. Я осталась
                                    очень довольна. Мне подобрали полностью новый гардероб от обуви до пальто и украшений,
                                    по моим запросам. К тому же, в ассоритменте оказались вещи, которые найти в стандартных
                                    магазинах практически невозможно. Плюс новый опыт, плюс разбор моего старого гардероба
                                    (теперь оттуда вообще ничего носить не хочется :) ), плюс потрясающая Лена, чуткий
                                    отзывчивый специалист, плюс атмосфера. Впервые за долгое время я выхожу на улицу, и
                                    чувствую себя мега комфортно, стильно и ярко. Да еще и весна такая теплая началась.
                                    Советую всем всем, кто хочет увидеть себя по-новому.
                                    <br>
                                    PS. Мой муж очень доволен результатом, а ему угодить сложно))))»
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Яна Лукаш</span>
                                <span>Business Development Manager at YouScan.</span>
                                <span>Стилист Лена</span>
                            </div>
                        </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_4.jpg"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    «Я потрапила у чарівні руки Наталі за порадою подруги. У той час мені дуже потрібно було
                                    перейти від неформального та легкого бізнес-кежуал стилю, який був допустимим у
                                    некомерційному секторі, до більш ділового, який би пасував державній службі. При цьому
                                    дуже не хотілося повторювати шаблони українських чиновників або стримано-холодний стиль,
                                    на кшталт канцлера Німеччини:) Наталя допомогла додати до гардеробу нові кольори,
                                    фактури та силуети. Зробити його свіжим і, у той же час, достатньо офіційним. За кілька
                                    років разом ми зібрали повноцінний гардероб на всі сезони, який потребуватиме тільки
                                    невеликого оновлення яскравими деталями або аксесуарами. Порівнюючи з сумами, які ти
                                    залишаєш під час несистемних походів магазинами, вдягтися у Kapsula - найкраще value for
                                    money. До того ж зберігаєш собі час!»
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Ирина Литовченко</span>
                                <span>Генеральний директор директорату стратегічного планування та євроінтеграції МОЗ України</span>
                                <span>Стилист Наталья</span>
                            </div>
                        </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_5.jpg"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    «Это просто потрясающий сервис! Наталья (стилист) тщательно готовится к встрече, потому
                                    что тебя ждет несколько десятков образов, а все вещи идеально подобраны по размерам.
                                    Вещи очень качественные, стоимость услуг более чем оправдана и не "кусается".
                                    Обязательно вернусь еще! Сервис экономит невероятное количество времени, никаких лишних
                                    вещей. Два часа примерок - и гардероб на сезон готов!»
                                </div>
                                </div>
                                <div class="review-item-description">
                                    <span class="name">Ольга Минько</span>
                                    <span>Chief Marketing Officer (CMO), Product at Reply</span>
                                    <span>Стилист Наталья</span>
                                </div>
                            </div>
                        <div class="review-item">
                            <div class="review-item-img">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/wardrobe/reviews-image/review_6.jpeg"
                                     class="no-lazy-load"
                                     alt="">
                            </div>
                            <div class="review-item-text">
                                <div class="healper-div">
                                    Последние 2 года я забыла про потраченное время на магазины и
                                    бесконечные примерки, и очереди. Kapsula - это просто спасение 1-1,5 часа и у меня
                                    гардероб на целый сезон да еще и качественно подобранный стилистом :)
                                    Девочки, спасибо вам большое за такой крутой и качественный сервис!
                                </div>
                            </div>
                            <div class="review-item-description">
                                <span class="name">Наталья Коцюбан</span>
                                <span>Brand Manager FemCare Category Region Ukraine & Central Europe</span>
                                <span>Стилист Наталья</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div id="rec101336564" class="r t-rec t-rec_pt_120 t-rec_pb_105"
                 style="padding-top:120px;padding-bottom:105px;background-color:#cfe2f1; " data-record-type="490"
                 data-bg-color="#cfe2f1" data-animationappear="off"><!-- t490 -->
                <div class="t490">
                    <div class="t-section__container t-container">
                        <div class="t-col t-col_12">
                            <div class="t-section__topwrapper t-align_center">
                                <div class="t-section__title t-title t-title_xs" field="btitle">Самые частые вопросы
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="t-container">
                        <div class="q-slider">
                            <div class="q-slider-item">
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3237-6264-4465-a630-653030643565/Kapsula_icons_money-.png"
                                         data-original="https://static.tildacdn.com/tild3237-6264-4465-a630-653030643565/Kapsula_icons_money-.png"
                                         class="t490__img t-img loaded no-lazy-load" imgfield="li_img__1476968722790"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1476968722790">
                                            <div data-customstyle="yes">Есть ли минимальный бюджет?</div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1476968722790">
                                            <div data-customstyle="yes">Бюджет может быть любым. Вы
                                                самостоятельно определяете сумму, в пределах которой стилист будет
                                                выбирать для вас
                                                вещи.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3233-3163-4338-a261-656433356165/Kapsula_icons_brands.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1476968700508"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1476968700508">
                                            <div data-customstyle="yes">Из каких магазинов будут вещи?</div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1476968700508">
                                            <div data-customstyle="yes">Все вещи стилист подбирает из магазина
                                                KAPSULA – это более 100 брендов одежды, обуви, сумок, украшений.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="q-slider-item">
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3963-3936-4336-b462-383464616165/Kapsula_icons_sizes-.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1556106906954"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1556106906954">
                                            <div data-customstyle="yes">
                                                <strong>А если мне не понравится подбор
                                                    вещей от стилиста?</strong></div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1556106906954">
                                            <div data-customstyle="yes"> Стилист определит, почему не подошли
                                                вещи и как можно скорректировать подбор образов. Мы пригласим вас на
                                                повторную примерку.
                                                Если не понравится, мы вернем деньги за услуги стилиста
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3030-6137-4564-b461-633364663666/Kapsula_icons_sizes-.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1553267286382"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1553267286382">
                                            <div data-customstyle="yes">А что если не сядет?</div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1553267286382">
                                            <div data-customstyle="yes">Стилист согласовывает с вами список
                                                желаемых покупок, и подбирает другие варианты вещей. Вы получаете их с
                                                доставкой на дом,
                                                или на повторной примерке в шоуруме.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="q-slider-item">
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3237-3264-4564-b836-356334663433/Kapsula_icons_video-.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1554207076589"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1554207076589">
                                            <div data-customstyle="yes">А есть ли удаленный подбор гардероба?
                                            </div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1554207076589">
                                            <div data-customstyle="yes">Да! Вы заполняете анкету, стилист
                                                подбирает вещи и высылает вам видео с комплектами. Вы оплачиваете то,
                                                что понравилось и
                                                получаете доставку вещей
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3430-6333-4438-b633-396165653638/Kapsula_icons_showro.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1554207100465"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1554207100465">
                                            <div data-customstyle="yes">А где проходит примерка со стилистом?
                                            </div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1554207100465">
                                            <div data-customstyle="yes">Примерка гардероба со стилистом проходит
                                                в Киеве, в шоуруме KAPSULA
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="q-slider-item">
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3231-3263-4637-b562-383137393134/Kapsula_icons_time_h.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1554207114694"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1554207114694">
                                            <div data-customstyle="yes">А если я хочу только разбор гардероба?
                                            </div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1554207114694">
                                            <div data-customstyle="yes">Такой услуги у нас нет. Возможно вам
                                                подойдет разбор гардероба сейчас, а подбор новых вещей с отложенной
                                                датой в течение 3-х
                                                месяцев?
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild6532-6431-4566-b666-313962393535/Kapsula_icons_smile-.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1554277053451"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1554277053451">
                                            <div data-customstyle="yes">А можно прийти с мужем на примерку
                                                гардероба?
                                            </div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1554277053451">
                                            <div data-customstyle="yes">Да! Если для вас важно в момент шопинга
                                                согласовать покупки, то приходите вместе :) Главное, чтобы вам было
                                                комфортно.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="q-slider-item">
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild6438-6232-4430-a431-643963316234/Kapsula_icons_perfec.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1554277096147"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1554277096147">
                                            <div data-customstyle="yes">А если я передумала и хочу вернуть?
                                            </div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1554277096147">
                                            <div data-customstyle="yes">У вас есть 14 дней с даты покупки на
                                                возврат или обмен вещей. Если передумали ли не подошло, мы поможем найти
                                                альтернативу
                                                или примем возврат.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3730-3739-4434-a638-383231313265/Kapsula_icons_QA-12.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1476968690512"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1476968690512">
                                            <div data-customstyle="yes">Вдруг мне не понравится стилист?</div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1476968690512">
                                            <div data-customstyle="yes">Не проблема. Мы предложим вам примерку с
                                                другим стилистом и учтем все ваши пожелания.<br></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="q-slider-item">
                                <div class="q-slider-item-content">
                                    <img src="https://static.tildacdn.com/tild3561-3437-4231-b539-616365356535/hanger.svg"
                                         class="t490__img t-img loading no-lazy-load" imgfield="li_img__1556106935645"
                                         style="width:80px;">
                                    <div class="t490__wrappercenter ">
                                        <div class="t-name t-name_sm" style="" field="li_title__1556106935645">
                                            <div data-customstyle="yes">
                                                <strong>А можно сделать разбор гардероба
                                                    онлайн?</strong></div>
                                        </div>
                                        <div class="t-descr t-descr_xxs" style="" field="li_descr__1556106935645">
                                            <div data-customstyle="yes"> К сожалению, нет, это не эффективно.
                                                Если вы вышлите фото и видео своих любимых вещей, то на примерке в
                                                шоуруме KAPSULA
                                                стилист даст рекомендации по ним.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="text-align: center">
                            <a class="t412__btn t-btn b24-web-form-popup-btn-7" style="color:#ffffff;background-color:#e95027;border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;font-family:Roboto;font-weight:400;text-transform:uppercase;">
                                <table style="width:100%; height:100%;">
                                    <tbody>
                                    <tr>
                                        <td>Записаться к стилисту</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="rec101333350" class="r t-rec r_anim r_showed" style=" " data-record-type="215">
                <a name="nashiyslugi" style="font-size:0;"></a>
            </div>
            <div id="rec101333353" class="r t-rec r_anim r_showed" style=" " data-record-type="215">
                <a name="wardrobe" style="font-size:0;"></a>
            </div>
            <div id="howWeDo1" class="mfp-hide white-popup-block">
                <div id="rec101402729" class="r t-rec" style=" " data-animationappear="off" data-record-type="396">
                    <!-- T396 -->
                    <div class="form-wrap">
                        <div id="howWeDoForm1"></div>
                    </div>
                </div>
            </div>
            <div id="howWeDo2" class="mfp-hide white-popup-block">
                <div id="rec101402718" class="r t-rec" style=" " data-animationappear="off" data-record-type="396">
                    <!-- T396 -->
                    <div class="form-wrap">
                        <div id="howWeDoForm2"></div>
                    </div>
                </div>
            </div>
            <div id="howWeDo3" class="mfp-hide white-popup-block">
                <div id="rec101395579" class="r t-rec" style=" " data-animationappear="off" data-record-type="396">
                    <!-- T396 -->
                    <div class="form-wrap">
                        <div id="howWeDoForm3"></div>
                    </div>
                </div>
            </div>
            <div id="rec101333382" class="r t-rec t-rec_pt_0 t-rec_pb_75"
                 style="padding-top:20px;padding-bottom:35px;background-color:#fcdee5; " data-animationappear="off"
                 data-record-type="452" data-bg-color="#fcdee5"><!-- T452 -->
                <div class="t452" id="t-footer_101333382">
                    <div class="t452__maincontainer" style="height: 80px;">
                        <div class="t452__content">
                            <div class="t452__col t452__col_hiddenmobile">
                                <div class="t452__typo t452__copyright t-name t-name_xs" field="text"
                                     style="color: #e95027;"> ©
                                    2019 KAPSULA
                                </div>
                            </div>
                            <div class="t452__col t452__col_center t-align_center">
                                <div class="t452__right_social_links">
                                    <div class="t452__right_social_links_wrap">
                                        <div class="t452__right_social_links_item">
                                            <a href="https://www.facebook.com/kapsula.com.ua/" target="_blank">
                                                <svg style="fill:#ffffff;" version="1.1" id="Layer_1"
                                                     xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                     width="30px" height="30px" viewBox="0 0 48 48"
                                                     enable-background="new 0 0 48 48" xml:space="preserve"> <desc>
                                                        Facebook
                                                    </desc>
                                                    <path d="M47.761,24c0,13.121-10.638,23.76-23.758,23.76C10.877,47.76,0.239,37.121,0.239,24c0-13.124,10.638-23.76,23.764-23.76 C37.123,0.24,47.761,10.876,47.761,24 M20.033,38.85H26.2V24.01h4.163l0.539-5.242H26.2v-3.083c0-1.156,0.769-1.427,1.308-1.427 h3.318V9.168L26.258,9.15c-5.072,0-6.225,3.796-6.225,6.224v3.394H17.1v5.242h2.933V38.85z"
                                                          fill="#e95027"></path> </svg>
                                            </a></div>
                                        <div class="t452__right_social_links_item">
                                            <a href="https://www.instagram.com/kapsula_official/" target="_blank">
                                                <svg style="fill:#ffffff;" width="30px" height="30px"
                                                     viewBox="0 0 30 30" xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink">
                                                    <desc>Instagram</desc>
                                                    <path d="M15,11.014 C12.801,11.014 11.015,12.797 11.015,15 C11.015,17.202 12.802,18.987 15,18.987 C17.199,18.987 18.987,17.202 18.987,15 C18.987,12.797 17.199,11.014 15,11.014 L15,11.014 Z M15,17.606 C13.556,17.606 12.393,16.439 12.393,15 C12.393,13.561 13.556,12.394 15,12.394 C16.429,12.394 17.607,13.561 17.607,15 C17.607,16.439 16.444,17.606 15,17.606 L15,17.606 Z"
                                                          fill="#e95027"></path>
                                                    <path d="M19.385,9.556 C18.872,9.556 18.465,9.964 18.465,10.477 C18.465,10.989 18.872,11.396 19.385,11.396 C19.898,11.396 20.306,10.989 20.306,10.477 C20.306,9.964 19.897,9.556 19.385,9.556 L19.385,9.556 Z"
                                                          fill="#e95027"></path>
                                                    <path d="M15.002,0.15 C6.798,0.15 0.149,6.797 0.149,15 C0.149,23.201 6.798,29.85 15.002,29.85 C23.201,29.85 29.852,23.202 29.852,15 C29.852,6.797 23.201,0.15 15.002,0.15 L15.002,0.15 Z M22.666,18.265 C22.666,20.688 20.687,22.666 18.25,22.666 L11.75,22.666 C9.312,22.666 7.333,20.687 7.333,18.28 L7.333,11.734 C7.333,9.312 9.311,7.334 11.75,7.334 L18.25,7.334 C20.688,7.334 22.666,9.312 22.666,11.734 L22.666,18.265 L22.666,18.265 Z"
                                                          fill="#e95027"></path>
                                                </svg>
                                            </a></div>
                                    </div>
                                </div>
                                <div class="footer-info">
                                    <span>тел: +38 044 499 11 90</span>
                                    <span>Жилянская 54, Киев</span>
                                    <span>ПН-ВС с 10.00 до 20.00</span>
                                </div>
                            </div>
                            <div class="t452__col t452__col_mobile">
                                <div class="t452__typo t452__copyright t-name t-name_xs" field="text"
                                     style="color: #e95027;"> ©
                                    2019 KAPSULA
                                </div>
                            </div>
                            <div class="t452__col">
                                <div class="t452__scroll t-align_right">
                                    <a class="t452__typo t-name t-name_xs t452_scrolltop" style="color: #e95027;"
                                       href="javascript:t452_scrollToTop();"> Вернуться к
                                        началу страницы <span class="t452__icon"> <svg width="5px" height="17px"
                                                                                       viewBox="0 0 6 20" version="1.1"> <defs></defs> <g
                                                        id="Welcome" stroke="none" stroke-width="1" fill="none"
                                                        fill-rule="evenodd" sketch:type="MSPage"> <g
                                                            id="Desktop-HD-Copy-39" sketch:type="MSArtboardGroup"
                                                            transform="translate(-569.000000, -1797.000000)"
                                                            fill="#e95027" "=""> <path
                                                            d="M565.662286,1804.2076 L562.095536,1806.87166 C561.958036,1807.00916 561.958036,1807.16385 562.095536,1807.30135 L565.662286,1809.96541 C565.799786,1810.10291 565.941411,1810.0431 565.941411,1809.83616 L565.941411,1808.11741 L581.816411,1808.11741 L581.816411,1806.05491 L565.941411,1806.05491 L565.941411,1804.33616 C565.941411,1804.18147 565.866474,1804.1141 565.769536,1804.14297 C565.737224,1804.1526 565.696661,1804.17322 565.662286,1804.2076 Z"
                                                            id="Shape" sketch:type="MSShapeGroup"
                                                            transform="translate(571.904411, 1807.088000) rotate(-270.000000) translate(-571.904411, -1807.088000) "></path> </g>
                                                </g></svg> </span> </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="global-site">
                    <a href="<?php echo site_url(); ?>">kapsula.com.ua</a>
                </div>
            </div>
        </div>
<!--        --><?php //wp_footer(); ?>

        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="/wp-content/plugins/ct-size-guide/assets/js/magnific.popup.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/wardrobe.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/slick/slick.min.js"></script>

    <script id="bx24_form_inline" data-skip-moving="true">
        (function (w, d, u, b) {
            w['Bitrix24FormObject'] = b;
            w[b] = w[b] || function () {
                arguments[0].ref = u;
                (w[b].forms = w[b].forms || []).push(arguments[0])
            };
            if (w[b]['forms']) return;
            var s = d.createElement('script');
            s.async = 1;
            s.src = u + '?' + (1 * new Date());
            var h = d.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s, h);
        })
        (window, document, 'https://crm.kapsula.com.ua/bitrix/js/crm/form_loader.js', 'b24form');
        b24form({
            "id": "7",
            "lang": "ru",
            "sec": "ajy69g",
            "type": "inline",
            "node": document.getElementById('contact-form')
        });
        b24form({
            "id": "8",
            "lang": "ru",
            "sec": "3n7ldv",
            "type": "inline",
            "node": document.getElementById('howWeDoForm1')
        });
        b24form({
            "id": "9",
            "lang": "ru",
            "sec": "xsntod",
            "type": "inline",
            "node": document.getElementById('howWeDoForm2')
        });
        b24form({
            "id": "6",
            "lang": "ru",
            "sec": "w14b6m",
            "type": "inline",
            "node": document.getElementById('howWeDoForm3')
        });
        b24form({"id": "5", "lang": "ru", "sec": "90whoj", "type": "button", "click": ""});

        b24form({"id":"7","lang":"ru","sec":"ajy69g","type":"button","click":""});

        if (window.innerWidth <= 959) {
            jQuery('.team-slider').slick();
        }
    </script>
</body>
</html>
